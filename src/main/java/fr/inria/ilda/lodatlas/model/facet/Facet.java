/*******************************************************************************
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.model.facet;

/**
 * A model class for facets.
 * 
 * @author Hande Gözükan
 *
 */
public class Facet {

	/**
	 * Name of the facet.
	 */
	private final String name;

	/**
	 * Number of documents for this facet.
	 */
	private final long count;

	/**
	 * Boolean value that indicates if this facet is selected for query.
	 */
	private final boolean isSelected;

	/**
	 * 
	 * @param name
	 *            name of the facet
	 * @param count
	 *            number of documents for this facet
	 * @param isSelected
	 *            boolean value to indicate if the facet is selected for query
	 */
	public Facet(String name, long count, boolean isSelected) {
		assert (name != null);
		assert (count >= 0);

		this.name = name;
		this.count = count;
		this.isSelected = isSelected;
	}

	/**
	 * Getter method for the name of the facet.
	 * 
	 * @return the name the name of the facet.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Getter method for the number of documents for this facet.
	 * 
	 * @return the count the number of documents for this facet.
	 */
	public long getCount() {
		return count;
	}

	/**
	 * Getter method for the boolean value that indicates if the facet is selected
	 * in the query.
	 * 
	 * @return the boolean value that indicates if the facet is selected in the
	 *         query.
	 */
	public boolean isSelected() {
		return isSelected;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (count ^ (count >>> 32));
		result = prime * result + (isSelected ? 1231 : 1237);
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Facet))
			return false;
		Facet other = (Facet) obj;
		if (count != other.count)
			return false;
		if (isSelected != other.isSelected)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Facet [name=" + name + ", document count=" + count + ", isSelected=" + isSelected + "]";
	}

}
