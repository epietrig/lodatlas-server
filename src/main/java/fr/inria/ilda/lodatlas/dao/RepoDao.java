/*******************************************************************************
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.dao;

import java.util.List;
import java.util.function.Consumer;

import org.apache.log4j.Logger;
import org.apache.lucene.search.BooleanQuery;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.ConstantScoreQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.BucketOrder;
import org.elasticsearch.search.sort.SortOrder;

import fr.inria.ilda.lodatlas.common.EsIndexStrings;
import fr.inria.ilda.lodatlas.commons.strings.DatasetStrings;
import fr.inria.ilda.lodatlas.commons.strings.ProcessedFileStrings;
import fr.inria.ilda.lodatlas.commons.strings.VocabularyStrings;

public class RepoDao {

	private static final int MAX_RESULT = 1000;
	private static final Logger logger = Logger.getLogger(RepoDao.class);

	public RepoDao() {
	}

	/**
	 * Getter method for complete repository data of the dataset from repository
	 * with name <code>repoName</code> and with dataset name
	 * <code>datasetName</code>.
	 * 
	 * @param name
	 *            name of the dataset.
	 * @return {@link SearchResponse} containing complete repository data for the
	 *         dataset.
	 */
	public SearchResponse getDataset(String repoName, String datasetName) {

		logger.info("Called getDataset for dataset " + datasetName + " from repo " + repoName);

		BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();

		queryBuilder.must(QueryBuilders.termQuery(DatasetStrings.REPO_NAME, repoName));
		queryBuilder.must(QueryBuilders.termQuery(EsIndexStrings.NAME_RAW, datasetName));

		// we do not need to calculate scores, so we use constant query
		ConstantScoreQueryBuilder constantQuery = QueryBuilders.constantScoreQuery(queryBuilder);

		SearchRequestBuilder requestBuilder = ElasticClient.searchDataset();

		requestBuilder.setQuery(constantQuery).setFetchSource(true);

		logger.debug("Query is\n" + constantQuery.toString());

		return requestBuilder.execute().actionGet();

	}

	/**
	 * Getter method for details of incoming and outgoing links of the dataset with
	 * name <code>name</code>.
	 * 
	 * @param name
	 *            name of the dataset.
	 * @return {@link SearchResponse} containing link details for dataset.
	 */
	public SearchResponse getDatasetLinkDetails(String repoName, String datasetName) {

		logger.info("Called getDatasetLinkDetails for dataset " + datasetName + " from repo " + datasetName);

		BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();

		queryBuilder.must(QueryBuilders.termQuery(DatasetStrings.REPO_NAME, repoName));
		queryBuilder.must(QueryBuilders.termQuery(EsIndexStrings.NAME_RAW, datasetName));

		// we do not need to calculate scores, so we use constant query
		ConstantScoreQueryBuilder constantQuery = QueryBuilders.constantScoreQuery(queryBuilder);

		SearchRequestBuilder requestBuilder = ElasticClient.searchDataset();

		requestBuilder.setQuery(constantQuery).setFetchSource(
				new String[] { DatasetStrings.OUTGOING_LINKS, DatasetStrings.INCOMING_LINKS }, new String[] {});

		logger.debug("Query is\n" + constantQuery.toString());

		return requestBuilder.execute().actionGet();
	}

	/**
	 * Getter method for basic dataset info for the datasets specified in
	 * <code>ids</code> list.
	 * 
	 * @param ids
	 *            the list of datasets, in the form <em>repoName;datasetName</em>
	 * @return {@link SearchResponse} containing basic dataset metadata containing
	 *         incoming and outgoing link counts.
	 */
	public SearchResponse getDatasets(List<String> ids) {
		logger.info("Called getDatasets. There are " + ids.size() + " datasets to fetch");

		BooleanQuery.setMaxClauseCount(ids.size() * 10);
		final BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();

		ids.forEach(new Consumer<String>() {

			@Override
			public void accept(String t) {

				String[] terms = t.split(";");

				if (terms.length == 2) {
					BoolQueryBuilder query = QueryBuilders.boolQuery();

					query.must(QueryBuilders.termQuery(DatasetStrings.REPO_NAME, terms[0]));
					query.must(QueryBuilders.termQuery(EsIndexStrings.NAME_RAW, terms[1]));

					queryBuilder.should(query);
				}
			}
		});

		// we do not need to calculate scores, so we use constant query
		ConstantScoreQueryBuilder constantQuery = QueryBuilders.constantScoreQuery(queryBuilder);

		SearchRequestBuilder requestBuilder = ElasticClient.searchDataset();

		requestBuilder.setQuery(constantQuery)
				.setFetchSource(new String[] { DatasetStrings.ID, DatasetStrings.REPO_NAME, DatasetStrings.NAME,
						DatasetStrings.TRIPLE_COUNT, DatasetStrings.METADATA_CREATED, DatasetStrings.METADATA_MODIFIED,
						DatasetStrings.OUTGOING_LINKS, DatasetStrings.INCOMING_LINKS }, new String[] {})
				.addSort(DatasetStrings.TRIPLE_COUNT, SortOrder.DESC).setFrom(0).setSize(MAX_RESULT);

		logger.debug("Query is\n" + constantQuery.toString());

		return requestBuilder.execute().actionGet();
	}

	/**
	 * Getter method for RDFSum data of the dataset with name <code>name</code>.
	 *
	 * @param datasetName
	 *            name of the dataset.
	 * @return {@link SearchResponse} containing resource file list for the dataset.
	 */
	public SearchResponse getDatasetResourceFileList(String repoName, String datasetName) {

		logger.info("Called getDatasetResourceFileList for dataset " + datasetName + " from repo " + repoName);

		TermQueryBuilder queryBuilder = QueryBuilders.termQuery(ProcessedFileStrings.DATASET_NAME, datasetName);

		// we do not need to calculate scores, so we use constant query
		ConstantScoreQueryBuilder constantQuery = QueryBuilders.constantScoreQuery(queryBuilder);

		logger.debug(constantQuery.toString());
		SearchRequestBuilder requestBuilder = ElasticClient.searchProcessedFile();

		requestBuilder.setQuery(constantQuery)
				.setFetchSource(new String[] { ProcessedFileStrings.DATASET_NAME, ProcessedFileStrings.RESOURCE_ID,
						ProcessedFileStrings.TRIPLE_COUNT, ProcessedFileStrings.HAS_ERROR, ProcessedFileStrings.ERROR,
						ProcessedFileStrings.FILE_NAME, ProcessedFileStrings.ORIGINAL_FILE_NAME }, new String[] {});

		logger.debug("Adding aggregations");
		requestBuilder
				.addAggregation(AggregationBuilders.terms(ProcessedFileStrings.RESOURCE_ID)
						.field(ProcessedFileStrings.RESOURCE_ID).order(BucketOrder.key(true)))
				.setFrom(0).setSize(MAX_RESULT);

		return requestBuilder.execute().actionGet();
	}

	/**
	 * Getter method for RDFSum data of the dataset with name <code>name</code>.
	 * 
	 * @param datasetName
	 *            name of the dataset.
	 * @return {@link SearchResponse} containing RDFSum data for the dataset.
	 */
	public SearchResponse getDatasetRdfSum(String repoName, String datasetName, String resourceId, String dumpName) {

		logger.info("Called getDatasetRdfSum for dataset " + datasetName + " resource id " + resourceId
				+ " dump file name " + dumpName);

		// we do not need to calculate scores, so we use constant query
		BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();

		boolQuery.must(QueryBuilders.termQuery(ProcessedFileStrings.REPO_NAME, repoName));
		boolQuery.must(QueryBuilders.termQuery(ProcessedFileStrings.DATASET_NAME, datasetName));
		boolQuery.must(QueryBuilders.termQuery(ProcessedFileStrings.RESOURCE_ID, resourceId));
		boolQuery.must(QueryBuilders.termQuery(ProcessedFileStrings.ORIGINAL_FILE_NAME, dumpName));

		ConstantScoreQueryBuilder constantQuery = QueryBuilders.constantScoreQuery(boolQuery);

		SearchRequestBuilder requestBuilder = ElasticClient.searchProcessedFile();
		requestBuilder.setSearchType(SearchType.DFS_QUERY_THEN_FETCH);

		logger.debug(constantQuery.toString());

		requestBuilder.setQuery(constantQuery)
				.setFetchSource(new String[] { ProcessedFileStrings.DATASET_NAME, ProcessedFileStrings.RESOURCE_ID,
						ProcessedFileStrings.TRIPLE_COUNT, ProcessedFileStrings.HAS_ERROR, ProcessedFileStrings.ERROR,
						ProcessedFileStrings.FILE_NAME, ProcessedFileStrings.ORIGINAL_FILE_NAME,
						ProcessedFileStrings.FD, ProcessedFileStrings.HEB }, new String[] {})
				.setFrom(0).setSize(MAX_RESULT);

		return requestBuilder.execute().actionGet();
	}

	/**
	 * Getter method for RDFSum data of the dataset with name <code>name</code>.
	 * 
	 * @param datasetName
	 *            name of the dataset.
	 * @return {@link SearchResponse} containing vocabulary list for the dataset.
	 */
	public SearchResponse getDatasetVocabulary(String repoName, String datasetName, String resourceId,
			String dumpName) {
		logger.info("Called getDatasetVocabulary for dataset " + datasetName + " resourceId " + resourceId
				+ " dump file name " + dumpName);

		// we do not need to calculate scores, so we use constant query
		BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();

		boolQuery.must(QueryBuilders.termQuery(ProcessedFileStrings.REPO_NAME, repoName));
		boolQuery.must(QueryBuilders.termQuery(ProcessedFileStrings.DATASET_NAME, datasetName));
		boolQuery.must(QueryBuilders.termQuery(ProcessedFileStrings.RESOURCE_ID, resourceId));
		boolQuery.must(QueryBuilders.termQuery(ProcessedFileStrings.ORIGINAL_FILE_NAME, dumpName));

		ConstantScoreQueryBuilder constantQuery = QueryBuilders.constantScoreQuery(boolQuery);

		SearchRequestBuilder requestBuilder = ElasticClient.searchProcessedFile();
		requestBuilder.setSearchType(SearchType.DFS_QUERY_THEN_FETCH);

		logger.debug(constantQuery.toString());

		requestBuilder.setQuery(constantQuery)
				.setFetchSource(
						new String[] { ProcessedFileStrings.DATASET_NAME, ProcessedFileStrings.RESOURCE_ID,
								ProcessedFileStrings.TRIPLE_COUNT, ProcessedFileStrings.HAS_ERROR,
								ProcessedFileStrings.ERROR, ProcessedFileStrings.FILE_NAME,
								ProcessedFileStrings.ORIGINAL_FILE_NAME, ProcessedFileStrings.VOCABULARIES },
						new String[] {})
				.setFrom(0).setSize(MAX_RESULT);

		return requestBuilder.execute().actionGet();
	}

	/**
	 * 
	 * @param uri
	 * @return {@link SearchResponse} containing vocabulary info.
	 */
	public SearchResponse getVocabulary(String uri) {

		logger.info("Called getVocabulary for vocabulary " + uri);

		TermQueryBuilder queryBuilder = QueryBuilders.termQuery(VocabularyStrings.URI, uri);

		// we do not need to calculate scores, so we use constant query
		ConstantScoreQueryBuilder constantQuery = QueryBuilders.constantScoreQuery(queryBuilder);

		SearchRequestBuilder requestBuilder = ElasticClient.searchVocabulary();

		requestBuilder.setQuery(constantQuery)
				.setFetchSource(new String[] { VocabularyStrings.URI, VocabularyStrings.LOV_URI,
						VocabularyStrings.TITLE, VocabularyStrings.PREFIX, VocabularyStrings.HOMEPAGE },
						new String[] {});

		return requestBuilder.execute().actionGet();
	}

}
