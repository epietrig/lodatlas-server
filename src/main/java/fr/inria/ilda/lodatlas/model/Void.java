/*******************************************************************************
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.model;

import java.util.ArrayList;
import java.util.List;

import javax.json.JsonArray;
import javax.json.JsonObject;

import fr.inria.ilda.lodatlas.commons.model.FormatType;
import fr.inria.ilda.lodatlas.commons.strings.DatasetStrings;
import fr.inria.ilda.lodatlas.commons.strings.ResourceStrings;
import fr.inria.ilda.lodatlas.model.dataset.DatasetLongResult;

/**
 * A class that generates VoID file for a dataset from JSON string.
 * 
 * @author Hande Gözükan
 *
 */
public class Void extends DatasetLongResult {

	/**
	 * Prefixes to be used in VoID file.
	 */
	private static final String PREFIX = "@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
			+ "@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .\n"
			+ "@prefix foaf: <http://xmlns.com/foaf/0.1/> .\n" + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
			+ "@prefix void: <http://rdfs.org/ns/void#> .\n";

	private final String url;

	private final String repoUrl;

	private final String desc;

	private final String author;

	private final String org;

	private final String license;

	private String sparqlEndpoint = "";

	private final List<String> dumpUrls = new ArrayList<>();

	private final List<String> examples = new ArrayList<>();

	public Void(String jsonString) {
		super(jsonString);

		this.url = getString(jsonObj, DatasetStrings.URL);
		this.repoUrl = getString(jsonObj, DatasetStrings.REPO_DATASET_URL);
		this.desc = getString(jsonObj, DatasetStrings.DESCRIPTION);
		this.author = getString(jsonObj, DatasetStrings.AUTHOR);
		JsonObject organization = getJsonObject(jsonObj, DatasetStrings.ORGANIZATION);
		this.org = getString(organization, DatasetStrings.NAME);
		this.license = getString(jsonObj, DatasetStrings.LICENSE_URL);

		JsonArray resources = getJsonArray(jsonObj, DatasetStrings.RESOURCES);
		for (int i = 0; i < resources.size(); i++) {
			JsonObject resource = resources.getJsonObject(i);

			String verifiedFormat = getString(resource, ResourceStrings.VERIFIED_FORMAT);
			String url = getString(resource, ResourceStrings.URL);
			if (FormatType.SPARQL.equals(FormatType.getFormat(verifiedFormat))) {
				this.sparqlEndpoint = url;
			} else {
				if (!FormatType.OTHER.equals(FormatType.getFormat(verifiedFormat))) {
					if (verifiedFormat.contains("example")) {
						examples.add(url);
					} else {
						if (!verifiedFormat.contains("dcat") && !verifiedFormat.contains("owl")
								&& !verifiedFormat.contains("rdfs") && !verifiedFormat.contains("void")
								&& !verifiedFormat.contains("meta")) {
							dumpUrls.add(url);
						}
					}
				}
			}
		}
	}

	public String toString() {
		StringBuilder voidString = new StringBuilder();
		voidString.append(PREFIX);
		voidString.append("\n\n:").append(this.name).append(" a void:Dataset ");
		if (!"".equals(this.url))
			voidString.append(";\n\tfoaf:homepage <").append(this.url).append("> ");
		if (!"".equals(this.repoUrl))
			voidString.append(";\n\tfoaf:page <").append(this.repoUrl).append("> ");
		if (!"".equals(this.title))
			voidString.append(";\n\tdcterms:title \"").append(this.title).append("\" ");
		if (!"".equals(this.desc))
			voidString.append(";\n\tdcterms:description \"").append(this.desc).append("\" ");
		if (!"".equals(this.author))
			voidString.append(";\n\tdcterms:creator \"").append(this.author).append("\" ");
		if (!"".equals(this.org))
			voidString.append(";\n\tdcterms:published \"").append(this.org).append("\" ");
		if (!"".equals(this.cD))
			voidString.append(";\n\tdcterms:created \"").append(this.cD).append("\"^^xsd:date ");
		if (!"".equals(this.mD))
			voidString.append(";\n\tdcterms:modified \"").append(this.mD).append("\"^^xsd:date ");
		if (!"".equals(this.license))
			voidString.append(";\n\tdcterms:license <").append(this.license).append("> ");
		if (this.tC != 0)
			voidString.append(";\n\tdcterms:triples ").append(this.tC).append(" ");
		if (!"".equals(this.sparqlEndpoint))
			voidString.append(";\n\tvoid:sparqlEndpoint <").append(this.sparqlEndpoint).append("> ");
		if (this.dumpUrls.size() > 0) {
			for (int i = 0; i < dumpUrls.size(); i++) {
				voidString.append(";\n\tvoid:dataDump <").append(dumpUrls.get(i)).append("> ");
			}
		}
		if (this.examples.size() > 0) {
			for (int i = 0; i < examples.size(); i++) {
				voidString.append(";\n\tvoid:exampleResource <").append(examples.get(i)).append("> ");
			}
		}

		voidString.append(".");

		if (this.outgoingLinks.size() > 0) {
			for (int i = 0; i < this.outgoingLinks.size(); i++) {
				Link link = this.outgoingLinks.get(i);

				voidString.append("\n\n:").append(this.name).append("_").append(link.getName())
						.append(" a void:Linkset;\n").append("\tvoid:target :").append(this.name).append(" ;\n")
						.append("\tvoid:target :").append(link.getName()).append(" ;\n").append("\tvoid:subset :")
						.append(this.name).append(" ;\n").append("\tvoid:triples ").append(link.getCount())
						.append(" .");
			}
		}

		if (this.incomingLinks.size() > 0) {
			for (int i = 0; i < this.incomingLinks.size(); i++) {
				Link link = this.incomingLinks.get(i);

				voidString.append("\n\n:").append(link.getName()).append("_").append(this.name)
						.append(" a void:Linkset;\n").append("\tvoid:target :").append(link.getName()).append(" ;\n")
						.append("\tvoid:target :").append(this.name).append(" ;\n").append("\tvoid:subset :")
						.append(link.getName()).append(" ;\n").append("\tvoid:triples ").append(link.getCount())
						.append(" .");
			}
		}

		return voidString.toString();
	}
}
