/*******************************************************************************
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.model.dataset;

import java.lang.reflect.Type;
import java.util.function.Consumer;

import javax.json.JsonValue;
import javax.json.bind.annotation.JsonbTypeDeserializer;
import javax.json.bind.serializer.DeserializationContext;
import javax.json.bind.serializer.JsonbDeserializer;
import javax.json.stream.JsonParser;
import javax.json.stream.JsonParser.Event;

import fr.inria.ilda.lodatlas.commons.strings.DatasetStrings;
import fr.inria.ilda.lodatlas.model.Link;

/**
 * A custom deserializer to deserialize incoming and outgoing link counts
 * fields.
 * 
 * @author Hande Gözükan
 *
 */
@JsonbTypeDeserializer(value = DatasetLinksDeserializer.class)
public class DatasetLinksDeserializer implements JsonbDeserializer<DatasetLinks> {

	@Override
	public DatasetLinks deserialize(JsonParser parser, DeserializationContext ctx, Type rtType) {
		DatasetLinks ds = new DatasetLinks();

		while (parser.hasNext()) {
			Event event = parser.next();
			if (event == JsonParser.Event.KEY_NAME && parser.getString().equals(DatasetStrings.OUTGOING_LINKS)) {
				parser.next();
				JsonValue val = parser.getValue();
				ds.olC = Long.parseLong(val.asJsonObject().get(DatasetStrings.COUNT).toString());
				JsonValue linksValue = val.asJsonObject().get(DatasetStrings.LINKS);
				linksValue.asJsonArray().forEach(new Consumer<JsonValue>() {

					@Override
					public void accept(JsonValue t) {
						String name = t.asJsonObject().getString(DatasetStrings.NAME);
						long count = Long.parseLong(t.asJsonObject().get(DatasetStrings.COUNT).toString());
						ds.addOutgoingLink(new Link(name, count));
					}
				});
			} else if (event == JsonParser.Event.KEY_NAME && parser.getString().equals(DatasetStrings.INCOMING_LINKS)) {
				parser.next();
				JsonValue val = parser.getValue();
				ds.ilC = Long.parseLong(val.asJsonObject().get(DatasetStrings.COUNT).toString());
				JsonValue linksValue = val.asJsonObject().get(DatasetStrings.LINKS);
				linksValue.asJsonArray().forEach(new Consumer<JsonValue>() {

					@Override
					public void accept(JsonValue t) {
						String name = t.asJsonObject().getString(DatasetStrings.NAME);
						long count = Long.parseLong(t.asJsonObject().get(DatasetStrings.COUNT).toString());
						ds.addIncomingLink(new Link(name, count));
					}
				});
			} else if (event == JsonParser.Event.KEY_NAME && parser.getString().equals(DatasetStrings.TITLE)) {
				parser.next();
				ds.title = parser.getString();
			} else if (event == JsonParser.Event.KEY_NAME && parser.getString().equals(DatasetStrings.REPO_NAME)) {
				parser.next();
				ds.rN = parser.getString();
			} else if (event == JsonParser.Event.KEY_NAME && parser.getString().equals(DatasetStrings.NAME)) {
				parser.next();
				ds.name = parser.getString();
			} else if (event == JsonParser.Event.KEY_NAME && parser.getString().equals(DatasetStrings.TRIPLE_COUNT)) {
				parser.next();
				ds.tC = Long.parseLong(parser.getString());
			} else if (event == JsonParser.Event.KEY_NAME
					&& parser.getString().equals(DatasetStrings.METADATA_CREATED)) {
				parser.next();
				ds.cD = parser.getString();
			} else if (event == JsonParser.Event.KEY_NAME
					&& parser.getString().equals(DatasetStrings.METADATA_MODIFIED)) {
				parser.next();
				ds.mD = parser.getString();
			} 
			
//			else if (event == JsonParser.Event.KEY_NAME && parser.getString().equals(DatasetStrings.ID)) {
//				parser.next();
//				ds.id = parser.getString();
//			}
		}

		return ds;
	}

}
