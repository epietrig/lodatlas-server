/*******************************************************************************
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.model;

import fr.inria.ilda.lodatlas.commons.strings.ProcessedFileStrings;

public class ResourceFileShortEx extends JsonParser {

	/**
	 * The name of the package that this resource belongs to.
	 */
	protected final String pkg_name;

	/**
	 * The id of the resource as unique identifier.
	 */
	protected final String resourceId;

	/**
	 * The name of the nt file processed.
	 */
	protected final String fileName;

	/**
	 * The original name of the file processed before being converted to nt if it
	 * was not in nt format.
	 */
	protected final String originalFileName;

	/**
	 * The boolean value to indicate if any errors encountered during file
	 * processing.
	 */
	protected final boolean hasError;

	/**
	 * The error encountered while processing. Empty string if no error is
	 * encountered.
	 */
	protected final String error;

	/**
	 * The number of triples that are processed.
	 */
	protected final long tripleCount;

	public ResourceFileShortEx(String jsonString) {
		super(jsonString);
		this.pkg_name = getString(jsonObj, ProcessedFileStrings.DATASET_NAME);
		this.resourceId = getString(jsonObj, ProcessedFileStrings.RESOURCE_ID);
		this.hasError = getBoolean(jsonObj, ProcessedFileStrings.HAS_ERROR);
		this.error = getString(jsonObj, ProcessedFileStrings.ERROR);
		this.tripleCount = getLong(jsonObj, ProcessedFileStrings.TRIPLE_COUNT);
		this.fileName = getString(jsonObj, ProcessedFileStrings.FILE_NAME);
		this.originalFileName = getString(jsonObj, ProcessedFileStrings.ORIGINAL_FILE_NAME);
	}

	public String getPkgName() {
		return pkg_name;
	}

	public String getResourceId() {
		return resourceId;
	}

	public String getFileName() {
		return fileName;
	}

	public String getOriginalFileName() {
		return originalFileName;
	}

	public boolean isHasError() {
		return hasError;
	}

	public String getError() {
		return error;
	}

	public long getTripleCount() {
		return tripleCount;
	}

}
