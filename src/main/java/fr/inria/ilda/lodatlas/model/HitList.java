/*******************************************************************************
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.model;

import java.util.LinkedList;
import java.util.List;

public class HitList<T> {

	private final List<T> hits = new LinkedList<>();

	private final long totalHits;

	private String error;

	public HitList(long totalHits) {
		this.totalHits = totalHits;
		this.error = "";
	}

	public HitList(String error) {
		this.totalHits = 0;
		this.error = error;
	}

	/**
	 * @return the totalHits
	 */
	public long getTotalHits() {
		return totalHits;
	}

	public boolean addHit(T hit) {
		return hits.add(hit);
	}

	public List<T> getHits() {
		return hits;
	}

	public String getError() {
		return error;
	}

}
