/*******************************************************************************
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.JsonbConfig;

import org.apache.log4j.Logger;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;

import fr.inria.ilda.lodatlas.commons.model.Vocabulary;
import fr.inria.ilda.lodatlas.commons.strings.ProcessedFileStrings;
import fr.inria.ilda.lodatlas.dao.DaoFactory;
import fr.inria.ilda.lodatlas.dao.RepoDao;
import fr.inria.ilda.lodatlas.model.FileRdfSum;
import fr.inria.ilda.lodatlas.model.FileVocabulary;
import fr.inria.ilda.lodatlas.model.Link;
import fr.inria.ilda.lodatlas.model.LinkList;
import fr.inria.ilda.lodatlas.model.Resource;
import fr.inria.ilda.lodatlas.model.ResourceFileShortEx;
import fr.inria.ilda.lodatlas.model.Resources;
import fr.inria.ilda.lodatlas.model.dataset.Dataset;
import fr.inria.ilda.lodatlas.model.dataset.DatasetBase;
import fr.inria.ilda.lodatlas.model.dataset.DatasetBaseDeserializer;
import fr.inria.ilda.lodatlas.model.dataset.DatasetCkanData;
import fr.inria.ilda.lodatlas.model.dataset.DatasetLinks;
import fr.inria.ilda.lodatlas.model.dataset.DatasetLinksDeserializer;

public class RepoService {

	private static final Logger logger = Logger.getLogger(RepoService.class);

	private RepoDao repoDao = DaoFactory.getRepoDao();

	public RepoService() {

	}

	/**
	 * 
	 * @param repoName
	 * @param datasetName
	 * @return
	 */
	public Dataset getDataset(String repoName, String datasetName) {
		logger.info("Getting dataset data for " + datasetName + " from repo " + repoName);

		try {
			SearchResponse sr = repoDao.getDataset(repoName, datasetName);

			if (sr != null && sr.getHits().totalHits > 0) {

				logger.debug("Query returned " + sr.getHits().totalHits + " hits");

				DatasetCkanData ckan = new DatasetCkanData(sr.getHits().getHits()[0].getSourceAsString());

				return new Dataset(ckan);
			} else {
				logger.warn("There is no result for dataset " + datasetName + " in repo " + repoName);
			}

		} catch (Exception ex) {
			logger.error("Exception while getting dataset info for dataset " + datasetName + " from repo " + repoName,
					ex);
		}

		return null;
	}

	/**
	 * Searches the dataset with name <code>datasetName</code> from the specified
	 * <code>repoName</code> in the ElasticSearch indexes and returns the list of
	 * datasets which appear in incoming and outgoing links with their basic info.
	 * 
	 * @param repoName
	 *            name of the repository that the dataset belongs to.
	 * @param datasetName
	 *            name of the dataset.
	 * @return returns the list of datasets as {@link LinkList} which appear in
	 *         incoming and outgoing links with their basic info.
	 */
	public LinkList getDatasetLinkDetails(String repoName, String datasetName) {

		logger.info("Getting dataset link list for " + datasetName + " from repo " + repoName);
		try {

			SearchResponse sr = repoDao.getDatasetLinkDetails(repoName, datasetName);

			if (sr != null && sr.getHits().totalHits > 0) {
				logger.debug("There are " + sr.getHits().totalHits + " hits");

				Jsonb jsonb = JsonbBuilder.create(new JsonbConfig().withDeserializers(new DatasetLinksDeserializer()));

				SearchHit hit = sr.getHits().getHits()[0];

				DatasetLinks datasetLinks = jsonb.fromJson(hit.getSourceAsString(), DatasetLinks.class);

				try {
					jsonb.close();
				} catch (Exception e) {
					logger.error("Exception while trying to close jsonb", e);
				}

				Collection<Link> incomingLinks = datasetLinks.getIncomingLinks();

				logger.info("There are " + incomingLinks.size() + " incoming links");

				Collection<Link> outgoingLinks = datasetLinks.getOutgoingLinks();
				logger.info("There are " + outgoingLinks.size() + " outgoing links");

				return new LinkList(getLinkDetails(repoName, incomingLinks), getLinkDetails(repoName, outgoingLinks));
			} else {
				logger.info("There is no result for dataset " + datasetName + " in repo " + repoName);
			}
		} catch (Exception ex) {
			logger.error("Exception while getting dataset link details for dataset " + datasetName + " from repo "
					+ repoName, ex);
		}

		return null;
	}

	/**
	 * 
	 * @param repoName
	 * @param datasetName
	 * @return
	 */
	public Collection<Resource> getDatasetResourceFileList(String repoName, String datasetName) {
		logger.info("Getting dataset resource file list for " + datasetName + " from repo " + repoName);
		try {
			SearchResponse sr = repoDao.getDatasetResourceFileList(repoName, datasetName);

			if (sr != null && sr.getHits().totalHits > 0) {
				logger.debug("There are " + sr.getHits().totalHits + " hits");

				Aggregations aggrs = sr.getAggregations();

				Resources resources = null;
				if (aggrs == null) {
					logger.warn("No aggregations for the result set");
				} else {

					resources = new Resources();

					Terms terms = aggrs.get(ProcessedFileStrings.RESOURCE_ID);

					for (Terms.Bucket entry : terms.getBuckets()) {
						resources.addResource(entry.getKeyAsString());
					}

					Iterator<SearchHit> iterator = sr.getHits().iterator();
					while (iterator.hasNext()) {
						SearchHit hit = iterator.next();
						try {
							resources.addResourceFile(new ResourceFileShortEx(hit.getSourceAsString()));
						} catch (Exception ex) {
							logger.error("Exception while adding resource file to the list for resource "
									+ hit.getSourceAsMap().get(ProcessedFileStrings.RESOURCE_ID), ex);
						}
					}
				}

				return resources.getResources();
			} else {
				logger.warn("There is no result for dataset " + datasetName + " in repo " + repoName
						+ " for resource file list");
			}
		} catch (Exception ex) {
			logger.error("Exception while getting dataset resource file list for dataset " + datasetName + " from repo "
					+ repoName, ex);
		}
		return null;
	}

	/**
	 * 
	 * @param repoName
	 * @param datasetName
	 * @param resourceId
	 * @param dumpName
	 * @return
	 */
	public FileRdfSum getDatasetRdfSum(String repoName, String datasetName, String resourceId, String dumpName) {
		logger.info("Getting dataset RDF sum for " + datasetName + " resource " + resourceId + " dump " + dumpName
				+ " from repo " + repoName);
		try {
			SearchResponse sr = repoDao.getDatasetRdfSum(repoName, datasetName, resourceId, dumpName);

			if (sr != null && sr.getHits().totalHits > 0) {
				logger.debug("There are " + sr.getHits().totalHits + " hits");

				return new FileRdfSum(sr.getHits().getHits()[0].getSourceAsString());
			} else {
				logger.warn("There is no result for dataset " + datasetName + " in repo " + repoName
						+ " for RDF summaries.");
			}
		} catch (Exception ex) {
			logger.error(
					"Exception while generating dataset rdf sum for dataset " + datasetName + " in repo " + repoName,
					ex);
		}
		return null;
	}

	/**
	 * Gets vocabulary list for the dump-file with name <code>dumpName</code> which
	 * belongs to resource with ID <code>resourceId</code> for the dataset specified
	 * by <code>name</code>.
	 * 
	 * @param name
	 *            name of the dataset.
	 * @param resourceId
	 *            id of the resource in the dataset that contains the dump file with
	 *            name <code>dumpName</code>.
	 * @param dumpName
	 *            name of the dump file for which the vocabulary list is required.
	 * @return vocabulary list for the dump-file with name <code>dumpName</code> in
	 *         the resource with ID <code>resourceId</code> for the dataset
	 *         specified by <code>name</code>.
	 */
	public FileVocabulary getDatasetVocabulary(String repoName, String datasetName, String resourceId,
			String dumpName) {
		logger.info("Getting dataset vocabulary list for " + datasetName + " resource " + resourceId + " dump "
				+ dumpName + " from repo " + repoName);
		try {

			SearchResponse sr = repoDao.getDatasetVocabulary(repoName, datasetName, resourceId, dumpName);

			if (sr != null && sr.getHits().totalHits > 0) {
				logger.debug("There are " + sr.getHits().totalHits + " hits");

				Map<String, Object> map = sr.getHits().getHits()[0].getSourceAsMap();

				List<String> vocabularies = (List<String>) map.get(ProcessedFileStrings.VOCABULARIES);

				FileVocabulary fileVocabulary = new FileVocabulary(sr.getHits().getHits()[0].getSourceAsString());

				vocabularies.forEach(new Consumer<String>() {

					@Override
					public void accept(String t) {
						logger.info("vocabulary is " + t);
						Vocabulary voc = getVocabulary(t);
						if (voc != null) {
							logger.info(voc.toString());
							fileVocabulary.addVocabulary(voc);
						}
					}
				});

				return fileVocabulary;
			} else {
				logger.warn("There is no result for dataset " + datasetName + " in repo " + repoName
						+ " for vocabularies.");
			}
		} catch (Exception ex) {
			logger.error("Exception while generating dataset vocabulary list for dataset " + datasetName + " in repo "
					+ repoName, ex);
		}
		return null;
	}

	/**
	 * 
	 * @param uri
	 * @return
	 */
	private Vocabulary getVocabulary(String uri) {
		logger.info("Getting vocabulary " + uri);
		Vocabulary vocabulary = null;
		try {

			SearchResponse sr = repoDao.getVocabulary(uri);
			if (sr != null && sr.getHits().totalHits > 0) {
				logger.debug("There are " + sr.getHits().totalHits + " hits");
				Jsonb jsonb = JsonbBuilder.create();
				vocabulary = jsonb.fromJson(sr.getHits().getHits()[0].getSourceAsString(), Vocabulary.class);
				try {
					jsonb.close();
				} catch (Exception e) {
					logger.error("Exception while trying to close jsonb", e);
				}
			}
		} catch (Exception ex) {
			logger.error("Exception while getting vocabulary " + uri, ex);
		}

		return vocabulary;
	}

	/**
	 * 
	 * @param repoName
	 * @param datasetLinks
	 * @return
	 */
	private List<DatasetBase> getLinkDetails(String repoName, Collection<Link> datasetLinks) {
		logger.info("Getting link details from repo " + repoName + " for " + datasetLinks.size() + " datasets");
		List<DatasetBase> detailedLinks = new ArrayList<>();

		// get details of links
		if (datasetLinks.size() > 0) {
			List<String> datasets = new LinkedList<>();

			// create list of datasets with repo name
			datasetLinks.forEach(new Consumer<Link>() {

				@Override
				public void accept(Link link) {
					datasets.add(repoName + ";" + link.getName());
					logger.info("link " + link.getName());
				}
			});

			// query elasticsearch indices
			SearchResponse sr = null;
			Jsonb jsonb = JsonbBuilder.create(new JsonbConfig().withDeserializers(new DatasetBaseDeserializer()));

			int startIndex = 0;
			int end = datasets.size();
			int temp = end > 1000 ? 1000 : end;

			// query at intervals of 1000 not to exceed maxClauseCount for elasticsearch
			while (end - startIndex > 1000) {

				logger.info("Getting from " + startIndex + " to " + temp);
				sr = repoDao.getDatasets(datasets.subList(startIndex, temp));

				if (sr != null && sr.getHits().totalHits > 0) {
					logger.info(sr.getHits().totalHits + " datasets returned from query as links");

					for (SearchHit hit : sr.getHits()) {
						detailedLinks.add(jsonb.fromJson(hit.getSourceAsString(), DatasetBase.class));
					}
					startIndex = temp;
					temp = (temp * 2 > end ? end : temp * 2);
				}
			}

			logger.info("Getting from " + startIndex + " to " + temp);
			sr = repoDao.getDatasets(datasets.subList(startIndex, temp));

			if (sr != null && sr.getHits().totalHits > 0) {
				logger.info(sr.getHits().totalHits + " datasets returned from query as links");

				for (SearchHit hit : sr.getHits()) {
					detailedLinks.add(jsonb.fromJson(hit.getSourceAsString(), DatasetBase.class));
				}

			}

			try {
				jsonb.close();
			} catch (Exception e) {
				logger.error("Exception while trying to close jsonb", e);
			}
		}

		return detailedLinks;
	}
}
