/*
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
'use strict';

var lodApp = lodApp || {};

lodApp.View = lodApp.View || {};

(function(o) {
	o.SubmitDatasetOverlayView = {
		getInstance : function(controller_) {

			var SubmitDatasetView = function(controller) {
				this.controller = controller;
				this.container = document.getElementById('overlay-container');

				this.init();
			};

			SubmitDatasetView.prototype.constructor = SubmitDatasetView;

			/**
			 * Initializes the submit dataset template and appends it to
			 * overlay.
			 */
			SubmitDatasetView.prototype.init = function() {

				// set header
				// change header in view
				this.container.querySelector('.header').innerHTML = "<span class=\"submit-dataset-header\">Submit Dataset</span>";

				// set contents of the about page
				var submitDatasetView = document
						.querySelector('template#submit-dataset').content;

				var submitDatasetViewClone = document.importNode(
						submitDatasetView, true);
				this.container.querySelector('#overlay-content').appendChild(
						submitDatasetViewClone);

				this.setEventListeners();

				this.resize();
			};

			/**
			 * 
			 */
			SubmitDatasetView.prototype.setEventListeners = function() {
				var that = this;
				// set listeners for Add resource
				document
						.getElementById("add-resource-link")
						.addEventListener(
								'click',
								function() {
									document
											.getElementById("add-resource-dialog").style = "display:block";
								});

				document.getElementById("add-resource-button")
						.addEventListener('click', function() {
							that.addResource();
						});

				document
						.getElementById("cancel-resource-button")
						.addEventListener(
								'click',
								function() {
									document
											.getElementById("add-resource-dialog").style = "display:none";
								});

				// set listeners for Add link
				document
						.getElementById("add-link-link")
						.addEventListener(
								'click',
								function() {
									document.getElementById("add-link-dialog").style = "display:block";
								});

				document.getElementById("add-link-button").addEventListener(
						'click', function() {
							that.addLink();
						});

				document
						.getElementById("cancel-link-button")
						.addEventListener(
								'click',
								function() {
									document.getElementById("add-link-dialog").style = "display:none";
								});

				document.getElementById("submit-json").addEventListener(
						'click', function() {
							that.submitJson();
						});

				document.getElementById("submit-void").addEventListener(
						'click',
						function() {
							that
									.submitVoid(document
											.querySelector("#void-url").value);
						});

			}

			/**
			 * Puts the values in the fields of the Submit Dataset view in
			 * datasetToSubmit object and sends it to server as JSON and by
			 * email to .
			 */
			SubmitDatasetView.prototype.submitJson = function() {
				var datasetToSubmit = {};

				// set values of dataset info part
				var datasetInfoNode = document
						.querySelector("#dataset-submit-info");

				var name = datasetInfoNode.querySelector("#name").value;
				if (name == "") {
					alert("Please fill in name field!");
					return;
				}

				datasetToSubmit.name = name;
				datasetToSubmit.repoName = "lodatlas";
				datasetToSubmit.title = datasetInfoNode.querySelector("#title").value;
				datasetToSubmit.notes = datasetInfoNode.querySelector("#desc").value;
				datasetToSubmit.organization = {};
				datasetToSubmit.organization.name = datasetInfoNode
						.querySelector("#org").value;
				datasetToSubmit.namespace = datasetInfoNode
						.querySelector("#namespace").value;
				datasetToSubmit.author = datasetInfoNode
						.querySelector("#author").value;
				datasetToSubmit.author_email = datasetInfoNode
						.querySelector("#author-email").value;
				datasetToSubmit.maintainer = datasetInfoNode
						.querySelector("#maintainer").value;
				datasetToSubmit.maintainer_email = datasetInfoNode
						.querySelector("#maintainer-email").value;
				datasetToSubmit.url = datasetInfoNode.querySelector("#url").value;
				datasetToSubmit.repoDatasetUrl = datasetInfoNode
						.querySelector("#repo-dataset-url").value;
				datasetToSubmit.metadata_created = datasetInfoNode
						.querySelector("#cd").value;
				datasetToSubmit.metadata_modified = datasetInfoNode
						.querySelector("#md").value;
				datasetToSubmit.state = datasetInfoNode.querySelector("#state").checked ? "Active"
						: "Not active";
				datasetToSubmit.version = datasetInfoNode
						.querySelector("#version").value;
				datasetToSubmit.license_title = datasetInfoNode
						.querySelector("#license-title").value;
				datasetToSubmit.license_url = datasetInfoNode
						.querySelector("#license-url").value;
				datasetToSubmit.tripleCount = datasetInfoNode
						.querySelector("#triple-count").value;
				// TODO change to format {name, display_name}
				datasetToSubmit.tags = datasetInfoNode.querySelector("#tags").value
						.split(",").forEach(function(tag) {
							return {
								name : tag,
								display_name : tag
							};
						});

				datasetToSubmit.resources = [];

				var resources = document
						.querySelector("#resources-submit-content").children;

				var resource;

				for (var i = 0; i < resources.length; i++) {
					var resourceNode = resources[i];

					resource = {};
					resource.id = i;
					resource.name = resourceNode.querySelector("#nameinput").innerText;
					resource.url = resourceNode.querySelector("#urlinput").innerText;
					resource.description = resourceNode
							.querySelector("#descarea").innerText;
					resource.format = resourceNode
							.querySelector("#formatSelect").value;
					resource.state = resourceNode.querySelector("#activeinput").innerText;
					resource.last_modified = resourceNode
							.querySelector("#lastupdateinput").innerText;
					datasetToSubmit.resources.push(resource);
				}

				datasetToSubmit.incominglinks = {
					count : 0,
					links : []
				};
				datasetToSubmit.outgoinglinks = {
					count : 0,
					links : []
				};

				var links = document.querySelector("#links-submit-content").children;

				var link;

				for (var i = 0; i < links.length; i++) {
					var linkNode = links[i];
					link = {};
					link.name = linkNode.querySelector("#linkNameInput").innerText;
					link.count = linkNode.querySelector("#linkCountInput").innerText;
					link.voidurl = linkNode.querySelector("#voidUrlInput").innerText;

					var type = linkNode.querySelector("#linkTypeSelect").innerText;

					if (type === "Incoming") {
						datasetToSubmit.incominglinks.links.push(link);
						datasetToSubmit.incominglinks.count += 1;
					} else {
						datasetToSubmit.outgoinglinks.links.push(link);
						datasetToSubmit.outgoinglinks.count += 1;
					}
				}

				// send to server as JSON
				var that = this;
				$.ajax({
					url : "/lodatlas/rest/submit/dataset",
					type : "POST",
					data : JSON.stringify(datasetToSubmit),
					contentType : "application/json",
					cache : false,
					dataType : "json",
					success : function(data, status, jqXHR) {
//						that.sendEmail(datasetToSubmit.name);
						alert("Dataset is saved in the database. It will be displayed in LODAtlas web site under \"lodatlas\" repository after being processed. \nThank you for your submission!");

						that.controller.closeOverlay();
						
//						document.getElementById("void-url").value = "";
//						document.getElementById("dataset-submit-form").reset();
//						this.resetValues();
					},
					error : function(jqXHR, status, thrownError) {
						console.log(jqXHR);
						alert('There was a problem saving the dataset. Please try later!');
					}
				});
			}

			SubmitDatasetView.prototype.sendEmail = function(name) {
//				var win = window.open(
//						'mailto:lodatlas@lri.fr?subject=datasetsubmitted&body='
//								+ name, '_self');
//				console.log(win);
			}

			/**
			 * Sends the specified <code>voidURL</code> to the server to be
			 * processed.
			 */
			SubmitDatasetView.prototype.submitVoid = function(voidURL) {
				var that = this;

				// console.log(datasetToSubmit);
				$
						.ajax({
							url : "/lodatlas/rest/submit/void",
							type : "POST",
							data : voidURL,
							contentType : "text/plain",
							cache : false,
							dataType : "text",
							success : function(data, status, jqXHR) {
								console.log('status is ' + status);
								// console.log(jqXHR);
								console.log(data);
								that.viewDatasetJson(data);
							},
							error : function(jqXHR, status, thrownError) {
								console.log(jqXHR);
								alert('Problem with VoID file. Please check the file and retry!');
							}
						});

				// check response

			}

			/**
			 * Views the specified <code>datasetJson</code> object in the
			 * Submit Dataset view.
			 */
			SubmitDatasetView.prototype.viewDatasetJson = function(datasetJson) {
				var json = JSON.parse(datasetJson);

				var datasetInfoNode = document
						.querySelector("#dataset-submit-info");

				datasetInfoNode.querySelector("#title").value = json.title
						|| "";
				datasetInfoNode.querySelector("#desc").value = json.notes || "";
				if (json.organization) {
					datasetInfoNode.querySelector("#org").value = json.organization.title
							|| "";
				}
				datasetInfoNode.querySelector("#namespace").value = json.namespace
						|| "";
				datasetInfoNode.querySelector("#author").value = json.author
						|| "";
				datasetInfoNode.querySelector("#author-email").value = json.author_email
						|| "";
				datasetInfoNode.querySelector("#maintainer").value = json.maintainer
						|| "";
				datasetInfoNode.querySelector("#maintainer-email").value = json.maintainer_email
						|| "";
				datasetInfoNode.querySelector("#url").value = json.url || "";
				datasetInfoNode.querySelector("#repo-dataset-url").value = json.repoDatasetUrl
						|| "";
				// .split('T')[0] part for original date is necessary to avoid
				// wrongly specified time zone to change the date
				datasetInfoNode.querySelector("#cd").value = (json.metadata_created ? new Date(
						json.metadata_created.split('T')[0]).toISOString()
						.split('T')[0]
						: "");
				datasetInfoNode.querySelector("#md").value = (json.metadata_modified ? new Date(
						json.metadata_modified.split('T')[0]).toISOString()
						.split('T')[0]
						: "");
				datasetInfoNode.querySelector("#state").checked = ((json.state || "") == "Active");
				datasetInfoNode.querySelector("#version").value = json.version
						|| "";
				datasetInfoNode.querySelector("#license-title").value = json.license_title
						|| "";
				datasetInfoNode.querySelector("#license-url").value = json.license_url
						|| "";
				datasetInfoNode.querySelector("#triple-count").value = json.tripleCount || 0;

				if (json.tags) {
					datasetInfoNode.querySelector("#tags").value = json.tags
							.join(", ");
				}

				// clear resources if any
				var resources = document
						.querySelector("#resources-submit-content");

				while (resources.hasChildNodes()) {
					resources.removeChild(resources.firstChild);
				}

				var that = this;

				// add resources from file
				if (json.resources) {

					json.resources.forEach(function(res) {
						that.addResourceFromRes(res);
					});

				}

				// clear links if any
				var links = document.querySelector("#links-submit-content");

				while (links.hasChildNodes()) {
					links.removeChild(links.firstChild);
				}

				// add links extracted from file
				if (json.outgoinglinks) {

					json.outgoinglinks.links.forEach(function(link) {
						that.addLinkFromLink(link);
					});
				}
			}

			/**
			 * 
			 */
			SubmitDatasetView.prototype.resetValues = function() {

				var resources = document
						.querySelector("#resources-submit-content");

				while (resources.hasChildNodes()) {
					resources.removeChild(resources.firstChild);
				}

				var links = document.querySelector("#links-submit-content");

				while (links.hasChildNodes()) {
					links.removeChild(links.firstChild);
				}
			}

			/**
			 * Adds new resource to the resource list.
			 */
			SubmitDatasetView.prototype.addResourceFromRes = function(res) {
				var that = this;

				// populate dataset resource
				var resource = document
						.querySelector('template#dataset-resource-submit').content;

				var resourceClone = document.importNode(resource, true);

				resourceClone.querySelector('#nameinput').value = res.name
						|| "";

				resourceClone.querySelector('#urlinput').value = res.url || "";
				resourceClone.querySelector('#descarea').value = res.description
						|| "";
				resourceClone.querySelector('#formatSelect').value = res.format
						|| "";
				resourceClone.querySelector('#activeinput').checked = res.state || false;
				resourceClone.querySelector('#lastupdateinput').value = (res.last_modified ? new Date(
						res.last_modified.split('T')[0]).toISOString().split(
						'T')[0]
						: "");

				resourceClone
						.querySelector('#remove-resource-link')
						.addEventListener(
								'click',
								function(event) {
									var elementToRemove = event.target.parentNode.parentNode.parentNode;
									elementToRemove.parentNode
											.removeChild(elementToRemove);
									that.resize();
								});

				var resourceList = document
						.getElementById('resources-submit-content');

				if (resourceList.childNodes.length === 0) {
					resourceList.appendChild(resourceClone);
				} else {
					resourceList.insertBefore(resourceClone,
							resourceList.childNodes[0]);
				}

				this.resize();
			}

			/**
			 * Adds new resource to the resource list.
			 */
			SubmitDatasetView.prototype.addResource = function() {
				var that = this;
				var addDialog = document.getElementById("add-resource-dialog");

				// populate dataset resource
				var resource = document
						.querySelector('template#dataset-resource-submit').content;

				var resourceClone = document.importNode(resource, true);

				resourceClone.querySelector('#nameinput').value = addDialog
						.querySelector("#nameinput").value;

				resourceClone.querySelector('#urlinput').value = addDialog
						.querySelector("#urlinput").value;
				resourceClone.querySelector('#descarea').value = addDialog
						.querySelector("#descarea").value;
				resourceClone.querySelector('#formatSelect').value = addDialog
						.querySelector("#formatSelect").value;
				resourceClone.querySelector('#activeinput').checked = addDialog
						.querySelector("#activeinput").checked;
				resourceClone.querySelector('#lastupdateinput').value = addDialog
						.querySelector("#lastupdateinput").value;

				resourceClone
						.querySelector('#remove-resource-link')
						.addEventListener(
								'click',
								function(event) {
									var elementToRemove = event.target.parentNode.parentNode.parentNode;
									elementToRemove.parentNode
											.removeChild(elementToRemove);
									that.resize();
								});

				var resourceList = document
						.getElementById('resources-submit-content');

				if (resourceList.childNodes.length === 0) {
					resourceList.appendChild(resourceClone);
				} else {
					resourceList.insertBefore(resourceClone,
							resourceList.childNodes[0]);
				}

				// reset values in the add dialog
				addDialog.querySelector("#nameinput").value = "";
				addDialog.querySelector("#urlinput").value = "";
				addDialog.querySelector("#descarea").value = "";
				addDialog.querySelector("#formatSelect").selectedIndex = 0;
				addDialog.querySelector("#activeinput").checked = true;
				addDialog.querySelector("#lastupdateinput").value = "";

				// hide add dialog
				document.getElementById("add-resource-dialog").style = "display:none";

				this.resize();
			}

			/**
			 * Adds new link to the link list
			 */
			SubmitDatasetView.prototype.addLink = function() {
				var that = this;
				var addDialog = document.getElementById("add-link-dialog");
				var linkCounts = document.getElementById("link-counts");

				// populate dataset resource
				var link = document
						.querySelector('template#dataset-link-submit').content;

				var linkClone = document.importNode(link, true);

				linkClone.querySelector('#link-name').innerHTML = addDialog
						.querySelector("#linkNameInput").value;

				linkClone.querySelector('#linkNameInput').value = addDialog
						.querySelector("#linkNameInput").value;
				linkClone.querySelector('#linkCountInput').value = addDialog
						.querySelector("#linkCountInput").value;
				linkClone.querySelector('#linkTypeSelect').textContent = addDialog
						.querySelector("#linkTypeSelect").options[addDialog
						.querySelector("#linkTypeSelect").selectedIndex].text;
				linkClone.querySelector('#voidUrlInput').value = addDialog
						.querySelector("#voidUrlInput").value;

				linkClone
						.querySelector('#remove-link-link')
						.addEventListener(
								'click',
								function(event) {
									var elementToRemove = event.target.parentNode.parentNode.parentNode;
									elementToRemove.parentNode
											.removeChild(elementToRemove);

									// if incoming
									if (event.target.parentNode.nextElementSibling.nextElementSibling
											.querySelector("#linkType").innerText === "Incoming") {
										linkCounts
												.querySelector('#iLinkTotalCount').innerText = parseInt(linkCounts
												.querySelector('#iLinkTotalCount').innerText) - 1;
									} else {
										linkCounts
												.querySelector('#oLinkTotalCount').innerText = parseInt(linkCounts
												.querySelector('#oLinkTotalCount').innerText) - 1;
									}

									that.resize();
								});

				var linkList = document.getElementById('links-submit-content');

				if (linkList.childNodes.length === 0) {
					linkList.appendChild(linkClone);
				} else {
					linkList.insertBefore(linkClone, linkList.childNodes[0]);
				}

				// if incoming
				if (addDialog.querySelector("#linkTypeSelect").selectedIndex == 0) {
					linkCounts.querySelector('#iLinkTotalCount').innerText = parseInt(linkCounts
							.querySelector('#iLinkTotalCount').innerText) + 1;
				} else {
					linkCounts.querySelector('#oLinkTotalCount').innerText = parseInt(linkCounts
							.querySelector('#oLinkTotalCount').innerText) + 1;
				}

				// reset values
				addDialog.querySelector("#linkNameInput").value = "";
				addDialog.querySelector("#linkCountInput").value = "";
				addDialog.querySelector("#linkTypeSelect").selectedIndex = 0;

				document.getElementById("add-link-dialog").style = "display:none";
				this.resize();
			}

			SubmitDatasetView.prototype.addLinkFromLink = function(voidLink) {
				var that = this;
				var linkCounts = document.getElementById("link-counts");

				// populate dataset resource
				var link = document
						.querySelector('template#dataset-link-submit').content;

				var linkClone = document.importNode(link, true);

				linkClone.querySelector('#link-name').innerHTML = voidLink.name
						|| "";

				linkClone.querySelector('#linkNameInput').value = voidLink.name
						|| "";
				linkClone.querySelector('#linkCountInput').value = voidLink.count || 0;
				linkClone.querySelector('#linkTypeSelect').selectedIndex = 1;
				linkClone.querySelector('#voidUrlInput').value = voidLink.voidurl
						|| voidLink.name || "";

				linkClone
						.querySelector('#remove-link-link')
						.addEventListener(
								'click',
								function(event) {
									var elementToRemove = event.target.parentNode.parentNode.parentNode;
									elementToRemove.parentNode
											.removeChild(elementToRemove);

									// if incoming
									if (event.target.parentNode.nextElementSibling.nextElementSibling
											.querySelector("#linkType").innerText === "Incoming") {
										linkCounts
												.querySelector('#iLinkTotalCount').innerText = parseInt(linkCounts
												.querySelector('#iLinkTotalCount').innerText) - 1;
									} else {
										linkCounts
												.querySelector('#oLinkTotalCount').innerText = parseInt(linkCounts
												.querySelector('#oLinkTotalCount').innerText) - 1;
									}

									that.resize();
								});

				var linkList = document.getElementById('links-submit-content');

				if (linkList.childNodes.length === 0) {
					linkList.appendChild(linkClone);
				} else {
					linkList.insertBefore(linkClone, linkList.childNodes[0]);
				}

				linkCounts.querySelector('#oLinkTotalCount').innerText = parseInt(linkCounts
						.querySelector('#oLinkTotalCount').innerText) + 1;

				this.resize();
			}

			/**
			 * Removes all content added to div with id 'overlay-content'.
			 */
			SubmitDatasetView.prototype.dispose = function() {
				this.container.querySelector('.header').innerHTML = "";

				var parent = this.container.querySelector('#overlay-content');

				this.container = null;
				this.controller = null;

				var childNodes = parent.childNodes;

				for (var i = childNodes.length - 1; i >= 0; i--) {
					parent.removeChild(parent.childNodes[i]);
				}
			};

			/**
			 * 
			 */
			SubmitDatasetView.prototype.resize = function() {
				document.getElementById('overlay-content').setAttribute(
						'style',
						('height:' + (window.innerHeight - 180) + 'px'));
			};

			return new SubmitDatasetView(controller_);

		}
	};
})(lodApp.View);
