/*******************************************************************************
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.model.dataset;

import fr.inria.ilda.lodatlas.commons.strings.DatasetStrings;
import fr.inria.ilda.lodatlas.model.JsonParser;

/**
 * A class for common search results.
 * 
 * @author Hande Gözükan
 *
 */
public class CommonResult extends JsonParser {

	protected final String rN;

	protected final String name;

	protected final String title;

	protected final long tC;

	/**
	 * 
	 * @param jsonString
	 */
	public CommonResult(String jsonString) {
		super(jsonString);

		this.rN = getString(jsonObj, DatasetStrings.REPO_NAME).replace("\"", "");
		this.name = getString(jsonObj, DatasetStrings.NAME);
		this.title = getString(jsonObj, DatasetStrings.TITLE);
		this.tC = Long.parseLong(getString(jsonObj, DatasetStrings.TRIPLE_COUNT));
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @return the tripleCount
	 */
	public long getTC() {
		return tC;
	}

	public String getRN() {
		return rN;
	}

}
