/*******************************************************************************
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.model.dataset;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.json.JsonArray;
import javax.json.JsonObject;

import fr.inria.ilda.lodatlas.commons.strings.DatasetStrings;
import fr.inria.ilda.lodatlas.commons.strings.ResourceStrings;
import fr.inria.ilda.lodatlas.model.ResourceInfo;

/**
 * 
 * @author Hande Gözükan
 *
 */
public class DatasetCkanData extends DatasetShortResult {

	private final String desc;

	private String homepage;

	private final String catalogPage;

	private final String licenseTitle;

	private final String licenseUrl;

	private final String maintainer;

	private final String maintainerEmail;

	private final String author;

	private final String authorEmail;

	private final String state;

	private final String version;

	private final String organizationTitle;

	private final String namespace;

	/**
	 * Tags for dataset.
	 */
	private String[] tags;

	/**
	 * List of resources specified for the dataset.
	 */
	private List<ResourceInfo> resources = new LinkedList<>();

	/**
	 * 
	 * @param json
	 *            json String which contains CKAN metadata.
	 */
	public DatasetCkanData(String jsonString) {
		super(jsonString);

		this.desc = getString(jsonObj, DatasetStrings.NOTES);
		this.homepage = getString(jsonObj, DatasetStrings.URL);
		this.catalogPage = getString(jsonObj, DatasetStrings.REPO_DATASET_URL);
		this.licenseTitle = getString(jsonObj, DatasetStrings.LICENSE_TITLE);
		this.licenseUrl = getString(jsonObj, DatasetStrings.LICENSE_URL);
		this.maintainer = getString(jsonObj, DatasetStrings.MAINTAINER);
		this.maintainerEmail = getString(jsonObj, DatasetStrings.MAINTAINER_EMAIL);
		this.author = getString(jsonObj, DatasetStrings.AUTHOR);
		this.authorEmail = getString(jsonObj, DatasetStrings.AUTHOR_EMAIL);
		this.state = getString(jsonObj, DatasetStrings.STATE);
		this.version = getString(jsonObj, DatasetStrings.VERSION);
		this.namespace = getString(jsonObj, DatasetStrings.NAMESPACE);
		this.organizationTitle = getString(getJsonObject(jsonObj, DatasetStrings.ORGANIZATION), DatasetStrings.TITLE);

		// set tags
		JsonArray tagsJson = getJsonArray(jsonObj, DatasetStrings.TAGS);

		this.tags = new String[tagsJson.size()];

		for (int i = 0; i < tagsJson.size(); i++) {
			JsonObject tag = tagsJson.getJsonObject(i);
			this.tags[i] = getString(tag, DatasetStrings.NAME);
		}

		// set resources
		JsonArray resourceList = getJsonArray(jsonObj, DatasetStrings.RESOURCES);

		for (int i = 0; i < resourceList.size(); i++) {
			JsonObject resource = resourceList.getJsonObject(i);
			String name = getString(resource, ResourceStrings.NAME);
			String desc = getString(resource, ResourceStrings.DESCRIPTION);
			String format = getString(resource, ResourceStrings.FORMAT);
			String url = getString(resource, ResourceStrings.URL);
			String state = getString(resource, ResourceStrings.STATE);
			String lastUpdate = getString(resource, ResourceStrings.LAST_MODIFIED);
			String verifiedFormat = getString(resource, ResourceStrings.VERIFIED_FORMAT);
			boolean canDownload = getBoolean(resource, ResourceStrings.CAN_DOWNLOAD);
			String contentType = getString(resource, ResourceStrings.CONTENT_TYPE);
			String downloadError = getString(resource, ResourceStrings.DOWNLOAD_ERROR);
			String resFileName = getString(resource, ResourceStrings.DUMP_FILENAME);
			String resourceId = getString(resource, ResourceStrings.ID);

			resources.add(new ResourceInfo(resourceId, name, desc, format, contentType, url, state, lastUpdate,
					verifiedFormat, canDownload, downloadError, resFileName));

		}
	}

	/**
	 * @return the tags
	 */
	public String[] getTags() {
		return tags;
	}

	/**
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * @return the homepage
	 */
	public String getHomepage() {
		return homepage;
	}

	/**
	 * @return the catalogPage
	 */
	public String getCatalogPage() {
		return catalogPage;
	}

	/**
	 * @return the licenseTitle
	 */
	public String getLicenseTitle() {
		return licenseTitle;
	}

	/**
	 * @return the licenseUrl
	 */
	public String getLicenseUrl() {
		return licenseUrl;
	}

	/**
	 * @return the maintainer
	 */
	public String getMaintainer() {
		return maintainer;
	}

	/**
	 * @return the maintainerEmail
	 */
	public String getMaintainerEmail() {
		return maintainerEmail;
	}

	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * @return the authorEmail
	 */
	public String getAuthorEmail() {
		return authorEmail;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * @return the organizationTitle
	 */
	public String getOrganizationTitle() {
		return organizationTitle;
	}

	public String getNamespace() {
		return this.namespace;
	}

	public Collection<ResourceInfo> getResources() {
		return this.resources;
	}

}
