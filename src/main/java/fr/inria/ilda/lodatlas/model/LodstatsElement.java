/*******************************************************************************
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.model;

import java.util.Collection;
import java.util.List;

public class LodstatsElement {

	private final String fileName;

	private final String originalFileName;

	private final List<String> vocabularies;

	private final RdfSum fd;

	private final RdfSum heb;

	private final long tripleCount;

	private final boolean hasError;

	private final String error;

	public LodstatsElement(String fileName, String originalFileName, List<String> vocabularies, RdfSum heb, RdfSum fd,
			long tripleCount, boolean hasError, String error) {
		this.fileName = fileName;
		this.originalFileName = originalFileName;
		this.vocabularies = vocabularies;
		this.heb = heb;
		this.fd = fd;
		this.tripleCount = tripleCount;
		this.hasError = hasError;
		this.error = error;
	}

	public String getOriginalFileName() {
		return originalFileName;
	}

	public String getFileName() {
		return fileName;
	}

	public RdfSum getFd() {
		return fd;
	}

	public RdfSum getHeb() {
		return heb;
	}

	public Collection<String> getVocabularies() {
		return vocabularies;
	}

	public long getTripleCount() {
		return this.tripleCount;
	}

	public boolean isHasError() {
		return hasError;
	}

	public String getError() {
		return error;
	}

}
