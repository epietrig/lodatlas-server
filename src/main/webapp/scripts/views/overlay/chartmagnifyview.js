/*
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
'use strict';

var lodApp = lodApp || {};

lodApp.View = lodApp.View || {};

(function(o) {

	o.ChartMagnifyView = {
		getInstance : function(controller_, data_, chartConfig_, magnifyConfig_) {

			var ChartMagnifyOverlay = function(controller, data, chartConfig,
					magnifyConfig) {
				this.controller = controller;
				this.chartConfig = chartConfig;
				this.magnifyConfig = magnifyConfig;

				this.exChartValues = {
					sectionId : chartConfig.sectionId,
					sectionClass : chartConfig.sectionClass,
					title : chartConfig.title,
					d3Type : chartConfig.d3Type,
					chartId : chartConfig.chartId,
					chartClass : chartConfig.chartClass,
					delay : chartConfig.chartDetails.delay
				};

				this.container = document
						.getElementById(magnifyConfig.parentId);
				this.chartElements = {};

				this.init(data);
			};

			ChartMagnifyOverlay.prototype.constructor = ChartMagnifyOverlay;

			ChartMagnifyOverlay.prototype.init = function(data) {
				var that = this;

				// change header in view
				this.container.querySelector('.header').innerHTML = this.chartConfig.title
						+ this.magnifyConfig.titleEnd;

				this.initChartContainer();
				this.renderChart(data);
			};

			ChartMagnifyOverlay.prototype.initChartContainer = function() {
				var overlayContent = this.container
						.querySelector('#overlay-content');
				overlayContent.style.overflow = 'none';
				overlayContent.setAttribute('style', ('height:'
						+ (window.innerHeight - 150) + 'px'));

				this.chartConfig.sectionId = this.magnifyConfig.sectionId;
				this.chartConfig.sectionClass = this.magnifyConfig.sectionClass;
				this.chartConfig.d3Type += this.magnifyConfig.d3TypeEnd;
				this.chartConfig.chartId = this.magnifyConfig.chartId;
				this.chartConfig.chartClass = this.magnifyConfig.chartClass;
				this.chartConfig.chartDetails.delay = this.magnifyConfig.delay;
				

				var chartListener = lodApp.Controller.ChartListener
						.getListenerInstance(this.controller, false);

				// creates chart container
				lodApp.View.ChartView.createSectionContainer(overlayContent,
						this.chartConfig);

				// sets the chart
				this.chart = lodApp.d3.getChartFactory().getChart(this,
						this.chartConfig, this);
				
				var chartElements = {};
				chartElements[this.chartConfig.chartId] =  this.chart;
				chartListener.setChartElements(chartElements);
			};

			/**
			 * Renders the data in the chart.
			 */
			ChartMagnifyOverlay.prototype.renderChart = function(data) {
				this.chart.setData(data);
			};

			/**
			 * Called when an element is clicked in the chart. Clears current
			 * selection on the chart and selects the specified element.
			 * 
			 * @param element
			 *            the element that is clicked on the chart.
			 * @param mode
			 *            the mode of the chart which is clicked.
			 */
			ChartMagnifyOverlay.prototype.clicked = function(element, mode) {
				this.chart.clearSelection();
				this.chart.setMode(mode);
				this.chart.selectElement(element, true, this.chart);
			};

			/**
			 * Called when an element is hovered by mouse in one the chart.
			 * Hovers the element and its links in the chart.
			 * 
			 * @param element
			 *            the element that is hovered on the chart.
			 * @param mode
			 *            the mode of the chart which is clicked.
			 */
			ChartMagnifyOverlay.prototype.mouseOvered = function(element, mode) {
				this.chart.clearSelection();
				this.chart.setMode(mode);
				this.chart.selectElement(element, false, this.chart);
			};

			/**
			 * Called when mouse is out from an element in the chart.
			 * 
			 * @param element
			 *            the element from which mouse is outed on the chart.
			 * @param mode
			 *            the mode of the chart where mouse out event is called.
			 */
			ChartMagnifyOverlay.prototype.mouseOuted = function(element, mode) {
				this.chart.clearSelection();
			};

			/**
			 * Releases all the sources used by this view.
			 */
			ChartMagnifyOverlay.prototype.dispose = function() {
				var parent = this.container.querySelector('#overlay-content');

				this.chartConfig.sectionId = this.exChartValues.sectionId;
				this.chartConfig.sectionClass = this.exChartValues.sectionClass;
				this.chartConfig.title = this.exChartValues.title;
				this.chartConfig.d3Type = this.exChartValues.d3Type;
				this.chartConfig.chartId = this.exChartValues.chartId;
				this.chartConfig.chartClass = this.exChartValues.chartClass;
				this.chartConfig.chartDetails.delay = this.exChartValues.delay;

				this.container = null;
				this.exChartValues = null;
				this.chartConfig = null;
				this.chart = null;
				this.controller = null;

				var childNodes = parent.childNodes;

				for (var i = childNodes.length - 1; i >= 0; i--) {
					parent.removeChild(parent.childNodes[i]);
				}
			};

			/**
			 * Resizes the overlay view and the chart.
			 */
			ChartMagnifyOverlay.prototype.resize = function() {
				document.getElementById('overlay-content').setAttribute(
						'style',
						('height:' + (window.innerHeight - 150) + 'px'));
				this.chart.resize();
			};

			return new ChartMagnifyOverlay(controller_, data_, chartConfig_,
					magnifyConfig_);
		}
	};

})(lodApp.View);
