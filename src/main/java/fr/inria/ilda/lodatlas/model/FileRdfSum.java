/*******************************************************************************
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.model;

import java.util.LinkedList;
import java.util.List;

import javax.json.JsonArray;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonString;
import javax.json.JsonValue;

import fr.inria.ilda.lodatlas.commons.strings.DatasetStrings;
import fr.inria.ilda.lodatlas.commons.strings.ProcessedFileStrings;

public class FileRdfSum extends ResourceFileShortEx {

	private RdfSum fd;

	private RdfSum heb;

	public FileRdfSum(String jsonString) {
		super(jsonString);

		JsonObject fdObject = getJsonObject(jsonObj, ProcessedFileStrings.FD);

		// get fd nodes
		JsonArray fdNodesArray = getJsonArray(fdObject, ProcessedFileStrings.NODES);
		List<Node> fdNodes = new LinkedList<>();

		if (fdNodesArray != null) {

			for (JsonValue value : fdNodesArray) {
				JsonObject nodeObject = (JsonObject) value;

				JsonArray labelArray = getJsonArray(nodeObject, ProcessedFileStrings.LABELS);
				List<String> nodeLabels = new LinkedList<>();

				if (labelArray != null) {
					for (JsonValue label : labelArray) {
						nodeLabels.add(((JsonString) label).getString());
					}
				}

				JsonArray groupArray = getJsonArray(nodeObject, ProcessedFileStrings.GROUPS);
				List<Integer> nodeGroups = new LinkedList<>();

				if (groupArray != null) {
					for (JsonValue group : groupArray) {
						nodeGroups.add(((JsonNumber) group).intValue());
					}
				}

				fdNodes.add(new Node(nodeLabels, nodeGroups, null));

			}

			// get fd links
			JsonArray fdLinksArray = getJsonArray(fdObject, DatasetStrings.LINKS);
			List<Link2> fdLinks = new LinkedList<>();

			for (JsonValue value : fdLinksArray) {
				JsonObject linkObj = (JsonObject) value;
				int source = linkObj.getInt(ProcessedFileStrings.SOURCE);
				int target = linkObj.getInt(ProcessedFileStrings.TARGET);
				int group = linkObj.getInt(ProcessedFileStrings.GROUP);
				String label = linkObj.getString(ProcessedFileStrings.LABEL);
				fdLinks.add(new Link2(source, target, group, label, ""));
			}

			this.fd = new RdfSum(fdNodes, fdLinks, null);
		}

		// ***********************************
		JsonObject hebObject = getJsonObject(jsonObj, ProcessedFileStrings.HEB);
		;

		// get heb nodes
		JsonArray hebNodesArray = getJsonArray(hebObject, ProcessedFileStrings.NODES);
		List<Node> hebNodes = new LinkedList<>();

		if (hebNodesArray != null) {
			for (JsonValue value : hebNodesArray) {
				JsonObject nodeObject = (JsonObject) value;
				String prop = nodeObject.getString(ProcessedFileStrings.PROP);

				JsonArray labelArray = getJsonArray(nodeObject, ProcessedFileStrings.LABELS);
				List<String> nodeLabels = new LinkedList<>();

				if (labelArray != null) {
					for (JsonValue label : labelArray) {
						nodeLabels.add(((JsonString) label).getString());
					}
				}

				JsonArray groupArray = getJsonArray(nodeObject, ProcessedFileStrings.GROUPS);
				List<Integer> nodeGroups = new LinkedList<>();

				if (groupArray != null) {
					for (JsonValue group : groupArray) {
						nodeGroups.add(((JsonNumber) group).intValue());
					}
				}

				hebNodes.add(new Node(nodeLabels, nodeGroups, prop));

			}

			// get heb links
			JsonArray hebLinksArray = getJsonArray(hebObject, DatasetStrings.LINKS);
			List<Link2> hebLinks = new LinkedList<>();

			for (JsonValue value : hebLinksArray) {
				JsonObject linkObj = (JsonObject) value;
				int source = linkObj.getInt(ProcessedFileStrings.SOURCE);
				int target = linkObj.getInt(ProcessedFileStrings.TARGET);
				int group = linkObj.getInt(ProcessedFileStrings.GROUP);
				String label = linkObj.getString(ProcessedFileStrings.LABEL);
				String uri = linkObj.getString(ProcessedFileStrings.URI);
				hebLinks.add(new Link2(source, target, group, label, uri));
			}

			// get heb groups
			JsonArray hebGroupsArray = getJsonArray(hebObject, ProcessedFileStrings.GROUPS);
			List<String> hebGroups = new LinkedList<>();

			for (JsonValue value : hebGroupsArray) {
				hebGroups.add(((JsonString) value).getString());
			}

			this.heb = new RdfSum(hebNodes, hebLinks, hebGroups);
		}
	}

	public RdfSum getFd() {
		return fd;
	}

	public RdfSum getHeb() {
		return heb;
	}

}
