/*
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
'use strict';

var lodApp = lodApp || {};

lodApp.View = lodApp.View || {};

(function(o) {
	o.DatasetDetailsView = {
		getInstance : function(controller_, data_) {

			var util = lodApp.util;
			var dataFunc = util.dataFunction;

			/**
			 * Initializes the dataset view with searchController and data for
			 * dataset.
			 */
			var DatasetView = function(controller, data) {
				this.controller = controller;

				this.chartConfig = viewConfig.DatasetSectionConfig;
				this.container = document.getElementById('overlay-container');
				this.chartElements = {};

				this.ckan = data.ckan;

				// boolean that indicates if the data for RDF Quotients tab is
				// already loaded
				this.rdfsumViewExists = false;

				// boolean that indicates if the data for Vocabularies tab is
				// already loaded.
				this.vocabularyViewExists = false;

				// boolean that indicates if the data for Chart View tab is
				// already loaded.
				this.chartViewExists = false;

				this.init();
			};

			DatasetView.prototype.constructor = DatasetView;

			/**
			 * Initializes parts of dataset details view.
			 * 
			 * @param data
			 *            dataset data
			 */
			DatasetView.prototype.init = function() {
				var that = this;
				this.initViews();
				this.displayCatalogData(this.ckan);
			};

			/**
			 * Creates catalog data and rdfsum parts of the view.
			 * 
			 * @param ckan
			 *            metadata of the dataset.
			 */
			DatasetView.prototype.initViews = function(datasetName) {
				var that = this;

				// add dataset view to overlay
				var datasetView = document.querySelector('template#dataset').content;

				var datasetViewClone = document.importNode(datasetView, true);
				this.container.querySelector('#overlay-content').appendChild(
						datasetViewClone);

				var rdfSumTab = this.container
						.querySelector("#rdf-summary-tab");

				// Loads RDF quotients data when the tab is clicked
				rdfSumTab.addEventListener('click', function() {
					if (!that.rdfsumViewExists) {
						that.rdfsumViewExists = true;

						if (that.resourceFilesList) {
							that.displayRDFSumList(that.resourceFilesList);
						} else {
							that.loadResourceFileList(that.displayRDFSumList);
						}
					}
				});

				var vocabularyTab = this.container
						.querySelector("#vocabulary-tab");

				// Loads vocabulary data when the tab is clicked
				vocabularyTab
						.addEventListener(
								'click',
								function() {
									if (!that.vocabularyViewExists) {
										that.vocabularyViewExists = true;

										if (that.resourceFilesList) {
											that
													.displayVocabularyList(that.resourceFilesList);
										} else {
											that
													.loadResourceFileList(that.displayVocabularyList);
										}
									}
								});

				var chartViewTab = this.container
						.querySelector("#chart-view-tab");

				chartViewTab.addEventListener('click', function() {
					if (!that.chartViewExists) {
						that.chartViewExists = true;
						that.loadChartData();
					}
				});
			};

			/**
			 * Loads the list of files for each processed resource of the
			 * current dataset in display and calls the 'callback' function
			 * specified.
			 */
			DatasetView.prototype.loadResourceFileList = function(callback) {
				// document.body.style.cursor = 'wait';
				this.controller.showDatasetResourceFileList(this.ckan.rn,
						this.ckan.name, callback, this);
			};

			/**
			 * Loads the RDF summary for the file specified by
			 * 'originalFileName' for the current dataset in display.
			 */
			DatasetView.prototype.loadFileRDFSum = function(originalFileName,
					resourceId) {
				this.controller.showFileRDFSum(this.ckan.rn, this.ckan.name,
						resourceId, originalFileName, this.displayFileRDFSum,
						this);
			};

			/**
			 * Loads the vocabularies for the file specified by
			 * 'originalFileName' for the current dataset in display.
			 */
			DatasetView.prototype.loadFileVocabulary = function(
					originalFileName, resourceId) {
				this.controller.showFileVocabulary(this.ckan.rn,
						this.ckan.name, resourceId, originalFileName,
						this.displayFileVocabulary, this);
			};

			/**
			 * Loads the vocabularies for the file specified by
			 * 'originalFileName' for the current dataset in display.
			 */
			DatasetView.prototype.loadChartData = function() {
				this.controller.showDatasetCharts(this.ckan.rn, this.ckan.name,
						this.displayCharts, this);
			};

			/**
			 * Displays catalog data in the Catalog data tab.
			 */
			DatasetView.prototype.displayCatalogData = function(ckan) {
				// console.log("Dataset view is being initialized. Ckan data
				// is:");
				// console.log(ckan);

				// create header for the dataset details view
				if (ckan.homepage !== "") {
					this.title = '<a href="' + ckan.homepage
							+ '" target="_blank">' + ckan.name + ' : '
							+ ckan.title + '</a>';
				} else {
					this.title = ckan.name + ' : ' + ckan.title;
				}

				this.title += '<span  style="padding-left: 20px;"><a href="'
						+ ckan.catalogPage
						+ '" target="_blank"><img src="img/ckan_logo.png" alt="Catalog page" width="15" height="15"  style="border: 1px solid gray;"></a>';

				// change header in view
				this.container.querySelector('.header').innerHTML = this.title;

				// set description content
				var descriptionContent = this.container
						.querySelector('#description-content');
				descriptionContent.innerHTML = ckan.desc;

				var lessMoreButton = this.container
						.querySelector('#less-more-button');

				lessMoreButton
						.addEventListener(
								'click',
								function() {
									if (lessMoreButton.textContent === 'more') {
										descriptionContent.className += " description-content-visible";
										lessMoreButton.innerHTML = "less";
									} else {
										descriptionContent.className = "description-content";
										lessMoreButton.innerHTML = "more";
									}
								});

				// set tags content
				if (ckan.tags)
					this.container.querySelector('#tags-content').textContent = ckan.tags
							.join(", ");

				// set counts content
				var numeric = this.container.querySelector('#numeric');
				numeric.querySelector('#tripleCount').textContent = util
						.formatNumber(dataFunc.tripleCountText(ckan));
				numeric.querySelector('#oLinkCount').textContent = util
						.formatNumber(dataFunc.outgoingLinkCount(ckan));
				numeric.querySelector('#iLinkCount').textContent = util
						.formatNumber(dataFunc.incomingLinkCount(ckan));

				// set info content
				var info = this.container.querySelector('#info');
				if (ckan.homepage !== "") {
					info.querySelector('#name').innerHTML = '<a href="'
							+ ckan.homepage + '" target="_blank">' + ckan.name
							+ '</a>';
					info.querySelector('#title').innerHTML = '<a href="'
							+ ckan.homepage + '" target="_blank">' + ckan.title
							+ '</a>';
				} else {
					info.querySelector('#name').innerHTML = ckan.name;
					info.querySelector('#title').innerHTML = ckan.title;
				}

				info.querySelector('#org').textContent = ckan.organizationTitle;
				if (ckan.namespace !== null && ckan.namespace !== '') {
					info.querySelector('#namespace').innerHTML = '<a href="'
							+ ckan.namespace + '" target="_blank">'
							+ ckan.namespace + '</a>';
					;
				}

				if (ckan.authorEmail) {
					info.querySelector('#author').innerHTML = '<a href="mailto:'
							+ ckan.authorEmail + '">' + ckan.author + '</a>';
				} else {
					info.querySelector('#author').textContent = ckan.author;
				}

				if (ckan.maintainerEmail) {
					info.querySelector('#maintainer').innerHTML = '<a href="mailto:'
							+ ckan.maintainerEmail
							+ '">'
							+ ckan.maintainer
							+ '</a>';
				} else {
					info.querySelector('#maintainer').textContent = ckan.maintainer;
				}
				info.querySelector('#catalogUrl').innerHTML = '<a href="'
						+ ckan.catalogPage + '" target="_blank">'
						+ ckan.catalogPage + '</a>';
				info.querySelector('#creationDate').textContent = util
						.formatDate(ckan.cd);
				info.querySelector('#modificationDate').textContent = util
						.formatDate(ckan.md);
				info.querySelector('#state').textContent = ckan.state;
				info.querySelector('#version').textContent = ckan.version;
				if (ckan.licenseUrl) {
					info.querySelector('#licenseTitle').innerHTML = '<a href="'
							+ ckan.licenseUrl + '" target="_blank">'
							+ ckan.licenseTitle + '</a>';
				} else {
					info.querySelector('#licenseTitle').textContent = ckan.licenseTitle;
				}

				// set resources content
				var resourcesData = ckan.resources;

				var resources = this.container.querySelector('#resources');

				for (var i = 0; i < resourcesData.length; i++) {
					var resourceData = resourcesData[i];

					// populate dataset resource
					var resource = document
							.querySelector('template#dataset-resource').content;

					var resourceClone = document.importNode(resource, true);
					var name = resourceData.name;
					var url = resourceData.url;

					if (name === null || name === "" || name === "null") {
						name = resourceData.description;
					}
					resourceClone.querySelector('#resource-name').innerHTML = name;
					resourceClone.querySelector('#desc').innerHTML = resourceData.description;
					resourceClone.querySelector('#format').textContent = resourceData.format;
					resourceClone.querySelector('#verifiedFormat').textContent = resourceData.verifiedFormat;
					resourceClone.querySelector('#mimeType').textContent = resourceData.contentType;
					resourceClone.querySelector('#state').innerHTML = resourceData.state;
					resourceClone.querySelector('#url').innerHTML = '<a href="'
							+ resourceData.url + '" target="_blank">'
							+ resourceData.url + '</a>';
					resourceClone.querySelector('#lastUpdate').textContent = util
							.formatDate(resourceData.lastUpdate);

					this.container.querySelector('#resources-content')
							.appendChild(resourceClone);
				}
			};

			/**
			 * Displays RDF summary data in RDF Summary tab.
			 */
			DatasetView.prototype.displayRDFSumList = function(
					resourceFilesList) {
				this.resourceFilesList = resourceFilesList;
				var viewSet = false;

				var that = this;

				var ckanResources = this.ckan.resources;

				// Creates rows for each resource that appears in
				// ckan.resources.
				// Also adds the rows for files for which download ended with
				// errors.
				for (var i = 0; i < ckanResources.length; i++) {
					var resourceData = ckanResources[i];
					// console.log(resourceData);

					var name = resourceData.name;
					var url = resourceData.url;

					if (name === null || name === "" || name === "null") {
						name = resourceData.description;
					}

					// populate resource field for RDF sums and ontologies
					if (resourceData.canDownload) {
						var rdfResource = document
								.querySelector('template#rdf-resource').content;
						var rdfResourceClone = document.importNode(rdfResource,
								true);
						this.container.querySelector('#rdfsum-resourcelist')
								.appendChild(rdfResourceClone);

						var rdfResourceAdded = document
								.getElementById("rdf-resource");

						// console.log(resourceData.resourceId);
						rdfResourceAdded.id = resourceData.resourceId
								+ "rdfsum";
						rdfResourceAdded.querySelector('#resource-name').innerHTML = "<a href='"
								+ url + "' target='_blank'>" + name + "</a>";
						// for the case name does not fit the area
						rdfResourceAdded.querySelector('#resource-name').title = name
								+ " " + url;

						var downloadError = resourceData.downloadError;

						// for resources which do not have processed dump
						// entries, with download error
						if (downloadError !== "") {
							var row = document.createElement('tr');
							row.id = name;
							var cell = document.createElement('td');
							cell.innerHTML = "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true' title='"
									+ resourceData.downloadError
									+ "'></span> Resource not downloaded";

							row.appendChild(cell);
							row.className = 'table-row';

							row
									.addEventListener(
											'click',
											function() {
												var selectedRow = row;
												return function() {
													var selection = document
															.querySelector(
																	'#rdfsum-resourcelist')
															.querySelector(
																	".table-row-selected");
													selection.className = "table-row";
													selectedRow.className = "table-row-selected";
													resetHeb();
													resetFd();
												};
											}());

							rdfResourceAdded.querySelector('#rdfsum-table')
									.querySelector("tbody").appendChild(row);

						}
					}
				}// end of ckan resource processing

				// Creates rows for each file associated to each resource and
				// adds the rows under resource row with corresponding resource
				// id.
				for (var i = 0; i < resourceFilesList.length; i++) {
					var resourceData = resourceFilesList[i];

					var resourceId = resourceData.resourceId;

					var files = resourceData.files;
					for (var j = 0; j < files.length; j++) {
						var file = files[j];

						var row = document.createElement('tr');
						row.id = file.originalFileName;
						var cell = document.createElement('td');

						cell.innerHTML = "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true' title='"
								+ file.error
								+ "' style='display:"
								+ (file.hasError ? 'inline' : 'none')
								+ "'> </span>"
								+ (file.originalFileName == "" ? file.fileName
										: file.originalFileName)
								+ " ("
								+ util.formatNumber(file.tripleCount)
								+ " triples)";
						row.appendChild(cell);
						row.className = 'table-row';

						row.addEventListener('click', function() {
							var selectedRow = row;
							var resId = resourceId;
							var is = that;
							var rD = file;
							return function() {
								var selection = document.querySelector(
										'#rdfsum-resourcelist').querySelector(
										".table-row-selected");
								selection.className = "table-row";
								selectedRow.className = "table-row-selected";
								is.loadFileRDFSum(rD.originalFileName, resId);
							};
						}());

						if (!viewSet && !file.hasError) {
							row.className = 'table-row-selected';
							this.loadFileRDFSum(file.originalFileName,
									resourceId);
							viewSet = true;
						}

						document.getElementById(resourceId + "rdfsum")
								.querySelector('#rdfsum-table').querySelector(
										"tbody").appendChild(row);

					}

				}

				if (document.getElementById("heb-button").checked) {
					document.getElementById("heb-div").style.display = "block";
					document.getElementById("fd-div").style.display = "none";
				} else {
					document.getElementById("fd-div").style.display = "block";
					document.getElementById("heb-div").style.display = "none";
				}

				document
						.getElementById("heb-button")
						.addEventListener(
								'click',
								function(event) {
									document.getElementById("fd-button").checked = false;
									document.getElementById("heb-div").style.display = "block";
									document.getElementById("fd-div").style.display = "none";
								});

				document
						.getElementById("fd-button")
						.addEventListener(
								'click',
								function(event) {
									document.getElementById("heb-button").checked = false;
									document.getElementById("fd-div").style.display = "block";
									document.getElementById("heb-div").style.display = "none";
								});
			};

			/**
			 * Displays the RDF sum visualization for the specified 'file'.
			 */
			DatasetView.prototype.displayFileRDFSum = function(file) {
				if (file.heb != null) {
					makeVizHeb(file.heb);
				} else {
					resetHeb();
				}

				if (file.fd != null) {
					makeVizFd(file.fd);
				} else {
					resetFd();
				}
			};

			/**
			 * Displays RDF summary data in RDF Summary tab.
			 */
			DatasetView.prototype.displayVocabularyList = function(
					resourceFilesList) {
//				console.log(resourceFilesList);
				this.resourceFilesList = resourceFilesList;
				var viewSet = false;

				var that = this;

				var ckanResources = this.ckan.resources;

				// Creates rows for each resource that appears in
				// ckan.resources.
				// Also adds the rows for files for which download ended with
				// errors.
				for (var i = 0; i < ckanResources.length; i++) {
					var resourceData = ckanResources[i];
					// console.log(resourceData);

					var name = resourceData.name;
					var url = resourceData.url;

					if (name === null || name === "" || name === "null") {
						name = resourceData.description;
					}

					// populate resource field for RDF sums and ontologies
					if (resourceData.canDownload) {
						var vocabularyResource = document
								.querySelector('template#vocabulary-resource').content;
						var vocabularyResourceClone = document.importNode(
								vocabularyResource, true);
						this.container
								.querySelector('#vocabulary-resourcelist')
								.appendChild(vocabularyResourceClone);

						var vocabularyResourceAdded = document
								.getElementById("vocabulary-resource");

						// console.log(resourceData.resourceId);
						vocabularyResourceAdded.id = resourceData.resourceId
								+ "vocab";
						vocabularyResourceAdded.querySelector('#resource-name').innerHTML = "<a href='"
								+ url + "' target='_blank'>" + name + "</a>";
						// for the case name does not fit the area
						vocabularyResourceAdded.querySelector('#resource-name').title = name
								+ " " + url;

						var downloadError = resourceData.downloadError;

						// for resources which do not have processed dump
						// entries, with download error
						if (downloadError !== "") {
							var row = document.createElement('tr');
							row.id = name;
							var cell = document.createElement('td');
							cell.innerHTML = "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true' title='"
									+ resourceData.downloadError
									+ "'></span> Resource not downloaded";

							row.appendChild(cell);
							row.className = 'table-row';

							row
									.addEventListener(
											'click',
											function() {
												var is = that;
												var selectedRow = row;
												return function() {
													var selection = document
															.querySelector(
																	'#vocabulary-resourcelist')
															.querySelector(
																	".table-row-selected");
													selection.className = "table-row";
													selectedRow.className = "table-row-selected";
													is.clearVocabularies();
												};
											}());

							vocabularyResourceAdded.querySelector(
									'#vocabulary-table').querySelector("tbody")
									.appendChild(row);

						}
					}
				}// end of ckan resource processing

				// Creates rows for each file associated to each resource and
				// adds the rows under resource row with corresponding resource
				// id.
				for (var i = 0; i < resourceFilesList.length; i++) {
					var resourceData = resourceFilesList[i];

					var resourceId = resourceData.resourceId;

					var files = resourceData.files;
					for (var j = 0; j < files.length; j++) {
						var file = files[j];

						var row = document.createElement('tr');
						row.id = file.originalFileName;
						var cell = document.createElement('td');

						cell.innerHTML = "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true' title='"
								+ file.error
								+ "' style='display:"
								+ (file.error.indexOf("lodstats") != -1 ? 'inline'
										: 'none')
								+ "'> </span>"
								+ (file.originalFileName == "" ? file.fileName
										: file.originalFileName)
								+ " ("
								+ util.formatNumber(file.tripleCount)
								+ " triples)";
						row.appendChild(cell);
						row.className = 'table-row';

						row.addEventListener('click', function(event) {
							var selectedRow = row;
							var resId = resourceId;
							var is = that;
							var rD = file;
							return function() {
								var selection = document.querySelector(
										'#vocabulary-resourcelist')
										.querySelector(".table-row-selected");
								selection.className = "table-row";
								selectedRow.className = "table-row-selected";
								is.loadFileVocabulary(rD.originalFileName,
										resId);
							};
						}());

						if (!viewSet && !file.hasError) {
							row.className = 'table-row-selected';
							this.loadFileVocabulary(file.originalFileName,
									resourceId);
							viewSet = true;
						}

						document.getElementById(resourceId + "vocab")
								.querySelector('#vocabulary-table')
								.querySelector("tbody").appendChild(row);
					}
				}
			};

			/**
			 * Displays ontology data in Ontology tab.
			 */
			DatasetView.prototype.displayFileVocabulary = function(file) {
				var vocabularyTable = document.getElementById(
						'vocabulary-list-table').querySelector('tbody');

				while (vocabularyTable.hasChildNodes()) {
					vocabularyTable.removeChild(vocabularyTable.firstChild);
				}

				if (file.hasVocabularies) {

					var vocabularies = file.vocabularies.sort(function(v1, v2) {
						var v1Title = v1.title.toLowerCase();
						var v2Title = v2.title.toLowerCase();
						if (v1Title < v2Title)
							return 1;
						else if (v1Title > v2Title)
							return -1;
						else
							return 0;
					});
					vocabularies
							.forEach(function(vocabulary) {
								var vocabularyItem = document
										.querySelector('template#vocabulary-item').content;
								var vocabularyItemClone = document.importNode(
										vocabularyItem, true);

								vocabularyItemClone.querySelector('#title').innerHTML = "<span>"
										+ vocabulary.title + "</span>";

								vocabularyItemClone.querySelector('#uri').innerHTML = '<span style="padding-left:15px; padding-top:10px;"><a href="'
										+ vocabulary.uri.replace('/', '//')
										+ '" target="_blank"><span class="glyphicon glyphicon-link" aria-hidden="true" title="'
										+ vocabulary.uri
										+ '"></span></a></span>';

								if (vocabulary.lovUri !== "") {
									vocabularyItemClone.querySelector('#lov').innerHTML = '<span  style="padding-left: 5px;" title="Show in Linked Open Vocabularies"><a href="'
											+ vocabulary.lovUri
											+ '" target="_blank"><img src="img/LOV_icon.png" alt="LOV" width="15" height="15"  style="border: 1px solid #ccc;"></a>';
								}

								document
										.getElementById('vocabulary-list-table')
										.querySelector('tbody').appendChild(
												vocabularyItemClone);
							});
				}
			};

			/**
			 * Removes all vocabularies listed in the vocabulary table.
			 */
			DatasetView.prototype.clearVocabularies = function() {
				var vocabularyTable = document.getElementById(
						'vocabulary-list-table').querySelector('tbody');

				while (vocabularyTable.hasChildNodes()) {
					vocabularyTable.removeChild(vocabularyTable.firstChild);
				}
			}

			/**
			 * Initializes analytics chart containers without data.
			 */
			DatasetView.prototype.displayCharts = function(resultset) {
				var dataFunction = util.dataFunction;

				// merge the datasets which appear both as incoming and outgoing
				// links
				var all = [];

				var names = [];

				var incoming = resultset.incoming;
				
				console.log(incoming);

				if (incoming !== undefined) {

					incoming.forEach(function(link) {
						link.classType = 'incoming';
						all.push(link);
						names.push(link.name);
					});
				}

				var outgoing = resultset.outgoing;

				if (outgoing !== undefined) {
					outgoing.forEach(function(link) {
						var incomingIndex = names.indexOf(link.name);
						// console
						// .log('name ' + link.name + " index "
						// + incomingIndex);
						if (incomingIndex > -1) {
							all[incomingIndex].classType = 'inout';
						} else {
							link.classType = 'outgoing';
							all.push(link);
						}
					});
				}

				this.container.querySelector("#number-of-datasets").innerHTML = all.length;

				if (all.length > 0) {

					var chartConfig = viewConfig.DatasetChartTabConfig;
					// no need for double click, so specify 'false'
					//
					// document.getElementById('overlay-content').setAttribute(
					// 'style',
					// ('height:' + (window.innerHeight - 180) + 'px'));

					var analyticsParent = this.container.querySelector('#'
							+ chartConfig.parentId);

					analyticsParent.className = "tab-pane active";
					this.chartElements = {};

					var that = this;
					// add sort combobox part
					var sortList = this.container.querySelector('#sortlist');
					var charts = chartConfig.charts;
					for (var i = 0; i < charts.length; i++) {
						var chart = charts[i];
						var option = document.createElement("option");
						option.text = chart.sortBy;
						option.value = i;
						sortlist.add(option);
					}

					// add sort listener
					sortList.addEventListener('change', function(event) {
						var chart = chartConfig.charts[event.target.value];
						that.sort(chart);
					});

					// init chart containers

					// // this is necessary to be able to view scrollbar
					// this.container.querySelector('#overlay-content').setAttribute(
					// 'style',
					// ('height:' + (window.innerHeight - 150) + 'px'));

					var chartListener = lodApp.Controller.ChartListener
							.getListenerInstance(this.controller, true);

					var chartItem;
					for ( var item in charts) {
						chartItem = charts[item];
						// creates chart container
						lodApp.View.ChartView.createChartContainer(
								analyticsParent, chartItem);

						// gets chart from chart factory and adds it to charts
						// list
						// of
						// this analytics group view for later access.
						this.chartElements[chartItem.chartId] = lodApp.d3
								.getChartFactory().getChart(this, chartItem,
										chartListener);
					}
					chartListener.setChartElements(this.chartElements);

					for ( var item in this.chartElements) {
						this.chartElements[item].setData(all);
					}

					this.sort(charts[0]);
				}
				this.resize();

			};

			/**
			 * Sorts the charts in this view depending on the selected type in
			 * the combo box.
			 * 
			 * @param event
			 *            event for selection change in combo box.
			 */
			DatasetView.prototype.sort = function(chart) {
				for ( var item in this.chartElements) {
					this.chartElements[item].sort(util.comparator(
							chart.chartDetails.dataFunction, chart.sortDesc));
				}
			};
			/**
			 * Renders the specified 'resultset' in all analytics views.
			 * 
			 * @param resultset
			 *            the list of datasets.
			 */
			DatasetView.prototype.renderCharts = function(data) {
				var charts = this.chartConfig.charts;
				var chartItem;
				for ( var item in charts) {
					chartItem = charts[item];
					this.chartElements[chartItem.chartId]
							.setData(data.ckan[chartItem.chartDetails.dataSet]);
				}
			};

			DatasetView.prototype.dispose = function() {
				this.container.querySelector('.header').innerHTML = "";

				var parent = this.container.querySelector('#overlay-content');

				this.container = null;
				this.chartElements = null;
				this.chartConfig = null;
				this.controller = null;

				var childNodes = parent.childNodes;

				for (var i = childNodes.length - 1; i >= 0; i--) {
					parent.removeChild(parent.childNodes[i]);
				}
			};

			DatasetView.prototype.resize = function() {
				document.getElementById('overlay-content').setAttribute(
						'style',
						('height:' + (window.innerHeight - 180) + 'px'));
				// TODO resize other views
				for ( var item in this.chartElements) {
					this.chartElements[item].resize();
				}
			};

			DatasetView.prototype.selectOnChart = function(element) {
				for ( var item in this.chartElements) {
					this.chartElements[item].selectElement(element,
							this.chartElements[item]);
				}
			};

			DatasetView.prototype.clearSelection = function(element) {
				for ( var item in this.chartElements) {
					this.chartElements[item].clearSelection();
				}
			};

			return new DatasetView(controller_, data_);
		}
	};

})(lodApp.View);
