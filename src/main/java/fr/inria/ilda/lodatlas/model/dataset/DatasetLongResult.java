/*******************************************************************************
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.model.dataset;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.json.JsonArray;
import javax.json.JsonObject;

import fr.inria.ilda.lodatlas.commons.strings.DatasetStrings;
import fr.inria.ilda.lodatlas.model.Link;

/**
 * 
 * @author Hande Gözükan
 *
 */
public class DatasetLongResult extends DatasetShortResult {

	/**
	 * List of outgoing links specified for the dataset.
	 */
	protected List<Link> outgoingLinks = new LinkedList<>();

	/**
	 * List of incoming links specified for the dataset.
	 */
	protected List<Link> incomingLinks = new LinkedList<>();

	public DatasetLongResult(String jsonString) {
		super(jsonString);

		JsonArray outgoingLinkList = getJsonArray(getJsonObject(jsonObj, DatasetStrings.OUTGOING_LINKS),
				DatasetStrings.LINKS);
		for (int i = 0; i < outgoingLinkList.size(); i++) {
			JsonObject link = outgoingLinkList.getJsonObject(i);

			outgoingLinks.add(new Link(getString(link, DatasetStrings.NAME), getLong(link, DatasetStrings.COUNT)));
		}

		JsonArray incomingLinkList = getJsonArray(getJsonObject(jsonObj, DatasetStrings.INCOMING_LINKS),
				DatasetStrings.LINKS);
		for (int i = 0; i < incomingLinkList.size(); i++) {
			JsonObject link = incomingLinkList.getJsonObject(i);

			incomingLinks.add(new Link(getString(link, DatasetStrings.NAME), getLong(link, DatasetStrings.COUNT)));
		}

	}

	public Collection<Link> getIncomingLinks() {
		return this.incomingLinks;
	}

	public Collection<Link> getOutgoingLinks() {
		return this.outgoingLinks;
	}

}
