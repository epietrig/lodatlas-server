FROM maven:3.5.4-jdk-8-slim as builder

LABEL maintainer="hande.gozukan.inria.fr"

ENV LANG en_US.utf8

# Install dependencies to build the project
RUN apt-get update && apt-get install -y --allow-unauthenticated \
    git \
    librdf0-dev \
    python-dev \
    python-librdf \
    python-pip \
    raptor2-utils && \
    pip install \
    bitarray \
    future \
    requests && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /opt

# Clone repo from GitLab repository with submodules
RUN git clone --recurse-submodules https://gitlab.inria.fr/epietrig/LODAtlas.git

# Checkout version xxx

WORKDIR /root/.m2
# Create settings.xml file for Maven
RUN touch settings.xml

# Add CEDAR teams private maven repository password to settings file
RUN echo '<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0" \
             xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" \
	     xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 \
	     https://maven.apache.org/xsd/settings-1.0.0.xsd"> \
  <servers> \
    <server> \
      <id>rdfsummary-releases</id> \
      <username>rdfsummary-private-reader</username> \
      <password>rfdsummary4maven6525766</password> \
    </server> \
    <server> \
      <id>oakcommons-releases</id> \
      <username>oak-private-reader</username> \
      <password>oakreader!</password> \
    </server> \
  </servers> \
</settings>' >  /root/.m2/settings.xml


WORKDIR /opt/LODAtlas

# Package 
RUN mvn clean package -Pprod -Dmaven.test.skip=true

#######################################################
# Start new image
#######################################################
FROM tomcat:8.0.49-jre8-alpine

WORKDIR /usr/local/tomcat/

# Copy lodatlas-server from previous build
COPY --from=builder /opt/LODAtlas/lodatlas-server/target/lodatlas-server.war ./webapps/lodatlas.war
#COPY conf/server.xml /usr/local/tomcat/conf/server.xml
#COPY conf/tomcat-users.xml /usr/local/tomcat/conf/tomcat-users.xml

EXPOSE 8080
#EXPOSE 8443
