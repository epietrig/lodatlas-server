/*******************************************************************************
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.rest;

import java.util.Collection;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import fr.inria.ilda.lodatlas.model.FileRdfSum;
import fr.inria.ilda.lodatlas.model.FileVocabulary;
import fr.inria.ilda.lodatlas.model.LinkList;
import fr.inria.ilda.lodatlas.model.Resource;
import fr.inria.ilda.lodatlas.model.dataset.Dataset;
import fr.inria.ilda.lodatlas.service.RepoService;

/**
 * {@link RepoResource} class provides REST API for exact match repository and
 * dataset data.
 * 
 * @author Hande Gözükan
 *
 */
@Singleton
@RequestScoped
@Path("repos")
@Produces(MediaType.APPLICATION_JSON)
public class RepoResource {

	private static final Logger logger = Logger.getLogger(RepoResource.class);

	@Inject
	RepoService repoService;

	private static final String REPO_NAME = "repoName";

	private static final String DATASET_NAME = "datasetName";

	private static final String RESOURCE_ID = "resourceId";

	private static final String DUMP_NAME = "dumpName";

	public RepoResource() {
	}

	/**
	 * Searches the dataset with name <code>datasetName</code> from the repository
	 * specified by <code>repoName</code> and returns the dataset info.
	 * 
	 * @param repoName
	 *            name of the repository that the dataset belongs to.
	 * @param datasetName
	 *            name of the dataset in the repository.
	 * @return dataset info provided by the repository for the dataset with name
	 *         <code>datasetName</code> from the specified <code>repoName</code>.
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{repoName}/datasets/{datasetName}")
	public Response getRepoDataset(@PathParam(REPO_NAME) String repoName, @PathParam(DATASET_NAME) String datasetName) {
		logger.info("Client asked for path: /repos/" + repoName + "/datasets/" + datasetName);
		Dataset result = null;
		try {
			result = repoService.getDataset(repoName, datasetName);
		} catch (Exception e) {
			logger.error("Exception while getting dataset fields from Elasticsearch indexes. Path: /repos/" + repoName
					+ "/datasets/" + datasetName, e);
			Response.serverError().entity(getErrorString("Internal server error. " + e.getMessage())).build();
		}
		return returnResult(result);
	}

	/**
	 * Searches the dataset with name <code>datasetName</code> from the repository
	 * specified by <code>repoName</code> and returns the list of datasets which
	 * appear in the incoming and outgoing links list of the dataset with
	 * <code>datasetName</code> with their basic info.
	 * 
	 * @param repoName
	 *            name of the repository that the dataset belongs to.
	 * @param datasetName
	 *            name of the dataset.
	 * @return returns the list of datasets which appear in incoming and outgoing
	 *         links with their basic info.
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{repoName}/datasets/{datasetName}/linkdetails")
	public Response getRepoDatasetLinks(@PathParam(REPO_NAME) String repoName,
			@PathParam(DATASET_NAME) String datasetName) {
		logger.info("Client asked for path: /repos/" + repoName + "/datasets/" + datasetName + "/linkdetails");
		LinkList result = null;
		try {
			result = repoService.getDatasetLinkDetails(repoName, datasetName);
		} catch (Exception e) {
			logger.error("Exception while getting dataset link details from Elasticsearch indexes. Path: /repos/"
					+ repoName + "/datasets/" + datasetName + "/linkdetails", e);
			Response.serverError().entity(getErrorString("Internal server error. " + e.getMessage())).build();
		}
		return returnResult(result);
	}

	/**
	 * Gets list of all resource dump files processed, including their short form
	 * info, for dataset with name <code>datasetName</code> from the repository
	 * specified by <code>repoName</code>.
	 * 
	 * @param repoName
	 *            name of the repository that the dataset belongs to.
	 * @param datasetName
	 *            name of the dataset.
	 * @return list of all dump files processed, including their short form info,
	 *         for dataset with name <code>datasetName</code> from the repository
	 *         specified by <code>repoName</code>.
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{repoName}/datasets/{datasetName}/resourcesshort")
	public Response getDatasetResourceFileList(@PathParam(REPO_NAME) String repoName,
			@PathParam(DATASET_NAME) String datasetName) {
		logger.info("Client asked for path: /repos/" + repoName + "/datasets/" + datasetName + "/resources");
		Collection<Resource> result = null;
		try {
			result = repoService.getDatasetResourceFileList(repoName, datasetName);
		} catch (Exception e) {
			logger.error("Exception while getting dataset link details from Elasticsearch indexes. Path: /repos/"
					+ repoName + "/datasets/" + datasetName + "/resources", e);
			Response.serverError().entity(getErrorString("Internal server error. " + e.getMessage())).build();
		}
		return returnResult(result);
	}

	/**
	 * Gets RDF quotients for the dump-file with specified by <code>dumpName</code>
	 * from the resource with <code>resourceId</code> of the dataset with name
	 * <code>datasetName</code> that belongs to the repository specified by
	 * <code>repoName</code>.
	 * 
	 * @param repoName
	 *            name of the repository that the dataset belongs to.
	 * @param datasetName
	 *            name of the dataset.
	 * @param resourceId
	 *            id of the resource that the dump file belongs to
	 * @param dumpName
	 *            name of the dump file
	 * @return RDF summary for the dump-file with specified by <code>dumpName</code>
	 *         from the resource with <code>resourceId</code> of the dataset with
	 *         name <code>datasetName</code> that belongs to the repository
	 *         specified by <code>repoName</code>.
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{repoName}/datasets/{datasetName}/resources/{resourceId}/rdfquotients/{dumpName}")
	public Response getDatasetRDFSum(@PathParam(REPO_NAME) String repoName, @PathParam(DATASET_NAME) String datasetName,
			@PathParam(RESOURCE_ID) String resourceId, @PathParam(DUMP_NAME) String dumpName) {

		logger.info("Client asked for path: /repos/" + repoName + "/datasets/" + datasetName + "/resources/"
				+ resourceId + "/rdfquotients/" + dumpName);
		FileRdfSum result = null;
		try {
			result = repoService.getDatasetRdfSum(repoName, datasetName, resourceId, dumpName);
		} catch (Exception e) {
			logger.error(
					"Exception while getting dataset resource file RDF quotients from Elasticsearch indexes. Path: /repos/"
							+ repoName + "/datasets/" + datasetName + "/resources/" + resourceId + "/rdfquotients/"
							+ dumpName,
					e);
			Response.serverError().entity(getErrorString("Internal server error. " + e.getMessage())).build();
		}
		return returnResult(result);
	}

	/**
	 * Gets vocabulary list for the dump-file with specified by
	 * <code>dumpName</code> from the resource with <code>resourceId</code> of the
	 * dataset with name <code>datasetName</code> that belongs to the repository
	 * specified by <code>repoName</code>.
	 * 
	 * @param repoName
	 *            name of the repository that the dataset belongs to.
	 * @param datasetName
	 *            name of the dataset.
	 * @param resourceId
	 *            id of the resource that the dump file belongs to
	 * @param dumpName
	 *            name of the dump file
	 * @return vocabulary list for the dump-file with specified by
	 *         <code>dumpName</code> from the resource with <code>resourceId</code>
	 *         of the dataset with name <code>datasetName</code> that belongs to the
	 *         repository specified by <code>repoName</code>.
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{repoName}/datasets/{datasetName}/resources/{resourceId}/vocabularies/{dumpName}")
	public Response getDatasetVocabulary(@PathParam(REPO_NAME) String repoName,
			@PathParam(DATASET_NAME) String datasetName, @PathParam(RESOURCE_ID) String resourceId,
			@PathParam(DUMP_NAME) String dumpName) {
		logger.info("Client asked for path: /repos/" + repoName + "/datasets/" + datasetName + "/resources/"
				+ resourceId + "/vocabularies/" + dumpName);
		FileVocabulary result = null;
		try {
			result = repoService.getDatasetVocabulary(repoName, datasetName, resourceId, dumpName);
		} catch (Exception e) {
			logger.error(
					"Exception while getting dataset resource file vocabularies from Elasticsearch indexes. Path: /repos/"
							+ repoName + "/datasets/" + datasetName + "/resources/" + resourceId + "/vocabularies/"
							+ dumpName,
					e);
			Response.serverError().entity(getErrorString("Internal server error. " + e.getMessage())).build();
		}
		return returnResult(result);
	}

	private Response returnResult(Object result) {
		if (result == null) {
			logger.info("No results found.");
			return Response.ok("{}", MediaType.APPLICATION_JSON).build();
		}
		return Response.ok(result, MediaType.APPLICATION_JSON).build();
	}

	/**
	 * Generates a JSON format error string for the specified <code>error</code>.
	 * 
	 * @param error
	 *            contents of error
	 * @return a JSON format error string for the specified <code>error</code>
	 */
	private String getErrorString(String error) {
		return "{\"error\": \"" + error + "\"}";
	}
}
