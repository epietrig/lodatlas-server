/*******************************************************************************
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.rest;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import fr.inria.ilda.lodatlas.model.HitList;
import fr.inria.ilda.lodatlas.model.SearchResult;
import fr.inria.ilda.lodatlas.model.dataset.DatasetBase;
import fr.inria.ilda.lodatlas.model.dataset.DatasetLinks;
import fr.inria.ilda.lodatlas.model.facet.FacetGroup;
import fr.inria.ilda.lodatlas.service.SearchService;

/**
 * {@link SearchResource} class provides REST API for dataset and facet search.
 * 
 * @author Hande Gözükan
 *
 */
@Singleton
@RequestScoped
@Path("/search")
@Produces(MediaType.APPLICATION_JSON)
public class SearchResource {

	@Inject
	SearchService searchService;

	// TODO This should be injected
	private static final Logger logger = Logger.getLogger(SearchResource.class);

	private static final String REPO = "repo";

	private static final String RDFSUM = "rdfsum";

	private static final String ORG = "org";

	private static final String TAG = "tag";

	private static final String FORMAT = "format";

	private static final String LICENSE = "license";

	private static final String PROPS = "props";

	private static final String CLASSES = "classes";

	private static final String VOCABS = "vocabs";

	private static final String FIELDS = "fields";

	private static final String IS_AND = "isAnd";

	private static final String SHOW = "show";

	private static final String IS_SHOW = "isShow";

	private static final String ID = "id";

	private static final String FROM = "from";

	private static final String SIZE = "size";

	// private static final String DEFAULT_FROM = "0";
	//
	// private static final String DEFAULT_SIZE = "1000";

	 @Context
	 UriInfo uriInfo;

	public SearchResource() {
	}

	/**
	 * Searches the datasets using the specified query parameters and returns all
	 * the facet lists with corresponding number of datasets and short format
	 * dataset metadata for the resultset.
	 *
	 * 
	 * @param repo
	 *            list of repositories selected from "Repositories" facet.
	 * @param rdfsum
	 *            must be set to "RDF quotients available" if only datasets with
	 *            RDFQuotients are required.
	 * @param org
	 *            list of names of the organizations from "Organizations" facet.
	 * @param tag
	 *            list of tags from "Tags" facet.
	 * @param format
	 *            list of formats from "Formats" facet.
	 * @param license
	 *            list of licenses from "Licenses" facet.
	 * @param props
	 *            list of ontology property URIs from "Properties" facet.
	 * @param classes
	 *            list of ontology class URIs from "Classes" facet.
	 * @param vocabs
	 *            list of ontology URIs from "Vocabularies" facet.
	 * @param fields
	 *            the list of queries to execute in selected fields among "name",
	 *            "title", "description", "classes", "properties", "vocabularies".
	 *            // TODO specify format of the queries
	 * @param isAnd
	 *            boolean value to indicate if the queries in fields list will be
	 *            combined with AND or OR.
	 * @param showMore
	 *            facet category names under which to display list of all facets
	 *            instead of displaying the first 10.
	 * @param from
	 *            index to start from to get the list of datasets.
	 * @param size
	 *            number of datasets to fetch starting from <code>from</code> index.
	 * @return facet lists with corresponding number of datasets and short format
	 *         dataset metadata for the resultset.
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFacetList(@QueryParam(REPO) List<String> repo, @QueryParam(RDFSUM) List<String> rdfsum,
			@QueryParam(ORG) List<String> org, @QueryParam(TAG) List<String> tag,
			@QueryParam(FORMAT) List<String> format, @QueryParam(LICENSE) List<String> license,
			@QueryParam(PROPS) List<String> props, @QueryParam(CLASSES) List<String> classes,
			@QueryParam(VOCABS) List<String> vocabs, @QueryParam(FIELDS) List<String> fields,
			@QueryParam(IS_AND) boolean isAnd, @QueryParam(SHOW) List<String> showMore,
			@DefaultValue("0") @QueryParam(FROM) int from, @DefaultValue("1000") @QueryParam(SIZE) int size) {

		logger.info("Client asked for path: /search");
		SearchResult<DatasetBase> result = null;
		try {
			logger.info("from is " + from + " size is " + size);
			result = searchService.getFacets(repo, rdfsum, org, tag, format, license, props, classes, vocabs, fields,
					isAnd, showMore, from, size);
			logger.info(result.getHitList().getTotalHits());
		} catch (Exception e) {
			logger.error("Exception while executing search in the Elasticsearch indexes", e);
			Response.serverError().entity(getErrorString("Internal server error. " + e.getMessage())).build();
		}
		return returnResult(result);
	}

	/**
	 * Searches the datasets using the specified query parameters and returns only
	 * the facet lists with counts for the facet category specified by
	 * <code>show</code> parameter. If <code>isShow</code> is <code>true</code> all
	 * list of facets is returned; otherwise only top 10 facets are returned.
	 * 
	 * @param repo
	 *            list of repositories selected from "Repositories" facets.
	 * @param rdfsum
	 *            must be set to "RDF quotients available" if only datasets with
	 *            RDFQuotients are required.
	 * @param org
	 *            list of names of the organizations from "Organizations" facets.
	 * @param tag
	 *            list of tags from "Tags" facets.
	 * @param format
	 *            list of formats from "Formats" facets.
	 * @param license
	 *            list of licenses from "Licenses" facets.
	 * @param props
	 *            list of ontology property URIs from "Properties" facets.
	 * @param classes
	 *            list of ontology class URIs from "Classes" facets.
	 * @param vocabs
	 *            list of ontology URIs from "Vocabularies" facets.
	 * @param fields
	 *            the list of queries to execute in selected fields among "name",
	 *            "title", "description", "classes", "properties", "vocabularies".
	 *            // TODO specify format of the queries
	 * @param isAnd
	 *            boolean value to indicate if the queries in fields list will be
	 *            combined with AND or OR. O
	 * @param categoryName
	 *            facet category name to show more/less. This field should be
	 *            specified.
	 * @param isShow
	 *            <code>true</code> to show all facets; <code>false</code> to show
	 *            only first 10 facet in the required facet category.
	 * 
	 * @return only the facet lists with counts for the facet category specified by
	 *         <code>show</code> parameter for the datasets satisfying the specified
	 *         query parameters. If <code>isShow</code> is <code>true</code> all
	 *         list of facets is returned; otherwise only top 10 facets are
	 *         returned.
	 * 
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("facet")
	public Response getSingleFacet(@QueryParam(REPO) List<String> repo, @QueryParam(RDFSUM) List<String> rdfsum,
			@QueryParam(ORG) List<String> org, @QueryParam(TAG) List<String> tag,
			@QueryParam(FORMAT) List<String> format, @QueryParam(LICENSE) List<String> license,
			@QueryParam(PROPS) List<String> props, @QueryParam(CLASSES) List<String> classes,
			@QueryParam(VOCABS) List<String> vocabs, @QueryParam(FIELDS) List<String> fields,
			@QueryParam(IS_AND) boolean isAnd, @QueryParam(SHOW) String categoryName,
			@QueryParam(IS_SHOW) boolean isShow) {

		logger.info("Client asked for single facet list for category " + categoryName + ". Path: /search/facet");

		if (categoryName == null || categoryName.equals("")) {
			logger.error("No facet category is specified.");
			return Response.serverError()
					.entity(getErrorString("Facet category name should be specified in the form show=<categoryname>."))
					.build();
		}

		FacetGroup result = null;
		try {
			result = searchService.getSingleFacet(repo, rdfsum, org, tag, format, license, props, classes, vocabs,
					fields, isAnd, categoryName, isShow);
		} catch (Exception e) {
			logger.error("Exception while executing single facet search in the Elasticsearch indexes", e);
			Response.serverError().entity("Internal server error.\n" + e.getMessage()).build();
		}
		return returnResult(result);
	}

	/**
	 * Gets long format data, including incoming and outgoing links, for the
	 * datasets in the <code>ids</code> list.
	 * 
	 * @param ids
	 *            the list of datasets, in the form <em>repoName;datasetName</em>
	 * @param from
	 *            index to start from to get the list of datasets.
	 * @param size
	 *            number of datasets to fetch starting from <code>from</code> index.
	 * @return basic dataset info with links for specified datasets, including
	 *         incoming and outgoing links.
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("datasetlinks")
	public Response getDatasetLinks(@QueryParam(ID) List<String> ids, @DefaultValue("0") @QueryParam(FROM) int from,
			@DefaultValue("1000") @QueryParam(SIZE) int size) {
		logger.info("Client asked for path /search/datasetlinks");

		HitList<DatasetLinks> result = null;
		try {
			result = searchService.getDatasetLinks(ids, from, size);
		} catch (Exception e) {
			logger.error("Exception while executing search in the Elasticsearch indexes", e);
			Response.serverError().entity(getErrorString("Internal server error. " + e.getMessage())).build();
		}
		return returnResult(result);
	}

	/**
	 * Returns concatenated void files as plain text for the datasets specified by
	 * <code>ids</code> list.
	 * 
	 * @param ids
	 *            the list of datasets, in the form <em>repoName;datasetName</em>
	 * @param from
	 *            index to start from to get the list of datasets.
	 * @param size
	 *            number of datasets to fetch starting from <code>from</code> index.
	 * @return concatenated void files as plain text for the datasets specified by
	 *         <code>ids</code> list.
	 */
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("void")
	public Response getVoid(@QueryParam(ID) List<String> ids, @DefaultValue("0") @QueryParam(FROM) int from,
			@DefaultValue("1000") @QueryParam(SIZE) int size) {
		logger.info("Client asked for path /search/void");

		String result = null;
		try {
			result = searchService.getVoid(ids, from, size);
		} catch (Exception e) {
			logger.error("Exception while executing search in the Elasticsearch indexes", e);
			Response.serverError().entity(getErrorString("Internal server error. " + e.getMessage())).build();
		}
		return returnResult(result);
	}

	private Response returnResult(Object result) {
		if (result == null) {
			logger.info("No results found.");
			return Response.ok("{}", MediaType.APPLICATION_JSON).build();
		}
		return Response.ok(result, MediaType.APPLICATION_JSON).build();
	}

	/**
	 * Generates a JSON format error string for the specified <code>error</code>.
	 * 
	 * @param error
	 *            contents of error
	 * @return a JSON format error string for the specified <code>error</code>
	 */
	protected String getErrorString(String error) {
		return "{\"error\": \"" + error + "\"}";
	}

}