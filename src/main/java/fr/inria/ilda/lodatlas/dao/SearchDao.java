/*******************************************************************************
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.ConstantScoreQueryBuilder;
import org.elasticsearch.index.query.MultiMatchQueryBuilder.Type;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.sort.SortOrder;

import fr.inria.ilda.lodatlas.common.EsIndexStrings;
import fr.inria.ilda.lodatlas.commons.strings.DatasetStrings;
import fr.inria.ilda.lodatlas.model.facet.FacetCategory;

/**
 * A class to connect to Elasticsearch server and executes queries on
 * Elasticsearch indexes.
 * 
 * @author Hande Gözükan.
 *
 */
public class SearchDao {

	private static final Logger logger = Logger.getLogger(SearchDao.class);

	private static final int MAX_SIZE = 1000;

	private static final int FACET_DEFAULT_SIZE = 10;

	private static final String[] resultFields = { DatasetStrings.ID, DatasetStrings.REPO_NAME, DatasetStrings.NAME,
			DatasetStrings.TITLE, DatasetStrings.NOTES, DatasetStrings.TRIPLE_COUNT,
			DatasetStrings.OUTGOING_LINKS + "." + DatasetStrings.COUNT,
			DatasetStrings.INCOMING_LINKS + "." + DatasetStrings.COUNT, DatasetStrings.METADATA_CREATED,
			DatasetStrings.METADATA_MODIFIED };

	/**
	 * 
	 */
	public SearchDao() {
	}

	/**
	 * Queries the datasets in Elasticsearch indexes satisfying the query specified
	 * and returns the {@link SearchResponse}.
	 * 
	 * 
	 * @param repo
	 *            list of repositories selected from "Repositories" facet.
	 * @param rdfsum
	 *            must be set to "RDF quotients available" if only datasets with
	 *            RDFQuotients are required.
	 * @param org
	 *            list of names of the organizations from "Organizations" facet.
	 * @param tag
	 *            list of tags from "Tags" facet.
	 * @param format
	 *            list of formats from "Formats" facet.
	 * @param license
	 *            list of licenses from "Licenses" facet.
	 * @param props
	 *            list of ontology property URIs from "Properties" facet.
	 * @param classes
	 *            list of ontology class URIs from "Classes" facet.
	 * @param vocabs
	 *            list of ontology URIs from "Vocabularies" facet.
	 * @param fields
	 *            the list of queries to execute in selected fields among "name",
	 *            "title", "description", "classes", "properties", "vocabularies".
	 *            // TODO specify format of the queries
	 * @param isAnd
	 *            boolean value to indicate if the queries in fields list will be
	 *            combined with AND or OR.
	 * @param showMore
	 *            facet category names under which to display list of all facets
	 *            instead of displaying the first 10.
	 * @param from
	 *            index to start from to get the list of datasets.
	 * @param size
	 *            number of datasets to fetch starting from <code>from</code> index.
	 * @return {@link SearchResponse} generated executing the ElasticSearch index
	 *         query.
	 */
	public SearchResponse getFacets(List<String> repo, List<String> rdfsum, List<String> org, List<String> tag,
			List<String> format, List<String> license, List<String> props, List<String> classes, List<String> vocabs,
			List<String> fields, boolean isAnd, List<String> showMore, int from, int size) {

		logger.info("Querying ElasticSearch indices for facets");
		SearchResponse sr = getQueryReponse(repo, rdfsum, org, tag, format, license, props, classes, vocabs, fields,
				isAnd, showMore, from, size, true);

		logger.info("The query returned " + sr.getHits().totalHits + " results");
		return sr;
	}

	/**
	 * 
	 * @param repo
	 *            list of repositories selected from "Repositories" facet.
	 * @param rdfsum
	 *            must be set to "RDF quotients available" if only datasets with
	 *            RDFQuotients are required.
	 * @param org
	 *            list of names of the organizations from "Organizations" facet.
	 * @param tag
	 *            list of tags from "Tags" facet.
	 * @param format
	 *            list of formats from "Formats" facet.
	 * @param license
	 *            list of licenses from "Licenses" facet.
	 * @param props
	 *            list of ontology property URIs from "Properties" facet.
	 * @param classes
	 *            list of ontology class URIs from "Classes" facet.
	 * @param vocabs
	 *            list of ontology URIs from "Vocabularies" facet.
	 * @param fields
	 *            the list of queries to execute in selected fields among "name",
	 *            "title", "description", "classes", "properties", "vocabularies".
	 *            // TODO specify format of the queries
	 * @param isAnd
	 *            boolean value to indicate if the queries in fields list will be
	 *            combined with AND or OR.
	 * @param categoryName
	 *            facet category name to show all or first 10 facets.
	 * @param isShow
	 *            <code>true</code> to show all facets; <code>false</code> to show
	 *            only first 10 facets.
	 * @return {@link SearchResponse} containing only the facet lists with counts
	 *         for the facet category specified by <code>show</code> parameter for
	 *         the datasets satisfying the specified query parameters. If
	 *         <code>isShow</code> is <code>true</code> all list of facets is
	 *         returned; otherwise only top 10 facets are returned.
	 */
	public SearchResponse getSingleFacet(List<String> repo, List<String> rdfsum, List<String> org, List<String> tag,
			List<String> format, List<String> license, List<String> props, List<String> classes, List<String> vocabs,
			List<String> fields, boolean isAnd, String categoryName, boolean isShow) {

		List<String> categories = new ArrayList<>();
		if (isShow) {
			categories.add(categoryName);
		}

		// TODO update to get only relevant aggregation
		SearchResponse sr = getQueryReponse(repo, rdfsum, org, tag, format, license, props, classes, vocabs, fields,
				isAnd, categories, 0, 0, false);
		return sr;
	}

	/**
	 * Getter method for long format data of the datasets specified in
	 * <code>ids</code> list.
	 * 
	 * @param ids
	 *            the list of datasets, in the form <em>repoName;datasetName</em>
	 * @param from
	 *            index to start from to get the list of datasets.
	 * @param size
	 *            number of datasets to fetch starting from <code>from</code> index.
	 * @return {@link SearchResponse} containing basic dataset metadata containing
	 *         incoming and outgoing links for the specified dataset list.
	 */
	public SearchResponse getDatasetLinks(List<String> ids, int from, int size) {
		logger.info("Building dataset query for dataset id list.");

		final BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();

		ids.forEach(new Consumer<String>() {

			@Override
			public void accept(String t) {

				logger.info(t);

				String[] terms = t.split(";");

				if (terms.length == 2) {
					BoolQueryBuilder query = QueryBuilders.boolQuery();

					query.must(QueryBuilders.termQuery(DatasetStrings.REPO_NAME, terms[0]));
					query.must(QueryBuilders.termQuery(EsIndexStrings.NAME_RAW, terms[1]));

					queryBuilder.should(query);
				}
			}
		});

		// queryBuilder.should(QueryBuilders.termsQuery(DatasetStrings.ID, ids));

		// we do not need to calculate scores, so we use constant query
		ConstantScoreQueryBuilder constantQuery = QueryBuilders.constantScoreQuery(queryBuilder);

		SearchRequestBuilder requestBuilder = ElasticClient.searchDataset();

		requestBuilder.setQuery(constantQuery)
				.setFetchSource(new String[] { DatasetStrings.ID, DatasetStrings.REPO_NAME, DatasetStrings.NAME,
						DatasetStrings.TRIPLE_COUNT, DatasetStrings.METADATA_CREATED, DatasetStrings.METADATA_MODIFIED,
						DatasetStrings.OUTGOING_LINKS, DatasetStrings.INCOMING_LINKS }, new String[] {})
				.setFrom(from).setSize(size > MAX_SIZE ? MAX_SIZE : size);

		logger.debug("Query is\n" + constantQuery.toString());

		return requestBuilder.execute().actionGet();
	}

	/**
	 * Searches the ElasticSearch index with name INDEX_NAME_CKAN to retrieve VoID
	 * relevant data for the datasets with exact name match in the specified
	 * <code>names</code> list.
	 * 
	 * @param ids
	 *            the list of datasets, in the form <em>repoName;datasetName</em>
	 * @param from
	 *            index to start from to get the list of datasets.
	 * @param size
	 *            number of datasets to fetch starting from <code>from</code> index.
	 * @return ElasticSearch search result hits.
	 */
	public SearchResponse getVoid(List<String> ids, int from, int size) {

		logger.debug("Starting query in ElasticSearch index.");

		final BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();

		ids.forEach(new Consumer<String>() {

			@Override
			public void accept(String t) {

				logger.info(t);

				String[] terms = t.split(";");

				if (terms.length == 2) {
					BoolQueryBuilder query = QueryBuilders.boolQuery();

					query.must(QueryBuilders.termQuery(DatasetStrings.REPO_NAME, terms[0]));
					query.must(QueryBuilders.termQuery(EsIndexStrings.NAME_RAW, terms[1]));

					queryBuilder.should(query);
				}
			}
		});

		// queryBuilder.should(QueryBuilders.termsQuery(EsIndexStrings.NAME_RAW, ids));

		// we do not need to calculate scores, so we use constant query
		ConstantScoreQueryBuilder constantQuery = QueryBuilders.constantScoreQuery(queryBuilder);

		SearchRequestBuilder requestBuilder = ElasticClient.searchDataset();

		requestBuilder.setQuery(constantQuery)
				.setFetchSource(new String[] { DatasetStrings.NAME, DatasetStrings.TITLE, DatasetStrings.DESCRIPTION,
						DatasetStrings.URL, DatasetStrings.REPO_DATASET_URL, DatasetStrings.AUTHOR,
						DatasetStrings.ORGANIZATION, DatasetStrings.METADATA_CREATED, DatasetStrings.METADATA_MODIFIED,
						DatasetStrings.LICENSE_URL, DatasetStrings.RESOURCES, DatasetStrings.TRIPLE_COUNT,
						DatasetStrings.OUTGOING_LINKS, DatasetStrings.INCOMING_LINKS }, new String[] {})
				.setFrom(from).setSize(size > MAX_SIZE ? MAX_SIZE : size);

		logger.debug("Query is\n" + constantQuery.toString());

		return requestBuilder.execute().actionGet();
	}

	/**
	 * 
	 * @param repo
	 * @param rdfsum
	 * @param org
	 * @param tag
	 * @param format
	 * @param license
	 * @param props
	 * @param classes
	 * @param vocabs
	 * @param fields
	 * @param isAnd
	 * @param showMore
	 * @return
	 */
	private SearchResponse getQueryReponse(List<String> repo, List<String> rdfsum, List<String> org, List<String> tag,
			List<String> format, List<String> license, List<String> props, List<String> classes, List<String> vocabs,
			List<String> fields, boolean isAnd, List<String> showMore, int from, int size, boolean fetchSource) {

		List<QueryBuilder> queryBuilders = new ArrayList<>();

		boolean fieldQueryExists = false;

		// add query for the text search fields
		if (fields.size() > 0) {
			logger.info("Field query exists.");
			QueryBuilder fieldQueryBuilder = addFieldQuery(fields, isAnd);
			if (fieldQueryBuilder != null) {
				queryBuilders.add(fieldQueryBuilder);
				fieldQueryExists = true;
			} else {
				logger.info("Could not generate field query, returned null.");
			}
		}

		// add query for selected facets
		// facet fields are not_analyzed, they have to be exact match, so term
		// query
		if (repo.size() > 0) {
			queryBuilders.add(QueryBuilders.termsQuery(FacetCategory.REPOSITORY.fieldName(), repo));
		}

		if (rdfsum.size() > 0) {
			queryBuilders.add(QueryBuilders.termsQuery(FacetCategory.RDFSUM.fieldName(), rdfsum));
		}

		if (org.size() > 0) {
			queryBuilders.add(QueryBuilders.termsQuery(FacetCategory.ORGANIZATION.fieldName(), org));
		}

		if (tag.size() > 0) {
			queryBuilders.add(QueryBuilders.termsQuery(FacetCategory.TAG.fieldName(), tag));
		}

		if (format.size() > 0) {
			queryBuilders.add(QueryBuilders.termsQuery(FacetCategory.FORMAT.fieldName(), format));
		}

		if (license.size() > 0) {
			queryBuilders.add(QueryBuilders.termsQuery(FacetCategory.LICENSE.fieldName(), license));
		}

		if (props.size() > 0) {
			queryBuilders.add(QueryBuilders.termsQuery(FacetCategory.PROPERTIES.fieldName(), props));
		}

		if (classes.size() > 0) {
			queryBuilders.add(QueryBuilders.termsQuery(FacetCategory.CLASSES.fieldName(), classes));
		}

		if (vocabs.size() > 0) {
			queryBuilders.add(QueryBuilders.termsQuery(FacetCategory.VOCABULARIES.fieldName(), vocabs));
		}

		QueryBuilder queryBuilder = null;

		if (queryBuilders.size() == 1) {
			queryBuilder = queryBuilders.get(0);
		} else {
			queryBuilder = QueryBuilders.boolQuery();

			for (QueryBuilder builder : queryBuilders) {
				((BoolQueryBuilder) queryBuilder).must(builder);
			}
		}

		SearchRequestBuilder requestBuilder = ElasticClient.searchDataset();

		// DFS_QUERY_THEN_FETCH tells Elasticsearch to first retrieve the local
		// IDF from each shard in order to calculate the global IDF across the whole
		// index
		requestBuilder.setSearchType(SearchType.DFS_QUERY_THEN_FETCH);

		requestBuilder.setQuery(queryBuilder);

		if (fetchSource) {
			requestBuilder.setFrom(from).setSize(size > MAX_SIZE ? MAX_SIZE : size).setFetchSource(resultFields,
					new String[] {});
		} else {
			requestBuilder.setFetchSource(false);
		}

		// add aggregation for each facet category
		for (FacetCategory item : FacetCategory.values()) {
			addAggregation(requestBuilder, item, showMore.contains(item.aggName()));
		}

		if (!fieldQueryExists)
			requestBuilder.addSort(DatasetStrings.REPO_NAME, SortOrder.ASC)
					.addSort(EsIndexStrings.NAME_RAW, SortOrder.ASC)
					.addSort(DatasetStrings.TRIPLE_COUNT, SortOrder.DESC);

		logger.info(requestBuilder.toString());
		// execute request
		SearchResponse sr = requestBuilder.execute().actionGet();
		logger.info("sr " + sr.getHits().getTotalHits());
		return sr;
	}

	/**
	 * Generates field queries with text search.
	 * 
	 * @param fields
	 * @param isAnd
	 * @return
	 */
	private QueryBuilder addFieldQuery(List<String> fields, boolean isAnd) {

		List<QueryBuilder> queryBuilders = new ArrayList<>();

		for (String field : fields) {

			logger.info("Adding single query for field " + field);
			QueryBuilder singleFieldQuery = addSingleFieldQuery(field);
			if (singleFieldQuery != null) {
				queryBuilders.add(singleFieldQuery);
			} else {
				logger.info("Single query returned null");
			}
		}

		if (queryBuilders.size() > 1) {
			BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();

			for (QueryBuilder queryBuilder : queryBuilders) {
				if (isAnd) {
					boolQueryBuilder.must(queryBuilder);
				} else {
					boolQueryBuilder.should(queryBuilder);
				}
			}
			return boolQueryBuilder;

		} else if (queryBuilders.size() == 1) {
			return queryBuilders.get(0);
		}
		return null;
	}

	/**
	 * Processes single field text search and builds the query.
	 * 
	 * @param field
	 * @return
	 */
	private QueryBuilder addSingleFieldQuery(String field) {
		logger.info("Building query for " + field);

		// replace consecutive spaces, tabs, etc with single space
		String[] parts = field.replaceAll("\\s\\s+", " ").split(";");

		String queryParamsAsString = parts[0].replace("\\;", ";");
		String[] terms = parts[1].split(",");

		List<String> params = new ArrayList<>();

		Pattern p = Pattern.compile("([^\"]\\S*|\".+?\")\\s*");
		Matcher matcher = p.matcher(queryParamsAsString);

		while (matcher.find()) {
			String exact = matcher.group(0).trim();
			if (!"".equals(exact)) {
				if (exact.startsWith("http://")) {
					exact = "\"" + exact + "\"";
				}
				logger.info("param exact " + exact);
				params.add(exact);
			}
		}

		if (params.size() > 0 && terms.length > 0) {

			QueryBuilder queryBuilder = null;

			List<QueryBuilder> queries = new ArrayList<>();

			for (String param : params) {
				if (param.startsWith("\"") && param.endsWith("\"")) {
					queryParamsAsString = queryParamsAsString.replace(param, "");
					queries.add(addMatchQuery(param, terms));
				}
			}
			if (!"".equals(queryParamsAsString.trim())) {
				queries.add(addMatchQuery(queryParamsAsString, terms));
			}
			if (queries.size() == 1) {
				queryBuilder = queries.get(0);
			} else {
				queryBuilder = QueryBuilders.boolQuery();
				for (QueryBuilder query : queries) {
					((BoolQueryBuilder) queryBuilder).should(query);
				}
			}
			return queryBuilder;
		} else {
			logger.info("param.size " + params.size() + " terms.size " + terms.length);
			return null;
		}

	}

	/**
	 * 
	 * @param queryTextOriginal
	 * @param fields
	 * @return
	 */
	private QueryBuilder addMatchQuery(String queryText, String[] fields) {
		logger.info("Adding query for query text " + queryText + " for fields " + fields.toString());

		if (queryText.startsWith("\"") && queryText.endsWith("\"")) {
			if (fields.length == 1) {
				return QueryBuilders.matchPhraseQuery(fields[0], queryText);
			} else {
				BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();

				for (String field : fields) {
					queryBuilder.should(QueryBuilders.matchPhraseQuery(field, queryText.replace("\"", "")));
				}
				return queryBuilder;
			}
		} else {
			logger.info("Initializing with " + (fields.length * 4) + " elements");
			String[] fieldsArray = new String[fields.length * 4];
			for (int i = 0; i < fields.length * 4; i += 4) {
				fieldsArray[i] = fields[i / 4];
				fieldsArray[i + 1] = fields[i / 4] + ".stem";
				fieldsArray[i + 2] = fields[i / 4] + ".shingle";
				fieldsArray[i + 3] = fields[i / 4] + ".split";
			}
			return QueryBuilders.multiMatchQuery(queryText, fieldsArray).type(Type.BEST_FIELDS).tieBreaker(0.2f);
		}
	}

	/**
	 * 
	 * @param requestBuilder
	 * @param facetCategory
	 * @param showMore
	 */
	private void addAggregation(SearchRequestBuilder requestBuilder, FacetCategory facetCategory, boolean showMore) {
		requestBuilder.addAggregation(AggregationBuilders.terms(facetCategory.aggName())
				.field(facetCategory.fieldName()).size(showMore ? MAX_SIZE : FACET_DEFAULT_SIZE));
	}

}
