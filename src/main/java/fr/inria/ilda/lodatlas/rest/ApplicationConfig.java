/*******************************************************************************
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.rest;

import javax.ws.rs.ApplicationPath;

import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;

import fr.inria.ilda.lodatlas.service.RepoService;
import fr.inria.ilda.lodatlas.service.SearchService;
import fr.inria.ilda.lodatlas.service.SubmitService;

@ApplicationPath("/rest")
public class ApplicationConfig extends ResourceConfig {

	public ApplicationConfig() {
		packages("fr.inria.ilda.lodatlas.rest");

		register(new AbstractBinder() {

			@Override
			protected void configure() {
				bind(new SearchService()).to(SearchService.class);
				bind(new RepoService()).to(RepoService.class);
				bind(new SubmitService()).to(SubmitService.class);
			}
		});
	}
}
