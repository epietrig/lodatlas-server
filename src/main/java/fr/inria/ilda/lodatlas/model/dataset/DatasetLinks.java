/*******************************************************************************
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.model.dataset;

import java.util.ArrayList;
import java.util.List;

import fr.inria.ilda.lodatlas.model.Link;

/**
 * 
 * @author Hande Gözükan
 *
 */
public class DatasetLinks extends DatasetBase {

	/**
	 * List of outgoing links specified for the dataset.
	 */
	protected ArrayList<Link> outgoingLinks = new ArrayList<Link>();

	/**
	 * List of incoming links specified for the dataset.
	 */
	protected ArrayList<Link> incomingLinks = new ArrayList<Link>();

	public DatasetLinks() {

	}

	public List<Link> getIncomingLinks() {
		return this.incomingLinks;
	}

	public List<Link> getOutgoingLinks() {
		return this.outgoingLinks;
	}

	public void addIncomingLink(Link link) {
		this.incomingLinks.add(link);
	}

	public void addOutgoingLink(Link link) {
		this.outgoingLinks.add(link);
	}
}
