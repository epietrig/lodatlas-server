package fr.inria.ilda.lodatlas.dao;

public class DaoFactory {

	private static SearchDao searchDao;

	private static RepoDao repoDao;

	public static SearchDao getSearchDao() {
		if (searchDao == null)
			searchDao = new SearchDao();
		return searchDao;
	}

	public static RepoDao getRepoDao() {
		if (repoDao == null)
			repoDao = new RepoDao();
		return repoDao;
	}

}
