package fr.inria.ilda.lodatlas.service;

import javax.json.JsonObject;

import org.apache.log4j.Logger;

import fr.inria.ilda.lodatlas.commons.voidprocess.VoidParser;
import fr.inria.ilda.lodatlas.dao.MongoConnector;

public class SubmitService {

	private static final Logger logger = Logger.getLogger(SubmitService.class);

	public SubmitService() {

	}

	public boolean saveDataset(String datasetJson) {

		// TODO check if valid json
		// if valid return true

		MongoConnector.addDataset(datasetJson);

		// else return false;
		return false;
	}

	public JsonObject getDatasetJson(String voidLink) {
		logger.info("Calling Void parser");

		VoidParser voidParser = new VoidParser();
		return voidParser.processDatasetVoid(voidLink);
	}
}
