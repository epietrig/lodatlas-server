/*******************************************************************************
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.model;

import java.util.Collection;
import java.util.List;

/**
 * 
 * @author Hande Gözükan
 *
 */
public class RdfSum {

	private final List<Node> nodes;

	private final List<Link2> links;

	private final List<String> groups;

	public RdfSum(List<Node> nodes, List<Link2> links, List<String> groups) {
		this.nodes = nodes;
		this.links = links;
		this.groups = groups;
	}

	public Collection<Node> getNodes() {
		return this.nodes;
	}

	public Collection<Link2> getLinks() {
		return this.links;
	}

	public Collection<String> getGroups() {
		return this.groups;
	}
}
