/*
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
'use strict';

/**
 * SearchSection view represents the "Search" part in the main user interface.
 */
var lodApp = lodApp || {};

lodApp.View = lodApp.View || {};

(function(o) {
	o.SearchSection = {
		getInstance : function(controller_) {

			var util = lodApp.util;
			var dataFunction = util.dataFunction;

			var MODES = {
				SIMPLE : "simple",
				ADVANCED : "advanced"
			};

			var SearchSectionView = function(controller) {
				this.controller = controller;
				this.mode = MODES.SIMPLE;
				this.current = 1;
				this.totalHits = 0;

				this.init();
			};

			SearchSectionView.constructor = SearchSectionView;

			SearchSectionView.prototype.init = function() {
				this.element = document.getElementById('search-section');

				// this is necessary because search text field is not cleared on
				// page refresh
				this.clearAdvanced();

				// add onclick event to search form
				var form = document.getElementById('search-form');

				// this is necessary because search text field is not cleared on
				// page refresh
				this.clearSimpler();

				var that = this;
				form.addEventListener('submit', function(event) {
					event.preventDefault();
					that.updateSearchCriteria(form);
				});

				var searchBox = document.getElementById('search-textbox');
				searchBox.focus();

				var advancedSearchButton = document
						.getElementById("advanced-search-button");
				advancedSearchButton.addEventListener('click', function() {
					that.showAdvancedSearch();
				});

				var simplerSearchButton = document
						.getElementById("simpler-search-button");
				simplerSearchButton.addEventListener('click', function() {
					that.showSimplerSearch();
				});

				this.pager = document.getElementById("page-list");
			};

			/**
			 * Shows advanced search boxes in the search group view.
			 */
			SearchSectionView.prototype.showAdvancedSearch = function() {

				this.mode = MODES.ADVANCED;

				this.addSearchItem();
				this.addSearchItem();

				document.getElementById("simpler-search").style.display = "none";
				this.clearSimpler();

				document.getElementById("advanced-search").style.display = "block";
			};

			/**
			 * Shows simpler search boxes in the search group view.
			 */
			SearchSectionView.prototype.showSimplerSearch = function() {
				this.mode = MODES.SIMPLE;

				document.getElementById("advanced-search").style.display = "none";
				document.getElementById("simpler-search").style.display = "block";
				this.clearAdvanced();
			};

			/**
			 * Adds searchItem from search-item template to the search-items
			 * div.
			 * 
			 * @param index
			 *            index where the element should be inserted. if not
			 *            specified, the new item is appended to the list.
			 */
			SearchSectionView.prototype.addSearchItem = function(i) {
				var index = i;

				var searchItems = document.getElementById("search-items");

				var size = searchItems.childElementCount;

				var searchItem = document.querySelector('template#search-item').content;

				var searchItemClone = document.importNode(searchItem, true);

				if (index < size) {
					searchItems.insertBefore(searchItemClone,
							searchItems.children[index]);
				} else {
					index = size;
					searchItems.appendChild(searchItemClone);
				}

				var addedNode = searchItems.children[index];

				var that = this;

				var addButton = addedNode.querySelector("#add-button");
				addButton.addEventListener('click', function() {
					var currentNode = addedNode;
					return function() {
						that.addSearchItemAfterNode(currentNode);
					}
				}());

				var removeButton = addedNode.querySelector("#remove-button");
				removeButton.addEventListener('click', function() {
					var currentNode = addedNode;
					return function() {
						that.removeSearchItem(currentNode);
					}
				}());
			};

			/**
			 * Adds a new search item after the specified node in search-items
			 * list.
			 * 
			 * @param node
			 */
			SearchSectionView.prototype.addSearchItemAfterNode = function(node) {
				var searchItems = document.getElementById("search-items");
				var index = Array.prototype.indexOf.call(searchItems.children,
						node);
				this.addSearchItem(index + 1);
			};

			/**
			 * Removes the specified item from search-items list.
			 */
			SearchSectionView.prototype.removeSearchItem = function(item) {
				var searchItems = document.getElementById("search-items");
				if (searchItems.childElementCount > 1) {
					searchItems.removeChild(item);
				} else {
					alert("Cannot remove final search item!");
				}
			};

			/**
			 * Resets the fields of simpler-search div.
			 */
			SearchSectionView.prototype.clearSimpler = function() {

				var simplerSearch = document.getElementById("simpler-search");
				simplerSearch.querySelector("#search-textbox").text = "";

				var form = document.getElementById('search-form');
				form.queryText.value = '';
				form.name.checked = true;
				form.title.checked = true;
				form.desc.checked = true;
				form.classes.checked = true;
				form.props.checked = true;
				form.vocabs.checked = true;
			}

			/**
			 * Resets the fields of advanced-search div.
			 */
			SearchSectionView.prototype.clearAdvanced = function() {
				document.getElementById("search-items").innerHTML = "";

				document.getElementById('any-button').checked = true;
			};

			// TODO change this
			SearchSectionView.prototype.updateSearchCriteria = function(form) {

				if (this.mode == MODES.SIMPLE) {
					var queryText = form.queryText.value;

					if (queryText === '') {
						this.controller.criteriaChanged([], false);
					} else {
						var selection = [];
						if (form.name.checked)
							selection.push("name");
						if (form.title.checked)
							selection.push("title");
						if (form.desc.checked)
							selection.push("notes");
						if (form.classes.checked) {
							selection.push("resources.usedClasses");
							selection.push("resources.classLabels");
						}
						if (form.props.checked) {
							selection.push("resources.usedProperties");
							selection.push("resources.propertyLabels");
						}
						if (form.vocabs.checked)
							selection.push("resources.vocabularies");

						// to change search criteria at least one field must be
						// selected.
						if (selection.length > 0) {
							this.controller.criteriaChanged([ queryText
									.replace(";", "\\;")
									+ ";" + selection.join(",") ], true);
						} else {
							alert('Please select at least one field for search!');
						}
					}
				} else if (this.mode == MODES.ADVANCED) {
					var fields = [];
					var searchItems = document.getElementById("search-items");

					for (var i = 0; i < searchItems.childElementCount; i++) {
						var element = searchItems.children[i];

						queryText = element.children[0].value;

						if (queryText !== '') {
							selection = [];

							if (element.children[2]
									.querySelector('#name-input').control.checked)
								selection.push("name");
							if (element.children[2]
									.querySelector('#title-input').control.checked)
								selection.push("title");
							if (element.children[2]
									.querySelector('#desc-input').control.checked)
								selection.push("notes");

							if (element.children[2]
									.querySelector('#classes-input').control.checked) {
								selection.push("resources.usedClasses");
								selection.push("resources.classLabels");
							}

							if (element.children[2]
									.querySelector('#props-input').control.checked) {
								selection.push("resources.usedProperties");
								selection.push("resources.propertyLabels");
							}

							if (element.children[2]
									.querySelector('#vocabs-input').control.checked)
								selection.push("resources.vocabularies");

							if (selection.length > 0) {
								fields.push(queryText.replace(";", "\\;") + ";"
										+ selection.join(","));
							}
						}
					}

					if (fields.length === 0) {
						alert("Please enter at least one search criteria");
					} else {
						var form = document.getElementById('search-form');
						// console.log(form.logical.value);
						this.controller.criteriaChanged(fields,
								(form.logical.value == "all" ? true : false));
					}

				}
			};

			SearchSectionView.prototype.renderPages = function(totalHits, from) {
				// console.log("total hits " + totalHits);
				// console.log("start " + from);

				if (this.totalHits != totalHits) {
					this.totalHits = totalHits;

					while (this.pager.firstChild) {
						this.pager.removeChild(this.pager.firstChild);
					}

					if (totalHits === 0) {
						return;
					}

					this.current = from;

					var pageCount = Math.ceil(totalHits
							/ this.controller.getPageSize());
					// console.log(pageCount);

					var li = document.createElement("li");
					li.setAttribute("id", "previous");
					if (this.current === 1) {
						li.setAttribute("class", "disabled");
					}
					li.innerHTML = '<a href="#" aria-label="Previous"> <span aria-hidden="true">&laquo;</span></a>';

					var that = this;

					li
							.addEventListener(
									'click',
									function() {
										return function() {
											if (this.className != 'disabled') {
												that.pager.children[that.current].className = '';
												that.current = that.current - 1;

												if (that.current === 1) {
													this.className = 'disabled';
												} else if (that.current < pageCount) {
													that.pager
															.querySelector('#next').className = '';
												}
												that.pager.children[that.current].className = 'active';
												that.controller
														.showNext(that.current);
											}
										};

									}());
					this.pager.appendChild(li);

					var i;

					for (i = 1; i <= pageCount; i++) {
						li = document.createElement("li");
						li.setAttribute("id", i);
						li.innerHTML = '<a href="#">' + i + '</a>';
						if (this.current == i) {
							li.setAttribute("class", "active");
						}
						li
								.addEventListener(
										'click',
										function() {
											var page = i;
											return function(event) {
												// set current not active
												that.pager.children[that.current].className = '';

												that.current = page;

												if (that.current === 1) {
													that.pager
															.querySelector('#previous').className = 'disabled';
												} else {
													that.pager
															.querySelector('#previous').className = '';
												}
												if (that.current === pageCount) {
													that.pager
															.querySelector('#next').className = 'disabled';
												} else {
													that.pager
															.querySelector('#next').className = '';
												}

												that.pager.children[that.current].className = 'active';
												that.controller
														.showNext(that.current);
											};

										}());

						this.pager.appendChild(li);
					}

					li = document.createElement("li");
					li.setAttribute("id", "next");
					li.innerHTML = '<a href="#" aria-label="Next"> <span aria-hidden="true">&raquo;</span></a>';
					if (this.current === pageCount) {
						li.setAttribute("class", "disabled");
					}
					li
							.addEventListener(
									'click',
									function() {
										return function() {
											if (this.className != 'disabled') {
												that.pager.children[that.current].className = '';
												that.current = that.current + 1;

												if (that.current === pageCount) {
													this.className = 'disabled';
												} else if (that.current > 1) {
													that.pager
															.querySelector('#previous').className = '';
												}
												that.pager.children[that.current].className = 'active';
												that.controller
														.showNext(that.current);
											}
										};

									}());
					this.pager.appendChild(li);
				} else if (this.totalHits != 0) {
					this.pager.children[this.current].className = 'active';
				}
			}

			return new SearchSectionView(controller_);

		}
	};

})(lodApp.View);
