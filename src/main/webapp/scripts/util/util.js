/*
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
'use strict';

var lodApp = lodApp || {};

lodApp.util = lodApp.util || {};

(function(o) {
	o.dataFunction = {
		identity : function(d) {
			return d;
		},
		repoName : function(d) {
			return d.rN;
		},
		name : function(d) {
			return d.name;
		},
		trimName : function(d) {
			var name = d.name;
			var length = name.length;
			name = name.substring(0, 5) + '...'
					+ name.substring(length - 6, length);
			return name;
		},
		trimNameHttp : function(d) {
			var name = d.name;
			var length = name.length;
			name = '...' + name.substring(length - 15, length);
			return name;
		},
		title : function(d) {
			return d.title;
		},
		tripleCount : function(d) {
			return d.tc;
		},
		tripleCountText : function(d) {
			return d.tc == 0 ? "N/A" : d.tc;
		},
		outgoingLinkCount : function(d) {
			return d.olC;
		},
		incomingLinkCount : function(d) {
			return d.ilC;
		},
		totalLinkCount : function(d) {
			return d.olC + d.ilC;
		},
		creationDate : function(d) {
			return new Date(d.cd);
		},
		modificationDate : function(d) {
			if (d.md != null && d.md != "")
				return new Date(d.md);
			else
				return new Date(d.cd);
		},
		indexFunction : function(d, i) {
			return i;
		},
		linkCount : function(d) {
			return d.count;
		},
		datahubio : function(d) {
			return "<https://datahub.io/dataset/" + d.name + ">";
		}
	};

	/**
	 * Creates a comparator function for sorting using the specified
	 * 'dataFunction'.
	 * 
	 * @param desc - boolean to indicate if descending
	 */
	o.comparator = function(dataFunction, desc) {
		if (desc) {
			return function sort(dataset1, dataset2) {
				return dataFunction(dataset2) - dataFunction(dataset1);
			};
		} else {
			return function sort(dataset1, dataset2) {
				return dataFunction(dataset1) - dataFunction(dataset2);
			};
		}
	};

	// /**
	// * Takes the specified array and sorts it depending on the specified
	// 'sortFunction'.
	// */
	// o.sort = function (array, sortFunction) {
	// result =[];
	// result = array.sort(function(dataset1, dataset2) {
	// return parseInt(sortFunction(dataset2)) -
	// parseInt(sortFunction(dataset1));
	// });
	//		
	// // TODO eğer triple countlar eşitse, link count'a göre sırala
	// return result;
	// };

	// o.formatPercent = function(number) {
	// return
	// };

	o.formatNumber = function(number) {
		if (number == "N/A")
			return number;
		return new Intl.NumberFormat('en-US').format(number);
	};

	o.formatDate = function(dateToFormat) {
		var date = new Date(dateToFormat);
		if (isFinite(date))
			return date.toLocaleDateString();
		return "";
	};

})(lodApp.util);
