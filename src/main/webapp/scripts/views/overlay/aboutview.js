/*
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
'use strict';

var lodApp = lodApp || {};

lodApp.View = lodApp.View || {};

(function(o) {
	o.AboutOverlayView = {
		getInstance : function(controller_) {

			var AboutView = function(controller) {
				this.controller = controller;
				this.container = document.getElementById('overlay-container');

				this.init();
			};

			AboutView.prototype.constructor = AboutView;

			/**
			 * Initializes the about template and appends it to overlay.
			 */
			AboutView.prototype.init = function() {

				// set header
				// change header in view
				this.container.querySelector('.header').innerHTML = "<span class=\"about-header\">About</span>";

				// set contents of the about page
				var aboutView = document.querySelector('template#about').content;

				var aboutViewClone = document.importNode(aboutView, true);
				this.container.querySelector('#overlay-content').appendChild(
						aboutViewClone);
				this.resize();
			};

			/**
			 * Removes all content added to div with id 'overlay-content'.
			 */
			AboutView.prototype.dispose = function() {
				this.container.querySelector('.header').innerHTML = "";

				var parent = this.container.querySelector('#overlay-content');

				var childNodes = parent.childNodes;

				for (var i = childNodes.length - 1; i >= 0; i--) {
					parent.removeChild(parent.childNodes[i]);
				}
				this.container = null;
				this.controller = null;
			};

			/**
			 * 
			 */
			AboutView.prototype.resize = function() {
				document.getElementById('overlay-content').setAttribute(
						'style',
						('height:' + (window.innerHeight - 180) + 'px'));
			};

			return new AboutView(controller_);

		}
	};
})(lodApp.View);
