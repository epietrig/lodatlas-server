/*
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
'use strict';

var lodApp = lodApp || {};

lodApp.View = lodApp.View || {};

(function(o) {
	o.TableView = {
		getInstance : function(parentId_, columnList_, controller_) {
			var dataFunction = util.dataFunction;

			var Table = function(parentId, columnList, controller) {
				this.controller = controller;
				this.parentView = document.getElementById(parentId);
				this.columns = columnList;
				this.init();
			};

			Table.constructor = Table;

			Table.prototype.init = function() {
				var table = document.querySelector('template#table-view').content;

				var tableClone = document.importNode(table, true);

				this.parentView.appendChild(tableClone);
				this.tableElement = this.parentView
						.querySelector('#table-content');
				this.list = [];
				this.initColumns();
			};

			Table.prototype.initColumns = function() {
				var tableHeaderRow = this.tableElement.querySelector('tr');
				var headerCell = document.createElement('th');
				headerCell.innerHTML = '<input type="checkbox" id="selectall" name="selectall" value="selectall">';

				tableHeaderRow.appendChild(headerCell);

				var that = this;

				headerCell
						.addEventListener(
								'click',
								function() {
									var thisTable = that.tableElement;
									return function(event) {
										var checkbox = thisTable
												.querySelector("input[name='selectall']")
										var checkboxes = thisTable
												.querySelectorAll("input[name='selecteddataset']");

										checkboxes
												.forEach(function(checkboxItem) {
													checkboxItem.checked = checkbox.checked;
												});
									};

								}());

				headerCell = document.createElement('th');
				headerCell.innerHTML = '';

				tableHeaderRow.appendChild(headerCell);

				for ( var item in this.columns) {
					var column = this.columns[item];

					headerCell = document.createElement('th');
					headerCell.innerHTML = column.name;
					if (column.className) {
						headerCell.className = column.className;
					}
					tableHeaderRow.appendChild(headerCell);
				}
			};

			Table.prototype.renderRows = function(rows, keepExisting) {
				var tableBody = this.tableElement.querySelector('tbody');
				var rowCount = tableBody.childElementCount;
				var i = rowCount;

				if (!keepExisting && rowCount > 0) {
//					console.log(tableBody);
					// remove the excess rows
					while (i > 0) {
						tableBody.removeChild(tableBody.children[--i]);
					}

					rowCount = 0;
					this.list = [];
				}

				// add rows starting from ith index
				while (i < rowCount + rows.length) {
					var index = i - rowCount;

					var rowData = rows[index];
					this.list.push(rowData);

					var that = this;

					var row = document.createElement('tr');
					var cell = document.createElement('td');
					var input = document.createElement('input');

					// assumes that the names of datasets are listed in the
					// second column of row data
					input.type = "checkbox";
					input.id = i;
					input.name = "selecteddataset";
					input.value = this.columns[2].func(rowData);

					input.addEventListener(
									'change',
									function() {
										var context = that;
										var currentInput = input;
										return function() {
											if (!currentInput.checked) {
												context.tableElement
														.querySelector("input[name='selectall']").checked = false;
											}
										};
									}());

					cell.appendChild(input);
					row.appendChild(cell);

					cell = document.createElement('td');
					cell.innerHTML = '<span class="glyphicon glyphicon-eye-open eye" aria-hidden="true" style="margin-top:7px"></span>';
					row.appendChild(cell);

					cell.addEventListener('click', function() {
						var rD = rowData;
						return function(event) {
//							console.log(event);
							if (event.target.nodeName !== 'TD') {
								event.preventDefault();
								that.controller.showDataset(rD);
							}
						};

					}(), false);

					cell = document.createElement('td');
					cell.innerHTML = i + 1;
					row.appendChild(cell);

					for (var j = 1; j < this.columns.length; j++) {
						var column = this.columns[j];

						cell = document.createElement('td');
						cell.innerHTML = column.func(rowData);
						if (column.className) {
							cell.className = column.className;
						}
						row.appendChild(cell);
					}

					row.className = 'table-row';

					row
							.addEventListener(
									'click',
									function() {
										var thisRow = row;
										return function(event) {
//											console.log(event);
											if (event.target.nodeName !== 'SPAN'
													&& event.target.nodeName !== 'INPUT') {
												var selection = thisRow
														.querySelector("input[name='selecteddataset']").checked;
												thisRow
														.querySelector("input[name='selecteddataset']").checked = !selection;
											}
										};

									}());

					row.addEventListener('mouseover', function() {
						var rD = rowData;
						return function() {
							var data = rD;
							that.controller.datasetHovered(data);
						};

					}());

					row.addEventListener('mouseout', function() {
						var rD = rowData;
						return function() {
							var data = rD;
							that.controller.datasetClear(data);
						};

					}());

					row.addEventListener('mousedown', function(event) {
						return function(event) {
							if (event.detail > 1) {
								event.preventDefault();
							}
						};

					}(), false);

					tableBody.appendChild(row);

					i++;
				}
			};

			/**
			 * If table is already empty, renders all items in the specified
			 * rows list. Otherwise, adds only the items which do not already
			 * exist in the list.
			 */
			Table.prototype.append = function(rows) {
				// If the list of the table is empty, adds all rows.
				if (this.list.length == 0) {
					this.renderRows(rows);
				} else {

					// Identidy to rows to add to the list, eliminating already
					// existing ones.
					var listToAdd = [];

					var that = this;
					rows.forEach(function(rowItem) {
						// checks if the rowItem already exists in the list
						var existingItem = that.list.find(function(item) {
							return item.name == rowItem.name;
						});

						// if it does not exist, adds it to the list to add
						if (existingItem === undefined) {
							listToAdd.push(rowItem);
						}
					});
					this.renderRows(listToAdd, true);
				}
			};

			/**
			 * Returns the items for which checkbox is checked in the table.
			 */
			Table.prototype.getSelection = function() {
				var selection = [];
				var checkboxes = this.tableElement
						.querySelectorAll("input[name='selecteddataset']");

				var that = this;
				checkboxes.forEach(function(checkboxItem) {
					if (checkboxItem.checked) {
						selection.push(that.list.find(function(item) {
							return item.name == checkboxItem.value
						}));
					}
				});
				return selection;
			}

			/**
			 * Removes the rows where checkbox is checked.
			 */
			Table.prototype.removeSelected = function() {
				var tableBody = this.tableElement.querySelector('tbody');
				var rowCount = tableBody.childElementCount;
				var i = 0;

				while (i < rowCount) {

					var row = tableBody.children[i];
					if (row.children[0].firstChild.checked) {
						tableBody.removeChild(tableBody.children[i]);
						rowCount--;
						this.list.splice(i, 1);
					} else {
						i++;
					}
				}
				this.clearSelection();
			};

			/**
			 * Returns the list of datasets that are viewed in this table.
			 */
			Table.prototype.getList = function() {
				return this.list;
			}

			/**
			 * Clears the checks for checked rows.
			 */
			Table.prototype.clearSelection = function() {
				var checkboxes = this.tableElement
						.querySelectorAll("input[name='selecteddataset']");

				var that = this;
				checkboxes.forEach(function(checkboxItem) {
					checkboxItem.checked = false;
				});

				this.tableElement.querySelector("input[name='selectall']").checked = false;
			}

			return new Table(parentId_, columnList_, controller_);
		}
	};
})(lodApp.View);
