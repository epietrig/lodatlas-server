/*******************************************************************************
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.model;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * A class to return the vocabulary list obtained from a resource file.
 * 
 * @author Hande Gözükan
 *
 */
public class FileVocabulary extends ResourceFileShortEx {

	private final static Logger logger = Logger.getLogger(FileVocabulary.class);
	protected boolean hasVocabularies;

	private List<fr.inria.ilda.lodatlas.commons.model.Vocabulary> vocabularies = new LinkedList<>();

	public FileVocabulary(String jsonString) {
		super(jsonString);

		logger.info("voc " + jsonString);
	}

	public void addVocabulary(fr.inria.ilda.lodatlas.commons.model.Vocabulary vocabulary) {
		vocabularies.add(vocabulary);
		hasVocabularies = true;
	}

	/**
	 * Boolean value to indicate if this resource has vocabulary list.
	 * 
	 * @return
	 */
	public boolean isHasVocabularies() {
		return hasVocabularies;
	}

	/**
	 * Getter method for the vocabulary list for the current resource file.
	 * 
	 * @return
	 */
	public Collection<fr.inria.ilda.lodatlas.commons.model.Vocabulary> getVocabularies() {
		return vocabularies;
	}
}
