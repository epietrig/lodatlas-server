/*
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
'use strict';

var lodApp = lodApp || {};

lodApp.Controller = lodApp.Controller || {};

(function(o) {

	o.ChartListener = {

		getListenerInstance : function(controller_, hasDoubleClick_) {

			var ChartListener = function(controller) {
				this.controller = controller;
				this.chartElements = {};
				this.setState(null, null, false);
			};

			ChartListener.prototype.constructor = ChartListener;

			/**
			 * Sets the chart elements that this listener listes
			 * 
			 * @param chartElements
			 */
			ChartListener.prototype.setChartElements = function(chartElements) {
				this.chartElements = chartElements;
			};

			/**
			 * Adds the 'chart' with specified 'chartId' to the list of charts
			 * that this listener listens to.
			 * 
			 * @param chartId
			 *            the id of the chart to listen to.
			 * @param chart
			 *            the chart to listen to.
			 */
			ChartListener.prototype.addChartElement = function(chartId, chart) {
				this.chartElements[chartId] = chart;
				if (this.element !== null) {
					chart.setMode(this.mode);
					chart.selectElement(this.element, this.fade);
				}
			};

			/**
			 * Removes the chart with specified 'chartId' from the list of
			 * charts.
			 * 
			 * @param chartId
			 *            the id of the chart to remove.
			 */
			ChartListener.prototype.removeChartElement = function(chartId) {
				delete this.chartElements[chartId];
			};

			/**
			 * The element which received the final even
			 * 
			 * @param element
			 * @param mode
			 * @param fade
			 */
			ChartListener.prototype.setState = function(element, mode, fade) {
				this.element = element;
				this.mode = mode;
				this.fade = fade;
			};

			/**
			 * Called when an element is clicked in one of the charts. Clears
			 * current selection on all chartElements and selects the specified
			 * element.
			 * 
			 * @param element
			 *            the element that is clicked on one of the charts.
			 * @param mode
			 *            the mode of the chart which is clicked. The same mode
			 *            is set to all other charts.
			 * @param context
			 *            the chart which is clicked
			 */
			ChartListener.prototype.clicked = function(element, mode, context) {
				this.setState(element, mode, true);
				for ( var item in this.chartElements) {
					var chartItem = this.chartElements[item];
					chartItem.clearSelection();
					chartItem.setMode(mode);
					chartItem.selectElement(element, true, context);
				}
			};

			/**
			 * Called when an element is hovered by mouse in one of the charts.
			 * Hovers the same element and its links in all other chartElements.
			 * 
			 * @param element
			 *            the element that is hovered on one of the charts.
			 * @param mode
			 *            the mode of the chart which is clicked. The same mode
			 *            is set to all other charts.
			 * @param context
			 *            the chart which is clicked
			 */
			ChartListener.prototype.mouseOvered = function(element, mode,
					context) {
				this.setState(element, mode, false);
				for ( var item in this.chartElements) {
					this.chartElements[item].clearSelection();
					this.chartElements[item].setMode(mode);
					this.chartElements[item].selectElement(element, false,
							context);
				}
			};

			/**
			 * Called when mouse is out from an element in one of the charts.
			 * Clears hover from all chartElements.
			 * 
			 * @param element
			 *            the element from which mouse is outed on one of the
			 *            charts.
			 * @param mode
			 *            the mode of the chart where mouse out event is called.
			 *            The same mode is set to all other charts.
			 */
			ChartListener.prototype.mouseOuted = function(element, mode) {
				this.setState(null, mode, false);

				this.clearSelection();
			};

			/**
			 * Clears any selection on all the charts.
			 */
			ChartListener.prototype.clearSelection = function() {
				for ( var item in this.chartElements) {
					this.chartElements[item].clearSelection();
				}
			};

			var ChartListenerWithDoubleClick = function(controller) {
				ChartListener.call(this, controller);
			};

			ChartListenerWithDoubleClick.prototype = Object
					.create(ChartListener.prototype);

			ChartListenerWithDoubleClick.prototype.constructor = ChartListenerWithDoubleClick;

			/**
			 * Called when an element is double clicked in one of the charts.
			 * Clears current selection on all chartElements and selects the
			 * specified element and opens the overlay view to display details
			 * for the selected element.
			 * 
			 * @param element
			 *            the element that is double clicked on one of the
			 *            charts.
			 * @param mode
			 *            the mode of the chart which is double clicked. The
			 *            same mode is set to all other charts.
			 * @param context
			 *            the chart which is clicked
			 */
			ChartListenerWithDoubleClick.prototype.doubleClicked = function(
					element, mode, context) {
				this.setState(element, mode, true);

				for ( var item in this.chartElements) {
					this.chartElements[item].setMode(mode);
				}
				this.controller.showDataset(element);
			};

			if (hasDoubleClick_) {
				return new ChartListenerWithDoubleClick(controller_);
			}
			return new ChartListener(controller_);
		}
	};
})(lodApp.Controller);
