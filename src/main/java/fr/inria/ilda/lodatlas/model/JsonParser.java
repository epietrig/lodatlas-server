/*******************************************************************************
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.model;

import java.io.StringReader;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue.ValueType;

/**
 * 
 * @author hande
 *
 */
public class JsonParser {

	protected final JsonObject jsonObj;

	/**
	 * 
	 * @param jsonString
	 */
	public JsonParser(String jsonString) {
		// System.out.println(jsonString);
		JsonReader reader = Json.createReader(new StringReader(jsonString));
		this.jsonObj = reader.readObject();
		reader.close();
	}

	/**
	 * Checks if the specified <code>key</code> exists in the specified
	 * <code>json</code> object and returns the String value.
	 * 
	 * @param json
	 *            JSON object to search for the value.
	 * @param key
	 *            the key to search for.
	 * @return the value in <code>json</code> if it exits; empty {@link String}
	 *         otherwise.
	 */
	protected String getString(JsonObject json, String key) {
		if (checkKey(json, key)) {
			return json.getString(key);
		} else
			return "";
	}

	/**
	 * Checks if the specified <code>key</code> exists in the specified
	 * <code>json</code> object and returns the {@link Long} value.
	 * 
	 * @param json
	 *            JSON object to search for the value.
	 * @param key
	 *            the key to search for.
	 * @return the value in <code>json</code> if it exits; 0 otherwise.
	 */
	protected long getLong(JsonObject json, String key) {
		if (checkKey(json, key)) {
			return json.getJsonNumber(key).longValue();
		} else
			return 0l;
	}

	/**
	 * Checks if the specified <code>key</code> exists in the specified
	 * <code>json</code> object and returns the {@link Boolean} value.
	 * 
	 * @param json
	 *            JSON object to search for the value.
	 * @param key
	 *            the key to search for.
	 * @return the value in <code>json</code> if it exits; false otherwise.
	 */
	protected boolean getBoolean(JsonObject json, String key) {
		if (checkKey(json, key)) {
			return json.getBoolean(key);
		} else
			return false;
	}

	/**
	 * Checks if the specified <code>key</code> exists in the specified
	 * <code>json</code> object and returns the {@link JsonObject} value.
	 * 
	 * @param json
	 *            JSON object to search for the value.
	 * @param key
	 *            the key to search for.
	 * @return the value in <code>json</code> if it exits; <code>null</code>
	 *         otherwise.
	 */
	protected JsonObject getJsonObject(JsonObject json, String key) {
		if (checkKey(jsonObj, key))
			return json.getJsonObject(key);
		else
			return null;
	}

	/**
	 * Checks if the specified <code>key</code> exists in the specified
	 * <code>json</code> object and returns the {@link JsonArray} value.
	 * 
	 * @param json
	 *            JSON object to search for the value.
	 * @param key
	 *            the key to search for.
	 * @return the value in <code>json</code> if it exits; <code>null</code>
	 *         otherwise.
	 */
	protected JsonArray getJsonArray(JsonObject json, String key) {
		if (checkKey(json, key))
			return json.getJsonArray(key);
		else
			return null;
	}

	/**
	 * Checks if the specified <code>json</code> is not <code>null</code>, the
	 * specified <code>key</code> exists in the specified <code>json</code> object
	 * and it is not <code>null</code>.
	 * 
	 * @param json
	 *            JSON object to search for the value.
	 * @param key
	 *            the key to search for.
	 * @return <code>true</code> if <code>json</code> is not <code>null</code>, the
	 *         value exists for the <code>key</code> and not <code>null</code>;
	 *         false otherwise.
	 */
	private boolean checkKey(JsonObject json, String key) {
		return json != null && json.containsKey(key) && json.get(key).getValueType() != ValueType.NULL;
	}
}
