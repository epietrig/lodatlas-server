/*
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
'use strict';

var lodApp = lodApp || {};

lodApp.Config.View = lodApp.Config.View || {};

var dataFunc = lodApp.util.dataFunction;

(function(o) {
	o.FilterSectionConfig = {
		parentId : 'filter-section',
		charts : [
				{
					filterType : 'Triple count - Total link count',
					sectionId : 'tlc-tc-scatter-filter-view',
					sectionClass : 'filter-view',
					title : 'Triple count - Total link count',
					info : 'Displays scatter plot for triple count and total link counts for all datasets.',
					drag : false,
					collapse : false,
					d3Type : 'scatter',
					chartId : 'tlc-tc-scatter-filter-chart',
					chartClass : 'filter-chart',
					chartDetails : {
						margins : {
							left : 110,
							top : 20,
							right : 40,
							bottom : 50
						},
						shapeClass : 'selector-rect',
						xAxisText : 'Triple count',
						xScale : 'log',
						xDataFunction : dataFunc.tripleCount,
						yAxisText : 'Total link count',
						yScale : 'linear',
						yDataFunction : dataFunc.totalLinkCount
					}
				},
				{
					filterType : 'Triple count - Outgoing link count',
					sectionId : 'olc-tc-scatter-filter-view',
					sectionClass : 'filter-view',
					title : 'Triple count - Outgoing link count',
					info : 'Displays scatter plot for triple count and outgoing link counts for all datasets.',
					drag : false,
					collapse : false,
					d3Type : 'scatter',
					chartId : 'olc-tc-scatter-filter-chart',
					chartClass : 'filter-chart',
					chartDetails : {
						margins : {
							left : 110,
							top : 20,
							right : 40,
							bottom : 50
						},
						shapeClass : 'selector-rect',
						xAxisText : 'Triple count',
						xScale : 'log',
						xDataFunction : dataFunc.tripleCount,
						yAxisText : 'Outgoing link count',
						yScale : 'linear',
						yDataFunction : dataFunc.outgoingLinkCount
					}
				},
				{
					filterType : 'Triple count - Incoming link count',
					sectionId : 'ilc-tc-scatter-filter-view',
					sectionClass : 'filter-view',
					title : 'Triple count - Incoming link count',
					info : 'Displays scatter plot for triple count and incoming link counts for all datasets.',
					drag : false,
					collapse : false,
					d3Type : 'scatter',
					chartId : 'ilc-tc-scatter-filter-chart',
					chartClass : 'filter-chart',
					chartDetails : {
						margins : {
							left : 110,
							top : 20,
							right : 40,
							bottom : 50
						},
						shapeClass : 'selector-rect',
						xAxisText : 'Triple count',
						xScale : 'log',
						xDataFunction : dataFunc.tripleCount,
						yAxisText : 'Incoming link count',
						yScale : 'linear',
						yDataFunction : dataFunc.incomingLinkCount
					}
				}, {
					filterType : 'Last update date',
					sectionId : 'md-filter-view',
					sectionClass : 'filter-time-view',
					title : 'Last update date',
					info : 'Displays last update date for all datasets.',
					drag : false,
					collapse : false,
					d3Type : 'time',
					chartId : 'md-filter-chart',
					chartClass : 'filter-chart',
					chartDetails : {
						margins : {
							left : 90,
							top : 20,
							right : 30,
							bottom : 50
						},
						hasBrush : true,
						shapeClass : 'selector-rect',
						dataFunction : dataFunc.modificationDate
					}
				} ]
	};

	o.DetailSectionConfig = {
		parentId : 'details-section',
		charts : [
				{
					sectionId : 'tc-detail-view',
					sectionClass : 'details-view',
					title : 'Dataset - Triple count',
					info : 'Displays names of the datasets and their corresponding triple counts.',
					d3Type : 'bar',
					chartId : 'tc-detail-chart',
					chartClass : 'details-chart',
					chartDetails : {
						margins : {
							left : 110,
							top : 25,
							right : 20,
							bottom : 115
						},
						shapeClass : 'bar',
						nameClass : 'name',
						showName : true,
						xAxisText : 'Dataset',
						yAxisText : 'Triple count',
						yScale : 'log',
						dataFunction : dataFunc.tripleCount,
						delay : true,
						showLinks : true
					},
					magnify : true,
					alllinks : true
				},
				{
					sectionId : 'olc-detail-view',
					sectionClass : 'details-view',
					title : 'Dataset - Outgoing link count',
					info : 'Displays names of the datasets and the number of datasets that each dataset links to.',
					d3Type : 'bar',
					chartId : 'olc-detail-chart',
					chartClass : 'details-chart',
					chartDetails : {
						margins : {
							left : 60,
							top : 25,
							right : 20,
							bottom : 115
						},
						shapeClass : 'bar',
						nameClass : 'name',
						showName : true,
						xAxisText : 'Dataset',
						yAxisText : 'Outgoing link count',
						yScale : 'log',
						dataFunction : dataFunc.outgoingLinkCount,
						delay : true,
						showLinks : true
					},
					magnify : true,
					alllinks : true
				},
				{
					sectionId : 'ilc-detail-view',
					sectionClass : 'details-view',
					title : 'Dataset - Incoming link count',
					info : 'Displays names of the datasets and the number of datasets that link to each dataset.',
					d3Type : 'bar',
					chartId : 'ilc-detail-chart',
					chartClass : 'details-chart',
					chartDetails : {
						margins : {
							left : 60,
							top : 25,
							right : 20,
							bottom : 115
						},
						shapeClass : 'bar',
						nameClass : 'name',
						showName : true,
						xAxisText : 'Dataset',
						yAxisText : 'Incoming link count',
						yScale : 'log',
						dataFunction : dataFunc.incomingLinkCount,
						delay : true,
						showLinks : true
					},
					magnify : true,
					alllinks : true
				},
				{
					sectionId : 'st-matrix-detail-view',
					sectionClass : 'details-view',
					title : 'Source -> Target links',
					info : 'Displays directed links between the datasets. The rows correspond to the source dataset of the link and columns correspond to the target dataset of the link.',
					chartId : 'st-matrix-detail-chart',
					chartClass : 'details-chart',
					d3Type : 'matrix',
					chartDetails : {
						margins : {
							left : 20,
							top : 38,
							right : 20,
							bottom : 38
						},
						nameClass : 'name',
						color : 'aqua',
						delay : true,
						showLinks : true
					},
					magnify : true,
					alllinks : true
				},
				{
					sectionId : 'st-edge-detail-view',
					sectionClass : 'details-view',
					title : 'Source & Target links',
					info : 'Displays all links between the datasets.',
					d3Type : 'edge',
					chartId : 'st-edge-detail-chart',
					chartClass : 'details-chart',
					chartDetails : {
						nameClass : 'name',
						linkClass : 'edge-link',
						delay : true,
						showLinks : true
					},
					magnify : true,
					alllinks : true
				},
				{
					sectionId : 'date-detail-view',
					sectionClass : 'details-view-double-time',
					title : 'Dataset creation date - last update date',
					info : 'Displays creation date and last update date of the datasets.',
					d3Type : 'double-time',
					chartId : 'date-detail-chart',
					chartClass : 'details-chart-double-time',
					chartDetails : {
						margins : {
							left : 30,
							top : 10,
							right : 30,
							bottom : 40
						},
						shapeClass : 'rect',
						chartNameUpper : 'Creation date',
						chartNameLower : 'Last update date',
						dataFunctionUpper : dataFunc.creationDate,
						dataFunctionLower : dataFunc.modificationDate,
						delay : true,
						showLinks : true
					},
					magnify : true,
					alllinks : true
				} ]
	};

	o.MagnifySectionConfig = {
		parentId : 'overlay-container',
		charts : [ {
			sectionClass : 'magnified-view',
			title : 'Source -> Target links (includes all linked datasets)',
			info : 'Displays datasets with all their first level incoming and outgoing link datasets.',
			drag : false,
			collapse : false,
			d3Type : 'matrixwhole',
			chartId : 'overlay-content',
			chartClass : 'magnified-chart',
			chartDetails : {
				color : 'aqua'
			}
		} ]
	};

	o.AnalyticsSectionConfig = {
		parentId : 'overlay-container', // the container that will host all the
		// charts in this section
		charts : [ {
			sectionId : 'tc-analytics-view',
			sectionClass : 'analytics-view',
			title : 'Dataset - Triple count',
			info : 'Displays triple count of all datasets.',
			sortBy : 'Triple count',
			sortDesc : true,
			d3Type : 'bar',
			chartId : 'tc-analytics-chart',
			chartClass : 'analytics-chart',
			chartDetails : {
				margins : {
					left : 110,
					top : 20,
					right : 40,
					bottom : 50
				},
				shapeClass : 'analytics-bar',
				nameClass : 'name',
				showName : true,
				horizontalAlign : true,
				xAxisText : 'Dataset',
				yAxisText : 'Triple count',
				yScale : 'log',
				dataFunction : dataFunc.tripleCount
			}
		}, {
			sectionId : 'tlc-analytics-view',
			sectionClass : 'analytics-view',
			title : 'Dataset - Total link count',
			info : 'Displays total link count of all datasets.',
			sortBy : 'Total link count',
			sortDesc : true,
			d3Type : 'bar',
			chartId : 'tlc-analytics-chart',
			chartClass : 'analytics-chart',
			chartDetails : {
				margins : {
					left : 110,
					top : 20,
					right : 40,
					bottom : 50
				},
				shapeClass : 'analytics-bar',
				nameClass : 'name',
				showName : true,
				horizontalAlign : true,
				xAxisText : 'Dataset',
				yAxisText : 'Total link count',
				yScale : 'log',
				dataFunction : dataFunc.totalLinkCount
			}
		}, {
			sectionId : 'olc-analytics-view',
			sectionClass : 'analytics-view',
			title : 'Dataset - Outgoing link count',
			info : 'Displays outgoing link count of all datasets.',
			sortBy : 'Outgoing link count',
			sortDesc : true,
			d3Type : 'bar',
			chartId : 'olc-analytics-chart',
			chartClass : 'analytics-chart',
			chartDetails : {
				margins : {
					left : 110,
					top : 20,
					right : 40,
					bottom : 50
				},
				shapeClass : 'analytics-bar',
				nameClass : 'name',
				showName : true,
				horizontalAlign : true,
				xAxisText : 'Dataset',
				yAxisText : 'Outgoing link count',
				yScale : 'log',
				dataFunction : dataFunc.outgoingLinkCount
			}
		}, {
			sectionId : 'ilc-analytics-view',
			sectionClass : 'analytics-view',
			title : 'Dataset - Incoming link count',
			info : 'Displays incoming link count of all datasets.',
			sortBy : 'Incoming link count',
			sortDesc : true,
			d3Type : 'bar',
			chartId : 'ilc-analytics-chart',
			chartClass : 'analytics-chart',
			chartDetails : {
				margins : {
					left : 110,
					top : 20,
					right : 40,
					bottom : 50
				},
				shapeClass : 'analytics-bar',
				nameClass : 'name',
				showName : true,
				horizontalAlign : true,
				xAxisText : 'Dataset',
				yAxisText : 'Incoming link count',
				yScale : 'log',
				dataFunction : dataFunc.incomingLinkCount
			}
		}, {
			sectionId : 'cd-analytics-view',
			sectionClass : 'analytics-time-view',
			title : 'Dataset - Creation date',
			info : 'Displays creation date of all datasets.',
			sortBy : 'Creation date',
			sortDesc : false,
			d3Type : 'time',
			chartId : 'cd-analytics-chart',
			chartClass : 'analytics-chart',
			chartDetails : {
				margins : {
					left : 110,
					top : 20,
					right : 40,
					bottom : 50
				},
				shapeClass : 'analytics-square',
				nameClass : 'name',
				showName : true,
				horizontalAlign : true,
				dataFunction : dataFunc.creationDate
			}
		}, {
			sectionId : 'md-analytics-view',
			sectionClass : 'analytics-time-view',
			title : 'Dataset - Last update date',
			info : 'Displays last update date of all datasets.',
			sortBy : 'Last update date',
			sortDesc : false,
			d3Type : 'time',
			chartId : 'md-analytics-chart',
			chartClass : 'analytics-chart',
			chartDetails : {
				margins : {
					left : 110,
					top : 20,
					right : 40,
					bottom : 50
				},
				shapeClass : 'analytics-square',
				nameClass : 'name',
				showName : true,
				horizontalAlign : true,
				dataFunction : dataFunc.modificationDate
			}
		} ]
	};

	o.DatasetChartTabConfig = {
		parentId : 'dataset-chart-view', // the container that will host all
		// the
		// charts in this section
		charts : [
				{
					sectionId : 'tc-dataset-chart-view',
					sectionClass : 'analytics-view',
					title : 'Dataset - Triple count',
					info : 'Displays triple count of all datasets which link to this dataset and which this dataset links to.',
					sortBy : 'Triple count',
					sortDesc : true,
					d3Type : 'bar',
					chartId : 'tc-dataset-chart',
					chartClass : 'analytics-chart',
					chartDetails : {
						margins : {
							left : 110,
							top : 20,
							right : 40,
							bottom : 50
						},
						shapeClass : 'analytics-bar',
						nameClass : 'name',
						showName : true,
						horizontalAlign : true,
						xAxisText : 'Dataset',
						yAxisText : 'Triple count',
						yScale : 'log',
						dataFunction : dataFunc.tripleCount,
						showLinks : false
					}
				},
				{
					sectionId : 'tlc-dataset-chart-view',
					sectionClass : 'analytics-view',
					title : 'Dataset - Total link count',
					info : 'Displays total link count of all datasets which link to this dataset and which this dataset links to.',
					sortBy : 'Total link count',
					sortDesc : true,
					d3Type : 'bar',
					chartId : 'tlc-dataset-chart',
					chartClass : 'analytics-chart',
					chartDetails : {
						margins : {
							left : 110,
							top : 20,
							right : 40,
							bottom : 50
						},
						shapeClass : 'analytics-bar',
						nameClass : 'name',
						showName : true,
						horizontalAlign : true,
						xAxisText : 'Dataset',
						yAxisText : 'Total link count',
						yScale : 'log',
						dataFunction : dataFunc.totalLinkCount,
						showLinks : false
					}
				},
				{
					sectionId : 'olc-dataset-chart-view',
					sectionClass : 'analytics-view',
					title : 'Dataset - Outgoing link count',
					info : 'Displays outgoing link count of all datasets that this dataset links to.',
					sortBy : 'Outgoing link count',
					sortDesc : true,
					d3Type : 'bar',
					chartId : 'olc-dataset-chart',
					chartClass : 'analytics-chart',
					chartDetails : {
						margins : {
							left : 110,
							top : 20,
							right : 40,
							bottom : 50
						},
						shapeClass : 'analytics-bar',
						nameClass : 'name',
						showName : true,
						horizontalAlign : true,
						xAxisText : 'Dataset',
						yAxisText : 'Outgoing link count',
						yScale : 'log',
						dataFunction : dataFunc.outgoingLinkCount,
						showLinks : false
					}
				},
				{
					sectionId : 'ilc-dataset-chart-view',
					sectionClass : 'analytics-view',
					title : 'Dataset - Incoming link count',
					info : 'Displays incoming link count of all datasets that links to this dataset.',
					sortBy : 'Incoming link count',
					sortDesc : true,
					d3Type : 'bar',
					chartId : 'ilc-dataset-chart',
					chartClass : 'analytics-chart',
					chartDetails : {
						margins : {
							left : 110,
							top : 20,
							right : 40,
							bottom : 50
						},
						shapeClass : 'analytics-bar',
						nameClass : 'name',
						showName : true,
						horizontalAlign : true,
						xAxisText : 'Dataset',
						yAxisText : 'Incoming link count',
						yScale : 'log',
						dataFunction : dataFunc.incomingLinkCount,
						showLinks : false
					}
				},
				{
					sectionId : 'cd-dataset-chart-view',
					sectionClass : 'analytics-time-view',
					title : 'Dataset - Creation date',
					info : 'Displays creation date of all datasets which link to this dataset and which this dataset links to..',
					sortBy : 'Creation date',
					sortDesc : false,
					d3Type : 'time',
					chartId : 'cd-dataset-chart',
					chartClass : 'analytics-chart',
					chartDetails : {
						margins : {
							left : 110,
							top : 20,
							right : 40,
							bottom : 50
						},
						shapeClass : 'analytics-square',
						nameClass : 'name',
						showName : true,
						horizontalAlign : true,
						dataFunction : dataFunc.creationDate,
						showLinks : false
					}
				},
				{
					sectionId : 'md-dataset-chart-view',
					sectionClass : 'analytics-time-view',
					title : 'Dataset - Last update date',
					info : 'Displays last update date of all datasets which link to this dataset and which this dataset links to..',
					sortBy : 'Last update date',
					sortDesc : false,
					d3Type : 'time',
					chartId : 'md-dataset-chart',
					chartClass : 'analytics-chart',
					chartDetails : {
						margins : {
							left : 110,
							top : 20,
							right : 40,
							bottom : 50
						},
						shapeClass : 'analytics-square',
						nameClass : 'name',
						showName : true,
						horizontalAlign : true,
						dataFunction : dataFunc.modificationDate,
						showLinks : false
					}
				} ]
	};

	o.MagnifyConfig = {
		parentId : 'overlay-container', // the container that will host
		// the magnified chart
		sectionId : 'container-magnify-view',
		sectionClass : 'overlay-content-view',
		titleEnd : '',
		d3TypeEnd : '',
		chartId : 'magnify-chart',
		chartClass : 'overlay-content-chart',
		delay : false
	};

	o.AllLinksConfig = {
		parentId : 'overlay-container', // the container that will host
		// the all links chart
		sectionId : 'container-magnify-view',
		sectionClass : 'overlay-content-view',
		titleEnd : ' (Including all incoming and outgoing links for all datasets)',
		d3TypeEnd : 'whole',
		chartId : 'magnify-chart',
		chartClass : 'overlay-content-chart',
		delay : false
	};

})(lodApp.Config.View);
