package fr.inria.ilda.lodatlas.model;

public class Repo {
	
	private String repoName;
	
	private int numberOfDatasets;
	
	public Repo()  {
		
	}

	public String getRepoName() {
		return repoName;
	}

	public void setRepoName(String repoName) {
		this.repoName = repoName;
	}

	public int getNumberOfDatasets() {
		return numberOfDatasets;
	}

	public void setNumberOfDatasets(int numberOfDatasets) {
		this.numberOfDatasets = numberOfDatasets;
	}
	
	

}
