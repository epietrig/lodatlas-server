/*******************************************************************************
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.model.facet;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 
 * @see Facet
 * 
 * @author Hande Gözükan
 *
 */
public class FacetList {

	private final Map<String, FacetGroup> groups = new LinkedHashMap<String, FacetGroup>();

	public FacetList() {
	}

	/**
	 * Adds a facet group with the specified <code>category</code> to the facet
	 * group list.
	 * 
	 * @param category
	 *            category of the group to add.
	 */
	public void addFacetGroup(String category) {
		FacetGroup facetgroup = groups.get(category);
		if (facetgroup == null) {
			groups.put(category, new FacetGroup(category));
		}
	}

	/**
	 * Adds the specified <code>facet</code> to the corresponding facet group in the
	 * facet group list. If no group exists for this <code>facet</code>, creates a
	 * new facet group.
	 * 
	 * @param category
	 *            category of the specified <code>facet</code>.
	 * @param facet
	 *            facet to add to facet group list.
	 * @return <code>true</code> if the specified <code>facet</code> is successfully
	 *         added to the facet group; <code>false</code> otherwise.
	 */
	public boolean addFacet(String category, Facet facet) {
		FacetGroup facetgroup = groups.get(category);
		if (facetgroup == null) {
			facetgroup = new FacetGroup(category);
			groups.put(category, facetgroup);
		}
		return facetgroup.addFacet(facet);
	}

	/**
	 * Returns the list of all facet groups.
	 * 
	 * @return the list of all facet groups containing facets.
	 */
	public Collection<FacetGroup> getGroups() {
		return groups.values();
	}

}
