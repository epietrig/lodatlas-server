/*******************************************************************************
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.service;

import java.util.List;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.JsonbConfig;

import org.apache.log4j.Logger;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;

import fr.inria.ilda.lodatlas.dao.DaoFactory;
import fr.inria.ilda.lodatlas.dao.SearchDao;
import fr.inria.ilda.lodatlas.model.HitList;
import fr.inria.ilda.lodatlas.model.SearchResult;
import fr.inria.ilda.lodatlas.model.Void;
import fr.inria.ilda.lodatlas.model.dataset.DatasetBase;
import fr.inria.ilda.lodatlas.model.dataset.DatasetBaseDeserializer;
import fr.inria.ilda.lodatlas.model.dataset.DatasetLinks;
import fr.inria.ilda.lodatlas.model.dataset.DatasetLinksDeserializer;
import fr.inria.ilda.lodatlas.model.facet.Facet;
import fr.inria.ilda.lodatlas.model.facet.FacetCategory;
import fr.inria.ilda.lodatlas.model.facet.FacetGroup;
import fr.inria.ilda.lodatlas.model.facet.FacetList;

/**
 * A class to implement business logic for Elasticsearch index queries.
 * 
 * @author Hande Gözükan
 *
 */
public class SearchService {

	private static final Logger logger = Logger.getLogger(SearchService.class);

	private SearchDao searchDao = DaoFactory.getSearchDao();

	public SearchService() {
	}

	/**
	 * Searches the datasets using the specified query parameters and returns all
	 * the facet lists with corresponding number of datasets and short format
	 * dataset metadata for the resultset.
	 * 
	 * @param repo
	 *            list of repositories selected from "Repositories" facet.
	 * @param rdfsum
	 *            must be set to "RDF quotients available" if only datasets with
	 *            RDFQuotients are required.
	 * @param org
	 *            list of names of the organizations from "Organizations" facet.
	 * @param tag
	 *            list of tags from "Tags" facet.
	 * @param format
	 *            list of formats from "Formats" facet.
	 * @param license
	 *            list of licenses from "Licenses" facet.
	 * @param props
	 *            list of ontology property URIs from "Properties" facet.
	 * @param classes
	 *            list of ontology class URIs from "Classes" facet.
	 * @param vocabs
	 *            list of ontology URIs from "Vocabularies" facet.
	 * @param fields
	 *            the list of queries to execute in selected fields among "name",
	 *            "title", "description", "classes", "properties", "vocabularies".
	 *            // TODO specify format of the queries
	 * @param isAnd
	 *            boolean value to indicate if the queries in fields list will be
	 *            combined with AND or OR.
	 * @param showMore
	 *            facet category names under which to display list of all facets
	 *            instead of displaying the first 10.
	 * @param from
	 *            index to start from to get the list of datasets.
	 * @param number
	 *            of datasets to fetch starting from <code>from</code> index.
	 * @return facet lists with corresponding number of datasets and short format
	 *         dataset metadata for the resultset.
	 */
	public SearchResult<DatasetBase> getFacets(List<String> repo, List<String> rdfsum, List<String> org,
			List<String> tag, List<String> format, List<String> license, List<String> props, List<String> classes,
			List<String> vocabs, List<String> fields, boolean isAnd, List<String> showMore, int from, int size) {

		logger.info("called dao is " + searchDao);

		// Query Elastic search indexes
		SearchResponse sr = searchDao.getFacets(repo, rdfsum, org, tag, format, license, props, classes, vocabs, fields,
				isAnd, showMore, from, size);

		// Initialize empty facet list
		FacetList facetList = new FacetList();
		for (FacetCategory facetCat : FacetCategory.values()) {
			facetList.addFacetGroup(facetCat.aggName());
		}

		logger.info("Populating facet lists");
		// get aggregations from search results
		Aggregations aggrs = sr.getAggregations();

		// populate facet list with the values from search results
		populateFacetList(aggrs, FacetCategory.REPOSITORY.aggName(), repo, facetList);
		populateFacetList(aggrs, FacetCategory.RDFSUM.aggName(), rdfsum, facetList);
		populateFacetList(aggrs, FacetCategory.ORGANIZATION.aggName(), org, facetList);
		populateFacetList(aggrs, FacetCategory.TAG.aggName(), tag, facetList);
		populateFacetList(aggrs, FacetCategory.FORMAT.aggName(), format, facetList);
		populateFacetList(aggrs, FacetCategory.LICENSE.aggName(), license, facetList);
		populateFacetList(aggrs, FacetCategory.PROPERTIES.aggName(), props, facetList);
		populateFacetList(aggrs, FacetCategory.CLASSES.aggName(), classes, facetList);
		populateFacetList(aggrs, FacetCategory.VOCABULARIES.aggName(), vocabs, facetList);

		logger.info("Generating hitlist");
		// populate hit list with sources
		HitList<DatasetBase> hitList = new HitList<DatasetBase>(sr.getHits().getTotalHits());
		Jsonb jsonb = JsonbBuilder.create(new JsonbConfig().withDeserializers(new DatasetBaseDeserializer()));

		for (SearchHit hit : sr.getHits()) {
			hitList.addHit(jsonb.fromJson(hit.getSourceAsString(), DatasetBase.class));
		}
		try {
			jsonb.close();
		} catch (Exception e) {
			logger.error("Exception while trying to close jsonb", e);
		}

		return new SearchResult<DatasetBase>(facetList, hitList);
	}

	/**
	 * Searches the datasets using the specified query parameters and returns only
	 * the facet lists with counts for the facet category specified by
	 * <code>show</code> parameter. If <code>isShow</code> is <code>true</code> all
	 * list of facets is returned; otherwise only top 10 facets are returned.
	 * 
	 * @param repo
	 *            list of repositories selected from "Repositories" facets.
	 * @param rdfsum
	 *            must be set to "RDF quotients available" if only datasets with
	 *            RDFQuotients are required.
	 * @param org
	 *            list of names of the organizations from "Organizations" facets.
	 * @param tag
	 *            list of tags from "Tags" facets.
	 * @param format
	 *            list of formats from "Formats" facets.
	 * @param license
	 *            list of licenses from "Licenses" facets.
	 * @param props
	 *            list of ontology property URIs from "Properties" facets.
	 * @param classes
	 *            list of ontology class URIs from "Classes" facets.
	 * @param vocabs
	 *            list of ontology URIs from "Vocabularies" facets.
	 * @param fields
	 *            the list of queries to execute in selected fields among "name",
	 *            "title", "description", "classes", "properties", "vocabularies".
	 *            // TODO specify format of the queries
	 * @param isAnd
	 *            boolean value to indicate if the queries in fields list will be
	 *            combined with AND or OR. O
	 * @param categoryName
	 *            facet category name to show more/less. This field should be
	 *            specified.
	 * @param isShow
	 *            <code>true</code> to show all facets; <code>false</code> to show
	 *            only first 10 facet in the required facet category.
	 * 
	 * @return only the facet lists with counts for the facet category specified by
	 *         <code>show</code> parameter for the datasets satisfying the specified
	 *         query parameters. If <code>isShow</code> is <code>true</code> all
	 *         list of facets is returned; otherwise only top 10 facets are
	 *         returned.
	 * 
	 */
	public FacetGroup getSingleFacet(List<String> repo, List<String> rdfsum, List<String> org, List<String> tag,
			List<String> format, List<String> license, List<String> props, List<String> classes, List<String> vocabs,
			List<String> fields, boolean isAnd, String categoryName, boolean isShow) {
		FacetList facetList = new FacetList();

		// Query Elastic search indexes
		SearchResponse sr = searchDao.getSingleFacet(repo, rdfsum, org, tag, format, license, props, classes, vocabs,
				fields, isAnd, categoryName, isShow);
		if (sr != null) {
			// Initialize empty facet list
			for (FacetCategory facetCat : FacetCategory.values()) {
				facetList.addFacetGroup(facetCat.aggName());
			}

			// get aggregations
			Aggregations aggrs = sr.getAggregations();

			// populate facet list with the values from search results
			if (FacetCategory.REPOSITORY.aggName().equals(categoryName)) {
				populateFacetList(aggrs, FacetCategory.REPOSITORY.aggName(), repo, facetList);
			} else if (FacetCategory.RDFSUM.aggName().equals(categoryName)) {
				populateFacetList(aggrs, FacetCategory.RDFSUM.aggName(), rdfsum, facetList);
			} else if (FacetCategory.ORGANIZATION.aggName().equals(categoryName)) {
				populateFacetList(aggrs, FacetCategory.ORGANIZATION.aggName(), org, facetList);
			} else if (FacetCategory.TAG.aggName().equals(categoryName)) {
				populateFacetList(aggrs, FacetCategory.TAG.aggName(), tag, facetList);
			} else if (FacetCategory.FORMAT.aggName().equals(categoryName)) {
				populateFacetList(aggrs, FacetCategory.FORMAT.aggName(), format, facetList);
			} else if (FacetCategory.LICENSE.aggName().equals(categoryName)) {
				populateFacetList(aggrs, FacetCategory.LICENSE.aggName(), license, facetList);
			} else if (FacetCategory.PROPERTIES.aggName().equals(categoryName)) {
				populateFacetList(aggrs, FacetCategory.PROPERTIES.aggName(), props, facetList);
			} else if (FacetCategory.CLASSES.aggName().equals(categoryName)) {
				populateFacetList(aggrs, FacetCategory.CLASSES.aggName(), classes, facetList);
			} else if (FacetCategory.VOCABULARIES.aggName().equals(categoryName)) {
				populateFacetList(aggrs, FacetCategory.VOCABULARIES.aggName(), vocabs, facetList);
			}

			FacetGroup grp = null;

			for (FacetGroup g : facetList.getGroups()) {
				if (g.getCategory().equals(categoryName))
					grp = g;
			}
			return grp;
		} else {
			return null;
		}
	}

	/**
	 * 
	 * @param aggrs
	 * @param repoName
	 * @param catFacets
	 * @param facetList
	 */
	private void populateFacetList(Aggregations aggrs, String repoName, List<String> catFacets, FacetList facetList) {
		Terms terms = aggrs.get(repoName);
		for (Terms.Bucket entry : terms.getBuckets()) {
			String selection = entry.getKeyAsString();
			Facet f = new Facet(selection, entry.getDocCount(), catFacets.contains(selection));
			facetList.addFacet(repoName, f);
		}
	}

	/**
	 * Gets long format data, including incoming and outgoing links, for the
	 * datasets in the <code>ids</code> list.<br>
	 * 
	 * The returned fields for each dataset under hits array are as follows:
	 * 
	 * <pre>
	 * rN: repository name that the dataset belongs to
	 * name: dataset name
	 * olC: number of datasets that this dataset links to, extracted from the links specified by the repository info
	 * outgoingLinks: list of outgoing links as 
	 *   name: dataset name
	 *   count: link count
	 * ilC: number of datasets that links to this dataset, extracted from the links specified by the repository info
	 * incomingLinks: list of incoming links as 
	 *   name: dataset name
	 *   count: link count
	 * tc: triple count of the dataset, specified by the repository info
	 * cd: creation date of the dataset
	 * md: modification date of the dataset
	 * </pre>
	 * 
	 * @param ids
	 *            the list of datasets, in the form <em>repoName;datasetName</em>
	 * @param from
	 *            index to start from to get the list of datasets.
	 * @param size
	 *            number of datasets to fetch starting from <code>from</code> index.
	 * @return
	 */
	public HitList<DatasetLinks> getDatasetLinks(List<String> ids, int from, int size) {
		SearchResponse sr = searchDao.getDatasetLinks(ids, from, size);

		if (sr != null) {
			logger.info("Constructing HitList");

			HitList<DatasetLinks> hitList = new HitList<DatasetLinks>(sr.getHits().getTotalHits());

			Jsonb jsonb = JsonbBuilder.create(new JsonbConfig().withDeserializers(new DatasetLinksDeserializer()));

			for (SearchHit hit : sr.getHits()) {
				// logger.info(hit.getSourceAsString());
				hitList.addHit(jsonb.fromJson(hit.getSourceAsString(), DatasetLinks.class));
			}

			try {
				jsonb.close();
			} catch (Exception e) {
				logger.error("Exception while trying to close jsonb", e);
			}

			return hitList;
		} else {
			return null;
		}
	}

	/**
	 * Gets concatenated void files as plain text for the datasets specified by
	 * <code>ids</code> list.
	 * 
	 * @param ids
	 *            the list of datasets, in the form <em>repoName;datasetName</em>
	 * @param from
	 *            index to start from to get the list of datasets.
	 * @param size
	 *            number of datasets to fetch starting from <code>from</code> index.
	 * @return concatenated void files as plain text for the datasets specified by
	 *         <code>ids</code> list.
	 */
	public String getVoid(List<String> ids, int from, int size) {
		logger.debug("Constructing void string from hitlist");

		SearchHits hits = searchDao.getVoid(ids, from, size).getHits();

		StringBuilder stringBuilder = new StringBuilder();
		Void voidInfo = null;

		for (SearchHit hit : hits) {
			voidInfo = new Void(hit.getSourceAsString());
			stringBuilder.append(voidInfo.toString());
		}

		return stringBuilder.toString();
	}

}
