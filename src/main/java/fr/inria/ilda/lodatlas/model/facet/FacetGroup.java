/*******************************************************************************
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.model.facet;

import java.util.LinkedList;
import java.util.List;

/**
 * A model class for a specific category of facets.
 * 
 * @see Facet
 * 
 * @author Hande Gözükan
 *
 */
public class FacetGroup {

	private final String category;

	private final List<Facet> list = new LinkedList<>();

	/**
	 * Constructs a {@link FacetGroup} instance for the specified
	 * <code>category</code> with an empty set of Facets.
	 * 
	 * @param category
	 *            the category of the {@link FacetGroup} instance.
	 */
	public FacetGroup(String category) {
		assert (category != null || !"".equals(category));

		this.category = category;
	}

	/**
	 * Getter method for the category of this facet group.
	 * 
	 * @return the category the category of this facet group.
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * Adds the specified <code>facet</code> to set of facets in this facet group.
	 * 
	 * @param facet
	 *            the facet to be added.
	 * @return <code>true</code> if the specified <code>facet</code> is added;
	 *         <code>false</code> otherwise.
	 */
	public boolean addFacet(Facet facet) {
		return list.add(facet);
	}

	/**
	 * Deletes all the facets in this facet group.
	 */
	public void clear() {
		list.clear();
	}

	public List<Facet> getList() {
		return list;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FacetGroup [category=" + category + ", list=" + list + "]";
	}

}
