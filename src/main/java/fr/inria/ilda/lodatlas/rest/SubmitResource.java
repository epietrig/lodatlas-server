/*******************************************************************************
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.rest;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.json.JsonObject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import fr.inria.ilda.lodatlas.service.SubmitService;

@Path("/submit")
@RequestScoped
@Consumes(MediaType.APPLICATION_JSON)
public class SubmitResource {

	private static final Logger logger = Logger.getLogger(SearchResource.class);

	@Inject
	SubmitService submitService;

	public SubmitResource() {
	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.TEXT_PLAIN })
	@Path("/dataset")
	public Response postMessage(String datasetJson) throws Exception {

		logger.info(datasetJson);
		if (submitService.saveDataset(datasetJson)) {
			logger.info("The dataset json is written to database.");
			return Response.ok().build();
		}
		return Response.notModified().build();
	}

	@POST
	@Consumes({ MediaType.TEXT_PLAIN })
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/void")
	public Response postVoid(String voidLink) {
		logger.info(voidLink);

		JsonObject datasetJSon = submitService.getDatasetJson(voidLink);
		logger.info(datasetJSon);
		if (datasetJSon == null) {
			return Response.serverError().build();
		}
		return Response.ok(datasetJSon.toString(), MediaType.APPLICATION_JSON).build();
	}

}
