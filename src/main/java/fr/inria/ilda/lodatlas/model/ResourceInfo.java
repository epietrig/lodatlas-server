/*******************************************************************************
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.model;

/**
 * A class that represents a Resource in a CKAN catalog.
 * 
 * @author Hande Gözükan
 *
 */
public class ResourceInfo {

	private final String resourceId;

	private final String name;

	private final String description;

	private final String format;

	private final String contentType;

	private final String url;

	private final String state;

	private final String lastUpdate;

	private final String verifiedFormat;

	private final String downloadError;

	private final boolean canDownload;

	private final String fileName;

	public ResourceInfo(String resourceId, String name, String description, String format, String contentType,
			String url, String state, String lastUpdate, String verifiedFormat, boolean canDownload,
			String downloadError, String fileName) {
		this.resourceId = resourceId;
		this.name = name;
		this.description = description;
		this.format = format;
		this.contentType = contentType;
		this.url = url;
		this.state = state;
		this.lastUpdate = lastUpdate;
		this.verifiedFormat = verifiedFormat;
		this.canDownload = canDownload;
		this.fileName = fileName;
		this.downloadError = downloadError;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the resourceId
	 */
	public String getResourceId() {
		return resourceId;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return the format
	 */
	public String getFormat() {
		return format;
	}

	/**
	 * @return the mimeType
	 */
	public String getContentType() {
		return contentType;
	}

	public String getDumpFileName() {
		return fileName;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @return the lastUpdate
	 */
	public String getLastUpdate() {
		return lastUpdate;
	}

	public String getVerifiedFormat() {
		return verifiedFormat;
	}

	public String getDownloadError() {
		return downloadError;
	}

	public boolean isCanDownload() {
		return canDownload;
	}

}
