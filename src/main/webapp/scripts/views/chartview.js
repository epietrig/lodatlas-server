/*
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
'use strict';

var lodApp = lodApp || {};

lodApp.View = lodApp.View || {};

(function(o) {
	/**
	 * This is used only for initializing chart frames from template.
	 * 
	 */
	o.ChartView = {
		/**
		 * Initializes an instance of 'chart-view' template with the specified
		 * as a child of the specified 'parent' with the attributes specified by
		 * 'chartItem' properties.
		 */
		createChartContainer : function(parent, chartItem, parentView) {
			// create instance of chart-view template
			var chartViewTemplate = document.querySelector('template#chart-view').content;

			// import chart instance to document
			var chartClone = document.importNode(chartViewTemplate, true);

			// modify chart attributes
			// section
			var section = chartClone.querySelector('section');
			section.id = chartItem.sectionId;
			section.className = chartItem.sectionClass;

			// chart-view-menu
			section.querySelector('#chart-view-title').textContent = chartItem.title;
			section.querySelector('#info-icon').setAttribute('title',
					chartItem.info);
			// chart
			var chart = section.querySelector('#chart');
			chart.id = chartItem.chartId;
			chart.className = chartItem.chartClass;
			
			// set event handlers

			// add drag even if chart is draggable
			if (chartItem.drag !== false) {
				section.addEventListener('dragstart',
						this.chartDragStart);
				section.addEventListener('drop', this.chartDrop);
				section.addEventListener('dragover',
						this.chartDragOver);
			} else {
				section.draggable = false;
				section.querySelector('.chart-view-menu').style.cursor = 'default';
			}

			// add collapse event if chart is collapsible
			if (chartItem.collapse !== false) {
				section.querySelector('#collapse-icon').addEventListener(
						'click', this.collapseView);
				section.querySelector('#expand-icon').addEventListener(
						'click', this.expandView);
			} else {
				// do not show collapse button if not collapsible
				section.querySelector('#collapse-icon').style.display = 'none';
			}

			// if chart is magnifiable, show magnify icon
			if (chartItem.magnify) {
				var magnifyIcon = section.querySelector('#magnify-icon');
				magnifyIcon.style.display = 'inline-block';
				magnifyIcon.addEventListener('click', function(event) {
					parentView.magnify(event, chartItem);
				});
			}
			
			// if chart can show all links, show all links icon
			if (chartItem.alllinks) {
				var allLinksIcon = section.querySelector('#all-links-icon');
				allLinksIcon.style.display = 'inline-block';
				allLinksIcon.addEventListener('click', function(event) {
					parentView.allLinks(event, chartItem);
				});
			}

			// append chart-view to parent elements content
			parent.querySelector('.section-content').appendChild(chartClone);
		},
		createSectionContainer : function(parent, chartItem) {
			// add section tag to the document
			var section = document.createElement('section');
			section.id = chartItem.sectionId;
			section.className = chartItem.sectionClass;
			
			// add div tag for chart to the document
			var chart = document.createElement('div');
			chart.id = chartItem.chartId;
			chart.className = chartItem.chartClass;
			section.appendChild(chart);
			
			// append section to specified parent element
			parent.appendChild(section);
		},
		/**
		 * Collapses the chart view when collapse icon is clicked and changes
		 * the location of collapsed chart as the last child of its parent node.
		 */
		collapseView : function(event) {
			var target = event.target;
			var sectionElement = target.parentNode.parentNode.parentNode;
			// minimizes the current view
			sectionElement.className += ' chart-view-collapsed';
			// hides the chart view
			var chartElement = target.parentNode.parentNode.nextElementSibling;
			chartElement.style.display = 'none';
			// hides the collapse icon
			target.style.display = 'none';

			// removes the element from its location in parent view and appends
			// it as
			// the last child
			sectionElement.parentNode.appendChild(sectionElement);

			// displays the expand icon
			target.nextElementSibling.style.display = 'inline-block';
		},
		/**
		 * Expands the chart view when expand icon is clicked and changes the
		 * location of expanded chart as the first child of its parent node.
		 */
		expandView : function(event) {
			var target = event.target;
			var sectionElement = target.parentNode.parentNode.parentNode;
			// shows the current view
			sectionElement.className = sectionElement.className.replace(
					' chart-view-collapsed', '');

			// shows the chart view
			var chartElement = target.parentNode.parentNode.nextElementSibling;
			chartElement.style.display = 'block';
			// hides the expand icon
			target.style.display = 'none';

			// removes the element from its location in parent view and appends
			// it as
			// the first child.
			var parentElement = sectionElement.parentNode;
			parentElement.insertBefore(sectionElement,
					parentElement.firstElementChild);

			// displays the collapse icon
			target.previousElementSibling.style.display = 'inline-block';
		},
		/**
		 * Sets the id of the chart that is dragged when drag starts.
		 */
		chartDragStart : function(event) {
//			console.log('---drag target ' + event.target.id);
			event.dataTransfer.setData('text', event.target.id);
		},
		chartDragOver : function(event) {
//			console.log('chart drag over ' + event.target.id);
			event.preventDefault();
		},
		/**
		 * Drops and inserts the dragged element before the element that it is
		 * over.
		 */
		chartDrop : function(event) {
			event.preventDefault();
			// console.log('chart dropped');
			var data = event.dataTransfer.getData('text');
//			console.log('data ' + data);
			var elementToMove = document.getElementById(data);
			var element;
			var targetElement = event.target;

			while (targetElement.id === '') {
				targetElement = targetElement.parentNode;
			}
			targetElement = targetElement.parentNode;
			var targetParent = targetElement.parentNode;

			// console.log("target id " + targetElement.id);
			// console.log("target parent id " + targetParent.id);

			// element to insert
			element = document.getElementById(targetElement.id);
			while (!element.getAttribute('draggable')) {
				element = element.parentElement;
			}
			// console.log('element to move ' + elementToMove.id);
			// console.log('insert before ' + element.id);
			var parent = elementToMove.parentNode;
			// console.log(parent);
			// move if the dragged element has the same parent with the
			// dropped element
			if (targetParent === parent)
				parent.insertBefore(elementToMove, element);
		}
	};
})(lodApp.View);
