/*
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
'use strict';

var lodApp = lodApp || {};

lodApp.Config = lodApp.Config || {};

(function(o) {
	/**
	 * This is the list and order of facets to be displayed on the screen.
	 */
	o.FacetCategories = [ {
		name : 'Repositories',
		short : 'repo'
	}, {
		name : 'RDFQuotients',
		short : 'rdfsum'
	}, {
		name : 'Organizations',
		short : 'org'
	}, {
		name : 'Tags',
		short : 'tag'
	}, {
		name : 'Formats',
		short : 'format'
	}, {
		name : 'Licenses',
		short : 'license'
	}, {
		name : 'Properties',
		short : 'props'
	}, {
		name : 'Classes',
		short : 'classes'
	}, {
		name : 'Vocabularies',
		short : 'vocabs'
	} ];
	
	o.RestApi = {
		url : '/lodatlas/rest/',
		contentType : 'application/json; charset=utf-8',
		dataType : 'json',
		search : 'search',
		facet : 'search/facet',
		datasetlinks: 'search/datasetlinks',
		void : 'search/void',
		dataset: function(repoName, datasetName) {
			return 'repos/' + encodeURIComponent(repoName) + "/datasets/" + encodeURIComponent(datasetName);
		},
		linkdetails: function(repoName, datasetName) {
			return 'repos/' + encodeURIComponent(repoName) + "/datasets/" + encodeURIComponent(datasetName) + "/linkdetails";
		},
		resourcelist: function(repoName, datasetName) {
			return 'repos/' + encodeURIComponent(repoName) + "/datasets/" + encodeURIComponent(datasetName) + "/resourcesshort";
		},
		rdfquotient: function(repoName, datasetName, resourceId, dumpName) {
			return 'repos/' + encodeURIComponent(repoName) + "/datasets/" + encodeURIComponent(datasetName) + "/resources/" + encodeURIComponent(resourceId) + "/rdfquotients" +"/" + encodeURIComponent(dumpName);
		},
		vocabulary:function(repoName, datasetName, resourceId, dumpName) {
			return 'repos/' + encodeURIComponent(repoName) + "/datasets/" + encodeURIComponent(datasetName) + "/resources/" + encodeURIComponent(resourceId) + "/vocabularies" +"/" + encodeURIComponent(dumpName);
		},
		repo : 'repos'
	};

})(lodApp.Config);
