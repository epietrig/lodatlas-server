# LODAtlas Server
LODAtlas server serves the content processed and indexed by [LODAtlas datamanager](https://gitlab.inria.fr/epietrig/lodatlas-datamanager) module through a web interface and REST API. 

The web interface lets users to make keyword and URI search on linked data datasets' metadata and contents combined with faceted navigation. It also provides interactive visualisations.

# Dependencies
* [lodatlas-commons](https://gitlab.inria.fr/epietrig/lodatlas-commons)
* MongoDB 3.2.2
* Elasticsearch Server 6.2.2
* Java 1.8
* Apache Tomcat Server 8.0.28

# Docker 

# License
LODAtlas Server is licensed under [GNU General Public License version 3](https://www.gnu.org/licenses/gpl-3.0.en.html)