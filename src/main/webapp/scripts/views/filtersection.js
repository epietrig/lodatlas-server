/*
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
'use strict';

/**
 * FilterSection view represents the "Plot and Filter" part in the main user
 * interface.
 */
var lodApp = lodApp || {};

lodApp.View = lodApp.View || {};

(function(o) {
	o.FilterSection = {
		getInstance : function(controller_) {

			/**
			 * All possible filter-charts listed as array in
			 * FilterSectionConfig. The fields for configuration is as follows:<br>
			 * 
			 * parentId: the Id of the parent (main-section) element to append
			 * the filter charts.<br>
			 * charts: the array of charts that can be used for filtering
			 * datasets. Each chart is an instance of 'chart-view' template. The
			 * template contains a 'section' element which comprises of two
			 * 'div' elements with ids 'chart-view-menu' and 'chart'. <br>
			 * 
			 * The fields to configure each chart-view instance are as follows:<br>
			 * 
			 * filterType: the text that will appear in the combo box to list
			 * filter types.<br>
			 * sectionId: Id for the section element of this chart-view.<br>
			 * sectionClass: CSS class for the section element.<br>
			 * title: title to set to 'chart-view-title' element of this
			 * chart-view.<br>
			 * info: explanation about this chart to set to info-icon to view as
			 * title when hovered.<br>
			 * drag: 'true' if this section is draggable; 'false' otherwise.<br>
			 * collapse: 'true' if this section is collapsable; 'false'
			 * otherwise.<br>
			 * chartId: Id for the 'chart' element of this chart-view.<br>
			 * chartClass: CSS class for the 'chart' element.<br>
			 * d3Type: type of the D3 chart. <br>
			 * 
			 * The fields to configure d3 chart are as follows:<br>
			 * chartDetails:<br>
			 * margins: margins for the chart <br>
			 * shapeClass: CSS class for the shapes that appear on the chart.
			 * <br>
			 * xAxisText: text that will appear as x-axis title. <br>
			 * xScale: scale type for x-axis. <br>
			 * xDataFunction: data function for x-axis. <br>
			 * yAxisText: text that will appear as y-axis title. <br>
			 * yScale: scale type for y-axis. <br>
			 * yDataFunction: data function for y-axis.
			 */
			var chartConfig = viewConfig.FilterSectionConfig;

			/**
			 * FilterSectionView displays all charts that can be used for filtering the
			 * list of datasets coming from search results.<br>
			 * 
			 * Initially FilterSectionView contains no charts but an option list in a
			 * combo-box to select the desired filter chart. Each added chart is
			 * appended to the end of the filter view.<br>
			 */
			var FilterSectionView = function(controller) {
				// search controller
				this.controller = controller;
				this.container = document.getElementById(chartConfig.parentId);
				this.filterElements = {};
				this.filterListener = lodApp.Controller.ChartListener
						.getListenerInstance(this.controller, true);
				this.currentFilterCount = 0;
				this.init();
			};

			FilterSectionView.constructor = FilterSectionView;

			/**
			 * Initializes the filter list in the combo-box with available
			 * filter types specified in 'chartConfig.charts[i].filterType'
			 * field and also initializes add filter/remove filter events.
			 */
			FilterSectionView.prototype.init = function() {
				var charts = chartConfig.charts;

				// initialize combo-box
				this.filterCombo = this.container.querySelector('#filterlist');

				var option, chart;

				for (var i = 0; i < charts.length; i++) {
					chart = charts[i];
					option = document.createElement('option');
					option.text = chart.filterType;
					option.value = i;
					this.filterCombo.add(option);
				}

				var that = this;

				// remove filter event
				var removeFilter = function(event) {
					// get section element(filter chart view) which contains
					// this remove button
					var sectionElement = event.target.parentNode.parentNode.parentNode;

					// get chartId
					var chartId = sectionElement.querySelector('.filter-chart').id;
					var filterToRemove = that.filterElements[chartId];
					that.removeFilter(sectionElement, filterToRemove, chartId);

					// update combo to enable selection of the filter
					var options = that.filterCombo.options;
					for ( var item in options) {
						var option = options[item];
						if (option.text === filterToRemove.filterType) {
							option.disabled = false;
						}
					}
				};

				// add add filter event
				var addFilter = function(event) {
					var index = that.filterCombo.selectedIndex;
					var option = that.filterCombo.options[index];
					if (!option.disabled) {
						// increment number of filters in the filter view.
						that.currentFilterCount++;
						// disable selected option in combo-box.
						option.disabled = true;

						var chart = charts[index];

						// create filter-chart container and add to filter view
						lodApp.View.ChartView.createChartContainer(
								that.container, chart);

						// enable remove filter event for the filter view
						var removeButton = that.container.querySelector(
								'#' + chart.sectionId).querySelector(
								'#remove-icon');
						removeButton.style.display = 'inline-block';
						removeButton.addEventListener('click', removeFilter);

						// add filter element to filter list
						that.filterElements[chart.chartId] = {
							// index of the added filter in filter list
							index : that.currentFilterCount,
							data : that.filteredData,
							chart : lodApp.d3.getChartFactory().getChart(that,
									chart, that.filterListener),
							filterType : chart.filterType
						};

						// set last-filtered data to view on the added chart.
						that.filterElements[chart.chartId].chart
								.setData(that.filteredData);

						// // add the created chart to filter listener, this
						// must
						// // be called after set data, so that any hover, click
						// or
						// // double click state in other charts can be applied
						// to
						// // this chart as well.
						// that.filterListener.addChartElement(chart.chartId,
						// that.filterElements[chart.chartId].chart);
					}
				};

				// enable add filter event for the filter view.
				var addButton = this.container.querySelector('#add-filter');
				addButton.addEventListener('click', addFilter);
			};

			/**
			 * Removes the filter which has specified 'sectionElement',
			 * 'filterToRemove' and 'chartId' and updates the data viewed in the
			 * filters after the filter to remove with the data filtered by the
			 * filter prior to the filter to remove. If the removed filter is
			 * the first one, the following filters are populated with search
			 * results.
			 * 
			 * @param sectionElement
			 *            the DOM element that contains the filter to remove.
			 * @param filterToRemove
			 *            filter element to remove.
			 * @param chartId
			 *            Id of the filter chart to remove.
			 */
			FilterSectionView.prototype.removeFilter = function(sectionElement,
					filterToRemove, chartId) {
				var index = filterToRemove.index;

				// remove DOM element for the filter to remove.
				this.container.querySelector('.section-content').removeChild(
						sectionElement);

				// remove filter element from filter list.
				delete this.filterElements[chartId];
				this.filterListener.removeChartElement(chartId);
				this.currentFilterCount--;

				// update indexes of filters after this filter
				for ( var item in this.filterElements) {
					var element = this.filterElements[item];
					if (element.index > index) {
						element.index--;
					}
				}

				// render data of this filter in the filters after this filter.
				// index - 1, because render updates the data after the
				// specified index.
				this.render(filterToRemove.data, index - 1);
			};

			/**
			 * Renders the specified 'resultset' in filter views after the
			 * specified index 'i' and informs the controller that the filtered
			 * data has changed.
			 * 
			 * @param resultset
			 *            the list of datasets.
			 * @param i
			 *            the index of the filter view in the filterElements of
			 *            this filter group view after which the data of the
			 *            filter views must be updated.
			 */
			FilterSectionView.prototype.render = function(resultset, i) {
				if (i === undefined) {
					i = -1;
				}
				this.filteredData = resultset;
				// console.log(resultset);
				// console.log('index ' + i);
				for ( var item in this.filterElements) {
					var element = this.filterElements[item];
					if (element.index > i) {
						element.data = resultset;
						element.chart.setData(resultset);
					}
				}
				this.controller.renderFilterResults(resultset);
			};

			/**
			 * Hovers the specified dataset and fades out all others in all of
			 * the filter charts.
			 * 
			 * @param dataset
			 *            dataset to hover.
			 */
			FilterSectionView.prototype.hover = function(dataset) {
				// console.log('hover');
				for ( var item in this.filterElements) {
					this.filterElements[item].chart
							.selectElement(dataset, true);
				}
			};

			/**
			 * Clears any hover on all of the filter charts.
			 */
			FilterSectionView.prototype.clearHover = function() {
				for ( var item in this.filterElements) {
					this.filterElements[item].chart.clearSelection();
				}
			};

			/**
			 * Updates filter selection starting from the selected chart.
			 * 
			 * 
			 */
			FilterSectionView.prototype.filterChanged = function(chartId, data,
					extent, xDataFunction, yDataFunction) {
				// console.log('brush extent');
				// console.log(extent);
				// get the filter element where brush selection has changed.
				var filterElement = this.filterElements[chartId];
				var filteredData = filterElement.data;

				var crossData = crossfilter(filterElement.data);
				var filterDimension = crossData.dimension(xDataFunction);

				// check if the brush is two dimensional
				if (yDataFunction) {
					// console.log(extent[0][0] + " " + extent[1][0]);
					// console.log(extent[0][1] + " " + extent[1][1]);

					// check if start and end points of extent are different
					// when no selection is made, they are equal. Then the chart
					// should display the final filtered data.
					if ((extent[0][0].valueOf() != extent[1][0].valueOf())
							|| (extent[0][1].valueOf() != extent[1][1]
									.valueOf())) {

						// first filter on xDataFunction and extent in-x axis.
						filterDimension = filterDimension.filter([
								extent[0][0], extent[1][0] ]);

						// second filter on yDataFunciton and extent in y-axis.
						filterDimension = crossData.dimension(yDataFunction);
						filterDimension = filterDimension.filter([
								extent[0][1], extent[1][1] ]);

						filteredData = filterDimension.bottom(Infinity);
					}
				}
				// if the brush moves only in one direction.
				else {
					// check if start and end points of extent are different
					// when no selection is made, they are equal. Then the chart
					// should display the final filtered data.
					if (extent[0].valueOf() != extent[1].valueOf()) {
						// filter on xDataFunction and extent in one direction.
						filterDimension = filterDimension.filter([ extent[0],
								extent[1] ]);
						// console.log(extent[0] + " " + extent[1]);
						// console.log(filterDimension.bottom(Infinity));
						filteredData = filterDimension.bottom(Infinity);
					}
				}

				this.render(filteredData, filterElement.index);

			};

			/**
			 * Getter method for the list of datasets filtered by last filter
			 * among the filter elements in this filter view. If there exists no
			 * filter chart initialized, returns the set of datasets listed in
			 * Search section.
			 */
			FilterSectionView.prototype.getFilteredData = function() {
				return this.filteredData;
			};

			/**
			 * Resizes all the filter charts depending on the browser size.
			 */
			FilterSectionView.prototype.resize = function() {
				// console.log("filter controller resize");
				for ( var item in this.filterElements) {
					console.log(this.filterElements[item].chart);
					this.filterElements[item].chart.resize();
				}
			};

			return new FilterSectionView(controller_);
		}
	};

})(lodApp.View);
