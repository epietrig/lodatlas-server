/*
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
'use strict';

/**
 * CartDatasetSection view represents the "Cart-Dataset List" part in the main user interface.
 */
var lodApp = lodApp || {};

lodApp.View = lodApp.View || {};

(function(o) {
	o.CartDatasetSection = {
		getInstance : function(controller_) {

			var util = lodApp.util;
			var dataFunction = util.dataFunction;
			var columnNames = [
					{
						'name' : '#',
						'func' : ''
					},
					{
						'name' : 'repo name',
						'func' : dataFunction.repoName
					},
					{
						'name' : 'name',
						'func' : dataFunction.name
					},
					{
						'name' : 'title',
						'func' : dataFunction.title
					},
					{
						'name' : 'triple count',
						'func' : function(d) {
							return util.formatNumber(dataFunction
									.tripleCountText(d));
						},
						"className" : "cell-number"
					},
					{
						'name' : 'outgoing link count',
						'func' : function(d) {
							return util.formatNumber(dataFunction
									.outgoingLinkCount(d));
						},
						"className" : "cell-number"
					},
					{
						'name' : 'incoming link count',
						'func' : function(d) {
							return util.formatNumber(dataFunction
									.incomingLinkCount(d));
						},
						"className" : "cell-number"
					} ];

			var CartDatasetSectionView = function(controller) {
				this.controller = controller;
				this.init();
			};

			CartDatasetSectionView.constructor = CartDatasetSectionView;

			/**
			 * Initializes the components of the view.
			 */
			CartDatasetSectionView.prototype.init = function() {

				// initializes the table which displays the selected datasets
				this.tableElement = document
						.getElementById('selected-dataset-list-section');
				this.tableElement.querySelector('#dataset-list-count').textContent = 0;
				this.table = lodApp.View.TableView.getInstance(
						'selected-result-table', columnNames, this.controller);

				var that = this;

				// sets event handler for View Analytics button
				document.getElementById('selection-analytics')
						.addEventListener(
								'click',
								function() {
									that.controller
											.showAnalyticsView(that.table
													.getList());
								});

				// sets event handler for Remove Selected button
				document.getElementById('remove-selected').addEventListener(
						'click', function() {
							that.table.removeSelected();
							that.controller.toggleUpdateDetails(true);
							that.adjustView(that.table.getList().length);
						});

				// sets event handler for Export Selected button
				document
						.getElementById('export-selected')
						.addEventListener('click',
								function() {
									var datasetList = that.getSelectedDatasetList();
							
									if (datasetList.length === 0) {
										alert('Please select at least one dataset!');
									} else {
										that.controller.getVoid(datasetList, that.exportDatasets, that);
									}

						});
			};

			/**
			 * Writes concatenated void info of datasets to an N3 file.
			 */
			CartDatasetSectionView.prototype.exportDatasets = function(voidInfo) {
				
				// code copied from
				// https://stackoverflow.com/questions/2897619/using-html5-javascript-to-generate-and-save-a-file
				var pom = document.createElement('a');
				pom
						.setAttribute(
								'href',
								'data:text/plain;charset=utf-8,'
										+ encodeURIComponent(voidInfo));
				pom.setAttribute('download', "datasets.n3");

				if (document.createEvent) {
					var event = document
							.createEvent('MouseEvents');
					event
							.initEvent('click', true,
									true);
					pom.dispatchEvent(event);
				} else {
					pom.click();
				}
				// end of copied code
			};

			/**
			 * Adds the datasets in the selectionList to the table and shows
			 * refresh button for details part.
			 */
			CartDatasetSectionView.prototype.addSelected = function(selectionList) {
				this.table.append(selectionList);

				this.adjustView(this.table.getList().length);

				this.controller.toggleUpdateDetails(true);
			};

			/**
			 * Shows/hides View Analytics, Remove Selected and Export selected
			 * buttons, adjusts the size of the table depending on the number of
			 * datasets.
			 */
			CartDatasetSectionView.prototype.adjustView = function(listLength) {
				if (listLength > 0) {
					document.getElementById('selection-analytics').style.display = "inline-block";
					document.getElementById('remove-selected').style.display = "inline-block";
					document.getElementById('export-selected').style.display = "inline-block";
				} else {
					document.getElementById('selection-analytics').style.display = "none";
					document.getElementById('remove-selected').style.display = "none";
					document.getElementById('export-selected').style.display = "none";
				}

				var tableDiv = document.getElementById('selected-result-table');

				if (listLength > 10) {
					tableDiv.className += " result-table";
					document.getElementById('remove-selected').style.margin = "30px 0 0 0";
				} else {
					tableDiv.className = "table-responsive";
					document.getElementById('remove-selected').style.margin = "10px 0 0 0";
				}
				this.tableElement.querySelector('#dataset-list-count').textContent = listLength;

				tableDiv.scrollTop = 1;
			};

			/**
			 * Returns the list of datasets displayed in the table.
			 */
			CartDatasetSectionView.prototype.getDatasetList = function() {
				return this.table.getList();
			};

			/**
			 * Returns the list of datasets that are selected in the table.
			 */
			CartDatasetSectionView.prototype.getSelectedDatasetList = function() {
				return this.table.getSelection();
			};

			return new CartDatasetSectionView(controller_);
		}
	};

})(lodApp.View);
