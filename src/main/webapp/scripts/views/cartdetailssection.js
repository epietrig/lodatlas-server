/*
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
'use strict';

/**
 * CartDetailsSection view represents the "Cart-Details" part in the main user interface.
 */

var lodApp = lodApp || {};

lodApp.View = lodApp.View || {};

(function(o) {
	o.CartDetailsSection = {
		getInstance : function(controller_) {

			var util = lodApp.util;
			var dataFunction = util.dataFunction;

			var chartConfig = viewConfig.DetailSectionConfig;

			var CartDetailsSectionView = function(controller) {
				this.controller = controller;
				this.chartElements = {};

				this.init();
			};

			CartDetailsSectionView.constructor = CartDetailsSectionView;

			CartDetailsSectionView.prototype.init = function() {
				var that = this;

				// add show details events
				var showDetails = function(event) {
					that.controller.updateDetails();
				};
				// init show details event
				var showButton = document.getElementById('update-details');
				showButton.addEventListener('click', showDetails);

				this.initContainers();
				this.toggleUpdate(true);
			};

			/**
			 * Initializes the charts that view the detail charts.
			 */
			CartDetailsSectionView.prototype.initContainers = function() {
				var charts = chartConfig.charts;

				// double click should work, so specify 'true'
				var chartListener = lodApp.Controller.ChartListener
						.getListenerInstance(this.controller, true);

				var chartItem;

				for (var item in charts) {
					chartItem = charts[item];
					lodApp.View.ChartView.createChartContainer(document
							.getElementById(chartConfig.parentId), chartItem,
							this);

					this.chartElements[chartItem.chartId] = lodApp.d3
							.getChartFactory().getChart(this, chartItem,
									chartListener);
				}

				chartListener.setChartElements(this.chartElements);
			}

			/**
			 * 
			 * @param resultset
			 */
			CartDetailsSectionView.prototype.render = function(resultset) {
				this.data = resultset;

				for ( var item in this.chartElements) {
					this.chartElements[item].setData(resultset);
				}
			};

			/**
			 * Shows or hides 'Update' button in Details section depending on
			 * the value of 'show' parameter.
			 * 
			 * @param show
			 *            true if the 'Update' button is to be shown; false
			 *            otherwise.
			 */
			CartDetailsSectionView.prototype.toggleUpdate = function(show) {
				var showButton = document.getElementById('update-details');
				var detailsSection = document.getElementById('details-section');
				var detailsContent = detailsSection
						.querySelector('.section-content');

				var sectionInfo = document
						.getElementById('details-section-info');

				if (show) {
					showButton.style.display = 'inline-block';
					detailsContent.classList.add('details-shadow');

					sectionInfo.style.height = "5px";

				} else {
					showButton.style.display = 'none';
					detailsContent.classList.remove('details-shadow');
//
//					sectionInfo.style.height = "25px";
				}
			}

			/**
			 * 
			 * @param event
			 * @param chartItem
			 */
			CartDetailsSectionView.prototype.magnify = function(event, chartConfig) {
				document.body.style.cursor = 'wait';
				// TODO send chart details here
				this.controller.showChartMagnify(this.data, chartConfig);
				document.body.style.cursor = 'default';
			};

			CartDetailsSectionView.prototype.allLinks = function(event, magnifyChartItem) {
				// document.getElementById('details-section').querySelector('.section-content').className
				// = 'deneme';
				// document.getElementById('center-button').style.display =
				// 'block';
				// document.getElementById('details-overlay').style.display =
				// 'inline-block';
				document.body.style.cursor = 'wait';
				// TODO send chart details here
				this.controller.showChartAllLinks(this.data, magnifyChartItem);
				document.body.style.cursor = 'default';
			};

			/**
			 * Resizes all charts in this detail group view.
			 */
			CartDetailsSectionView.prototype.resize = function() {
				for ( var item in this.chartElements) {
					this.chartElements[item].resize();
				}
			};

			return new CartDetailsSectionView(controller_);
		}
	};

})(lodApp.View);
