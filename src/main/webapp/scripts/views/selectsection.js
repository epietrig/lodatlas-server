/*
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
'use strict';

/**
 * SelectSection view represents the "Select" part in the main user interface.
 */
var loadApp = lodApp || {};

lodApp.View = lodApp.View || {};

(function(o) {
	o.SelectSection = {
			getInstance: function(controller_) {
				
				var dataFunction = lodApp.util.dataFunction;
				var columnNames = [
					{
						'name' : '#',
						'func' : ''
					},
					{
						'name' : 'repo name',
						'func' : dataFunction.repoName
					},
					{
						'name' : 'name',
						'func' : dataFunction.name
					},
					{
						'name' : 'title',
						'func' : dataFunction.title
					},
					{
						'name' : 'triple count',
						'func' : function(d) {
							return util.formatNumber(dataFunction
									.tripleCountText(d));
						},
						"className" : "cell-number"

					},
					{
						'name' : 'outgoing link count',
						'func' : function(d) {
							return util.formatNumber(dataFunction
									.outgoingLinkCount(d));
						},
						"className" : "cell-number"
					},
					{
						'name' : 'incoming link count',
						'func' : function(d) {
							return util.formatNumber(dataFunction
									.incomingLinkCount(d));
						},
						"className" : "cell-number"
					} ];
				
				var SelectSectionView = function(controller) {
					this.controller = controller;
					this.init();
				};
				
				SelectSectionView.constructor = SelectSectionView;
				
				/**
				 * Creates the table and the View Analytics and Add Selected buttons. 
				 */
				SelectSectionView.prototype.init = function() {
					// create table to view search/filter results
					this.table = lodApp.View.TableView.getInstance(
							'search-result-table', columnNames,
							this.controller);

					var tableDiv = document.getElementById('search-result-table');
					// this does not work here on page refresh
					tableDiv.scrollTop = 1;
					
					var that = this;
					
					var analyticsButton = document.getElementById("analytics");
					analyticsButton.addEventListener('click', function() {
						that.controller.showAnalyticsView(that.table.getList());
					});

					// add listener for Add Selected button
					var addSelectedButton = document
							.getElementById("add-selected-button");
					addSelectedButton.addEventListener('click', function() {
						that.controller.addSelected(that.table.getSelection());
						that.table.clearSelection();
					});
				};
				
				/**
				 * Displays the search/filter results in the table.
				 * 
				 * @param result
				 */
				SelectSectionView.prototype.render = function(result) {
					// set total number of hits
					document.querySelector('#search-result-count').innerHTML = result.length;

					// display search results in table
					this.table.renderRows(result, false);

					if (result.length > 0) {
						document.getElementById('search-analytics').style.display = "inline-block";
						document.getElementById('add-selected').style.display = "inline-block";
					} else {
						document.getElementById('search-analytics').style.display = "none";
						document.getElementById('add-selected').style.display = "none";
					}
					// scroll to the top of the table
					var tableDiv = document.getElementById('search-result-table');
					if (result.length > 10) {
						tableDiv.className += " result-table";
						document.getElementById('add-selected').style.margin = "30px 0 0 0";
					} else {
						tableDiv.className = "table-responsive";
						document.getElementById('add-selected').style.margin = "10px 0 0 0";
					}
					tableDiv.scrollTop = 1;
				};
				
				return new SelectSectionView(controller_);
			}
	};
	
}(lodApp.View));
