/*
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/**
 * FacetItemView is a child view of FacetGroupView. For each facet item, an item
 * view is created and added to the parent view.
 */

'use strict';

var lodApp = lodApp || {};

lodApp.View = lodApp.View || {};

(function(o) {
	o.FacetItemView = {
		getInstance : function(parentView_) {
			var dataFunction = util.dataFunction;

			/**
			 * Initializes an ItemView as a child of the specified 'parentView'.
			 * 
			 * @param parentView
			 *            a FacetGroupView specified as parent for this item
			 *            view.
			 */
			var ItemView = function(parentView) {
				this.parentView = parentView;
				this.init();
			};

			ItemView.prototype.constructor = ItemView;

			/**
			 * Generates an instance of template with id 't-facet-item' and
			 * appends it to parentView which is a FacetGroupView.
			 */
			ItemView.prototype.init = function() {
				var facetItem = document.querySelector('template#t-facet-item').content;

				var facetItemClone = document.importNode(facetItem, true);
				this.element = this.parentView.appendChild(facetItemClone);
			};

			/**
			 * Renders the specified facet 'item' in this ItemView.
			 * 
			 * @param item
			 *            the item to display in this view.
			 */
			ItemView.prototype.render = function(item) {
				this.item = item;
				this.element.id = item.name;

				var name = this.item.name;
				name = (name === '' ? 'Not specified' : name);
				if (name.startsWith("http://")) {
					this.element.querySelector('#item-name').innerHTML = "<a href='"
							+ name + "' target='_blank'>" + name + "</a>";
					this.element.querySelector('#item-name').style.direction = "rtl";
				} else {
					this.element.querySelector('#item-name').textContent = name;
				}
				this.element.querySelector('#item-name').title = name;
				this.element.querySelector('#item-count').textContent = this.item.count;

				this.element.querySelector('#checkbox').checked = this.item.selected;

				var that = this;
				this.element.querySelector('#checkbox').addEventListener(
						'change', function(event) {
							event.stopPropagation();
							that.updateSelection();
							event.preventDefault();
						});
			};

			/**
			 * Sets the selected property of the view and informs the parentView
			 * of the change.
			 */
			ItemView.prototype.updateSelection = function() {
				var checked = this.element.querySelector('#checkbox').checked;
				if (this.item.selected != checked) {
					this.item.selected = checked;

					// update facet results depending on selection
					this.parentView.updateSelection(this.item);
				}
			};

			/**
			 * Removes this item view from its parent element.
			 */
			ItemView.prototype.removeFromParent = function() {
				return this.element.parentNode.removeChild(this.element);
			};

			return new ItemView(parentView_);
		}
	};

})(lodApp.View);
