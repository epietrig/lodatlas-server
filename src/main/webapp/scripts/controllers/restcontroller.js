/*
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
'use strict';

var lodApp = lodApp || {};

lodApp.Controller = lodApp.Controller || {};

var restApi = lodApp.Config.RestApi;

(function(o) {
	o.RestController = {

		/**
		 * Gets query results for the specified 'queryString' and send the data
		 * to specified 'callback'.
		 */
		getSearchResults : function(apiPath, queryString, callback, context,
				cType, dType, from) {
			$('*').addClass('wait');
			var contentType = cType || restApi.contentType;
			var dataType = dType || restApi.dataType;

			console.log("REST url is: " + restApi.url + apiPath + queryString)
			var searchResult = {};
			window.$
					.ajax({
						type : 'GET',
						url : restApi.url + apiPath + queryString,
						contentType : contentType,
						dataType : dataType,
						success : function(data, status, jqXHR) {
							// console.log(jqXHR);
							document.getElementById('message').style.display = 'none';
							if (typeof callback === "function") {
								callback.call(context, data, from);
							}
							$('*').removeClass('wait');
						},
						error : function(jqXHR, status, thrownError) {
//							console.log('status is ' + status);
//							console.log(jqXHR);
							alert("The server returned error:\n" + thrownError);
							$('*').removeClass('wait');
						}
					});
		}
	};

})(lodApp.Controller);
