/*******************************************************************************
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.model.dataset;

import fr.inria.ilda.lodatlas.commons.strings.DatasetStrings;

/**
 * 
 * @author Hande Gözükan
 *
 */
public class DatasetShortResult extends CommonResult {
	
	
	/**
	 * Outgoing link count
	 */
	protected final long olC;

	/**
	 * Incoming link count
	 */
	protected final long ilC;

	/**
	 * Creation date
	 */
	protected final String cD;

	/**
	 * Modification date
	 */
	protected final String mD;

	public DatasetShortResult(String jsonString) {
		super(jsonString);

		this.cD = getString(jsonObj, DatasetStrings.METADATA_CREATED).replace("\"", "");
		this.mD = getString(jsonObj, DatasetStrings.METADATA_MODIFIED).replace("\"", "");
		this.olC = getLong(getJsonObject(jsonObj, DatasetStrings.OUTGOING_LINKS), DatasetStrings.COUNT);
		this.ilC = getLong(getJsonObject(jsonObj, DatasetStrings.INCOMING_LINKS), DatasetStrings.COUNT);
	}

	/**
	 * @return the olC
	 */
	public long getOlC() {
		return olC;
	}

	/**
	 * @return the ilC
	 */
	public long getIlC() {
		return ilC;
	}

	/**
	 * @return the creation date
	 */
	public String getCD() {
		return this.cD;
	}

	/**
	 * @return the modification date
	 */
	public String getMD() {
		return this.mD;
	}

}
