/*
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/**
 * FacetListView populates the div tag with id nav-facet in index.html. The list
 * and order of facets to be displayed is specified by config.FacetCategories.
 */

'use strict';

var lodApp = lodApp || {};

lodApp.View = lodApp.View || {};

var config = lodApp.Config;

(function(o) {

	o.FacetListView = {
		getInstance : function(controller_) {

			/**
			 * Creates an instance of FacetListView that manupulates the
			 * nav-facet div.
			 */
			var ListView = function(controller) {
				this.controller = controller;
				this.element = document.getElementById('nav-facet');
				this.groupViews = {};
				var category;
				// creates a FacetGroupView for each category.
				for ( var item in config.FacetCategories) {
					category = config.FacetCategories[item];
					this.groupViews[category.short] = o.FacetGroupView
							.getInstance(category, this);
				}
			};

			ListView.constructor = ListView;

			/**
			 * Renders each FacetGroupView with the facet lists for each
			 * category in the specified 'facets' object.
			 */
			ListView.prototype.render = function(facets) {
				this.facetList = facets;
				for ( var item in this.facetList.groups) {
					var groupView = this.groupViews[this.facetList.groups[item].category];
					groupView.render(this.facetList.groups[item].list);
				}
			};

			/**
			 * Informs the search controller from selection change to update the
			 * facet values depending on the new selection.
			 */
			ListView.prototype.updateSelection = function(category, item) {
				this.controller.selectionChanged(category, item);
			};

			ListView.prototype.showMore = function(facetGroup, isShowMore) {
				this.controller.showMore(facetGroup, isShowMore);
			};

			/**
			 * Appends the specified 'child' element to this element and returns
			 * the appended element.
			 */
			ListView.prototype.appendChild = function(child) {
				this.element.appendChild(child);
				return this.element.lastElementChild;
			};

			return new ListView(controller_);
		}
	};
})(lodApp.View);
