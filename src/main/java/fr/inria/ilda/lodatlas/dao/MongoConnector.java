/*******************************************************************************
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.dao;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

/**
 * 
 * @author Hande Gözükan
 *
 */
public class MongoConnector {

	private static final Logger logger = LoggerFactory.getLogger(MongoConnector.class);

	/**
	 * MongoDb client.
	 */
	private static final MongoClient mongoClient = new MongoClient("localhost", 27017);

	/**
	 * Mongo database instance.
	 */
	private static final MongoDatabase mongodb = mongoClient.getDatabase("lodatlas");

	private static final MongoCollection<Document> collection = mongodb.getCollection("datasets");

	public static void addDataset(String json) {
		collection.insertOne(Document.parse(json));
	}

	public static void dispose() {
		mongoClient.close();
	}

}
