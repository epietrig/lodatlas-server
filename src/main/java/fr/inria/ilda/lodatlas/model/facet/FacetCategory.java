/*******************************************************************************
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.model.facet;

/**
 * A class that lists the facet categories in the application with their
 * parameter names and field names in ElasticSearch indexes.
 * 
 * @author Hande Gözükan
 *
 */
public enum FacetCategory {

	REPOSITORY("repo", "repoName"), RDFSUM("rdfsum", "rdfsum"), ORGANIZATION("org", "organization.title"), TAG("tag",
			"tags.name"), FORMAT("format", "resources.format"), LICENSE("license",
					"license_title"), VOCABULARIES("vocabs", "resources.vocabularies.raw"), PROPERTIES("props",
							"resources.usedProperties.raw"), CLASSES("classes", "resources.usedClasses.raw");

	/**
	 * Parameter name for the facet in lodatlas
	 */
	private final String aggregationName;

	/**
	 * Field name for the facet in elastic search index
	 */
	private final String fieldName;

	FacetCategory(String aggregationName, String fieldName) {
		this.aggregationName = aggregationName;
		this.fieldName = fieldName;
	}

	public String aggName() {
		return aggregationName;
	}

	public String fieldName() {
		return fieldName;
	}
}
