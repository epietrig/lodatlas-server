/*
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
'use strict';

var lodApp = lodApp || {};

lodApp.d3 = lodApp.d3 || {};

(function(o) {
	o.getScaleFactory = function() {

		/**
		 * 
		 */
		var Scale = function(scale) {
			this.scale = scale;
		};

		Scale.prototype.constructor = Scale;

		Scale.prototype.getd3Scale = function() {
			return this.scale;
		};

		Scale.prototype.domain = function(domain) {
			this.scale.domain(domain);
		};

		Scale.prototype.range = function(range) {
			this.scale.rangeRound(range);
		};

		Scale.prototype.rangeValue = function() {
			this.scale.range();
		};

		Scale.prototype.getTickFormat = function() {
			return d3.format(".1s");
		}

		Scale.prototype.getFormat = function() {
			return d3.format(".1s");
		}

		Scale.prototype.scaleValue = function(n) {
			return this.scale(n);
		};

		Scale.prototype.scaledHeight = function(chartHeight, n) {
			return chartHeight - this.scale(n);
		};

		Scale.prototype.getTickValues = function(n) {
			var domain = this.scale.domain();
			// console.log('domain');
			// console.log(domain);
			var step = Math.ceil((domain[domain.length - 1] - domain[0]) / 10);
			// console.log('step ' + step);
			var tickArray = [];
			var range = d3.range(domain[0], domain[domain.length - 1], step);
			// console.log(range);
			tickArray = tickArray.concat(range);
			tickArray = tickArray.concat([ domain[domain.length - 1] ]);
			// console.log("length = " + domain.length);
			// console.log(tickArray);
			return tickArray;
		};

		/**
		 * 
		 */
		var OrdinalScale = function() {
			Scale.call(this, d3.scale.ordinal());
		};

		OrdinalScale.prototype = Object.create(Scale.prototype);

		OrdinalScale.prototype.constructor = OrdinalScale;

		OrdinalScale.prototype.range = function(range) {
			this.scale.rangeBands(range, .1);
		};

		OrdinalScale.prototype.rangeValue = function() {
			return this.scale.rangeBand();
		};

		OrdinalScale.prototype.domain = function(data, datafunction) {
			// console.log("scale data");
			// console.log(data);
			this.scale.domain(d3.range(1, data.length + 1));
		};

		OrdinalScale.prototype.scaleValue = function(n) {
			return this.scale(n);
		};

		/**
		 * 
		 */
		var LinearScale = function() {
			Scale.call(this, d3.scale.linear());
		};

		LinearScale.prototype = Object.create(Scale.prototype);

		LinearScale.prototype.constructor = LinearScale;

		LinearScale.prototype.domain = function(data, dataFunction) {
			var dataMin = d3.min(data, dataFunction);
			var dataMax = d3.max(data, dataFunction);
			this.scale.domain([ dataMin, dataMax + 10 ]).nice();
		};

		/**
		 * 
		 */
		var LogScale = function() {
			Scale.call(this, d3.scale.log().base(2));
		};

		LogScale.prototype = Object.create(Scale.prototype);

		LogScale.prototype.constructor = LogScale;

		LogScale.prototype.domain = function(data, dataFunction) {
			this.dataMin = d3.min(data, dataFunction);
			this.dataMax = d3.max(data, dataFunction);
			var dataMin = this.dataMin === 0 ? 1 : this.dataMin;
			// console.log('max ' + dataMax);
			this.scale.domain([ dataMin, this.dataMax ]).nice();
			this.scale.clamp(true);
		};

		LogScale.prototype.scaleValue = function(n) {
			return (n === 0) ? 0 : this.scale(n);
		};

		LogScale.prototype.scaledHeight = function(chartHeight, n) {
			return (n === 0) ? 0 : (chartHeight - this.scale(n));
		};

		LogScale.prototype.getTickValues = function(n) {
			var domain = this.scale.domain();
//			console.log('domain');
//			console.log(domain);
			var min = domain[0];
			var max = domain[1];

			var diff = max - min;
			var minLog = this.getBaseLog(2, min);
			var maxLog = this.getBaseLog(2, max);

			var diffLog = maxLog - minLog;

			var offset = Math.ceil(diffLog / (n - 1));
			var tickArray = [];

			min = 0;
			tickArray.push(min);

			var sum = offset;

			for (var i = 0; i < n; i++) {
//				console.log(i);
				min = Math.pow(2, sum);
				if ((min < max) && ((2 * min) <= max)) {
					tickArray.push(min);
				}
				sum += offset;
			}
			tickArray.push(max);

//			console.log(tickArray);
			return tickArray;
		};

		/**
		 * Function copied from
		 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/log
		 */
		LogScale.prototype.getBaseLog = function(x, y) {
			return Math.log(y) / Math.log(x);
		}

		// #################### TimeScale ###########################

		var TimeScale = function() {
			Scale.call(this, d3.time.scale());
			// this.tickInterval = d3.time.months;
			// this.tickCountPerInt = 3;
			this.format = "%d %b %Y";
			this.tickFormat = "%d %b %Y"; // %Y only
		};

		TimeScale.prototype = Object.create(Scale.prototype);

		TimeScale.prototype.constructor = TimeScale;

		TimeScale.prototype.getFormat = function() {
			return d3.time.format(this.format);
		};

		TimeScale.prototype.getTickFormat = function() {
			return d3.time.format(this.tickFormat);
		};

		TimeScale.prototype.setTicks = function(axis) {
			// axis.ticks(this.tickInterval, this.tickCountPerInt);
			// axis.ticks(this.type, this.count);
			// axis.tickValues(d3.time.months(this.minDate, this.maxDate, 3));
		}

		TimeScale.prototype.domain = function(data, dataFunction) {
			this.minDate = d3.min(data, dataFunction);
			this.maxDate = d3.max(data, dataFunction);
			// console.log(this.minDate);
			// console.log(this.maxDate);
			var diff = this.maxDate - this.minDate;
			// console.log(diff);
			if (diff < 3600000) {
				// this.type = d3.time.minutes;
				// this.count = 36000000;
				this.scale.domain([ this.minDate, this.maxDate ]).nice(
						d3.time.minute);
				this.tickFormat = "%d %b %Y-%H:%M:%S";
			}
			// if time diff is less than a day
			else if (diff < 86400000) {
				// this.type = d3.time.hours;
				// this.count = 36000000;
				this.scale.domain([ this.minDate, this.maxDate ]).nice(
						d3.time.hour);
				this.tickFormat = "%d %b %Y-%Hh";
			} else if (diff < 2678400000) {
				// this.type = d3.time.days;
				// this.count = Math.floor(diff / 864000000);
				this.scale.domain([ this.minDate, this.maxDate ]).nice(
						d3.time.day);
				this.tickFormat = "%d %b %Y";
			} else {
				// this.type = d3.time.months;
				// this.count = Math.floor(diff / 26784000000);
				this.scale.domain([ this.minDate, this.maxDate ]).nice(
						d3.time.month);
				this.tickFormat = "%b %Y";
			}
			// console.log('type ' + this.type);
			// console.log('count ' + this.count);
		};

		TimeScale.prototype.scaleValue = function(n) {
			return this.scale(n);
		};

		TimeScale.prototype.scaledHeight = function(chartHeight, n) {
			return chartHeight - this.scale(n);
		};

		// ##########################################################

		var getd3Scale = function(type) {
			switch (type) {
			case 'ordinal':
				return new OrdinalScale();
			case 'log':
				return new LogScale();
			case 'linear':
				return new LinearScale();
			case 'time':
				return new TimeScale();
			default:
				return null;
			}
		};

		return {
			getScale : function(type) {
				return getd3Scale(type);
			}
		};
	};
})(lodApp.d3);
