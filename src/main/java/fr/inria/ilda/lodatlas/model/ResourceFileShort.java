package fr.inria.ilda.lodatlas.model;

public class ResourceFileShort {

	/**
	 * The name of the package that this resource belongs to.
	 */
	protected String datasetName;

	/**
	 * The id of the resource as unique identifier.
	 */
	protected String resourceId;

	/**
	 * The name of the nt file processed.
	 */
	protected String fileName;

	/**
	 * The original name of the file processed before being converted to nt if it
	 * was not in nt format.
	 */
	protected String originalFileName;

	/**
	 * The boolean value to indicate if any errors encountered during file
	 * processing.
	 */
	protected boolean hasError;

	/**
	 * The error encountered while processing. Empty string if no error is
	 * encountered.
	 */
	protected String error;

	/**
	 * The number of triples that are processed.
	 */
	protected long tripleCount;

	public ResourceFileShort() {
	}

	public String getDatasetName() {
		return datasetName;
	}

	public void setDatasetName(String datasetName) {
		this.datasetName = datasetName;
	}

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getOriginalFileName() {
		return originalFileName;
	}

	public void setOriginalFileName(String originalFileName) {
		this.originalFileName = originalFileName;
	}

	public boolean isHasError() {
		return hasError;
	}

	public void setHasError(boolean hasError) {
		this.hasError = hasError;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public long getTripleCount() {
		return tripleCount;
	}

	public void setTripleCount(long tripleCount) {
		this.tripleCount = tripleCount;
	}

}
