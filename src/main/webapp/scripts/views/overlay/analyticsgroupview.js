/*
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
'use strict';

var lodApp = lodApp || {};

lodApp.View = lodApp.View || {};

var util = lodApp.util;

(function(o) {
	o.AnalyticsGroupView = {
		getInstance : function(controller_, data_) {

			var header = "Analytics "
					+ "<span class=\"analytics-header\"> "
					+ "<span class=\"label-text\">Sort by:</span> <select id=\"sortlist\" class=\"selector-box\" name=\"sortlist\"></select><span style=\"padding-left: 40px;\"><span id=\"number-of-datasets-analytics\"	class=\"result-text\"></span><span>datasets</span></span>"
					+ "</span>";

			var chartConfig = viewConfig.AnalyticsSectionConfig;

			var AnalyticsView = function(controller, data) {
				this.controller = controller;

				this.container = document.getElementById(chartConfig.parentId);
				this.chartElements = {};

				this.init(data);
			};

			AnalyticsView.prototype.constructor = AnalyticsView;

			AnalyticsView.prototype.init = function(data) {
				var that = this;

				// change header
				this.container.querySelector('.header').innerHTML = header;

				this.container.querySelector('#number-of-datasets-analytics').innerHTML = data.length;

				// add sort part
				var sortList = this.container.querySelector('#sortlist');
				var charts = chartConfig.charts;
				for (var i = 0; i < charts.length; i++) {
					var chart = charts[i];
					var option = document.createElement("option");
					option.text = chart.sortBy;
					option.value = i;
					sortlist.add(option);
				}

				// add sort listener
				sortList.addEventListener('change', function(event) {
					var chart = chartConfig.charts[event.target.value];
					that.sort(chart);
				});

				this.initContainers();
				this.renderCharts(data);

				this.sort(charts[0]);
//				console.log(charts[0]);
			};

			/**
			 * Initializes analytics chart containers without data.
			 */
			AnalyticsView.prototype.initContainers = function() {
				var charts = chartConfig.charts;

				// this is necessary to be able to view scrollbar
				this.container.querySelector('#overlay-content').setAttribute(
						'style',
						('height:' + (window.innerHeight - 150) + 'px'));

				var chartListener = lodApp.Controller.ChartListener
						.getListenerInstance(this.controller, false);

				var chartItem;
				for ( var item in charts) {
					chartItem = charts[item];
					// creates chart container
					lodApp.View.ChartView.createChartContainer(this.container,
							chartItem);

					// gets chart from chart factory and adds it to charts list
					// of this analytics group view for later access.
					this.chartElements[chartItem.chartId] = lodApp.d3
							.getChartFactory().getChart(this, chartItem,
									chartListener);
				}

				chartListener.setChartElements(this.chartElements);
			};

			/**
			 * Renders the data in all analytics views.
			 * 
			 * @param resultset
			 *            the list of datasets.
			 */
			AnalyticsView.prototype.renderCharts = function(data) {
				for ( var item in this.chartElements) {
					this.chartElements[item].setData(data);
				}
			};

			/**
			 * Called when an element is hovered by mouse in one of the charts.
			 * Hovers the same element and its links in all other charts.
			 * 
			 * @param element
			 *            the element that is hovered on one of the charts.
			 * @param mode
			 *            the mode of the chart which is clicked. The same mode
			 *            is set to all other charts.
			 */
			AnalyticsView.prototype.mouseOvered = function(element, mode) {
				for ( var item in this.chartElements) {
					this.chartElements[item].clearSelection();
					this.chartElements[item].setMode(mode);
					this.chartElements[item].selectElement(element, false,
							this.chartElements[item]);
				}
			};

			/**
			 * Called when mouse is out from an element in one of the charts.
			 * Clears hover from all charts.
			 * 
			 * @param element
			 *            the element from which mouse is outed on one of the
			 *            charts.
			 * @param mode
			 *            the mode of the chart where mouse out event is called.
			 *            The same mode is set to all other charts.
			 */
			AnalyticsView.prototype.mouseOuted = function(element, mode) {
				this.clearSelection(element);
			};

			/**
			 * Clears any selection on all the charts.
			 */
			AnalyticsView.prototype.clearSelection = function() {
				for ( var item in this.chartElements) {
					this.chartElements[item].clearSelection();
				}
			};

			/**
			 * Sorts the charts in this view depending on the selected type in
			 * the combo box.
			 * 
			 * @param event
			 *            event for selection change in combo box.
			 */
			AnalyticsView.prototype.sort = function(chart) {
				for ( var item in this.chartElements) {
					this.chartElements[item].sort(util.comparator(
							chart.chartDetails.dataFunction, chart.sortDesc));
				}
			};

			/**
			 * Releases all the sources used by this view.
			 */
			AnalyticsView.prototype.dispose = function() {
//				this.container.querySelector('.header').innerHTML = "";

				var parent = this.container.querySelector('#overlay-content');

				this.container = null;
				this.chartElements = null;
				this.chartConfig = null;
				this.controller = null;
				
				var childNodes = parent.childNodes;

				for (var i = childNodes.length - 1; i >= 0; i--) {
					parent.removeChild(parent.childNodes[i]);
				}
			};

			/**
			 * Resizes the charts in this view.
			 */
			AnalyticsView.prototype.resize = function() {
				document.getElementById('overlay-content').setAttribute(
						'style',
						('height:' + (window.innerHeight - 150) + 'px'));
				for ( var item in this.chartElements) {
					this.chartElements[item].resize();
				}
			};

			return new AnalyticsView(controller_, data_);
		}
	};

})(lodApp.View);
