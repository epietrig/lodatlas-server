/*
 * LODAtlas Server
 * Copyright (C) 2018  INRIA Emmanuel Pietriga <emmanuel.pietriga@inria.fr>
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

var hebCtx = {
	width : 960, // adapt to actual SVG size
	height : 960,
	PROP_LABEL_MAX_LENGTH : 10,
	NS_ARC_THICKNESS : 14,
	TYPE_ARC_THICKNESS : 14,
	highlightColor : "#d78383", // red-ish
	color : d3.scale.category20(),
	noNScolor : "#CCC",
	NO_NS : "nns",
	NO_TYPE : "ntp",
	mainGroup : null,
	defs : null,
	nodes : null,
	links : null,
	rotate : 0,
	panning : false,
	rotating : false,
	translate : [ 0, 0 ],
	scale : 1,
	protate : 0,
};

var hebConsts = {
	MODE_PAN : 0,
	MODE_ROTATE : 1,
};

/* -------------------------------------------------------------------------- */

var buildHEB = function(graph) {

	var cluster = d3.layout.cluster().size([ 360, hebCtx.innerRadius ]).sort(
			null).value(function(d) {
		return d.size;
	});

	var bundle = d3.layout.bundle();

	var line = d3.svg.line.radial().interpolate("bundle").tension(.85).radius(
			function(d) {
				return d.y;
			}).angle(function(d) {
		return d.x / 180 * Math.PI;
	});

	hebCtx.mainGroup.append("g").attr("id", "linkSplinesG");
	hebCtx.mainGroup.append("g").attr("id", "linkLabelsG");
	hebCtx.mainGroup.append("g").attr("id", "linkArcsG");
	hebCtx.mainGroup.append("g").attr("id", "typeArcsG");
	hebCtx.mainGroup.append("g").attr("id", "typeLabelsG");

	var linkSplines = d3.select("#linkSplinesG").selectAll("path.link"), linkLabels = d3
			.select("#linkLabelsG").selectAll("text.node"), linkArcs = d3
			.select("#linkArcsG").selectAll("path");

	var root = {
		name : ""
	};
	// first level in the hierarchy, holding namespace groups
	root.children = generateGroupLevel(graph['groups'], root)
	// second level in the hierarchy, holding typed nodes (that have one or more
	// classes)
	var typedNodeMap = {};
	var prop2utnode = {};
	for (var i = 0; i < graph['nodes'].length; i++) {
		var n = graph['nodes'][i];
		if (n.labels.length > 0) {
			var sclass = n.labels.join();
			// add it to first declared NS group
			// XXX: we could consider duplicating in each NS...
			var typedNode = {
				name : sclass,
				parent : root.children[n.groups[0]],
				children : []
			};
			root.children[n.groups[0]].children.push(typedNode);
			typedNodeMap[i] = typedNode;
		} else {
			if (prop2utnode.hasOwnProperty(n.prop)) {
				typedNodeMap[i] = prop2utnode[n.prop];
			} else {
				var untypedNode = {
					name : "?",
					parent : root.children[root.children.length - 1],
					children : []
				};
				root.children[root.children.length - 1].children
						.push(untypedNode);
				typedNodeMap[i] = untypedNode;
				prop2utnode[n.prop] = untypedNode;
			}
		}
	}
	// walk all links to build third level in the hierarchy (leaf nodes),
	// holding property cells
	// which are considered as the actual nodes for the visualized graph
	// and also populate the list of sources/targets
	// - init list of all edges to be bundled and laid out, linking leaf nodes
	var rslinks = [];
	hebCtx.label2group = {};
	for (var i = 0; i < graph['links'].length; i++) {
		var prop = graph['links'][i];
		var sourceNode = typedNodeMap[prop.source];
		var targetNode = typedNodeMap[prop.target];
		// console.log(sourceNode);
		// console.log(targetNode);

		var leaf4source, leaf4target;
		if (sourceNode.parent.name == "nns") {
			// if sourceNode is untyped
			if (sourceNode.children.length > 0) {
				// if it already has a child, link to this child
				leaf4source = sourceNode.children[0];
			} else {
				leaf4source = {
					name : prop.label,
					key : prop.label,
					parent : sourceNode
				};
				sourceNode.children.push(leaf4source);
			}
		} else {
			var leafIndex = getLeafNodeIndex(prop.label, sourceNode.children);
			if (leafIndex >= 0) {
				leaf4source = sourceNode.children[leafIndex];
			} else {
				leaf4source = {
					name : prop.label,
					key : prop.label,
					parent : sourceNode
				};
				sourceNode.children.push(leaf4source);
			}
		}
		if (targetNode.parent.name == "nns") {
			// if sourceNode is untyped
			if (targetNode.children.length > 0) {
				// if it already has a child, link to this child
				leaf4target = targetNode.children[0];
			} else {
				leaf4target = {
					name : prop.label,
					key : prop.label,
					parent : targetNode
				};
				targetNode.children.push(leaf4target);
			}
		} else {
			var leafIndex = getLeafNodeIndex(prop.label, targetNode.children);
			if (leafIndex >= 0) {
				leaf4target = targetNode.children[leafIndex];
			} else {
				leaf4target = {
					name : prop.label,
					key : prop.label,
					parent : targetNode
				};
				targetNode.children.push(leaf4target);
			}
		}
		rslinks.push({
			source : leaf4source,
			target : leaf4target
		});
		hebCtx.label2group[prop.label] = prop.group;
	}
	// edge bundling hierarchy
	var ebh = {
		"" : root
	};
	var rsnodes = cluster.nodes(ebh[""]);

	hebCtx.links = linkSplines.data(bundle(rslinks)).enter().append("path")
			.each(function(d) {
				d.source = d[0], d.target = d[d.length - 1];
			}).attr("class", "link").attr("d", line).style("stroke",
					function(d) {
						return hebCtx.color(hebCtx.label2group[d.source.name]);
					});

	var leafNodes = rsnodes.filter(function(n) {
		return !n.children && n.depth > 2;
	});
	// hebCtx.leafNodes = leafNodes;

	hebCtx.linkLabels = linkLabels.data(leafNodes).enter().append("text").attr(
			"class", "node").attr("dy", ".31em").attr(
			"transform",
			function(d) {
				// console.log(d);
				return "rotate("
						+ (d.x - 90)
						+ ")translate("
						+ (d.y + hebCtx.NS_ARC_THICKNESS + 4
								+ hebCtx.TYPE_ARC_THICKNESS + 4) + ",0)"
						+ (d.x < 180 ? "" : "rotate(180)");
			}).style("text-anchor", function(d) {
		return d.x < 180 ? "start" : "end";
	}).text(
			function(d) {
				return (d.key.length > hebCtx.PROP_LABEL_MAX_LENGTH) ? d.key
						.substring(0, hebCtx.PROP_LABEL_MAX_LENGTH)
						+ "..." : d.key;
			}).on("mouseover", mouseEnteredLinkLabel).on("mouseout",
			mouseExitedLinkLabel);

	// https://github.com/mbostock/d3/wiki/SVG-Shapes#arc
	var propArc = d3.svg.arc().innerRadius(
			hebCtx.innerRadius + hebCtx.NS_ARC_THICKNESS + 4).outerRadius(
			hebCtx.innerRadius + hebCtx.NS_ARC_THICKNESS + 4
					+ hebCtx.TYPE_ARC_THICKNESS);

	linkArcs.data(leafNodes).enter().append("path").attr(
			"d",
			function(d, i) {
				propArc.startAngle(d.x * Math.PI / 180).endAngle(
						d.x * Math.PI / 180 + 0.02);
				return propArc();
			}).attr("stroke", "#444").style("fill", function(d) {
		return hebCtx.color(hebCtx.label2group[d.name]);
	});

	typedNodeAngleSpansT = {};
	for (var i = 0; i < leafNodes.length; i++) {
		if (typedNodeAngleSpansT.hasOwnProperty(leafNodes[i].parent.name)) {
			var angleInfo = typedNodeAngleSpansT[leafNodes[i].parent.name];
			if (angleInfo.minAngleDeg > leafNodes[i].x) {
				angleInfo.minAngleDeg = leafNodes[i].x;
			}
			if (angleInfo.maxAngleDeg < leafNodes[i].x) {
				angleInfo.maxAngleDeg = leafNodes[i].x;
			}
		} else {
			var angleInfo = {
				minAngleDeg : leafNodes[i].x,
				maxAngleDeg : leafNodes[i].x,
				name : leafNodes[i].parent.name
			};
			typedNodeAngleSpansT[leafNodes[i].parent.name] = angleInfo;
		}
	}
	var typedNodeAngleSpans = [];
	for ( var k in typedNodeAngleSpansT) {
		var tnas = typedNodeAngleSpansT[k];
		if ((tnas.maxAngleDeg - tnas.minAngleDeg) < 2) {
			tnas.minAngleDeg -= 1;
			tnas.maxAngleDeg += 1;
		}
		typedNodeAngleSpans.push(tnas);
	}
	var nsArc = d3.svg.arc().innerRadius(hebCtx.innerRadius).outerRadius(
			hebCtx.innerRadius + hebCtx.NS_ARC_THICKNESS);

	var arcs = [];

	d3.select("#typeArcsG").selectAll("path").data(typedNodeAngleSpans).enter()
			.append("path").attr(
					"d",
					function(d, i) {
						nsArc.startAngle(d.minAngleDeg * Math.PI / 180)
								.endAngle(d.maxAngleDeg * Math.PI / 180);
						var a = nsArc();
						// console.log(a);
						arcs.push(a);
						return a;
					}).attr("stroke", "#444").attr("fill", "#DDD");

	// http://bl.ocks.org/jebeck/196406a3486985d2b92e
	// typed node info as textPaths
	for (var i = 0; i < typedNodeAngleSpans.length; i++) {
		hebCtx.defs.append("path").attr(
				{
					"id" : typedNodeAngleSpans[i].name.replace(",", ""),
					"d" : generateArcPath(typedNodeAngleSpans[i].minAngleDeg,
							typedNodeAngleSpans[i].maxAngleDeg,
							hebCtx.innerRadius * 1.02)
				});
	}
	d3.select("#typeArcsG").selectAll("text").data(typedNodeAngleSpans).enter()
			.append("text").append("textPath").attr("startOffset", '50%')
			.style("text-anchor", "middle").attr("xlink:href", function(d, i) {
				return "#" + typedNodeAngleSpans[i].name.replace(",", "");
			}).text(function(d) {
				return d.name
			});

};

var getLeafNodeIndex = function(propLabel, leafNodeList) {
	for (var i = 0; i < leafNodeList.length; i++) {
		if (leafNodeList[i].name == propLabel) {
			return i;
		}
	}
	return -1;
}

var generateArcPath = function(minAngleDeg, maxAngleDeg, radius) {
	var spx = radius * Math.cos((minAngleDeg - 90) * Math.PI / 180);
	var spy = radius * Math.sin((minAngleDeg - 90) * Math.PI / 180);
	var epx = radius * Math.cos((maxAngleDeg - 90) * Math.PI / 180);
	var epy = radius * Math.sin((maxAngleDeg - 90) * Math.PI / 180);
	var res = "M" + spx + "," + spy;
	res += " A" + radius + "," + radius;
	res += ((maxAngleDeg - minAngleDeg) <= 180) ? " 0 0,1 " : " 0 1,1 ";
	res += epx + "," + epy;
	return res;
};

var generateGroupLevel = function(groups, root) {
	var res = [];
	for (var i = 0; i < groups.length; i++) {
		res.push({
			"name" : groups[i],
			"group" : i,
			"parent" : root,
			children : []
		});
	}
	// additional group for untyped placeholders
	var nt = {
		"name" : hebCtx.NO_NS,
		"group" : res.length,
		"parent" : root,
		children : []
	};
	nt.children.push({
		"name" : hebCtx.NO_TYPE,
		parent : nt,
		children : []
	});
	res.push(nt);
	return res;
};

/* -------------------------------------------------------------------------- */

function mouseEnteredLinkLabel(d) {
	// reset all link label colors
	hebCtx.linkLabels.each(function(n) {
		n.target = n.source = false;
	});

	// color hovered links
	hebCtx.links.classed("link--target", function(l) {
		if (l.target === d)
			return l.source.source = true;
	}).classed("link--source", function(l) {
		if (l.source === d)
			return l.target.target = true;
	}).filter(function(l) {
		return l.target === d || l.source === d;
	}).each(function() {
		this.parentNode.appendChild(this);
	});

	// color hovered link labels
	hebCtx.linkLabels.classed("node--target", function(n) {
		return n.target;
	}).classed("node--source", function(n) {
		return n.source;
	});

	// expand name of hovered link labels
	d3.selectAll("text.node--target").text(function(d2) {
		return d2.key;
	});
	d3.selectAll("text.node--source").text(function(d2) {
		return d2.key;
	});
	d3.select(this).text(function(d2) {
		return d2.key;
	});
};

function mouseExitedLinkLabel(d) {
	hebCtx.links.classed("link--target", false).classed("link--source", false);

	hebCtx.linkLabels.classed("node--target", false).classed("node--source",
			false);

	// contract name of hovered link labels
	d3.selectAll("text.node").text(function(d2) {
		return getTrimmedPropLabel(d2.key);
	});
};

/* -------------------------------------------------------------------------- */

function zoomed() {
	if (hebCtx.panning) {
		hebCtx.translate = d3.event.translate;
		hebCtx.scale = d3.event.scale;
	} else if (hebCtx.rotating) {
		// inverse horizontal drag direction / rotation direction mapping
		// depending on whether cursor is in upper or lower part of the
		// visualization
		// (provides stronger sense of direct manipulation given the circular
		// nature
		// of this visualization)
		var upperSide = (d3.event.sourceEvent.y <= hebCtx.rootTranslate) ? -1
				: 1;
		hebCtx.rotate += ((hebCtx.protate - d3.event.translate[0]) > 0) ? upperSide * 4
				: -4 * upperSide;
		hebCtx.protate = d3.event.translate[0];
	}
	hebCtx.mainGroup.attr("transform", "translate(" + hebCtx.translate
			+ ")scale(" + hebCtx.scale + ")rotate(" + hebCtx.rotate + ")");
}

/* -------------------------------------------------------------------------- */

var buildPropertyListHeb = function(graph) {
	
	// extract list of unique prop URIs and bind it to the list items
	var propURIs = [];
	var propColorGroup = [];
//	console.log(graph.links.length);
	for (var i = 0; i < graph.links.length; i++) {
		if (propURIs.indexOf(graph.links[i].label) == -1) {
			propURIs.push(graph.links[i].label);
			propColorGroup.push(graph.links[i].group);
		}
	}

	var propList = [];
	for (var i = 0; i < propURIs.length; i++) {
		propList.push({
			label : propURIs[i],
			group : propColorGroup[i]
		});
	}

	var propComp = function(a, b) {
		// sort by namespace, then alphabetically
		if (a.group < b.group) {
			return -1;
		} else if (a.group > b.group) {
			return 1;
		} else {
			if (a.label < b.label) {
				return -1;
			} else if (a.label > b.label) {
				return 1;
			} else {
				return 0;
			}
		}
	}
	propList.sort(propComp);
	
//	console.log(propList);

	var pldiv = d3.select("#propListPane-heb").selectAll("div").data(propList)
			.enter().append("div").attr("class", function(d, i) {
				if (i % 2 == 0) {
					return "proplistitem even";
				} else {
					return "proplistitem odd";
				}
			}).on(
					"mouseover",
					function(d) {
						d3.select(this).style("background-color",
								hebCtx.highlightColor);
						highlightPropInstances(graph, d.label);
					}).on("mouseleave", function(d) {
				resetPropInstanceHighlighting();
				d3.select(this).attr("style", null);
			});

	pldiv.append("div").attr("class", "legend").style("background-color",
			function(d) {
				return hebCtx.color(d.group)
			});

	pldiv.append("div").text(function(d) {
		return d.label;
	});

};

var resetPropInstanceHighlighting = function() {
}

var highlightPropInstances = function(graph, propLocalName) {
};

// var resetPropListHighlighting = function(){
// d3.select("#propListPane").selectAll("div.proplistitem")
// .style("background-color", null);
// };

// var highlightPropListItems = function(graph, node){
// var propertiesToHighlight = [];
// for (var i=0;i<graph.links.length;i++){
// if (graph.links[i].source.index == node.index ||
// graph.links[i].target.index == node.index){
// if (propertiesToHighlight.indexOf(graph.links[i].label) == -1){
// propertiesToHighlight.push(graph.links[i].label);
// }
// }
// }
// d3.select("#propListPane").selectAll("div.proplistitem")
// .style("background-color", function(d){
// if (propertiesToHighlight.indexOf(d.label) == -1){return null;}
// else {return fdCtx.highlightColor;}
// });
// };

// var highlightPropListItem = function(propLocalName){
// d3.select("#propListPane").selectAll("div.proplistitem")
// .style("background-color", function(d){
// if (d.label == propLocalName){return fdCtx.highlightColor;;}
// else {return null;}
// });
// };

/* -------------------------------------------------------------------------- */
// Visualization params adjustments
/* -------------------------------------------------------------------------- */

var adjustPropLabelLength = function(keyEvent) {
	hebCtx.PROP_LABEL_MAX_LENGTH += (keyEvent.shiftKey) ? -1 : 1;
	if (hebCtx.PROP_LABEL_MAX_LENGTH < 0) {
		hebCtx.PROP_LABEL_MAX_LENGTH = 0;
	}
	d3.selectAll("text.node").text(function(d) {
		return getTrimmedPropLabel(d.key);
	});
}

var getTrimmedPropLabel = function(propLb) {
	return (propLb.length > hebCtx.PROP_LABEL_MAX_LENGTH) ? propLb.substring(0,
			hebCtx.PROP_LABEL_MAX_LENGTH)
			+ "..." : propLb;
};

/* -------------------------------------------------------------------------- */

var initVizHeb = function() {

	var zoom = d3.behavior.zoom().scaleExtent([ 1, 10 ]).on("zoom", zoomed);

	var diameter = hebCtx.height, radius = diameter / 3;
	hebCtx.innerRadius = radius - 120;

//	hebCtx.rootTranslate = 1.2 * radius;
	hebCtx.rootTranslate = radius;

	if (d3.select("#heb").select("svg") != null)
		d3.select("#heb").select("svg").remove();
	var svg = d3.select("#heb").append("svg").attr("width", "100%").attr(
			"height", diameter)

	var rect = svg.append("rect").attr("width",
			d3.selectAll("svg").attr("width")).attr("height",
			d3.selectAll("svg").attr("height")).style("fill", "none").style(
			"pointer-events", "all").call(zoom);

	var svgmg = svg.append("g").attr(
			"transform",
			"translate(" +  (1.5 * hebCtx.rootTranslate) + "," + hebCtx.rootTranslate
					+ ")");

	hebCtx.defs = d3.select("svg").append("defs");
	hebCtx.mainGroup = svgmg.append("g");
	hebCtx.svg = svgmg;

	d3.select("body").on("keydown", function() {
		if (d3.event.keyCode == 32) {
			// pressing space bar
			if (!d3.event.repeat) {
				toggleMode(hebConsts.MODE_PAN);
			}
		} else if (d3.event.code == "KeyR") {
			if (!d3.event.repeat) {
				toggleMode(hebConsts.MODE_ROTATE);
			}
		} else if (d3.event.code == "KeyT") {
			adjustPropLabelLength(d3.event);
		}
	}).on("keyup", function() {
	});
};

var toggleMode = function(mode) {
	if (mode == hebConsts.MODE_PAN) {
		if (hebCtx.panning) {
			hebCtx.panning = false;
			d3.select("#status").text("");
		} else {
			hebCtx.panning = true;
			hebCtx.rotating = false;
			d3.select("#status").text("Drag to pan, mouse wheel to zoom");
		}
	} else if (mode == hebConsts.MODE_ROTATE) {
		if (hebCtx.rotating) {
			hebCtx.rotating = false;
			d3.select("#status").text("");
		} else {
			hebCtx.rotating = true;
			hebCtx.panning = false;
			d3.select("#status").text("Drag to rotate");
		}
	}
};

var loadData = function(rdfSummary) {
	buildHEB(rdfSummary);
	buildPropertyListHeb(rdfSummary);
	// d3.json(rdfSummary, function(error, graph) {
	// if (error) throw error;
	// buildHEB(graph);
	// buildPropertyList(graph);
	// });
};

var makeVizHeb = function(rdfSummary) {
	resetHeb();
//	console.log(rdfSummary);
	loadData(rdfSummary);
};

var resetHeb = function() {
	initVizHeb();
	d3.select("#propListPane-heb").selectAll("div").remove();
	d3.select("#propListPane-heb").on("mouseleave", function(d) {
		resetPropInstanceHighlighting();
	});
}
