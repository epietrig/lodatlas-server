/*
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
'use strict';

var lodApp = lodApp || {};

lodApp.d3 = lodApp.d3 || {};

(function(o) {
	o.getChartFactory = function() {
		var scaleFactory = lodApp.d3.getScaleFactory();
		var dataFunc = lodApp.util.dataFunction;

		var CONST = {
			MATRIX_PADDING : 5,
			SCATTER_SQUARE_SIZE : 8,
			TIME_CHART_HEIGHT : 85
		};

		var defaultWidth = 500;
		var defaultHeight = 500;

		var getName = function(d) {
			var len = dataFunc.name(d).length;
			if (len > 13) {
				return dataFunc.trimName(d);
			}
			return dataFunc.name(d);
		};

		/**
		 * Getter method for the center coordinates of a rectangle.
		 * 
		 * @param selection
		 *            the rectangle to get center coordinates.
		 */
		var getRectCenter = function(selection) {
			var cx = Number(selection.attr('x'))
					+ Number(selection.attr('width') / 2.0);
			var cy = Number(selection.attr('y'))
					+ Number(selection.attr('height') / 2.0);

			return [ cx, cy ];
		};

		/**
		 * the arc to use for selections on scatter.
		 */
		var selectionArc = d3.svg.arc().innerRadius(10).outerRadius(7)
				.startAngle(0).endAngle(360 * (Math.PI / 180));

		var setElementClass = function(selection, link, className) {
			var selectedRect = selection.select('#name' + dataFunc.name(link));
			Chart.prototype.setClass.call(this, selectedRect, className);

			selectedRect.append('title').attr('class', 'selection-title').text(
					'name: ' + dataFunc.name(link));
			// console.log(selectedRect.data);
		};

		var removeElementClass = function(selection, link, className) {
			var selectedRect = selection.select('#name' + dataFunc.name(link));
			Chart.prototype.removeClass.call(this, selectedRect, className);
		};

		/**
		 * The modes that a chart can have are: 1. NONE<br>
		 * 2. CLICKED<br>
		 * 3. HOVERED<br>
		 * 
		 * The modes change depending on the following mouse events:<br>
		 * 1. click (defined on SVG and elements)<br>
		 * 2. double click (defined on elements)<br>
		 * 3. mouse over (defined on elements)<br>
		 * 4. mouse out (defined on elements)<br>
		 * 
		 * For the events that are defined for SVG, the element is undefined.
		 * 
		 * The initial mode for a chart is MODES.NONE.
		 * 
		 * Mode transitions are as follows:<br>
		 * 
		 * MODES.NONE --- click(element)---> MODES.CLICKED <br>
		 * MODES.NONE --- click(undefined) ---> MODES.NONE <br>
		 * MODES.NONE --- doubleClick(element) ---> MODES.CLICKED <br>
		 * MODES.NONE --- mouseOver(element) ---> HOVERED<br>
		 * mouseOut cannot be called in MODES.NONE.
		 * 
		 * MODES.CLICKED --- click(element) ---> MODES.CLICKED<br>
		 * MODES.CLICKED --- click(undefined) ---> MODES.NONE<br>
		 * MODES.CLICKED --- doubleClick(element) ---> MODES.CLICKED<br>
		 * MODES.CLICKED --- mouseOver(element) ---> MODES.CLICKED<br>
		 * MODES.CLICKED --- mouseOut(element) ---> MODES.CLICKED<br>
		 * 
		 * MODES.HOVERED ---> click(element) ---> MODES.CLICKED <br>
		 * MODES.HOVERED ---> doubleClick(element) ---> MODES.CLICKED <br>
		 * MODES.HOVERED ---> mouseOut(element) ---> MODES.NONE <br>
		 * click(undefined) cannot be called in MODES.HOVERED. <br>
		 * mouseOver cannot be called in MODES.HOVERED.
		 * 
		 */
		var MODES = {
			/**
			 * Initial mode for any chart.
			 */
			NONE : {
				onClick : function(element) {
					// console.log('called click in NONE mode.');
					// console.log(element);
					// console.log(this.chartListener);

					// if an element exists at the clicked point and a click
					// listener event exists in chart listener
					// if SVG is clicked where there is no element, the element
					// is undefined.
					if (element && this.chartListener.clicked) {
						d3.event.stopPropagation();
						// this.mode = MODES.CLICKED;
						this.chartListener
								.clicked(element, MODES.CLICKED, this);
					}
				},
				// This is likely to be never called as in the first click,
				// onClick function which changes the mode to MODES.CLICKED. So
				// double click is always called in MODES.CLICKED.
				onDoubleClick : function(element) {
					// console.log('called doubleClick in NONE mode.');

					// no need to check for double click, it is not added as
					// event to SVG.
					if (this.chartListener.doubleClicked) {
						// this.mode = MODES.CLICKED;
						this.chartListener.doubleClicked(element,
								MODES.CLICKED, this);
					}
				},
				onMouseOver : function(element) {
					// console.log('called mouseOver in NONE mode.');
					// console.log(element);

					if (this.chartListener.mouseOvered) {
						// this.mode = MODES.HOVERED;
						this.chartListener.mouseOvered(element, MODES.HOVERED,
								this);
					}
				},
				onMouseOut : function(element) {
					// console
					// .log('called mouseOut in NONE mode. This should never be
					// called.');
					// console.log(element);
				}
			},
			CLICKED : {
				onClick : function(element) {
					// console.log('called click in CLICKED mode.');
					// console.log(element);
					if (element === undefined) {
						if (this.chartListener.clearSelection) {
							// clear selection will change the mode to
							// MODES.NONE
							this.chartListener.clearSelection();
						} else {
							this.clearSelection();
						}
					}
					// if an element exists at the clicked point and a click
					// listener event exists in chart listener
					// if SVG is clicked where there is no element, the element
					// is undefined.
					else if (element && this.chartListener.clicked) {
						d3.event.stopPropagation();
						this.chartListener.clicked(element, this.mode, this);
					}
				},
				onDoubleClick : function(element) {
					// console.log('called doubleClick in CLICKED mode.');

					// no need to check for double click, it is not added as
					// event to SVG.
					if (this.chartListener.doubleClicked) {
						this.chartListener.doubleClicked(element, this.mode,
								this);
					}
				},
				// MouseOver in MODES.CLICKED does not change anything.
				onMouseOver : function(element) {
					// console
					// .log('called mouseOver in CLICKED mode. Nothing to do');
					// console.log(element);
				},
				// Mouse out in MODES.CLICKED does not change anything.
				onMouseOut : function(element) {
					// console
					// .log('called mouseOut in CLICKED mode. Nothing to do');
					// console.log(element);
				}
			},
			HOVERED : {
				onClick : function(element) {
					this.clicked(element, MODES.CLICKED, this);
					// in HOVERED mode no need to check for element, changes to
					// HOVERED mode only if an element is HOVERED
					if (this.chartListener.clicked) {
						d3.event.stopPropagation();
						// this.mode = MODES.CLICKED;
						this.chartListener
								.clicked(element, MODES.CLICKED, this);
					}
				},
				onDoubleClick : function(element) {
					// console.log('called doubleClick in HOVERED mode.');

					// no need to check for double click, it is not added as
					// event to SVG.
					if (this.chartListener.doubleClicked) {
						this.chartListener.doubleClicked(element, this.mode,
								this);
					}
				},
				onMouseOver : function(element) {
					// console
					// .log('called mouseOver in HOVERED mode. This should never
					// be called.');
					// console.log(element);
				},
				onMouseOut : function(element) {
					// console.log('called mouseOut in HOVERED mode.');
					// console.log(element);

					if (this.chartListener.mouseOuted) {
						// this.mode = MODES.NONE;
						this.chartListener
								.mouseOuted(element, MODES.NONE, this);
					}
				}
			}
		};

		// ########## Chart #####################################

		/**
		 * This is the base implementation for a chart.
		 * 
		 * @param parentView
		 *            the view that displays this chart.
		 * @param chartConfig:
		 *            the configuration data for the chart.
		 * @param chartListener:
		 *            the controller that listens to the events of this chart.
		 */
		var Chart = function(parentView, chartConfig, chartListener) {
			this.parentView = parentView;
			this.parentId = chartConfig.chartId;
			this.chartListener = chartListener;
			this.init(chartConfig.chartDetails);
		};

		Chart.prototype.constructor = Chart;

		/**
		 * Initializes the chart with the specified chartDetails.
		 * 
		 * @param chartDetails
		 *            the configuration for the chart.
		 */
		Chart.prototype.init = function(chartDetails) {
			// Cannot set this by getting the width of the parent. When an
			// element is created and added to DOM, clientWidth value does not
			// return the right value. For that reason, all charts update
			// chartWidth and chartHeight each time new data is set.
			// TODO find a better solution.
			this.chartWidth = 0;
			this.chartHeight = 0;

			this.margins = chartDetails.margins || {
				left : 20,
				top : 20,
				right : 20,
				bottom : 20
			};

			// get parent container as d3 selection
			var parentDiv = d3.select("#" + this.parentId);

			if (parentDiv === null)
				throw new Error("Parent node cannot be null.");
			if (parentDiv === undefined)
				throw new Error("Parent node cannot be undefined.");

			var that = this;
			// initialize chart SVG element
			this.svg = parentDiv.append('svg').attr('width', '100%').attr(
					'height', '100%').on('click', function(element) {
				that.onClick(element);
			});

			// append main group to append the elements of the chart within the
			// margins.
			this.svg.append('g').attr('id', 'mainGroup').attr(
					'transform',
					'translate(' + this.margins.left + ', ' + this.margins.top
							+ ')');

			// CSS class for shapes in the chart
			this.shapeClass = chartDetails.shapeClass;

			// if the data in x-axis should display names.
			this.showName = chartDetails.showName || false;
			this.nameClass = chartDetails.nameClass;

			// if true
			this.horizontalAlign = chartDetails.horizontalAlign || false;

			// the value that indicates if the links of a dataset should be
			// highlighted when hovered.
			this.showLinks = chartDetails.showLinks || false;

			this.mode = MODES.NONE;

			this.delay = 0;
			this.updateDuration = 0;
			this.shortDuration = 0;

			if (chartDetails.delay) {
				this.delay = 1500;
				this.updateDuration = 1000;
				this.shortDuration = 500;
			}
		};

		/**
		 * Sets the chart listener for this chart.
		 * 
		 * @param chartListener
		 *            the chart listener to set for this chart.
		 */
		Chart.prototype.setChartListener = function(chartListener) {
			this.chartListener = chartListener;
		};

		/**
		 * Sets mode for the chart.
		 * 
		 * @param mode
		 *            the mode to set to the chart.
		 */
		Chart.prototype.setMode = function(mode) {
			this.mode = mode;
		};

		/**
		 * Updates the chart size depending on the current width of the parent
		 * node of the chart. When chart size is updated, the elements a part
		 * from the shape elements of the chart are also resized.
		 */
		Chart.prototype.resizeChartElements = function() {
			// get width and height of the parent node
			var width = parseFloat(this.svg.node().parentNode.clientWidth);
			var height = parseFloat(this.svg.node().parentNode.clientHeight);
			// console.log('width ' + width + ' height ' + height);

			// set width and height of the chart according to margins
			this.chartWidth = width - this.margins.left - this.margins.right;
			this.chartHeight = height - this.margins.top - this.margins.bottom;
			// console.log('cwidth ' + this.chartWidth + ' cheight '
			// + this.chartHeight);

			this.resizeElements();
		};

		/**
		 * Resizes the chart and its elements.
		 */
		Chart.prototype.resize = function() {
			this.resizeChartElements();
			this.updateShapes();
		}

		/**
		 * Calls mode.onClick event when an element is clicked on the chart.
		 * 
		 * @param element
		 *            the element that is clicked.
		 */
		Chart.prototype.onClick = function(element) {
			// console.log("clicked");
			// console.log(element);
			this.mode.onClick.call(this, element);
		};

		/**
		 * When an element is clicked on the chart, the previous selection is
		 * cleared, new mode is set, and the clicked element is selected on the
		 * chart.
		 * 
		 * @param element
		 *            the element that is clicked.
		 * @param mode
		 *            the new mode to set to the chart.
		 * @param context
		 *            the chart which is clicked
		 */
		Chart.prototype.clicked = function(element, mode, context) {
			this.clearSelection();
			this.setMode(mode);
			this.selectElement(element, true, context);
		};

		/**
		 * Calls the chartListener.doubleClicked event when an element is double
		 * clicked on the chart.
		 * 
		 * @param element
		 *            the element that is double clicked.
		 */
		Chart.prototype.dblclicked = function(element) {
			this.mode.onDoubleClick.call(this, element);
		};

		/**
		 * Calls the chartListener.mouseOvered event when an element is hovered
		 * on the chart.
		 * 
		 * @param element
		 *            the element that is hovered.
		 */
		Chart.prototype.mouseOvered = function(element) {
			this.mode.onMouseOver.call(this, element);
		};

		/**
		 * Calls the chartListener.mouseOuted event when mouse exits an element
		 * on the chart.
		 * 
		 * @param element
		 *            the element that mouse has exited.
		 */
		Chart.prototype.mouseOuted = function(element) {
			this.mode.onMouseOut.call(this, element);
		};

		Chart.prototype.clearClick = function(element) {
			// console.log(element);
			if (this.chartListener.clearSelection) {
				// console.log('called clear click');
				if (this.mode !== MODES.CLICKED) {
					this.chartListener.clearSelection();
				}
			}
		};

		Chart.prototype.selectElement = function(element, fade, context) {
		};

		Chart.prototype.clearSelection = function() {
			this.mode = MODES.NONE;
		};

		Chart.prototype.setChildrenClass = function(selection, className) {
			selection.each(function(d, i) {
				d3.selectAll(this.childNodes).classed(className, true);
			});
		};

		Chart.prototype.removeChildrenClass = function(selection, className) {
			selection.each(function(d, i) {
				d3.selectAll(this.childNodes).classed(className, false);
			});
		};

		Chart.prototype.setClass = function(selection, className) {
			selection.classed(className, true).classed('fade', false).classed(
					'fade-scatter', false);
		};

		Chart.prototype.removeClass = function(selection, className) {
			selection.classed(className, false);
		};

		Chart.prototype.clear = function(selection) {
			var that = this;
			selection.each(function(d, i) {
				d3.selectAll(this.childNodes).classed('hovered', false)
						.classed('square-hover', false).classed('target-link',
								false).classed('target', false).classed(
								'source-link', false).classed('source', false)
						.classed('both', false).classed('both-link', false)
						.classed('fade', false).classed('fade-scatter', false);
			});
		};

		// ########## BarChart #####################################
		/**
		 * Initializes a BarChart instance according to the configuration
		 * specified by 'chartConfig'.<br>
		 * BarChart displays values for dataFunction specified in 'chartConfig'
		 * in y-axis and name function values in x-axis.
		 * 
		 * @param chartConfig
		 *            configuration details for this BarChart. Fields are as
		 *            follows:<br>
		 *            margins: margins of the chart inside SVG element.<br>
		 *            shapeClass: CSS class for the rectangles of the bar chart.<br>
		 *            nameClass: CSS class for the name strings.<br>
		 *            showName: 'true' to display names under x-axis and hide
		 *            x-axis ticks; 'false' otherwise.<br>
		 *            xAxisText: title for x-axis<br>
		 *            yAxisText: title for y-axis<br>
		 *            dataFunction: the function that takes data and returns the
		 *            field to display in y-axis.<br>
		 *            delay: 'true' to update chart with a delay when chart data
		 *            changes; 'false' otherwise.<br>
		 *            showLinks: 'true' to highlight the links of a selection
		 *            when hovered; 'false' otherwise.<br>
		 * 
		 * @param chartListener
		 *            the listener for chart events.
		 */
		var BarChart = function(parentView, chartConfig, chartListener) {
			Chart.call(this, parentView, chartConfig, chartListener);
			var chartDetails = chartConfig.chartDetails;

			// set data function for y-axis.
			this.dataFunction = chartDetails.dataFunction || dataFunc.identity;

			// create main group of the chart
			var mainGroup = this.svg.select('#mainGroup');

			// add bar group element to append chart bars
			mainGroup.append('g').attr('id', 'barGroup');

			// if chart will display names, add the group element to append bar
			// names
			if (this.showName) {
				mainGroup.append('g').attr('id', 'nameGroup');
			}

			// ##### initialize x and y scales ##########
			this.xScale = scaleFactory.getScale(chartDetails.xScale
					|| 'ordinal');

			this.yScale = scaleFactory
					.getScale(chartDetails.yScale || 'linear');

			var yAxisText = (chartDetails.yScale === 'log' ? chartDetails.yAxisText
					+ " (log)"
					: chartDetails.yAxisText)
					|| "";

			// #### initialize axis #########
			// initialize x axis
			this.xAxis = d3.svg.axis().orient('bottom').scale(
					this.xScale.getd3Scale());

			// add x axis to svg
			mainGroup.append("g").attr('id', 'xAxisGroup').attr("class",
					"xAxisG").append("text").attr("class", "axisText").attr(
					"y", -3).style("text-anchor", "end").text(
					chartDetails.xAxisText || "");

			// initialize y axis
			this.yAxis = d3.svg.axis().orient('left').scale(
					this.yScale.getd3Scale()).tickFormat(
					this.yScale.getTickFormat());

			// add y axis to svg
			mainGroup.append("g").attr('id', 'yAxisGroup').attr("class",
					"yAxisG").attr("transform", "translate(" + (-20) + ", 0)")
					.attr("width", 20).append("text").attr("transform",
							"rotate(-90)").attr("y", 6).attr("dy", ".71em")
					.style("text-anchor", "end").text(yAxisText);
		};

		BarChart.prototype = Object.create(Chart.prototype);

		BarChart.prototype.constructor = BarChart;

		/**
		 * Sets the data to display in the bar chart. <br>
		 * Updates bars, names, scales and axis.
		 * 
		 * @param data
		 *            the data to display in the bar chart.
		 */
		BarChart.prototype.setData = function(data) {
			this.clearSelection();

			// This is done here, because the width of a newly created and added
			// element to DOM cannot be obtained correctly.
			// TODO find a better solution to that.
			this.resizeChartElements();

			// sets data to display
			this.data = data;

			// update scales
			this.xScale.domain(this.data, this.dataFunction);
			this.yScale.domain(this.data, this.dataFunction);

			// update bars
			var barGroup = this.svg.select('#barGroup').selectAll(
					'.' + this.shapeClass).data(this.data);
			this.exitBars(barGroup);
			this.updateBars(barGroup);
			this.enterBars(barGroup);

			// update names
			if (this.showName) {
				var nameGroup = this.svg.select('#nameGroup').selectAll(
						'.' + this.nameClass).data(this.data);

				this.exitBarNames(nameGroup);
				this.updateBarNames(nameGroup);
				this.enterBarNames(nameGroup);
			}

			// update x axis
			if (!this.showName) {
				this.xAxis.tickFormat(this.xScale.getFormat());
				this.xAxis.tickValues(this.xScale.getTickValues(10));
			} else {
				this.xAxis.tickFormat("");
				this.xAxis.tickSize(0);
			}
			this.svg.select('#xAxisGroup').transition().delay(this.delay)
					.duration(this.updateDuration).call(this.xAxis);

			// update y axis
			this.yAxis.tickValues(this.yScale.getTickValues(8));
			this.svg.select('#yAxisGroup').transition().delay(this.delay)
					.duration(this.updateDuration).call(this.yAxis);
		};

		/**
		 * Appends the missing rectangles on the selection depending on the
		 * number of data of the selection.
		 * 
		 * @param selection
		 *            the rectangles in the barGroup.
		 */
		BarChart.prototype.enterBars = function(selection) {
			var that = this;

			selection.enter().append("rect").attr("class", function(element) {
				// if classType for the element is not specified, set is
				// whatever specified in config
				return element.classType ? element.classType : that.shapeClass;
			}).attr("width", 0).attr("height", 0).attr("y", that.chartHeight)
					.on("click", function(element) {
						that.onClick(element);
					}).on("mouseout", function(element) {
						that.mouseOuted(element);
					}).on("mouseover", function(element) {
						that.mouseOvered(element);
					}).on("dblclick", function(element) {
						that.dblclicked(element);
					}).append("title").text("");

			this.updateBars(selection);
		};

		/**
		 * Updates the rectangles on the selection depending on the data of the
		 * selection.
		 * 
		 * @param selection
		 *            the rectangles in the barGroup.
		 */
		BarChart.prototype.updateBars = function(selection) {
			var that = this;
			var selectionLast = selection
					.transition()
					.delay(this.delay)
					.duration(this.updateDuration)
					.attr("id", function(d) {
						return 'name' + dataFunc.name(d);
					})
					.attr("width", that.xScale.rangeValue())
					.attr("x", function(d, i) {
						return that.xScale.scaleValue(i + 1);
					})
					.attr("y", function(d) {
						return that.yScale.scaleValue(that.dataFunction(d));
					})
					.attr(
							"height",
							function(d) {
								return that.yScale.scaledHeight(
										that.chartHeight, that.dataFunction(d));
							});

			if (!this.horizontalAlign) {
				selectionLast.select("title").text(
						function(d) {
							var s = "name: "
									+ dataFunc.name(d)
									+ '\n'
									+ that.yScale.getFormat()(
											that.dataFunction(d));
							return s;
						});
			}
		};

		/**
		 * Removes the excess rectangles from the selection depending on the
		 * number of data of the selection.
		 * 
		 * @param selection
		 *            the rectangles in the barGroup.
		 */
		BarChart.prototype.exitBars = function(selection) {
			var that = this;
			selection.exit().transition().duration(this.shortDuration).attr(
					"class", "disappear").transition().duration(
					this.updateDuration).attr("y", function(d) {
				return that.chartHeight;
			}).attr("height", 0).remove();
		};

		/**
		 * Appends the missing text elements for name on the selection depending
		 * on the number of data of the selection.
		 * 
		 * @param selection
		 *            the text elements in the barGroup.
		 */
		BarChart.prototype.enterBarNames = function(selection) {
			var that = this;
			if (!this.horizontalAlign) {
				selection.enter().append("text").attr("class", that.nameClass)
						.style("text-anchor", "end").on("click",
								function(element) {
									that.onClick(element);
								}).on("mouseout", function(element) {
							that.mouseOuted(element);
						}).on("mouseover", function(element) {
							that.mouseOvered(element);
						}).on("dblclick", function(element) {
							that.dblclicked(element);
						}).append("title").text("");

				this.updateBarNames(selection);
			}
		}

		/**
		 * Updates the text elements on the selection depending on the data of
		 * the selection.
		 * 
		 * @param selection
		 *            the text elements in the barGroup.
		 */
		BarChart.prototype.updateBarNames = function(selection) {
			var that = this;
			if (!this.horizontalAlign) {
				selection
						.transition()
						.delay(this.delay)
						.duration(this.updateDuration)
						.attr("x", function(d, i) {
							return 0;
						})
						.attr(
								"y",
								function(d, i) {
									return that.chartWidth
											- that.xScale
													.scaleValue(that.data.length
															- i)
											- that.xScale.rangeValue() / 2;
								}).attr("id", function(d) {
							return 'name' + dataFunc.name(d);
						}).text(function(d) {
							return getName(d);
						}).attr("transform", "rotate(-90)").select("title")
						.text(
								function(d) {
									var s = "name: "
											+ dataFunc.name(d)
											+ '\n'
											+ that.yScale.getFormat()(
													that.dataFunction(d));
									return s;
								});
			}

		}

		/**
		 * Removes the excess text elements from the selection depending on the
		 * number of data of the selection.
		 * 
		 * @param selection
		 *            the text elements in the barGroup.
		 */
		BarChart.prototype.exitBarNames = function(selection) {
			var that = this;
			selection.exit().transition().duration(this.updateDuration).attr(
					'class', 'disappear').transition().duration(
					this.updateDuration).text(function(d) {
				return "";
			}).remove();
		}

		/**
		 * Resizes the chart depending on parent container's size.
		 */
		BarChart.prototype.resizeElements = function() {
			// update the location of name group
			if (this.showName) {
				this.svg.select('#' + 'nameGroup').attr('transform',
						'translate(0 , ' + (this.chartHeight + 25) + ')');
			}

			// update scale ranges
			this.xScale.range([ 0, this.chartWidth ]);
			this.yScale.range([ this.chartHeight, 0 ]);

			// update x axis
			// update the location of x axis and xAxis text
			this.svg.select('#xAxisGroup').attr("transform",
					"translate(0, " + (this.chartHeight + 15) + ")").attr(
					"width", this.chartWidth).call(this.xAxis).select(
					'.axisText').attr('x', this.chartWidth);

			// update y axis length
			this.svg.select('#yAxisGroup').attr("height", this.chartHeight)
					.call(this.yAxis);
		};

		/**
		 * Updates the location of bars and names in this bar chart.
		 */
		BarChart.prototype.updateShapes = function() {
			// update location of bars and names
			this.updateBars(this.svg.selectAll('.' + this.shapeClass));

			if (this.showName) {
				this.updateBarNames(this.svg.selectAll('.' + this.nameClass));
			}
		};

		/**
		 * Highlights the specified element in this bar chart. If showLinks is
		 * 'true' for this chart, also highlights the incoming and outgoing
		 * links to this element.
		 * 
		 * @param element
		 *            the element to highlight.
		 * @param fade
		 *            boolean value to indicate if other elements should fade
		 *            out or not.
		 */
		BarChart.prototype.selectElement = function(element, fade, context) {
			var bars = this.svg.select('#barGroup');
			var names = this.svg.select('#nameGroup');
			// console.log("element " + element.name);

			if (fade) {
				this.setChildrenClass(bars, 'fade');
				this.setChildrenClass(names, 'fade');
			}

			if (this.showLinks) {
				var that = this;
				// color incoming links
				element.incomingLinks.forEach(function(link) {
					// console.log("incoming " + link.name);
					that.setClass(bars.select('#name' + link.name), 'source');
					that.setClass(names.select('#name' + link.name), 'source');
				});

				var linkTemp = undefined;
				// color outgoing links
				element.outgoingLinks.forEach(function(link) {
					linkTemp = bars.select('#name' + link.name);
					// console.log("outgoing " + link.name);

					if (!linkTemp.empty() && linkTemp.classed('source')) {
						that.removeClass(linkTemp, 'source');
						that.removeClass(names.select('#name' + link.name),
								'source');
						that.setClass(linkTemp, 'both');
						that
								.setClass(names.select('#name' + link.name),
										'both');
					} else {
						that.setClass(linkTemp, 'target');
						that.setClass(names.select('#name' + link.name),
								'target');
					}
				});
			}

			var barElement = bars.select('#name' + dataFunc.name(element));

			// set class for hovered bar element, remove all other classess
			this.removeClass(barElement, 'source');
			this.removeClass(barElement, 'target');
			this.removeClass(barElement, 'both');
			this.setClass(barElement, 'hovered');

			// console.log("this-context");
			// console.log(this);
			// console.log(context);

			// if we would like to display the name of the element only for the
			// selection chart
			if (this.horizontalAlign && this === context) {
				var x = parseFloat(barElement.attr("x"));
				var width = parseFloat(barElement.attr("width"));
				var xValue = x + width;
				names.append("text").attr("class", this.nameClass + " hovered")
						.style("text-anchor", "end").attr("id", 'added').attr(
								"x", xValue).attr("y", 10).text(
								dataFunc.name(element));
			} else {
				var nameElement = names
						.select('#name' + dataFunc.name(element));
				// set class for hovered name element, remove all other classess
				this.removeClass(nameElement, 'source');
				this.removeClass(nameElement, 'target');
				this.removeClass(nameElement, 'both');
				this.setClass(nameElement, 'hovered');
			}
		}

		/**
		 * Clears any highlight class added to bar and name elements.
		 */
		BarChart.prototype.clearSelection = function() {
			Chart.prototype.clearSelection.call(this);
			var bars = this.svg.select('#barGroup');
			var names = this.svg.select('#nameGroup');

			this.svg.selectAll('#added').remove();

			this.clear(bars);
			this.clear(names);
		};

		/**
		 * Sorts the elements in this bar chart depending on the specified
		 * sortFunction.
		 * 
		 * @param sortFunction
		 *            the function that will be used to sort elements of this
		 *            chart.
		 */
		BarChart.prototype.sort = function(sortFunction) {
			var barGroup = this.svg.select('#barGroup').selectAll('rect');
			barGroup.sort(sortFunction);
			this.updateBars(barGroup);
		};

		// ########## BarChartWithBrush #####################################

		/**
		 * Initializes a BarChartWithBrush instance according to the
		 * configuration specified by 'chartConfig'.<br>
		 * BarChartWithBrush allows the user make selection on the x-axis. <br>
		 * BarChartWithBrush displays values for dataFunction specified in
		 * 'chartConfig' in y-axis and name function values in x-axis.
		 * 
		 * @param chartConfig
		 *            configuration details for this BarChart. Fields are as
		 *            follows:<br>
		 *            margins: margins of the chart inside SVG element.<br>
		 *            shapeClass: CSS class for the rectangles of the bar chart.<br>
		 *            nameClass: CSS class for the name strings.<br>
		 *            showName: 'true' to display names under x-axis and hide
		 *            x-axis ticks; 'false' otherwise.<br>
		 *            xAxisText: title for x-axis<br>
		 *            yAxisText: title for y-axis<br>
		 *            dataFunction: the function that takes data and returns the
		 *            field to display in y-axis.<br>
		 *            delay: 'true' to update chart with a delay when chart data
		 *            changes; 'false' otherwise.<br>
		 *            showLinks: 'true' to highlight the links of a selection
		 *            when hovered; 'false' otherwise.<br>
		 *            brushSize: the number of elements the brush contains
		 *            initially.<br>
		 * 
		 * @param chartListener
		 *            the listener for chart events.
		 */
		var BarChartWithBrush = function(chartConfig, chartListener) {
			BarChart.call(this, parentView, chartConfig, chartListener);

			// configure brush and add to bar chart.
			this.brushSize = chartConfig.brushSize;

			this.brushStart = 0;
			this.brushEnd = this.brushSize;

			var that = this;

			this.brush = d3.svg.brush().on("brushend", function() {
				that.selectionChanged();
			}).on("brush", function() {
				// synchronizes all other brushes in other bar charts, but
				// but does not to change selected data since the brush is
				// still being changed.
				that.brushChanged();
			});

			this.brushScale = d3.scale.linear().domain([ 0, this.chartWidth ]);
			this.addBrush();

		};

		BarChartWithBrush.prototype = Object.create(BarChart.prototype);

		BarChartWithBrush.prototype.constructor = BarChartWithBrush;

		/**
		 * 
		 * @param data
		 */
		BarChartWithBrush.prototype.setData = function(data) {
			BarChart.prototype.setData.call(this, data);
			// TODO think of what to do if data length is 0.
			this.brushScale.range([ 1, this.data.length + 1 ]);

			var dataLength = this.data.length;

			this.brushStart = 1;
			this.brushEnd = (dataLength >= this.brushSize ? this.brushSize
					: dataLength);

			this.updateBrush();
			// TODO change this only if data changes, probably data will change,
			// just check
			this.selectionChanged();
		};

		/**
		 * 
		 */
		BarChartWithBrush.prototype.brushChanged = function() {
			var brushSelection = this.brush.extent();

			// change brush start and end only by 'brush' event and not by
			// 'brushend' event.
			// if changed by 'brushend' event, when you leave the mouse, the
			// location
			// changes from actual selected location.
			this.brushStart = Math.floor(this.brushScale(brushSelection[0]));
			this.brushEnd = Math.floor(this.brushScale(brushSelection[1]));

			// console.log(brushSelection + " start " + this.brushStart + ' end
			// ' + this.brushEnd);
			this.chartListener.brushChanged(this.brushStart, this.brushEnd);
		};

		BarChartWithBrush.prototype.selectionChanged = function() {
			this.parentView.filterChanged(this.data, this.brushStart - 1,
					this.brushEnd - 1);
		};

		/**
		 * 
		 * @param start
		 * @param end
		 */
		BarChartWithBrush.prototype.updateBrushExtent = function(start, end) {
			this.brushStart = start;
			this.brushEnd = end;
			this.updateBrush();
		};

		BarChartWithBrush.prototype.addBrush = function() {
			this.svg.select('#mainGroup').append("g").attr("class", "brush")
					.call(this.brush).selectAll("rect").attr("y", 0).attr(
							"height", this.chartHeight + 20);

			this.svg.selectAll("g.resize").append("ellipse").attr("rx", 3)
					.attr("ry", 13).attr("cy", (this.chartHeight + 20) / 2)
					.attr("class", "brush-handle");
		};

		BarChartWithBrush.prototype.updateBrush = function() {
			this.brush.x(this.xScale.getd3Scale()).extent(
					[ this.brushScale.invert(this.brushStart),
							this.brushScale.invert(this.brushEnd) ]);
			this.svg.selectAll(".brush").call(this.brush);
		};

		// ########## ScatterChartWithBrush ##########
		/**
		 * 
		 */
		var ScatterChartWithBrush = function(parentView, chartConfig,
				chartListener) {
			Chart.call(this, parentView, chartConfig, chartListener);

			var chartDetails = chartConfig.chartDetails;

			// set data function
			this.xDataFunction = chartDetails.xDataFunction
					|| dataFunc.identity;
			this.yDataFunction = chartDetails.yDataFunction
					|| dataFunc.identity;

			var mainGroup = this.svg.select('#mainGroup');

			// add brush first to make mouse events be detected by rectangles
			var that = this;

			this.brush = d3.svg.brush().on("brushend", function() {
				that.brushSelectionChanged();
			}).on("brush", function() {
				that.brushChanged();
			});

			this.svg.select('#mainGroup').append("g").attr("class", "brush")
					.call(this.brush);

			// add rectGroup element to append chart rectangles
			mainGroup.append('g').attr('id', 'rectGroup');

			// ###### initialize x and y scales ######
			this.xScale = scaleFactory.getScale(chartDetails.xScale
					|| 'ordinal');

			this.yScale = scaleFactory
					.getScale(chartDetails.yScale || 'linear');

			// ###### initialize axis #########
			// initialize x axis
			this.xAxis = d3.svg.axis().orient('bottom').scale(
					this.xScale.getd3Scale()).tickFormat(
					this.xScale.getTickFormat()).outerTickSize(0);

			// add x axis to SVG
			mainGroup.append("g").attr('id', 'xAxisGroup').attr("class",
					"xAxisG").append("text").attr("class", "axisText").attr(
					"y", -3).style("text-anchor", "end").text(
					chartDetails.xAxisText || "");

			// initialize y axis
			this.yAxis = d3.svg.axis().orient('left').scale(
					this.yScale.getd3Scale()).tickFormat(
					this.yScale.getTickFormat());

			// add axis to SVG
			mainGroup.append("g").attr('id', 'yAxisGroup').attr("class",
					"yAxisG").attr("transform", "translate(" + (-20) + ", 0)")
					.attr("width", 20).append("text").attr("transform",
							"rotate(-90)").attr("y", 6).attr("dy", ".71em")
					.style("text-anchor", "end").text(
							chartDetails.yAxisText || "");
		};

		ScatterChartWithBrush.prototype = Object.create(Chart.prototype);

		ScatterChartWithBrush.prototype.constructor = ScatterChartWithBrush;

		/**
		 * Sets the data to display in the scatter chart. <br>
		 * Updates rectangles, scales, axis and brush.
		 * 
		 * @param data
		 *            the data to display in the scatter chart.
		 */
		ScatterChartWithBrush.prototype.setData = function(data) {
			this.clearSelection();
			this.resizeChartElements();

			// sets data to display
			this.data = data;

			// update scales
			this.xScale.domain(this.data, this.xDataFunction);
			this.yScale.domain(this.data, this.yDataFunction);

			// update rects
			var rectGroup = this.svg.select('#rectGroup').selectAll(
					'.' + this.shapeClass).data(this.data, dataFunc.name);
			this.exitRects(rectGroup);
			this.updateRects(rectGroup);
			this.enterRects(rectGroup);

			// update x-axis
			this.xAxis.tickValues(this.xScale.getTickValues(20));
			this.svg.select('#xAxisGroup').transition().delay(this.delay)
					.duration(this.updateDuration).call(this.xAxis);

			// update y-axis
			this.yAxis.ticks(5);
			this.svg.select('#yAxisGroup').transition().delay(this.delay)
					.duration(this.updateDuration).call(this.yAxis);

			this.brush.extent([ [ 1, 1 ], [ 1, 1 ] ]);
			this.svg.selectAll(".brush").call(this.brush);
			this.brushSelectionChanged();
		};

		/**
		 * Appends the missing rect elements for name on the selection depending
		 * on the number of data of the selection.
		 * 
		 * @param selection
		 *            the text elements in the rectGroup.
		 */
		ScatterChartWithBrush.prototype.enterRects = function(selection) {
			var that = this;

			selection.enter().append("rect").attr("class", that.shapeClass)
					.attr("width", 0).attr("height", 0).attr("y",
							that.chartHeight).on("click", function(element) {
						that.onClick(element);
					}).on("mouseout", function(element) {
						that.mouseOuted(element);
					}).on("mouseover", function(element) {
						that.mouseOvered(element);
					}).on("dblclick", function(element) {
						that.dblclicked(element);
					});

			this.updateRects(selection);
		};

		/**
		 * Updates the rect elements on the selection depending on the data of
		 * the selection.
		 * 
		 * @param selection
		 *            the text elements in the rectGroup.
		 */
		ScatterChartWithBrush.prototype.updateRects = function(selection) {
			var that = this;
			selection.transition().delay(this.delay).duration(
					this.updateDuration).attr("id", function(d) {
				return 'name' + dataFunc.name(d);
			}).attr("width", CONST.SCATTER_SQUARE_SIZE).attr("x",
					function(d, i) {
						return that.xScale.scaleValue(that.xDataFunction(d));
					}).attr("y", function(d) {
				return that.yScale.scaleValue(that.yDataFunction(d)) - 5;
			}).attr("height", CONST.SCATTER_SQUARE_SIZE);
		};

		/**
		 * Removes the excess rect elements from the selection depending on the
		 * number of data of the selection.
		 * 
		 * @param selection
		 *            the text elements in the rectGroup.
		 */
		ScatterChartWithBrush.prototype.exitRects = function(selection) {
			var that = this;
			selection.exit().transition().duration(this.shortDuration).attr(
					"class", "disappear").transition().duration(
					this.updateDuration).attr("y", function(d) {
				return that.chartHeight;
			}).attr("height", 0).remove();
		};

		/**
		 * Resizes the chart depending on parent containers size.
		 */
		ScatterChartWithBrush.prototype.resizeElements = function() {
			var brushExtent = this.brush.extent();
			// console.log(brushExtent);
			// update scale ranges
			this.xScale.range([ 0, this.chartWidth ]);
			this.yScale.range([ this.chartHeight, 0 ]);

			// update brush scales
			this.brush.x(this.xScale.getd3Scale());
			this.brush.y(this.yScale.getd3Scale());

			if (brushExtent !== null) {
				this.brush.extent(brushExtent);
			}
			this.svg.selectAll(".brush").call(this.brush);

			// update the location of x axis and xAxis text
			this.svg.select('#xAxisGroup').attr("transform",
					"translate(0, " + (this.chartHeight + 15) + ")").attr(
					"width", this.chartWidth).call(this.xAxis).select(
					'.axisText').attr('x', this.chartWidth);

			// update y axis length
			this.svg.select('#yAxisGroup').attr("height", this.chartHeight)
					.call(this.yAxis);
		};

		/**
		 * Updates the location of rects in this scatter chart.
		 */
		ScatterChartWithBrush.prototype.updateShapes = function() {
			this.updateRects(this.svg.selectAll('.' + this.shapeClass));
		};

		/**
		 * Highlights the specified element in this scatter chart. If showLinks
		 * is 'true' for this chart, also highlights the incoming and outgoing
		 * links to this element.
		 * 
		 * @param element
		 *            the element to highlight.
		 * @param fade
		 *            boolean value to indicate if other elements should fade
		 *            out or not.
		 */
		ScatterChartWithBrush.prototype.selectElement = function(element, fade,
				context) {
			var rects = this.svg.select('#rectGroup');
			// console.log('called select element');
			// console.log(element);

			if (fade) {
				this.setChildrenClass(rects, 'fade-scatter');
			}

			if (this.showLinks) {
				var that = this;
				// color incoming links
				element.incomingLinks.forEach(function(link) {
					that.setClass(rects.select('#name' + link.name), 'source');
				});

				var rectTemp = undefined;

				// color outgoing links
				element.outgoingLinks.forEach(function(link) {

					rectTemp = rects.select('#name' + link.name);
					if (!rectTemp.empty() && rectTemp.classed('source')) {
						that.removeClass(rectTemp, 'source');
						that.setClass(rectTemp, 'both');
					} else {
						that.setClass(rectTemp, 'target');
					}
				});
			}

			var selectedRect = rects.select('#name' + dataFunc.name(element));
			// console.log(selectedRect);
			var center = getRectCenter(selectedRect);

			var that = this;

			var title = "name: " + dataFunc.name(element) + '\ntriple count: '
					+ this.xScale.getFormat()(this.xDataFunction(element))
					+ '\nlink count: '
					+ this.yScale.getFormat()(this.yDataFunction(element));

			rects.append("path").attr("d", selectionArc).attr("transform",
					"translate(" + center[0] + "," + center[1] + ")").attr(
					'id', 'selection').attr('class', 'hovered').append('title')
					.attr('class', 'selection-title').text(title);

			selectedRect.append('title').attr('class', 'selection-title').text(
					title);

			this.removeClass(selectedRect, 'source');
			this.removeClass(selectedRect, 'target');
			this.removeClass(selectedRect, 'both');
			this.setClass(selectedRect, 'hovered');
		}

		/**
		 * Clears any highlight class added to rect elements.
		 */
		ScatterChartWithBrush.prototype.clearSelection = function() {
			Chart.prototype.clearSelection.call(this);

			this.svg.select('#selection').remove();
			this.svg.selectAll('.selection-title').remove();
			this.clear(this.svg.select('#rectGroup'));
		};

		/**
		 * Informs any listener to this chart that the filter selected by brush
		 * has changed.
		 */
		ScatterChartWithBrush.prototype.brushSelectionChanged = function() {
			this.clearSelection();
			var brushSelection = this.brush.extent();
			// console.log(brushSelection);
			// make this check properly. setting brush extent [[1,1], [1,1]]
			// does not work
			if (brushSelection[0][0] != undefined
					&& brushSelection[0][1] != undefined
					&& brushSelection[1][0] != undefined
					&& brushSelection[1][1] != undefined
					&& brushSelection[0][0] != 1 && brushSelection[0][1] != 1
					&& brushSelection[1][0] != 1 && brushSelection[1][1] != 1) {

				this.parentView.filterChanged(this.parentId, this.data,
						brushSelection, this.xDataFunction, this.yDataFunction);
			}
		};

		/**
		 * 
		 */
		ScatterChartWithBrush.prototype.brushChanged = function() {
			var brushSelection = this.brush.extent();
		};

		// ########## TimeChart #####################################

		/**
		 * Initializes a TimeChart instance according to the configuration
		 * specified by 'chartConfig'.<br>
		 * TimeChart displays dates related to datasets on a time line.
		 * 
		 * @param chartConfig
		 *            configuration details for this TimeChart. Fields are as
		 *            follows:<br>
		 *            margins: margins of the chart inside SVG element.<br>
		 *            hasBrush: 'true' to display brush on the chart, 'false'
		 *            otherwise.<br>
		 *            shapeClass: CSS class for the rectangles of the time
		 *            chart.<br>
		 *            dataFunction: the function that takes data and returns the
		 *            field to display in y-axis.<br>
		 *            delay: 'true' to update chart with a delay when chart data
		 *            changes; 'false' otherwise.<br>
		 *            showLinks: 'true' to highlight the links of a selection
		 *            when hovered; 'false' otherwise.<br>
		 * 
		 * @param chartListener
		 *            the listener for chart events.
		 */
		var TimeChart = function(parentView, chartConfig, chartListener) {
			Chart.call(this, parentView, chartConfig, chartListener);

			var chartDetails = chartConfig.chartDetails;

			this.hasBrush = chartDetails.hasBrush;

			// set data function
			this.dataFunction = chartDetails.dataFunction || dataFunc.identity;

			var mainGroup = this.svg.select('#mainGroup');

			if (this.hasBrush) {
				// add brush
				var that = this;

				this.brush = d3.svg.brush().on("brushend", function() {
					that.brushSelectionChanged();
				});

				mainGroup.append("g").attr("class", "brush").call(this.brush);
			}

			// add rect group element to append chart bars
			mainGroup.append('g').attr('id', 'rectGroup');

			// if chart will display names, add the group element to append bar
			// names
			if (this.showName) {
				mainGroup.append('g').attr('id', 'nameGroup');
			}

			// initialize time scale, the scale set is of type d3Scales.js not
			// d3.js scales
			this.scale = scaleFactory.getScale('time');

			// initialize bottom axis
			this.axis = d3.svg.axis().orient('bottom').scale(
					this.scale.getd3Scale()).tickSize(-45);

			// add bottom axis
			mainGroup.append("g").attr('id', 'xAxisGroup').attr("class",
					"xAxisG");

			// initialize top axis
			this.axisTop = d3.svg.axis().orient('top').scale(
					this.scale.getd3Scale()).tickFormat("").tickSize(0);

			// add top axis
			mainGroup.append("g").attr('id', 'xAxisTopGroup').attr('class',
					'xAxisG');
		};

		TimeChart.prototype = Object.create(Chart.prototype);

		TimeChart.prototype.constructor = TimeChart;

		/**
		 * Sets the data to display in this time chart. <br>
		 * Updates rectangles, scales and axis.
		 * 
		 * @param data
		 *            the data to display in the time chart.
		 */
		TimeChart.prototype.setData = function(data) {
			this.clearSelection();
			this.resizeChartElements();

			// sets data to display
			this.data = data;

			// updates scale
			this.scale.domain(this.data, this.dataFunction);
			this.scale.setTicks(this.axis); // for the moment this changes
			// nothing

			// updates bars
			var rectGroup = this.svg.select('#rectGroup').selectAll(
					'.' + this.shapeClass).data(this.data, dataFunc.name);
			this.exitRects(rectGroup);
			this.updateRects(rectGroup);
			this.enterRects(rectGroup);

			// update names
			if (this.showName) {
				var nameGroup = this.svg.select('#nameGroup').selectAll(
						'.' + this.nameClass).data(this.data);
			}

			// updates axis
			this.axis.tickFormat(this.scale.getTickFormat());

			this.svg.select('#xAxisGroup').transition().delay(this.delay)
					.duration(this.updateDuration).call(this.axis).selectAll(
							'line').style('opacity', '0.5');
			this.svg.select('#xAxisTopGroup').transition().delay(this.delay)
					.duration(this.updateDuration).call(this.axisTop);

			if (this.hasBrush) {
				this.brush.extent([ 1, 1 ]);
				this.svg.selectAll(".brush").call(this.brush);
				this.brushSelectionChanged();
			}
		};

		/**
		 * Appends the missing rect elements for name on the selection depending
		 * on the number of data of the selection.
		 * 
		 * @param selection
		 *            the text elements in the rectGroup.
		 */
		TimeChart.prototype.enterRects = function(selection) {
			var that = this;

			selection.enter().append("rect").attr("class", function(element) {
				return element.classType ? element.classType : that.shapeClass;
			}).attr("width", 0).attr("height", 0).attr("y", that.chartHeight)
					.on("click", function(element) {
						that.onClick(element);
					}).on("mouseout", function(element) {
						that.mouseOuted(element);
					}).on("mouseover", function(element) {
						that.mouseOvered(element);
					}).on("dblclick", function(element) {
						that.dblclicked(element);
					});

			this.updateRects(selection);
		};

		/**
		 * Updates the rect elements on the selection depending on the data of
		 * the selection.
		 * 
		 * @param selection
		 *            the text elements in the rectGroup.
		 */
		TimeChart.prototype.updateRects = function(selection) {
			var that = this;
			selection.transition().delay(this.delay).duration(
					this.updateDuration).attr("id", function(d) {
				return 'name' + dataFunc.name(d);
			}).attr("width", CONST.SCATTER_SQUARE_SIZE).attr("x",
					function(d, i) {
						return that.scale.scaleValue(that.dataFunction(d));
					}).attr("y", function(d) {
				return that.chartHeight - 10;
			}).attr("height", CONST.SCATTER_SQUARE_SIZE);
		};

		/**
		 * Removes the excess rect elements from the selection depending on the
		 * number of data of the selection.
		 * 
		 * @param selection
		 *            the text elements in the rectGroup.
		 */
		TimeChart.prototype.exitRects = function(selection) {
			var that = this;
			selection.exit().transition().duration(this.shortDuration).attr(
					"class", "disappear").transition().duration(
					this.updateDuration).attr("y", function(d) {
				return that.chartHeight;
			}).attr("height", 0).remove();
		};

		TimeChart.prototype.brushSelectionChanged = function() {
			var brushSelection = this.brush.extent();
			if (brushSelection[0] != undefined
					&& brushSelection[1] != undefined && brushSelection[0] != 1
					&& brushSelection[1] != 1) {
				this.parentView.filterChanged(this.parentId, this.data,
						brushSelection, this.dataFunction);
			}
		};

		/**
		 * Resizes the chart depending on parent containers size.
		 */
		TimeChart.prototype.resizeElements = function() {
			if (this.showName) {
				this.svg.select('#' + 'nameGroup').attr('transform',
						'translate(0 , ' + (this.chartHeight + 25) + ')');
			}

			// update scale ranges
			this.scale.range([ 0, this.chartWidth ]);

			// update axis length
			this.svg.select('#xAxisGroup').attr("transform",
					"translate(0, " + (this.chartHeight + 15) + ")").call(
					this.axis);

			this.svg.select('#xAxisTopGroup').attr("transform",
					"translate(0, " + (this.chartHeight - 30) + ")").call(
					this.axisTop);

			if (this.hasBrush) {
				var brushExtent = this.brush.extent();

				// update brush scale
				this.brush.x(this.scale.getd3Scale());

				// set previous extent
				if (brushExtent !== null) {
					this.brush.extent(brushExtent);
				}

				this.svg.selectAll(".brush").call(this.brush).selectAll("rect")
						.attr("y", this.chartHeight - 30).attr("height",
								this.chartHeight + 11);
			}
		};

		/**
		 * Updates the location of rects in this scatter chart.
		 */
		TimeChart.prototype.updateShapes = function() {
			this.updateRects(this.svg.select('#rectGroup').selectAll(
					'.' + this.shapeClass));
		};

		/**
		 * Highlights the specified element in this time chart. If showLinks is
		 * 'true' for this chart, also highlights the incoming and outgoing
		 * links to this element.
		 * 
		 * @param element
		 *            the element to highlight.
		 * @param fade
		 *            boolean value to indicate if other elements should fade
		 *            out or not.
		 */
		TimeChart.prototype.selectElement = function(element, fade, context) {
			var rects = this.svg.select('#rectGroup');

			if (fade) {
				this.setChildrenClass(rects, 'fade-scatter');
			}

			if (this.showLinks) {
				var that = this;
				// color incoming links
				element.incomingLinks.forEach(function(link) {
					that.setClass(rects.select('#name' + link.name), 'source');
				});

				var tempRect = undefined;

				// color outgoing links
				element.outgoingLinks.forEach(function(link) {
					tempRect = rects.select('#name' + link.name);
					if (!tempRect.empty() && tempRect.classed('source')) {
						that.setClass(tempRect, 'both');
					} else {
						that.setClass(tempRect, 'target');
					}
				});
			}

			var selectedRect = rects.select('#name' + dataFunc.name(element));
			
			// append to the end so that it can be painted on top
			var selectedRectEl = selectedRect[0][0];
			selectedRectEl.parentElement.append(selectedRectEl);

			// emphasize selection
			var center = getRectCenter(selectedRect);

			var that = this;

			var title = "name: " + dataFunc.name(element) + '\n'
					+ this.scale.getFormat()(this.dataFunction(element));

			rects.append("path").attr("d", selectionArc).attr("transform",
					"translate(" + center[0] + "," + center[1] + ")").attr(
					'id', 'selection').attr('class', 'hovered');

			// if we would like to display the name of the element only for the
			// selection chart
			if (this.horizontalAlign && this === context) {
				var names = this.svg.select('#nameGroup');
				names.append("text").attr("class", this.nameClass + " hovered")
						.style("text-anchor", "end").attr("id", 'added').attr(
								"x",
								center[0] + (CONST.SCATTER_SQUARE_SIZE / 2))
						.attr("y", 12).text(dataFunc.name(element));
			}
	
			this.removeClass(selectedRect, 'source');
			this.removeClass(selectedRect, 'target');
			this.removeClass(selectedRect, 'both');
			this.setClass(selectedRect, 'hovered');
		}

		/**
		 * Clears any highlight class added to rect elements.
		 */
		TimeChart.prototype.clearSelection = function() {
			Chart.prototype.clearSelection.call(this);

			this.svg.select('#selection').remove();
			this.clear(this.svg.select('#rectGroup'));

			// removes the added name
			this.svg.selectAll('#added').remove();
		};

		/**
		 * Sorts the elements in this time chart depending on the specified
		 * sortFunction.
		 * 
		 * @param sortFunction
		 *            the function that will be used to sort elements of this
		 *            chart.
		 */
		TimeChart.prototype.sort = function(sortFunction) {
			var rectGroup = this.svg.select('#rectGroup').selectAll(
					'.' + this.shapeClass);
			rectGroup.sort(sortFunction);
			this.updateRects(rectGroup);
		};

		// ########## DoubleTimeChart #####################################

		/**
		 * Initializes a DoubleTimeChart instance according to the configuration
		 * specified by 'chartConfig'.<br>
		 * DoubleTimeChart displays dates related to datasets on a time line.
		 * 
		 * @param chartConfig
		 *            configuration details for this TimeChart. Fields are as
		 *            follows:<br>
		 *            margins: margins of the chart inside SVG element.<br>
		 *            shapeClass: CSS class for the rectangles of the time
		 *            chart.<br>
		 *            dataFunction: the function that takes data and returns the
		 *            field to display in y-axis.<br>
		 *            delay: 'true' to update chart with a delay when chart data
		 *            changes; 'false' otherwise.<br>
		 *            showLinks: 'true' to highlight the links of a selection
		 *            when hovered; 'false' otherwise.<br>
		 * 
		 * @param chartListener
		 *            the listener for chart events.
		 */
		var DoubleTimeChart = function(parentView, chartConfig, chartListener) {
			Chart.call(this, parentView, chartConfig, chartListener);

			var chartDetails = chartConfig.chartDetails;

			// set data function
			this.dataFunctionUpper = chartDetails.dataFunctionUpper
					|| dataFunc.identity;
			this.chartNameUpper = chartDetails.chartNameUpper;

			this.dataFunctionLower = chartDetails.dataFunctionLower
					|| dataFunc.identity;
			this.chartNameLower = chartDetails.chartNameLower;

			var mainGroup = this.svg.select('#mainGroup');

			// add upper chart components

			var upperChart = mainGroup.append('g').attr('id', 'upperChart');

			// add rect group element to append chart bars
			upperChart.append('g').attr('id', 'rectGroup');

			// initialize time scale for upper chart, the scale set is of type
			// d3Scales.js not d3.js scales
			this.upperScale = scaleFactory.getScale('time');

			// initialize bottom axis for upper chart
			this.upperAxis = d3.svg.axis().orient('bottom').scale(
					this.upperScale.getd3Scale()).tickSize(-45);

			// add bottom axis
			upperChart.append("g").attr('id', 'xAxisGroup').attr("class",
					"xAxisG");

			// initialize top axis for upper chart
			this.upperAxisTop = d3.svg.axis().orient('top').scale(
					this.upperScale.getd3Scale()).tickFormat("").tickSize(0);

			// add top axis
			upperChart.append("g").attr('id', 'xAxisTopGroup').attr('class',
					'xAxisG');

			// add lower chart components

			var lowerChart = mainGroup.append('g').attr('id', 'lowerChart');

			// add rect group element to append chart bars
			lowerChart.append('g').attr('id', 'rectGroup');

			// initialize time scale, the scale set is of type d3Scales.js
			// not d3.js scales
			this.lowerScale = scaleFactory.getScale('time');

			// initialize bottom axis for lower chart
			this.lowerAxis = d3.svg.axis().orient('bottom').scale(
					this.lowerScale.getd3Scale()).tickSize(-45);

			// add bottom axis
			lowerChart.append("g").attr('id', 'xAxisGroup').attr("class",
					"xAxisG");

			// initialize top axis for lower chart
			this.lowerAxisTop = d3.svg.axis().orient('top').scale(
					this.lowerScale.getd3Scale()).tickFormat("").tickSize(0);

			// add top axis
			lowerChart.append("g").attr('id', 'xAxisTopGroup').attr('class',
					'xAxisG');
		};

		DoubleTimeChart.prototype = Object.create(Chart.prototype);

		DoubleTimeChart.prototype.constructor = DoubleTimeChart;

		/**
		 * Sets the data to display in this time chart. <br>
		 * Updates rectangles, scales and axis.
		 * 
		 * @param data
		 *            the data to display in the time chart.
		 */
		DoubleTimeChart.prototype.setData = function(data) {
			this.clearSelection();
			this.resizeChartElements();

			// sets data to display
			this.data = data;

			// updates upper scale
			this.upperScale.domain(this.data, this.dataFunctionUpper);
			this.upperScale.setTicks(this.upperAxis); // for the moment this
			// changes nothing

			var upperChart = this.svg.select('#upperChart');
			upperChart.append('text').text(this.chartNameUpper).attr('class',
					'chart-title')
					.attr('transform', 'translate(0, ' + 15 + ')');

			// updates upper bars
			var upperRectGroup = upperChart.select('#rectGroup').selectAll(
					'.' + this.shapeClass).data(this.data, dataFunc.name);
			this.exitRects(upperRectGroup);
			this.updateRects(upperRectGroup, this.upperScale,
					this.dataFunctionUpper);
			this.enterRects(upperRectGroup, this.upperScale,
					this.dataFunctionUpper);

			// updates upper axis
			this.upperAxis.tickFormat(this.upperScale.getTickFormat());

			upperChart.select('#xAxisGroup').transition().delay(this.delay)
					.duration(this.updateDuration).call(this.upperAxis)
					.selectAll('line').style('opacity', '0.5');
			upperChart.select('#xAxisTopGroup').transition().delay(this.delay)
					.duration(this.updateDuration).call(this.upperAxisTop);

			// updates lower bars

			// updates lower scale
			this.lowerScale.domain(this.data, this.dataFunctionLower);
			this.lowerScale.setTicks(this.lowerAxis); // for the moment this
			// changes nothing

			var lowerChart = this.svg.select('#lowerChart');
			lowerChart.append('text').text(this.chartNameLower).attr('class',
					'chart-title')
					.attr('transform', 'translate(0, ' + 15 + ')');

			var lowerRectGroup = lowerChart.select('#rectGroup').selectAll(
					'.' + this.shapeClass).data(this.data, dataFunc.name);
			this.exitRects(lowerRectGroup);
			this.updateRects(lowerRectGroup, this.lowerScale,
					this.dataFunctionLower);
			this.enterRects(lowerRectGroup, this.lowerScale,
					this.dataFunctionLower);

			// updates lower axis
			this.lowerAxis.tickFormat(this.lowerScale.getTickFormat());

			lowerChart.select('#xAxisGroup').transition().delay(this.delay)
					.duration(this.updateDuration).call(this.lowerAxis)
					.selectAll('line').style('opacity', '0.5');
			lowerChart.select('#xAxisTopGroup').transition().delay(this.delay)
					.duration(this.updateDuration).call(this.lowerAxisTop);
		};

		/**
		 * Appends the missing rect elements for name on the selection depending
		 * on the number of data of the selection.
		 * 
		 * @param selection
		 *            the text elements in the rectGroup.
		 * @param scale
		 *            the scale for the selection.
		 * @param dataFunction
		 *            the data function for the selection.
		 */
		DoubleTimeChart.prototype.enterRects = function(selection, scale,
				dataFunction) {
			var that = this;

			selection.enter().append("rect").attr("class", that.shapeClass).on(
					"click", function(element) {
						that.onClick(element);
					}).on("mouseout", function(element) {
				that.mouseOuted(element);
			}).on("mouseover", function(element) {
				that.mouseOvered(element);
			}).on("dblclick", function(element) {
				that.dblclicked(element);
			}).attr("width", 0).attr("height", 0).attr("y",
					CONST.TIME_CHART_HEIGHT);

			this.updateRects(selection, scale, dataFunction);
		};

		/**
		 * Updates the rect elements on the selection depending on the data of
		 * the selection.
		 * 
		 * @param selection
		 *            the text elements in the rectGroup.
		 * @param scale
		 *            the scale for the selection.
		 * @param dataFunction
		 *            the data function for the selection.
		 */
		DoubleTimeChart.prototype.updateRects = function(selection, scale,
				dataFunction) {
			var that = this;
			selection.transition().delay(this.delay).duration(
					this.updateDuration).attr("id", function(d) {
				return 'name' + dataFunc.name(d);
			}).attr("width", CONST.SCATTER_SQUARE_SIZE).attr("x",
					function(d, i) {
						return scale.scaleValue(dataFunction(d));
					}).attr("y", function(d) {
				return CONST.TIME_CHART_HEIGHT - 25;
			}).attr("height", CONST.SCATTER_SQUARE_SIZE);
		};

		/**
		 * Removes the excess rect elements from the selection depending on the
		 * number of data of the selection.
		 * 
		 * @param selection
		 *            the text elements in the rectGroup.
		 */
		DoubleTimeChart.prototype.exitRects = function(selection) {
			var that = this;
			selection.exit().transition().duration(this.shortDuration).attr(
					"class", "disappear").transition().duration(
					this.updateDuration).attr("y", function(d) {
				return that.chartHeight;
			}).attr("height", 0).remove();
		};

		/**
		 * Resizes the chart depending on parent containers size.
		 */
		DoubleTimeChart.prototype.resizeElements = function() {
			var mainGroup = this.svg.select('#mainGroup');

			// resize upper part
			// update scale ranges
			this.upperScale.range([ 0, this.chartWidth ]);

			var upperChart = mainGroup.select('#upperChart');

			// update axis length
			upperChart.select('#xAxisGroup').attr('transform',
					'translate(0, ' + (CONST.TIME_CHART_HEIGHT) + ')').call(
					this.upperAxis);

			upperChart.select('#xAxisTopGroup').attr("transform",
					"translate(0, " + (CONST.TIME_CHART_HEIGHT - 45) + ")")
					.call(this.upperAxisTop);

			// resize lower part
			this.lowerScale.range([ 0, this.chartWidth ]);

			var lowerChart = mainGroup.select('#lowerChart');

			lowerChart.attr('transform', 'translate(' + 0 + ','
					+ (CONST.TIME_CHART_HEIGHT + 30) + ')');
			// update axis length
			lowerChart.select('#xAxisGroup').attr('transform',
					'translate(0, ' + CONST.TIME_CHART_HEIGHT + ')').call(
					this.lowerAxis);

			lowerChart.select('#xAxisTopGroup').attr("transform",
					"translate(0, " + (CONST.TIME_CHART_HEIGHT - 45) + ")")
					.call(this.lowerAxisTop);
		};

		/**
		 * Updates the location of rects in this scatter chart.
		 */
		DoubleTimeChart.prototype.updateShapes = function() {
			this.updateRects(this.svg.select('#upperChart')
					.select('#rectGroup').selectAll('.' + this.shapeClass),
					this.upperScale, this.dataFunctionUpper);
			this.updateRects(this.svg.select('#lowerChart')
					.select('#rectGroup').selectAll('.' + this.shapeClass),
					this.lowerScale, this.dataFunctionLower);
		};

		/**
		 * Highlights the specified element in this time chart. If showLinks is
		 * 'true' for this chart, also highlights the incoming and outgoing
		 * links to this element.
		 * 
		 * @param element
		 *            the element to highlight.
		 * @param fade
		 *            boolean value to indicate if other elements should fade
		 *            out or not.
		 */
		DoubleTimeChart.prototype.selectElement = function(element, fade,
				context) {
			var rectsUpper = this.svg.select('#upperChart')
					.select('#rectGroup');
			var rectsLower = this.svg.select('#lowerChart')
					.select('#rectGroup');

			if (fade) {
				this.setChildrenClass(rectsUpper, 'fade-scatter');
				this.setChildrenClass(rectsLower, 'fade-scatter');
			}

			if (this.showLinks) {
				var that = this;
				// color incoming links
				element.incomingLinks.forEach(function(link) {
					setElementClass(rectsUpper, link, 'source');
					setElementClass(rectsLower, link, 'source');
				});

				var tempLink = undefined;
				// color outgoing links
				element.outgoingLinks
						.forEach(function(link) {
							tempLink = rectsUpper.select('#name'
									+ dataFunc.name(link));
							if (!tempLink.empty() && tempLink.classed('source')) {
								removeElementClass(rectsUpper, link, 'source');
								removeElementClass(rectsLower, link, 'source');
								setElementClass(rectsUpper, link, 'both');
								setElementClass(rectsLower, link, 'both');
							} else {
								setElementClass(rectsUpper, link, 'target');
								setElementClass(rectsLower, link, 'target');
							}
						});
			}

			// emphasize selection in upper chart
			var selectedRectUpper = rectsUpper.select('#name'
					+ dataFunc.name(element));
			
			// append to the end so that it can be painted on top
			var selectedRectUpperEl = selectedRectUpper[0][0];
			selectedRectUpperEl.parentElement.append(selectedRectUpperEl);


			var center = getRectCenter(selectedRectUpper);

			var that = this;

			var titleUpper = "name: "
					+ dataFunc.name(element)
					+ '\n'
					+ this.upperScale.getFormat()(
							this.dataFunctionUpper(element));

			rectsUpper.append("path").attr("d", selectionArc).attr("transform",
					"translate(" + center[0] + "," + center[1] + ")").attr(
					'id', 'selection').attr('class', 'hovered');

			selectedRectUpper.append('title').attr('class', 'selection-title')
					.text(titleUpper);

			// emphasize selection in lower chart
			var selectedRectLower = rectsLower.select('#name'
					+ dataFunc.name(element));
			
			// append to the end so that it can be painted on top
			var selectedRectLowerEl = selectedRectLower[0][0];
			selectedRectLowerEl.parentElement.append(selectedRectLowerEl);


			center = getRectCenter(selectedRectLower);

			var titleLower = "name: "
					+ dataFunc.name(element)
					+ '\n'
					+ this.lowerScale.getFormat()(
							this.dataFunctionLower(element));

			rectsLower.append("path").attr("d", selectionArc).attr("transform",
					"translate(" + center[0] + "," + center[1] + ")").attr(
					'id', 'selection').attr('class', 'hovered');

			selectedRectLower.append('title').attr('class', 'selection-title')
					.text(titleLower);

			this.removeClass(selectedRectUpper, 'source');
			this.removeClass(selectedRectUpper, 'target');
			this.removeClass(selectedRectUpper, 'both');
			this.setClass(selectedRectUpper, 'hovered');

			this.removeClass(selectedRectLower, 'source');
			this.removeClass(selectedRectLower, 'target');
			this.removeClass(selectedRectLower, 'both');
			this.setClass(selectedRectLower, 'hovered');
		}

		/**
		 * Clears any highlight class added to rect elements.
		 */
		DoubleTimeChart.prototype.clearSelection = function() {
			Chart.prototype.clearSelection.call(this);

			this.svg.select('#selection').remove();
			this.svg.select('#selection-title').remove();
			
			this.clear(this.svg.select('#upperChart').select('#rectGroup'));
			this.clear(this.svg.select('#lowerChart').select('#rectGroup'));
		};

		// ########## EdgeChart #####################################
		/**
		 * Initializes an EdgeChart instance according to the configuration
		 * specified by 'chartConfig'.<br>
		 * EdgeChart displays dataset names on a circle and shows the links in
		 * between.
		 * 
		 * @param chartConfig
		 *            configuration details for this EdgeChart. Fields are as
		 *            follows:<br>
		 *            nameClass: CSS class for the name strings.<br>
		 *            delay: 'true' to update chart with a delay when chart data
		 *            changes; 'false' otherwise.<br>
		 *            showLinks: 'true' to highlight the links of a selection
		 *            when hovered; 'false' otherwise.<br>
		 * 
		 * @param chartListener
		 *            the listener for chart events.
		 */
		var EdgeChart = function(parentView, chartConfig, chartListener) {
			Chart.call(this, parentView, chartConfig, chartListener);

			this.linkClass = chartConfig.chartDetails.linkClass;

			var mainGroup = this.svg.select('#mainGroup');

			// add group for links
			mainGroup.append('g').attr('id', 'linkGroup');

			// add group for nodes
			mainGroup.append('g').attr('id', 'nameGroup');
		};

		EdgeChart.prototype = Object.create(Chart.prototype);

		EdgeChart.prototype.constructor = EdgeChart;

		/**
		 * Sets the data to display in the edge chart. <br>
		 * Updates links and names.
		 * 
		 * @param data
		 *            the data to display in the edge chart.
		 */
		EdgeChart.prototype.setData = function(data) {
			// console.log('called set data for edge');
			this.clearSelection();
			this.resizeChartElements();

			// cluster layout which computes location of the dataset names
			var clusterNodes = d3.layout.cluster().size(
					[ 360, this.innerRadius ])
			// to avoid to transform data to hierarchical format
			.children(function(d) {
				return d;
			}).nodes(data);

			// update names
			var names = this.svg.select('#nameGroup').selectAll(
					'.' + this.nameClass).data(clusterNodes[0]);
			// take clusterNodes[0] since it returns the node array, others are
			// repeating the elements of 0th node
			// console.log('clusterNodes');
			// console.log(clusterNodes);
			this.exitNames(names);
			this.updateNames(names);
			this.enterNames(names);

			// update links
			// create link paths from source to target datasets
			var linkPaths = d3.layout.bundle()(this.createLinkSet(data));
			linkPaths.forEach(function(d) {
				d.source = d[0], d.target = d[d.length - 1];
			});

			var links = this.svg.select('#linkGroup').selectAll(
					'.' + this.linkClass).data(linkPaths);
			this.exitLinks(links);
			this.updateLinks(links);
			this.enterLinks(links);
		};

		/**
		 * Appends the missing text elements on the selection depending on the
		 * number of data of the selection.
		 * 
		 * @param selection
		 *            the text elements in the nameGroup.
		 */
		EdgeChart.prototype.enterNames = function(selection) {
			var that = this;
			selection.enter().append("text").attr("class", that.nameClass).on(
					"click", function(element) {
						that.onClick(element);
					}).on("mouseout", function(element) {
				that.mouseOuted(element);
			}).on("mouseover", function(element) {
				that.mouseOvered(element);
			}).on("dblclick", function(element) {
				that.dblclicked(element);
			});

			this.updateNames(selection);
		};

		/**
		 * Updates the text elements on the selection depending on the data of
		 * the selection.
		 * 
		 * @param selection
		 *            the text elements in the nameGroup.
		 */
		EdgeChart.prototype.updateNames = function(selection) {
			var that = this;
			selection.transition().delay(this.delay).duration(
					this.updateDuration).attr("id", function(d) {
				return 'name' + dataFunc.name(d);
			}).attr("dy", ".31em").attr(
					"transform",
					function(d) {
						return "rotate(" + (d.x - 90) + ") translate("
								+ (d.y + 5) + ",0)"
								+ (d.x < 180 ? "" : "rotate(180)");
					}).style("text-anchor", function(d) {
				return d.x < 180 ? "start" : "end";
			}).text(function(d) {
				return getName(d);
			});
		};

		/**
		 * Removes the excess text elements from the selection depending on the
		 * number of data of the selection.
		 * 
		 * @param selection
		 *            the text elements in the nameGroup.
		 */
		EdgeChart.prototype.exitNames = function(selection) {
			selection.exit().transition().duration(this.shortDuration).attr(
					"class", "disappear").transition().duration(
					this.updateDuration).attr("text", function(d) {
				return "";
			}).attr(
					"transform",
					function(d) {
						return "rotate(20)translate(" + (d.y + 8) + ",0)"
								+ (d.x < 180 ? "" : "rotate(180)");
					}).style("text-anchor", function(d) {
				return d.x < 180 ? "end" : "end";
			}).remove();
		};

		/**
		 * Appends the missing path elements on the selection depending on the
		 * number of data of the selection.
		 * 
		 * @param selection
		 *            the path elements in the linkGroup.
		 */
		EdgeChart.prototype.enterLinks = function(selection) {
			selection.enter().append("path");

			this.updateLinks(selection);
		}

		/**
		 * Updates the path elements on the selection depending on the data of
		 * the selection.
		 * 
		 * @param selection
		 *            the path elements in the linkGroup.
		 */
		EdgeChart.prototype.updateLinks = function(selection) {
			// construct line generator to draw lines
			var line = d3.svg.line.radial().interpolate('bundle').tension(.85)
					.radius(function(d) {
						return d.y;
					}).angle(function(d) {
						return d.x / 180 * Math.PI;
					});

			selection.attr("class", 'link-disappear').attr('id', function(d) {
				return 'name' + d.source.name + d.target.name;
			}).attr("d", line).transition().delay(2 * this.updateDuration)
					.duration(this.updateDuration)
					.attr("class", this.linkClass);
		}

		/**
		 * Removes the excess path elements from the selection depending on the
		 * number of data of the selection.
		 * 
		 * @param selection
		 *            the path elements in the linkGroup.
		 */
		EdgeChart.prototype.exitLinks = function(selection) {
			selection.exit().transition().duration(this.shortDuration).attr(
					"class", "mark-link-disappear").transition().duration(
					this.updateDuration).attr("class", "link-disappear").attr(
					"d", "").remove();
		}

		/**
		 * Resizes the chart depending on parent container's size.
		 */
		EdgeChart.prototype.resizeElements = function() {
			var radius = this.chartHeight / 2;
			this.innerRadius = radius - 50;

			var x = (this.chartWidth + this.margins.left + this.margins.right) / 2.0;
			var y = (this.chartHeight + this.margins.top + this.margins.bottom) / 2.0;

			var mainGroup = this.svg.select('#mainGroup');

			mainGroup.attr('transform', 'translate(' + x + ', ' + y + ')');
		};

		/**
		 * Updates the location of names and links in this edge chart.
		 */
		EdgeChart.prototype.updateShapes = function() {
			this.updateNames(this.svg.select('#nameGroup').selectAll(
					'.' + this.nameClass));
			this.updateLinks(this.svg.select('#linkGroup').selectAll(
					'.' + this.linkClass));
		};

		/**
		 * Highlights the specified element in this edge chart. If showLinks is
		 * 'true' for this chart, also highlights the incoming and outgoing
		 * links to this element.
		 * 
		 * @param element
		 *            the element to highlight.
		 * @param fade
		 *            boolean value to indicate if other elements should fade
		 *            out or not.
		 */
		EdgeChart.prototype.selectElement = function(element, fade, context) {
			var links = this.svg.select('#' + 'linkGroup');
			var names = this.svg.select('#' + 'nameGroup');

			this.setChildrenClass(links, 'fade');
			if (fade) {
				this.setChildrenClass(names, 'fade');
			}

			if (this.showLinks) {
				var that = this;
				// color incoming links
				element.incomingLinks.forEach(function(link) {
					that.setClass(links.select('#name' + link.name
							+ element.name), 'source-link');
					that.setClass(names.select('#name' + link.name), 'source');
				});

				var tempLink = undefined;

				// color outgoing links
				element.outgoingLinks
						.forEach(function(link) {
							tempLink = links.select('#name' + link.name
									+ element.name);

							if (!tempLink.empty()
									&& tempLink.classed('source-link')) {

								that.removeClass(tempLink, 'source-link');
								that.removeClass(names.select('#name'
										+ link.name), 'source');
								that.setClass(tempLink, 'both-link');
								that.setClass(
										names.select('#name' + link.name),
										'both');

							} else {
								that.setClass(links.select('#name'
										+ element.name + link.name),
										'target-link');
								that.setClass(
										names.select('#name' + link.name),
										'target');
							}
						});
			}

			this.setClass(names.select('#name' + dataFunc.name(element)),
					'hovered');
			this.removeClass(names.select('#name' + dataFunc.name(element)),
					'target');
			this.removeClass(names.select('#name' + dataFunc.name(element)),
					'source');
			this.removeClass(names.select('#name' + dataFunc.name(element)),
					'both');
		}

		/**
		 * Clears any highlight class added to bar and name elements.
		 */
		EdgeChart.prototype.clearSelection = function() {
			Chart.prototype.clearSelection.call(this);
			var links = this.svg.select('#' + 'linkGroup');
			var names = this.svg.select('#' + 'nameGroup');

			this.clear(links);
			this.clear(names);
		};

		/**
		 * Creates links in the form of { source: sourceNode, target:
		 * targetNode} from the outgoing links of the dataset nodes in the
		 * dataset list.
		 * 
		 * @returns {Array}
		 */
		EdgeChart.prototype.createLinkSet = function(data) {
			var map = {};

			// Compute a map from name to node.
			data.forEach(function(d) {
				map[d.name] = d;
			});

			var linkset = [];

			// for each node in the data, create link set for outgoing links
			data.forEach(function(d) {
				d.outgoingLinks.forEach(function(link) {
					// include only the links where target node exists in
					// dataset list
					if (map[link.name]) {
						linkset.push({
							source : d,
							target : map[link.name]
						});
					}
				});

			});

			return linkset;
		};

		// ########## MatrixChart #####################################
		/**
		 * Initializes a MatrixChart instance according to the configuration
		 * specified by 'chartConfig'.<br>
		 * MatrixChart displays source datasets on left column and target
		 * datasets on top row. Inbetween in filled with cells showing the
		 * number of links from source dataset to target dataset.
		 * 
		 * @param chartConfig
		 *            configuration details for this MatrixChart. Fields are as
		 *            follows:<br>
		 *            margins: margins of the chart inside SVG element.<br>
		 *            color: the color of cells where link count is greater than
		 *            0. For higher number of links a darker tone of the color
		 *            is used, for fewer number of links a lighter tone is used.<br>
		 *            delay: 'true' to update chart with a delay when chart data
		 *            changes; 'false' otherwise.<br>
		 *            showLinks: 'true' to highlight the links of a selection
		 *            when hovered; 'false' otherwise.<br>
		 * 
		 * @param chartListener
		 *            the listener for chart events.
		 */
		var MatrixChart = function(parentView, chartConfig, chartListener) {
			Chart.call(this, parentView, chartConfig, chartListener);
			var chartDetails = chartConfig.chartDetails;

			// initialize scales for names on the left and on the top
			this.leftScale = scaleFactory.getScale('ordinal');
			this.topScale = scaleFactory.getScale('ordinal');

			// set color scales
			this.colorScale = d3.scale.category20b();
			this.colorScale2 = d3.scale.log();
			this.color = d3.rgb(chartDetails.color) || d3.rgb("aqua");

			var mainGroup = this.svg.select('#mainGroup');

			// add group for top names
			mainGroup
					.append('g')
					.attr('id', 'topGroup')
					.attr(
							'transform',
							'translate('
									+ (2 * this.margins.left + 2 * CONST.MATRIX_PADDING)
									+ ', '
									+ (this.margins.top - 2 * CONST.MATRIX_PADDING)
									+ ')');

			// add group for left names
			mainGroup.append('g').attr('id', 'leftGroup').attr(
					'transform',
					'translate(' + this.margins.left + ', '
							+ (this.margins.top) + ')');

			// add group for rows for cells
			mainGroup.append('g').attr('id', 'rowGroup').attr(
					'transform',
					'translate('
							+ (2 * this.margins.left + CONST.MATRIX_PADDING)
							+ ', ' + (this.margins.top - CONST.MATRIX_PADDING)
							+ ')');
		};

		MatrixChart.prototype = Object.create(Chart.prototype);

		MatrixChart.prototype.constructor = MatrixChart;

		/**
		 * Sets the data to display in the matrix chart. <br>
		 * Updates top names, left names, scales and cells.
		 * 
		 * @param data
		 *            the data to display in the matrix chart.
		 */
		MatrixChart.prototype.setData = function(data) {
			this.clearSelection();
			this.resizeChartElements();

			// sets data to display
			this.data = data;

			// creates the link matrix to display link counts in the cells
			this.createLinkMatrix();

			// updates scales
			this.leftScale.domain(this.leftData);
			this.topScale.domain(this.topData);

			// update names on the top
			var topNames = this.svg.select('#topGroup').selectAll(
					'.' + this.nameClass).data(this.topData, dataFunc.name);
			this.exitTopNames(topNames);
			this.updateTopNames(topNames);
			this.enterTopNames(topNames);

			// update names on the left
			var leftNames = this.svg.select('#leftGroup').selectAll(
					'.' + this.nameClass).data(this.leftData, dataFunc.name);
			this.exitLeftNames(leftNames);
			this.updateLeftNames(leftNames);
			this.enterLeftNames(leftNames);

			// update rows
			var rows = this.svg.select('#rowGroup').selectAll('.row').data(
					this.leftData, dataFunc.name);
			this.exitRows(rows);
			this.updateRows(rows);
			this.enterRows(rows);

			// update cells
			var cells = rows.selectAll('.square').data(this.topData,
					dataFunc.name);
			this.exitCells(cells);
			this.updateCells(cells);
			this.enterCells(cells);
		};

		/**
		 * Appends the missing text elements on the selection depending on the
		 * number of data of the selection.
		 * 
		 * @param selection
		 *            the text elements in the topGroup.
		 */
		MatrixChart.prototype.enterTopNames = function(selection) {
			var that = this;

			selection.enter().append("text").attr("x", this.chartHeight).style(
					"text-anchor", "start").on("click", function(element) {
				that.onClick(element);
			}).on("mouseout", function(element) {
				that.mouseOuted(element);
			}).on("mouseover", function(element) {
				that.mouseOvered(element);
			}).on("dblclick", function(element) {
				that.dblclicked(element);
			});

			this.updateTopNames(selection);
		};

		/**
		 * Updates the text elements on the selection depending on the data of
		 * the selection.
		 * 
		 * @param selection
		 *            the text elements in the topGroup.
		 */
		MatrixChart.prototype.updateTopNames = function(selection) {
			var that = this;

			selection.transition().delay(this.delay).duration(
					this.updateDuration).attr("id", function(d) {
				return 'name' + dataFunc.name(d);
			}).attr("class", function(d, i) {
				if (i >= that.data.length)
					return ' additional-link';
				else
					return that.nameClass;
			}).attr("x", 0).attr(
					"y",
					function(d, i) {
						return that.topScale.scaleValue(i + 1)
								+ (that.topScale.rangeValue() / 3);
					}).text(function(d) {
				return getName(d);
			}).attr("transform", "rotate(-90)");
		};

		/**
		 * Removes the excess text elements from the selection depending on the
		 * number of data of the selection.
		 * 
		 * @param selection
		 *            the text elements in the topGroup.
		 */
		MatrixChart.prototype.exitTopNames = function(selection) {
			selection.exit().transition().duration(this.shortDuration).attr(
					"class", "disappear").transition().duration(
					this.updateDuration).attr("x", this.chartHeight)
					.transition().duration(this.updateDuration).text("")
					.remove();
		};

		/**
		 * Appends the missing text elements on the selection depending on the
		 * number of data of the selection.
		 * 
		 * @param selection
		 *            the text elements in the leftGroup.
		 */
		MatrixChart.prototype.enterLeftNames = function(selection) {
			var that = this;
			selection.enter().append("text").attr("x", 0).style("text-anchor",
					"end").on("click", function(element) {
				that.onClick(element);
			}).on("mouseout", function(element) {
				that.mouseOuted(element);
			}).on("mouseover", function(element) {
				that.mouseOvered(element);
			}).on("dblclick", function(element) {
				that.dblclicked(element);
			});

			this.updateLeftNames(selection);
		};

		/**
		 * Updates the text elements on the selection depending on the data of
		 * the selection.
		 * 
		 * @param selection
		 *            the text elements in the leftGroup.
		 */
		MatrixChart.prototype.updateLeftNames = function(selection) {
			var that = this;

			selection.transition().delay(this.delay).duration(
					this.updateDuration).attr("id", function(d) {
				return 'name' + dataFunc.name(d);
			}).attr("class", function(d, i) {
				if (i >= that.data.length)
					return ' additional-link';
				else
					return that.nameClass;
			}).attr("x", that.margins.left).attr(
					"y",
					function(d, i) {
						return that.leftScale.scaleValue(i + 1)
								+ (that.leftScale.rangeValue() / 3);
					}).text(function(d) {
				return getName(d);
			});
		};

		/**
		 * Removes the excess text elements from the selection depending on the
		 * number of data of the selection.
		 * 
		 * @param selection
		 *            the text elements in the leftGroup.
		 */
		MatrixChart.prototype.exitLeftNames = function(selection) {
			selection.exit().transition().duration(this.shortDuration).attr(
					"class", "disappear").transition().duration(
					this.updateDuration).attr("x", 0).transition().duration(
					this.updateDuration).text("").remove();
		};

		/**
		 * Appends the missing rows on the selection depending on the number of
		 * data of the selection.
		 * 
		 * @param selection
		 *            the rows in the rowGroup.
		 */
		MatrixChart.prototype.enterRows = function(selection) {
			selection.enter().append("g").attr("class", "row");

			this.updateRows(selection);
		};

		/**
		 * Updates the rows on the selection depending on the data of the
		 * selection.
		 * 
		 * @param selection
		 *            the rows in the rowGroup.
		 */
		MatrixChart.prototype.updateRows = function(selection) {
			var that = this;

			selection.transition().delay(this.delay).duration(
					this.updateDuration).attr("id", function(d) {
				return "row" + dataFunc.name(d);
			}).attr(
					"transform",
					function(d, i) {
						return "translate(0, "
								+ that.leftScale.scaleValue(i + 1) + ")";
					});
		};

		/**
		 * Removes the excess rows and their cells from the selection depending
		 * on the number of data of the selection.
		 * 
		 * @param selection
		 *            the rows in the rowGroup.
		 */
		MatrixChart.prototype.exitRows = function(selection) {
			selection.exit().transition().duration(this.shortDuration)
					.selectAll(".square").attr("class", "disappear")
					.transition().duration(this.updateDuration)
					.attr("width", 0).attr("height", 0).attr("transform",
							"translate(0, 0)");

			selection.exit().transition().delay(
					this.shortDuration + this.updateDuration).remove();
		};

		/**
		 * Appends the missing cells on the selection depending on the number of
		 * data of the selection.
		 * 
		 * @param selection
		 *            the cells in a specific row in the rowGroup.
		 */
		MatrixChart.prototype.enterCells = function(selection) {
			selection.enter().append("rect").attr("class", "square").attr(
					"width", 0).attr("height", 0);

			this.updateCells(selection);
		}

		/**
		 * Updates the cells on the selection depending on the data of the
		 * selection.
		 * 
		 * @param selection
		 *            the cells in a specific row in the rowGroup.
		 */
		MatrixChart.prototype.updateCells = function(selection) {
			var that = this;

			selection.transition().delay(this.delay).duration(
					this.updateDuration).attr("id", function(d, i, j) {
				return "column" + dataFunc.name(d);
			}).attr("width", that.topScale.rangeValue()).attr("height",
					that.leftScale.rangeValue()).attr("x", function(d, i) {
				return that.topScale.scaleValue(i + 1);
			}).attr("y", 0).attr(
					"fill",
					function(d, i, j) {
						var value = that.linkMatrix[j][i];
						// var returnVal = value == 0 ? "rgb(238,238,238)" :
						// colorScale(value);
						var returnVal = value == 0 ? "rgb(238,238,238)"
								: that.color.darker(that.colorScale2(value));

						return returnVal;
					});

			selection.on(
					"mouseover",
					function(d, i, j) {
						// mark target column, filter the cells which have the
						// same column as selected cell's column
						that.svg.selectAll(".row").selectAll(".square").filter(
								function(cell, k, l) {
									return i == k;
								}).classed("square-hover", true);

						// set column name as hovered
						that.svg.select('#topGroup').select(
								"#name" + dataFunc.name(d)).classed("hovered",
								true);

						// mark source row
						d3.select(this.parentNode).selectAll(".square")
								.classed("square-hover", true);

						// set row name as hovered
						that.svg.select('#leftGroup').select(
								"#name"
										+ dataFunc.name(d3.select(
												this.parentNode).datum()))
								.classed("hovered", true);

					}).on(
					"mouseout",
					function(d, i, j) {
						that.svg.selectAll(".row").selectAll(".square")
								.classed("square-hover", false);

						that.svg.select('#topGroup').select(
								"#name" + dataFunc.name(d)).classed("hovered",
								false);

						that.svg.select('#leftGroup').select(
								"#name"
										+ dataFunc.name(d3.select(
												this.parentNode).datum()))
								.classed("hovered", false);
					});

			selection.append("title").text(
					function(d, i, j, k) {
						return dataFunc.name(d3.select(
								this.parentNode.parentNode).datum())
								+ " → "
								+ dataFunc.name(d)
								+ "\n"
								+ d3.format(",")(that.linkMatrix[j][i])
								+ " links";
					}).attr("class", "chartinfo"); // this style does not exist
			// in style file
		};

		/**
		 * Removes the excess cells from the selection depending on the number
		 * of data of the selection.
		 * 
		 * @param selection
		 *            the cells in the rowGroup.
		 */
		MatrixChart.prototype.exitCells = function(selection) {
			selection.exit().transition().duration(this.shortDuration).attr(
					"class", "disappear").transition().duration(
					this.updateDuration).attr("width", 0).attr("height", 0)
					.remove();
		};

		/**
		 * Resizes the chart depending on parent containers size.
		 */
		MatrixChart.prototype.resizeElements = function() {
			var x = (this.chartWidth - this.chartHeight) / 2.0;
			var y = this.margins.top;

			var mainGroup = this.svg.select('#mainGroup');

			mainGroup.attr('transform', 'translate(' + x + ', ' + y + ')');

			// update scale ranges
			this.topScale.range([ 0, this.chartHeight ]);
			this.leftScale.range([ 0, this.chartHeight ]);
		};

		/**
		 * Updates the location of bars and names in this bar chart.
		 */
		MatrixChart.prototype.updateShapes = function() {

			// update location names and cells
			this.updateTopNames(this.svg.select('#topGroup').selectAll(
					'.' + this.nameClass));
			this.updateLeftNames(this.svg.select('#leftGroup').selectAll(
					'.' + this.nameClass));
			var rows = this.svg.select('#rowGroup').selectAll('.row');
			this.updateRows(rows);
			this.updateCells(rows.selectAll('.square'));
		};

		/**
		 * Creates a matrix that holds the number of links from source to target
		 * datasets.
		 */
		MatrixChart.prototype.createLinkMatrix = function() {
			var dataLength = this.data.length;
			this.topData = this.data;
			this.leftData = this.data;
			var that = this;

			// create index matrix for node names
			this.indexMatrix = [];
			for (var i = 0; i < dataLength; i++) {
				var node = this.data[i];
				this.indexMatrix[node.name] = i;
			}

			// initialize linkMatrix with zeros
			this.linkMatrix = [];

			for (var i = 0; i <= dataLength; i++) {
				var row = [];
				for (var j = 0; j <= dataLength; j++) {
					row.push(0);
				}
				this.linkMatrix.push(row);
			}

			// fill linkMatrix with number of links for datasets.
			for (var i = 0; i < dataLength; i++) {
				var row = this.linkMatrix[i];
				var node = this.data[i];

				node.outgoingLinks.forEach(function(link) {
					var index = that.indexMatrix[dataFunc.name(link)];
					if (index)
						row[index] = dataFunc.linkCount(link);
				});
			}

		};

		/**
		 * Highlights the specified element in this matrix chart. If showLinks
		 * is 'true' for this chart, also highlights the incoming and outgoing
		 * links to this element.
		 * 
		 * @param element
		 *            the element to highlight.
		 * @param fade
		 *            boolean value to indicate if other elements should fade
		 *            out or not.
		 */
		MatrixChart.prototype.selectElement = function(element, fade, context) {
			var that = this;

			var topNames = this.svg.select('#' + 'topGroup');
			var leftNames = this.svg.select('#' + 'leftGroup');
			var rows = this.svg.select('#' + 'rowGroup');
			var squares = rows.selectAll('.row');

			if (fade) {
				this.setChildrenClass(topNames, 'fade');
				this.setChildrenClass(leftNames, 'fade');
				this.setChildrenClass(squares, 'fade');
			}

			if (this.showLinks) {
				// color incoming links
				element.incomingLinks.forEach(function(link) {
					that.setClass(leftNames.select('#name' + link.name),
							'source');
					that.removeChildrenClass(rows.select('#row' + link.name),
							'fade');
				});

				var tempLink = undefined;
				// color outgoing links
				element.outgoingLinks.forEach(function(link) {
					tempLink = leftNames.select('#name' + link.name);

					if (!tempLink.empty() && tempLink.classed('source')) {
						that.setClass(topNames.select('#name' + link.name),
								'both');

						that.removeClass(leftNames.select('#name' + link.name),
								'source');
						that.setClass(leftNames.select('#name' + link.name),
								'both');
					} else {

						that.setClass(topNames.select('#name' + link.name),
								'target');
					}
					that.removeClass(rows.selectAll('#column' + link.name),
							'fade');
				});
			}

			// set selected dataset column hovered
			this.setClass(rows.selectAll('#column' + dataFunc.name(element)),
					'square-hover');
			this.removeClass(
					rows.selectAll('#column' + dataFunc.name(element)), 'fade');

			// set selected dataset row hovered
			this.setChildrenClass(rows.select('#row' + dataFunc.name(element)),
					'square-hover');
			this.removeChildrenClass(rows.select('#row'
					+ dataFunc.name(element)), 'fade');

			// set selected top name hovered
			var topNameElement = topNames.select('#name'
					+ dataFunc.name(element));
			this.removeClass(topNameElement, 'source');
			this.removeClass(topNameElement, 'both');
			this.removeClass(topNameElement, 'target');
			this.setClass(topNameElement, 'hovered');

			// set selected left name hovered
			var leftNameElement = leftNames.select('#name'
					+ dataFunc.name(element));
			this.removeClass(leftNameElement, 'source');
			this.removeClass(leftNameElement, 'both');
			this.removeClass(leftNameElement, 'target');
			this.setClass(leftNameElement, 'hovered');
		}

		/**
		 * Clears any highlight class added to top name, left name elements or
		 * cells.
		 */
		MatrixChart.prototype.clearSelection = function() {
			Chart.prototype.clearSelection.call(this);

			var topNames = this.svg.select('#' + 'topGroup');
			var leftNames = this.svg.select('#' + 'leftGroup');
			var rows = this.svg.select('#' + 'rowGroup');
			var squares = rows.selectAll('.row');

			this.clear(topNames);
			this.clear(leftNames);
			this.clear(squares);
		};

		// ########## MatrixWholeChart #####################################
		/**
		 * 
		 */
		var MatrixWholeChart = function(parentView, chartConfig, chartListener) {
			MatrixChart.call(this, parentView, chartConfig, chartListener);
		};

		MatrixWholeChart.prototype = Object.create(MatrixChart.prototype);

		MatrixWholeChart.prototype.constructor = MatrixWholeChart;

		/**
		 * Resizes the chart depending on parent containers size.
		 */
		MatrixWholeChart.prototype.resizeElements = function() {
			if (this.leftData) {
				var rectWidth = this.chartHeight / this.leftData.length;
				this.chartWidth = rectWidth * this.topData.length;
				var x = this.margins.left;
				var y = this.margins.top;

				var mainGroup = this.svg.select('#mainGroup');

				mainGroup.attr('transform', 'translate(' + x + ', ' + y + ')');

				// update scale ranges
				this.topScale.range([ 0, this.chartWidth ]);
				this.leftScale.range([ 0, this.chartHeight ]);
			}
		};

		/**
		 * Creates a matrix that holds the number of links from source to target
		 * datasets including all incoming and outgoing links for all datasets
		 * in the selection.
		 */
		MatrixWholeChart.prototype.createLinkMatrix = function() {
			var dataLength = this.data.length;

			this.topData = this.data.slice();
			this.leftData = this.data.slice();

			var that = this;
			// create index matrix for node names
			this.indexMatrix = [];
			this.topIndex = [];
			this.leftIndex = [];

			var links;
			var leftIndex = 0;
			var topIndex = 0;
			for (var i = 0; i < dataLength; i++) {
				var node = this.data[i];
				this.indexMatrix[node.name] = i;

			}

			for (var i = 0; i < dataLength; i++) {
				var node = this.data[i];

				// add incoming link dataset names in leftIndex
				links = node.incomingLinks;

				for (var j = 0; j < links.length; j++) {
					var link = links[j];
					var linkName = link.name;
					// create node to add leftData
					if (this.indexMatrix[linkName] === undefined
							&& this.leftIndex[linkName] === undefined) {
						this.leftIndex[linkName] = leftIndex++;
						var tempNode = {};
						tempNode.name = linkName;
						tempNode.outgoingLinks = [];
						tempNode.incomingLinks = [];

						var oLink = {};
						oLink.name = node.name;
						oLink.count = link.count;

						tempNode.outgoingLinks.push(oLink);
						this.leftData.push(tempNode);
					}
				}

				// add outgoing link dataset names in topIndex
				links = node.outgoingLinks;

				for (var j = 0; j < links.length; j++) {
					var link = links[j];
					var linkName = link.name;
					// create node to add topData
					if (this.indexMatrix[linkName] === undefined
							&& this.topIndex[linkName] === undefined) {
						this.topIndex[linkName] = topIndex++;
						var tempNode = {};
						tempNode.name = linkName;
						tempNode.incomingLinks = [];
						tempNode.outgoingLinks = [];

						var oLink = {};
						oLink.name = node.name;
						oLink.count = link.count;

						tempNode.incomingLinks.push(oLink);
						this.topData.push(tempNode);
					}
				}
			}

			// console.log("left " + this.leftData.length);
			// console.log("top " + this.topData.length);

			// initialize linkMatrix with zeros
			this.linkMatrix = [];
			for (var i = 0; i <= this.leftData.length; i++) {
				var row = [];
				for (var j = 0; j <= this.topData.length; j++) {
					row.push(0);
				}
				this.linkMatrix.push(row);
			}

			// fill linkMatrix with number of links for datasets.
			for (var i = 0; i < this.leftData.length; i++) {
				var row = this.linkMatrix[i];
				var node = this.leftData[i];

				node.outgoingLinks.forEach(function(link) {
					// get index of the outgoing link
					var index = that.indexMatrix[dataFunc.name(link)];
					// if exists in the core set of datasets
					if (index > -1)
						row[index] = dataFunc.linkCount(link);
					// if doesn't exist in the core set of datasets
					else {
						index = that.topIndex[dataFunc.name(link)];
						if (index > -1) {
							row[30 + index] = dataFunc.linkCount(link);
						}
					}
				});
			}

			this.chartWidth = this.topData.length * 20;
			this.chartHeight = this.leftData.length * 20;

			// initialize chart svg
			this.svg.attr('width',
					this.chartWidth + this.margins.left + this.margins.right)
					.attr(
							'height',
							this.chartHeight + this.margins.top
									+ this.margins.bottom);
			this.resizeElements();
		};

		// ########## PointChart #####################################
		/**
		 * Extends BarChart, replaces bars with circles.
		 */
		var PointChart = function(parentView, chartConfig, chartListener) {
			BarChart.call(this, parentView, chartConfig, chartListener);

			this.radius = 2.5;
		};

		PointChart.prototype = Object.create(BarChart.prototype);

		PointChart.prototype.constructor = PointChart;

		PointChart.prototype.enterBars = function() {
			var that = this;

			this.barGroup.enter().append("circle").attr("class",
					that.shapeClass).attr("r", 0).on("click",
					function(element) {
						that.onClick(element);
					}).on("mouseout", function(element) {
				that.mouseOuted(element);
			}).on("mouseover", function(element) {
				that.mouseOvered(element);
			}).on("dblclick", function(element) {
				that.dblclicked(element);
			}).append("title").text("");
			this.updateBars();
		};

		PointChart.prototype.updateBars = function() {
			var that = this;
			this.barGroup.transition().delay(this.delay).duration(
					this.updateDuration).attr("id", function(d) {
				return 'name' + dataFunc.name(d);
			}).attr(
					"cx",
					function(d, i) {
						return that.xScale.scaleValue(i + 1)
								+ (that.xScale.rangeValue() / 2);
					}).attr(
					"cy",
					function(d) {
						return that.yScale.scaleValue(that.dataFunction(d))
								+ (that.xScale.rangeValue() / 2);
					}).attr("r", this.radius).select("title")
					.text(
							function(d) {
								var s = "name: "
										+ dataFunc.name(d)
										+ '\n'
										+ "title: "
										+ dataFunc.title(d)
										+ '\n'
										+ that.yScale.getFormat()(
												that.dataFunction(d));
								return s;
							});
		};

		PointChart.prototype.exitBars = function() {
			var that = this;
			this.barGroup.exit().transition().duration(this.shortDuration)
					.attr("class", "disappear").transition().duration(
							this.updateDuration).attr("r", function(d) {
						return 0;
					}).remove();
		};

		// ########## PieChart #####################################
		/**
		 * 
		 */
		var PieChart = function(parentView, chartConfig, chartListener) {
			Chart.call(this, parentView, chartConfig, chartListener);
			// set data function
			this.dataFunction = chartConfig.chartDetails.dataFunction
					|| dataFunc.identity;

			this.radius = this.chartWidth / 2;

			var mainGroup = this.svg.select('#mainGroup');

			mainGroup.attr('transform', 'translate(' + this.radius + ', '
					+ this.radius + ')');
			this.arcs = mainGroup.append('g').attr('id', 'arcGroup').selectAll(
					'.arc');
			this.names = mainGroup.append('g').attr('id', 'nameGroup')
					.selectAll('text');
			this.color = d3.scale.ordinal().range(
					[ "#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56",
							"#d0743c", "#ff8c00" ]);

		};

		PieChart.prototype = Object.create(Chart.prototype);

		PieChart.prototype.constructor = PieChart;

		PieChart.prototype.selectElement = function(element, fade, context) {
			var arcs = this.svg.select('#' + 'arcGroup');
			var names = this.svg.select('#' + 'nameGroup');

			if (fade) {
				this.setChildrenClass(arcs, 'fade');
				this.setChildrenClass(names, 'fade');
			}

			if (this.showLinks) {
				var that = this;
				// color incoming links
				element.incomingLinks.forEach(function(link) {
					that.setClass(arcs
							.select('#arc' + link.name + element.name),
							'source-link');
					that.setClass(names.select('#name' + link.name), 'source');
				});

				// color outgoing links
				element.outgoingLinks.forEach(function(link) {
					that.setClass(arcs
							.select('#arc' + element.name + link.name),
							'target-link');
					that.setClass(names.select('#name' + link.name), 'target');
				});
			}

			this.setClass(arcs.select('#arc' + dataFunc.name(element)),
					'hovered');
			this.setClass(names.select('#name' + dataFunc.name(element)),
					'hovered');
		}

		PieChart.prototype.clearSelection = function() {
			Chart.prototype.clearSelection.call(this);
			var arcs = this.svg.select('#' + 'arcGroup');
			var names = this.svg.select('#' + 'nameGroup');

			this.clear(arcs);
			this.clear(names);
		};

		PieChart.prototype.setData = function(data) {
			this.clearSelection();
			this.resizeChartElements();
			var pie = d3.layout.pie().value(this.dataFunction);
			// .padAngle(.04);

			var pieData = pie(data);

			this.arcs = this.arcs.data(pieData);

			this.exitArcs();
			this.updateArcs();
			this.enterArcs();

			this.names = this.names.data(pieData);

			this.exitNames();
			this.updateNames();
			this.enterNames();
		};

		PieChart.prototype.enterArcs = function() {
			var that = this;
			this.arcs.enter().append("path").on("click", function(element) {
				that.onClick(element);
			}).on("mouseout", function(element) {
				that.mouseOuted(element);
			}).on("mouseover", function(element) {
				that.mouseOvered(element);
			}).on("dblclick", function(element) {
				that.dblclicked(element);
			}).append("title").text("");
			this.updateArcs();
		};

		PieChart.prototype.updateArcs = function() {
			var arc = d3.svg.arc().outerRadius(this.radius - 70).innerRadius(
					this.radius - 130);

			var that = this;
			this.arcs.transition().delay(this.delay).duration(
					this.updateDuration).attr("id", function(d) {
				return 'arc' + dataFunc.name(d.data);
			}).attr("d", arc).style("fill", function(d, i) {
				return that.color(i);
			}).style("cursor", "default").select('title').text(
					function(d) {
						return dataFunc.name(d.data) + '\n'
								+ util.formatNumber(d.data.count) + ' links';
					});
		};

		PieChart.prototype.exitArcs = function() {
			this.arcs.exit().transition().duration(this.shortDuration)
			// .attr("class", "disappear")
			// .transition().duration(this.updateDuration)
			// .attr("text",function(d) {
			// return "";})
			// .attr("transform", function(d) {
			// return "rotate(20)translate(" + (d.y + 8)
			// + ",0)" + (d.x < 180 ? "" : "rotate(180)");
			// })
			// .style("text-anchor", function(d) {
			// return d.x < 180 ? "end" : "end";})
			.remove();
		};

		PieChart.prototype.enterNames = function() {
			var that = this;
			this.names.enter().append("text").attr("class", that.nameClass).on(
					"click", function(element) {
						that.onClick(element.data);
					}).on("mouseout", function(element) {
				// that.clearClick(element.data);
			}).on("dblclick", function(element) {
				that.dblclicked(element.data);
			});
			this.updateNames();
		};

		PieChart.prototype.updateNames = function() {
			var arc = d3.svg.arc().outerRadius(this.radius - 30).innerRadius(
					this.radius - 90);

			// http://bl.ocks.org/GerHobbelt/3175935
			// Computes the angle of an arc, converting from radians to degrees.
			function angle(d, offset, threshold) {
				var a = (d.startAngle + d.endAngle) * 90 / Math.PI + offset;
				return a > threshold ? a - 180 : a;
			}

			var that = this;
			this.names.transition().delay(this.delay).duration(
					this.updateDuration).attr("id", function(d) {
				return 'name' + dataFunc.name(d.data);
			}).attr("dy", "5px").attr("text-anchor", function(d) {
				var a = angle(d, 0, 0);
				return a < 0 ? "start" : "end";
			}).attr(
					"transform",
					function(d) {
						return "translate(" + arc.centroid(d) + ")rotate("
								+ angle(d, -90, 90) + ")";
					}).text(function(d) {
				return dataFunc.name(d.data);
			});

			this.names.append('title').text(
					function(d) {
						return dataFunc.name(d.data) + '\n'
								+ util.formatNumber(d.data.count) + ' links';
					});
		};

		PieChart.prototype.exitNames = function() {
			this.names.exit().transition().duration(this.shortDuration)
			// .attr("class", "disappear")
			// .transition().duration(this.updateDuration)
			.attr("text", "")
			// .attr("transform", function(d) {
			// return "rotate(20)translate(" + (d.y + 8)
			// + ",0)" + (d.x < 180 ? "" : "rotate(180)");
			// })
			// .style("text-anchor", function(d) {
			// return d.x < 180 ? "end" : "end";})
			.remove();
		};

		// ##############################################################

		return {
			getChart : function(parentView, chartConfig, chartListener) {
				switch (chartConfig.d3Type) {
				case 'bar':
					return new BarChart(parentView, chartConfig, chartListener);
					break;
				case 'bar-brush':
					return new BarChartWithBrush(parentView, chartConfig,
							chartListener);
					break;
				case 'point':
					return new PointChart(parentView, chartConfig,
							chartListener);
					break;
				case 'matrix':
					return new MatrixChart(parentView, chartConfig,
							chartListener);
					break;
				case 'matrixwhole':
					return new MatrixWholeChart(parentView, chartConfig,
							chartListener);
					break;
				case 'edge':
					return new EdgeChart(parentView, chartConfig, chartListener);
					break;
				case 'pie':
					return new PieChart(parentView, chartConfig, chartListener);
					break;
				case 'table':
					return new TableChart(parentView, chartConfig,
							chartListener);
					break;
				case 'chord':
					return new ChordChart(parentView, chartConfig,
							chartListener);
					break;
				case 'scatter':
					return new ScatterChartWithBrush(parentView, chartConfig,
							chartListener);
					break;
				case 'time':
					return new TimeChart(parentView, chartConfig, chartListener);
					break;
				case 'double-time':
					return new DoubleTimeChart(parentView, chartConfig,
							chartListener);
					break;
				default:
					return new BarChart(parentView, chartConfig, chartListener);
					break;
				}
			},
			getMode : function() {
				return MODES;
			}
		};
	};

})(lodApp.d3);
