/*******************************************************************************
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.model;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class Resources {

	private final Map<String, Resource> resources = new LinkedHashMap();

	public Resources() {

	}

	public void addResource(String resourceId) {
		Resource resource = resources.get(resourceId);
		if (resource == null) {
			resources.put(resourceId, new Resource(resourceId));
		}
	}

	public boolean addResourceFile(ResourceFileShortEx file) {
		Resource resource = resources.get(file.getResourceId());
		if (resource == null) {
			resource = new Resource(file.getResourceId());
			resources.put(file.getResourceId(), resource);
		}
		return resource.addFile(file);
	}

	public Collection<Resource> getResources() {
		return resources.values();
	}
}
