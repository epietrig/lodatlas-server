/*******************************************************************************
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.model;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class Resource {

	/**
	 * The id of the resource as unique identifier.
	 */
	private final String resourceId;

	private final List<ResourceFileShortEx> files = new LinkedList<>();

	/**
	 * 
	 * @param pkg_name
	 * @param resourceId
	 */
	public Resource(String resourceId) {
		this.resourceId = resourceId;
	}

	public String getResourceId() {
		return resourceId;
	}

	public boolean addFile(ResourceFileShortEx file) {
		return files.add(file);
	}

	public Collection<ResourceFileShortEx> getFiles() {
		return files;
	}
}
