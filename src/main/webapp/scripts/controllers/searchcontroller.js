/*
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
'use strict';

var lodApp = lodApp || {};

lodApp.Controller = lodApp.Controller || {};

var restController = lodApp.Controller.RestController;

var restApi = lodApp.Config.RestApi;

var dataFunc = lodApp.util.dataFunction;

(function(o) {

	o.SearchTopController = {
		getInstance : function() {

			var PAGE_SIZE = 1000;
			
			var MAX_DATASET_SIZE = 50;
			
			var SearchController = function() {
				// the facet categories in searchCriteria must comply with
				// config.FacetCategories
				this.searchCriteria = {
					repo : [],
					rdfsum : [],
					tag : [],
					org : [],
					format : [],
					license : [],
					props : [],
					classes : [],
					vocabs : [],
					fields : [],
					isAnd : [],
					show : []
				};
				
			};

			SearchController.constructor = SearchController;

			/**
			 * Initializes the views controlled by this controller.
			 */
			SearchController.prototype.init = function() {
				// start loading data before initializing views
				this.search(this.displayViews);

				// initialize FacetListView to view facet categories and facets.
				this.facetListView = lodApp.View.FacetListView
						.getInstance(this);

				// initialize SearchSection to view "Search" section to make
				// text search
				this.searchSection = lodApp.View.SearchSection
						.getInstance(this);

				// initialize FilterSection to view "Plot and Filter"
				// section to view the filter alternatives and select datasets
				// using the filters.
				this.filterSection = lodApp.View.FilterSection
						.getInstance(this);
				
				// initialize SelectSection to view "Select" section
				// that displays the list of datasets resulting from search
				// and/or filter.
				this.selectSection = lodApp.View.SelectSection
						.getInstance(this);
				
				// initialize CartDatasetSection to view "Cart- Dataset List"
				// section that displays the dataset list
				// selected to display in the "Cart-Details" section.
				this.cartDatasetSection = lodApp.View.CartDatasetSection
						.getInstance(this);

				// initialize CartDetailsSection to view "Cart-Details" section
				// that displays the charts on filtered dataset list.
				this.cartDetailsSection = lodApp.View.CartDetailsSection
						.getInstance(this);
			};

			/**
			 * Connects to server to search for datasets depending on the
			 * selected facets and search criteria stored in
			 * 'this.searchCriteria'. Finally renders the search results.
			 */
			SearchController.prototype.search = function(callback, from) {
				// window.setTimeout(3000,1);
				var queryString = this.getQueryString();
				if (from != undefined) {
					if (!queryString.endsWith("?")) {
						queryString += "&"
					}
					queryString += "from=" + ((from - 1 ) * 1000) + "&size=" + PAGE_SIZE;
				}
				restController.getSearchResults(restApi.search,
						queryString, callback, this, undefined, undefined, from);
			};

			/**
			 * Displays the data specified by searchResult in views for the
			 * first time.
			 * 
			 * This function is called as a callback by restcontroller. This is
			 * done to view all data at the same time rather than first
			 * displaying the views and then filling them with data.
			 */
			SearchController.prototype.displayViews = function(searchResult) {
				// wait until the searchSection is created

				if (this.searchSection) {
					document.getElementById('message').style.display = 'none';
					this.renderResults(searchResult);
					document.getElementById('view').style.display = 'block';
				} else {
					setTimeout(this.displayViews(searchResult), 1000);
				}
			};

			/**
			 * Generates the query string depending on the values in
			 * this.searchCriteria.
			 * 
			 * @returns query string in the form "?repo=datahub&org=Linked
			 *          Education Cloud&show=org" as an example.
			 */
			SearchController.prototype.getQueryString = function() {
				var that = this;
				
				var queryString = Object.keys(this.searchCriteria).map(function(key) {
					var list = that.searchCriteria[key];
					
					var st = list.map(function(item){
						return key + '=' + encodeURIComponent(item); 
					}).join('&');
					
					return st;
				}).filter(function(result) {
					return result != "";
				}).join('&');
				
				return "?" + queryString;
			};

			/**
			 * Renders the search results from facet and search criteria in the
			 * views controlled by this controller.
			 * 
			 */
			SearchController.prototype.renderResults = function(searchResult, from) {
				this.searchResult = searchResult;

				// populate facet list view with results
				this.facetListView.render(this.searchResult.facetList);

				// populate filter group view with results
				// hits contain name, title, tc, olc, ilc, cd, md
				this.filterSection.render(this.searchResult.hitList.hits);

				// populate search filter result view with results
				// hitList contains total number of hits and array of hits
				this.selectSection.render(this.searchResult.hitList.hits);
				
				this.searchSection.renderPages(this.searchResult.hitList.totalHits, from == undefined ? 1 : from, this.searchResult.hitList.scrollId);

			};
			
			/**
			 * Renders the search results from facet and search criteria in the
			 * views controlled by this controller.
			 * 
			 */
			SearchController.prototype.renderFilterResults = function(filterResult) {
				this.filterResult = filterResult;

				this.selectSection.render(this.filterResult);
			};


			/**
			 * Updates 'searchCriteria' when an item is selected or deselected
			 * in the facets.
			 * 
			 * @param category
			 *            facet category
			 * @param item
			 *            item for which selection property has changed
			 */
			SearchController.prototype.selectionChanged = function(category,
					item) {
				// console.log('selection changed: category ' + category
				// + ' item ' + item.name + ' is '
				// + (item.selected ? 'selected' : 'not selected'));

				// if selection already exists in search criteria, remove it
				var index = this.searchCriteria[category].indexOf(item.name);
				if (item.selected && index < 0) {
					this.searchCriteria[category].push(item.name);
				} else {
					if (index >= 0) {
						this.searchCriteria[category].splice(index, 1);
					}
				}
				this.search(this.renderResults);
			};

			/**
			 * Updates 'searchCriteria' when query text or selected fields
			 * change in search group view.
			 * 
			 * @param fields
			 *            array of query parameters including search string and
			 *            query fields separated by '}'. Query fields are comma
			 *            separated.
			 * @params isAnd boolean value to indicate how to combine fields if
			 *         there is more than one.
			 */
			SearchController.prototype.criteriaChanged = function(fields, isAnd) {
				this.searchCriteria.fields = fields;
//				console.log(fields);
				if (fields.length === 0) { 
					this.searchCriteria.isAnd = [];
				} else {
					this.searchCriteria.isAnd = [ isAnd ];
				}
				this.search(this.renderResults);
			};
			
			/**
			 */
			SearchController.prototype.showNext = function(from) {
				this.search(this.renderResults, from);
			};

			/**
			 * Shows more of a specific facet group.
			 * 
			 * @param context
			 *            the facet group that asks to show more.
			 * @param isShowMore
			 *            boolean value to indicate show more or less; true for
			 *            more.
			 */
			SearchController.prototype.showMore = function(context, isShowMore) {
				var index = this.searchCriteria['show']
						.indexOf(context.category.short);
				var queryString = '';
				if (isShowMore && index < 0) {
					this.searchCriteria['show'].push(context.category.short);
					queryString = this.getQueryString() + '&' + 'isShow='
							+ isShowMore;
					
					restController.getSearchResults(restApi.facet,
							queryString, context.renderSingle, context);
				} else {
					if (index >= 0) {
						this.searchCriteria['show'].splice(index, 1);
						queryString = this.getQueryString();
						if (queryString === '')
							queryString = '?show=' + context.category.short
									+ '&' + 'isShow=' + isShowMore;
						else
							queryString += '&show=' + context.category.short
									+ '&' + 'isShow=' + isShowMore;
						
						restController.getSearchResults(restApi.facet,
								queryString, context.renderSingle, context);
					}
				}
			};
			
			/**
			 * Adds the selected datasets to the selected dataset list.
			 */
			SearchController.prototype.addSelected = function(selectionList) {
				this.cartDatasetSection.addSelected(selectionList); 
			};

			/**
			 * Updates the list of datasets displayed in the Details section
			 * when Update button in Details section is clicked.
			 */
			SearchController.prototype.updateDetails = function() {

				var dataToSend = this.cartDatasetSection.getDatasetList();
				if (dataToSend.length == 0) {
					alert('No dataset is selected to view in details section. Please select at least one dataset.');
					return;
				}
				// console.log(that.filteredData);
				if (dataToSend.length > MAX_DATASET_SIZE) {
					alert("The number of selected datasets is too high to display in \"Details\" section.\nOnly first " + MAX_DATASET_SIZE + " of the "
							+ dataToSend.length
							+ ' datasets will be displayed.');
					dataToSend = dataToSend.slice(0, MAX_DATASET_SIZE);
				}
				
				var queryString = dataToSend.map(function(d) {
					return "id=" + d.rN + ";" + dataFunc.name(d);
				}).join('&');

				queryString = '?' + queryString;
				
				restController.getSearchResults(restApi.datasetlinks, queryString,
						this.renderDetails, this);
			};

			/**
			 * Shows or hides 'Update' button in Details section depending on
			 * the value of 'show' parameter.
			 * 
			 * @param show
			 *            true if the 'Update' button is to be shown; false
			 *            otherwise.
			 */
			SearchController.prototype.toggleUpdateDetails = function(show) {
				this.cartDetailsSection.toggleUpdate(show);
			};

			/**
			 * Displays the results for filtered datasets in Details part.
			 * 
			 * @param details
			 *            the details of the filtered datasets.
			 */
			SearchController.prototype.renderDetails = function(details) {
				this.cartDetailsSection.render(details.hits);
				this.toggleUpdateDetails(false);
			};
			
	  		SearchController.prototype.getVoid = function(datasetList, callback, context) {
	  			
	  			// TODO check if greater than 1000
	  			var queryString = datasetList.map(function(d) {
					return "id=" + d.rN + ";" + dataFunc.name(d);
				}).join('&');

				queryString = '?' + queryString;
	  			
	  			restController.getSearchResults(restApi.void, queryString,
						callback, context, "'text/plain; charset=utf-8'", "text");
			};
			
			/**
			 * Creates the query string for the selected dataset, gets data from
			 * the server using rest controller and views the dataset details
			 * overlay for the specified 'dataset'.
			 * 
			 * @param dataset
			 *            dataset for which the detail charts will be displayed.
			 */
			SearchController.prototype.showDataset = function(dataset) {
//				console.log(dataset);
				if (this.overlayView) {
					this.disposeOverlay();
				}
				restController.getSearchResults(restApi.dataset(dataset.rN, dataset.name), "",
						this.renderDatasetData, this);

			};
			
			/**
			 * Creates the query string for the selected dataset, gets data from
			 * the server using rest controller and views this data in the
			 * dataset details overlay chart view.
			 * 
			 * @param repoName
			 *            name of the repository that the dataset belongs to.
			 * @param dataset
			 *            dataset for which the detail charts will be displayed.
			 */
			SearchController.prototype.showDatasetCharts = function(repoName, datasetName, callback, context) {
				restController.getSearchResults(restApi.linkdetails(repoName, datasetName), "",
						callback, context);

			};
			
			/**
			 * Creates the query string for the selected dataset, gets the list
			 * of resources that have RDF summaries from the server using rest
			 * controller and views the RDF summaries in details overlay RDF
			 * summary tab for the specified 'dataset'.
			 * 
			 * @param dataset
			 *            dataset for which the detail charts will be displayed.
			 */
			SearchController.prototype.showDatasetResourceFileList = function(repoName, datasetName, callback, context) {
				restController.getSearchResults(restApi.resourcelist(repoName, datasetName), "",
						callback, context);

			};
			
			/**
			 * Creates the query string for the selected dataset resource file
			 * and gets file list from the server using rest controller for
			 * resource file RDF sums.
			 * 
			 * @param dataset
			 *            dataset for which the detail charts will be displayed.
			 */
			SearchController.prototype.showFileRDFSum = function(repoName, datasetName, resourceId, originalFileName, callback, context) {
				restController.getSearchResults(restApi.rdfquotient(repoName, datasetName, resourceId, originalFileName), "",
						callback, context);

			};

			/**
			 * Creates the query string for the selected dataset resource file
			 * and gets file list from the server using rest controller for the
			 * resource file vocabularies.
			 * 
			 * @param dataset
			 *            dataset for which the detail charts will be displayed.
			 */
			SearchController.prototype.showFileVocabulary = function(repoName, datasetName, resourceId, originalFileName, callback, queryPath, context) {
				restController.getSearchResults(restApi.vocabulary(repoName, datasetName, resourceId, originalFileName), "",
						callback, context);
			};

			/**
			 * Displays the dataset data in dataset overlay view.
			 * 
			 * @param dataset
			 *            the data for the dataset to display.
			 */
			SearchController.prototype.renderDatasetData = function(dataset) {
				if (!this.overlayView) {
					this.showOverlay(lodApp.View.DatasetDetailsView
							.getInstance(this, dataset));
				}
			};

			/**
			 * Hovers the specified dataset in the filters in FilterSection.
			 * 
			 * @param dataset
			 *            hovered dataset.
			 */
			SearchController.prototype.datasetHovered = function(dataset) {
				this.filterSection.hover(dataset);
			}

			/**
			 * Unhovers the specified dataset in the filters in FilterSection.
			 * 
			 * @param dataset
			 *            unhovered dataset.
			 */
			SearchController.prototype.datasetClear = function(dataset) {
				this.filterSection.clearHover(dataset);
			}

			/**
			 * Creates and shows analytics view for the selected set of
			 * datasets.
			 */
			SearchController.prototype.showAnalyticsView = function(datasetList) {
				if (!this.overlayView) {
					this.showOverlay(lodApp.View.AnalyticsGroupView
							.getInstance(this, datasetList));
				}
			};
			

			/**
			 * Creates and shows the specified view in overlay.
			 */
			SearchController.prototype.showOverlayView = function(view) {
				if (!this.overlayView) {
					this.showOverlay(view);
				}
			};

			SearchController.prototype.showChartMagnify = function(data,
					chartConfig) {
				if (!this.overlayView) {
					this.showOverlay(lodApp.View.ChartMagnifyView.getInstance(
							this, data, chartConfig,
							lodApp.Config.View.MagnifyConfig));
				}
			};

			SearchController.prototype.showChartAllLinks = function(data,
					chartDetails) {
				if (!this.overlayView) {
					this.showOverlay(lodApp.View.ChartMagnifyView.getInstance(
							this, data, chartDetails,
							lodApp.Config.View.AllLinksConfig));
				}
			};

			/**
			 * Opens overlay and displays the specified view in overlay.
			 * 
			 * @param view
			 *            the view to display in the overlay.
			 */
			SearchController.prototype.showOverlay = function(view) {
				this.overlayView = view;
				
				var that = this;

				var overlayDiv = document.getElementById('overlay');
				// enlarge overlay view
				overlayDiv.style.height = "100%";

				// close button listener
				var listener = function(event) {
					overlayDiv.style.height = "0%";
					event.target.removeEventListener('click', listener);
					// dispose a bit later so that the children are still on the
					// screen during transition.
					that.timeoutFunc = setTimeout(that.disposeOverlay.bind(that), 500);
				};

				// add close button listener
				overlayDiv.querySelector('#overlay-close').addEventListener(
						'click', listener);
			};
			
			SearchController.prototype.closeOverlay = function() {
				var overlayDiv = document.getElementById('overlay');
				
				overlayDiv.style.height = "0%";
				event.target.removeEventListener('click', listener);
				// dispose a bit later so that the children are still on the
				// screen during transition.
				this.timeoutFunc = setTimeout(this.disposeOverlay.bind(this), 500);
			}
			

			/**
			 * Disposes the view stored in this.overlayView.
			 */
			SearchController.prototype.disposeOverlay = function() {
				this.overlayView.dispose();
				this.overlayView = null;
				clearTimeout(this.timeoutFunc);
			};
			
			/**
			 * Resizes all necessary views.
			 */
			SearchController.prototype.resize = function() {
				this.filterSection.resize();
				this.cartDetailsSection.resize();
				if (this.overlayView) {
					this.overlayView.resize();
				}
			};

			SearchController.prototype.getPageSize = function() {
				return PAGE_SIZE;
			};
			
			return new SearchController();
		}
	};
})(lodApp.Controller);
