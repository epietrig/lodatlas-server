/*
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
'use strict';

var lodApp = lodApp || {};
var viewConfig = lodApp.Config.View;
var controller = lodApp.Controller;

(function(o) {
	o.init = function() {
		// document.body.style.cursor = 'wait';
		var searchController = controller.SearchTopController.getInstance();
		searchController.init();

		// add event listener to about button
		document.getElementById("about-button").addEventListener(
				'click',
				function() {
					searchController.showOverlayView(lodApp.View.AboutOverlayView
							.getInstance(searchController));
				});

		// add event listener to submit dataset button
		document.getElementById("submit-dataset-button").addEventListener(
				'click',
				function() {
					searchController
							.showOverlayView(lodApp.View.SubmitDatasetOverlayView
									.getInstance(searchController));
				});

		window.addEventListener('resize', function() {
			searchController.resize();
			// console.log('resized');
		});

		// window.addEventListener('beforeunload', function() {
		// searchController.clearViews();
		// console.log('refreshed');
		// });
	};

	/**
	 * Collapses the section content when collapse icon is clicked in a section.
	 */
	o.collapseSection = function(event) {
		var target = event.target;
		// hides the section content
		var contentElement = target.parentNode.parentNode.nextElementSibling;
		while (contentElement !== null) {
			contentElement.style.display = 'none';
			contentElement = contentElement.nextElementSibling;
//			console.log(contentElement);
		}
		// hides the collapse icon
		target.style.display = 'none';

		// displays the expand icon
		target.nextElementSibling.style.display = 'inline-block';
	};

	/**
	 * Expands the section content when expand icon is clicked in a section.
	 */
	o.expandSection = function(event) {
		var target = event.target;
		// shows the section content
		var contentElement = target.parentNode.parentNode.nextElementSibling;
//		console.log(contentElement);
		while (contentElement !== null) {
			contentElement.style.display = 'block';
			contentElement = contentElement.nextElementSibling;
//			console.log(contentElement);
		}
		// hides the expand icon
		target.style.display = 'none';

		// displays the collapse icon
		target.previousElementSibling.style.display = 'inline-block';
	};

})(lodApp);

window.addEventListener('load', function() {
//	console.log('onload');
});

lodApp.init();
