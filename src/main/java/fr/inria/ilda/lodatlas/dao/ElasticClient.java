/*******************************************************************************
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.dao;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.apache.log4j.Logger;
import org.elasticsearch.action.admin.cluster.health.ClusterHealthResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.discovery.MasterNotDiscoveredException;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

import fr.inria.ilda.lodatlas.common.Config;
import fr.inria.ilda.lodatlas.commons.es.EsUtil;

/**
 * 
 * @author Hande Gözükan
 *
 */
public class ElasticClient {

	private static final Logger logger = Logger.getLogger(ElasticClient.class);
	
	private final static String INDEX_NAME_DATASET = "dataset";

	private final static String INDEX_NAME_PROCESSED_FILE = "processedfile";

	private final static String INDEX_NAME_VOCABULARY = "vocabulary";

	/**
	 * ElasticSearch client.
	 */
	private static Client client;

	static {
		final Settings settings = Settings.builder().put(EsUtil.CLUSTER_NAME_KEY, Config.ES_CLUSTER_NAME).build();
		try {
			client = new PreBuiltTransportClient(settings)
					.addTransportAddress(new TransportAddress(InetAddress.getByName(Config.ES_HOST), Config.ES_PORT));
		} catch (UnknownHostException e) {
			logger.warn("Problem while creating TransportClient for ElasticSearch.", e);
			if (client != null) {
				client.close();
			}
		}

		ClusterHealthResponse hr = null;
		try {
			hr = client.admin().cluster().prepareHealth().setWaitForGreenStatus()
					.setTimeout(TimeValue.timeValueMillis(250)).execute().actionGet();
		} catch (MasterNotDiscoveredException e) {
			logger.warn("ElasticSearch master node not discovered.", e);
		}

		if (hr != null) {
			System.out.println("Data nodes found:" + hr.getNumberOfDataNodes());
			System.out.println("Timeout? :" + hr.isTimedOut());
			System.out.println("Status:" + hr.getStatus().name());
		}

	}
	
	public static Client getClient() {
		logger.info("Asked for ElasticSearch client");
		if (client == null) {
			logger.warn("No client instance exists, initializing...");
			new ElasticClient();
		}
		return client;
	}
	
	public static SearchRequestBuilder searchDataset() {
		return client.prepareSearch(INDEX_NAME_DATASET);
	}

	public static SearchRequestBuilder searchProcessedFile() {
		return client.prepareSearch(INDEX_NAME_PROCESSED_FILE);
	}

	public static SearchRequestBuilder searchVocabulary() {
		return client.prepareSearch(INDEX_NAME_VOCABULARY);
	}

	public static void dispose() {
		client.close();
	}
}
