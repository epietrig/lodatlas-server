/*
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/**
 * FacetGroupView is a child view of FacetListView. It displays a list of facet
 * items under a specific category. It populates its parent FacetListView
 * appending a facet category group and its facet items to it.
 */

'use strict';

var lodApp = lodApp || {};

lodApp.View = lodApp.View || {};

(function(o) {
	o.FacetGroupView = {
		getInstance : function(category_, parentView_) {

			/**
			 * Initializes a GroupView for the specified 'category' as a child
			 * of the specified 'parentView'.
			 * 
			 * @param category
			 *            category for this facet group.
			 * @param parentView
			 *            FacetListView specified as parent for this facet group
			 *            view.
			 */
			var GroupView = function(category, parentView) {
				this.category = category;
				this.parentView = parentView;
				this.itemViews = [];
				this.init();
			};

			GroupView.constructor = GroupView;

			/**
			 * Generates an instance of template with id 't-facet-group', sets
			 * its category and appends it to parentView.
			 */
			GroupView.prototype.init = function() {
				var facet = document.querySelector('template#t-facet-group').content;

				var facetClone = document.importNode(facet, true);
				this.element = this.parentView.appendChild(facetClone);
				this.element.id = this.category.short;

				this.element.querySelector('#facet-group-header').textContent = this.category.name;

				var showMoreLink = this.element.querySelector('#footer-link');
				showMoreLink.textContent = 'Show more ' + this.category.name;
				var that = this;
				showMoreLink.addEventListener('click', function(event) {
					that.showMore(event);
				});
			};

			/**
			 * When clicked on Show more link at #footer-link, shows more or
			 * less facets.
			 * 
			 * @param event
			 */
			GroupView.prototype.showMore = function(event) {
				var nodeId = event.target.id;
				var node = this.element.querySelector('#' + nodeId);
				if (node.textContent.indexOf('Show more') != -1) {
					node.textContent = 'Show less';
					this.parentView.showMore(this, true);
				} else {
					node.textContent = 'Show more ' + this.category.name;
					this.parentView.showMore(this, false);
				}
			};
			
			/**
			 * Renders the facets in the facetArray and removes the ones that do
			 * not exist in the list.
			 */
			GroupView.prototype.renderSingle = function(facetArray) {
				this.render(facetArray.list);
			}

			/**
			 * Renders the facets in the facetArray and removes the ones that do
			 * not exist in the list.
			 */
			GroupView.prototype.render = function(facetArray) {
				var facetArrayLength = facetArray.length;
				var itemViewsLength = this.itemViews.length;

				// if there are no facets remove all existing child item views.
				if (facetArrayLength === 0) {
					for (var i = itemViewsLength - 1; i > -1; i--) {
						this.remove(i);
					}

					// do not display show more part
					this.element.querySelector('.facet-footer').style.display = 'none';
					// display a message to inform that there are no facets
					this.element.querySelector('.facet-message').style.display = 'inline-block';

				} else {
					// if there exists at least one facet, do not display
					// message.
					this.element.querySelector('.facet-message').style.display = 'none';

					// Display show more only if facet count is greater than or
					// equal to 10.
					if (facetArrayLength >= 10) {
						this.element.querySelector('.facet-footer').style.display = 'inline-block';
					}

					if (facetArrayLength >= itemViewsLength) {

						for (var i = 0; i < itemViewsLength; i++) {
							var itemView = this.itemViews[i];
							itemView.render(facetArray[i]);
						}

						while (i < facetArrayLength) {
							var itemView = o.FacetItemView.getInstance(this);
							this.itemViews.push(itemView);
							itemView.render(facetArray[i++]);
						}
					} else if (facetArrayLength < itemViewsLength) {
						for (var i = 0; i < facetArrayLength; i++) {
							var itemView = this.itemViews[i];
							itemView.render(facetArray[i]);
						}
						while (i < this.itemViews.length) {
							this.remove(i);
						}
					}
				}
			};

			/**
			 * Removes the child view at the specified index.
			 * 
			 * @param index
			 *            index of the child view to remove.
			 */
			GroupView.prototype.remove = function(index) {
				var itemView = this.itemViews[index];
				itemView.removeFromParent();
				this.itemViews.splice(index, 1);
			}

			/**
			 * Appends the specified 'child' element to this element and returns
			 * the appended element.
			 */
			GroupView.prototype.appendChild = function(child) {
				this.element.querySelector('#facet-group-item-list')
						.appendChild(child);
				return this.element.querySelector('#facet-group-item-list').lastElementChild;
			};

			/**
			 * Informs the parentView from selection change in a facet item.
			 * This is called from FacetItemView.
			 */
			GroupView.prototype.updateSelection = function(item) {
				this.parentView.updateSelection(this.category.short, item);
			};

			return new GroupView(category_, parentView_);
		}
	};

})(lodApp.View);
