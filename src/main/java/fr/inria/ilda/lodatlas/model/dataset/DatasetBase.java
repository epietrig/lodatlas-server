/*******************************************************************************
 * LODAtlas Server
 * Copyright (C) 2015-2018 INRIA Hande Gözükan <hande.gozukan@inria.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.inria.ilda.lodatlas.model.dataset;

import javax.json.bind.annotation.JsonbProperty;

import fr.inria.ilda.lodatlas.commons.strings.DatasetStrings;

/**
 * 
 * @author Hande Gözükan
 *
 */
public class DatasetBase {
	
//	protected String id;

	protected String rN;

	protected String name;

	protected String title;

	protected long tC;

	/**
	 * Outgoing link count
	 */
	protected long olC;

	/**
	 * Incoming link count
	 */
	protected long ilC;

	/**
	 * Creation date
	 */
	protected String cD;

	/**
	 * Modification date
	 */
	protected String mD;

	public DatasetBase() {
	}

	public String getrN() {
		return rN;
	}

	@JsonbProperty(DatasetStrings.REPO_NAME)
	public void setrN(String rN) {
		this.rN = rN;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public long getTC() {
		return tC;
	}

	@JsonbProperty(DatasetStrings.TRIPLE_COUNT)
	public void setTC(long tC) {
		this.tC = tC;
	}

	public long getOlC() {
		return olC;
	}

	@JsonbProperty(DatasetStrings.OUTGOING_LINKS + "." + DatasetStrings.COUNT)
	public void setOlC(long olC) {
		this.olC = olC;
	}

	public long getIlC() {
		return ilC;
	}

	@JsonbProperty(DatasetStrings.INCOMING_LINKS + "." + DatasetStrings.COUNT)
	public void setIlC(long ilC) {
		this.ilC = ilC;
	}

	public String getCD() {
		return cD;
	}

	@JsonbProperty(DatasetStrings.METADATA_CREATED)
	public void setCD(String cD) {
		this.cD = cD;
	}

	public String getMD() {
		return mD;
	}

	@JsonbProperty(DatasetStrings.METADATA_MODIFIED)
	public void setMD(String mD) {
		this.mD = mD;
	}
	
//	public String getId() {
//		return this.id;
//	}
//	
//	public void getId(String id) {
//		this.id = id;
//	}
}
